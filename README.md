# Rust通用代码生成器：莲花

### 动词算子式通用代码生成器阵列全面开源

动词算子式通用代码生成器阵列已全面开源。本通用代码生成器的两个Jar软件依赖如下，皆已全部开源：

曲速引擎前端代码生成器：[https://gitee.com/jerryshensjf/WarpEngine](https://gitee.com/jerryshensjf/WarpEngine)

表反射引擎ReflectTable: [https://gitee.com/jerryshensjf/ReflectTable](https://gitee.com/jerryshensjf/ReflectTable)

### 项目介绍

Rust通用代码生成器：莲花，是Java写成的Rust通用代码生成器，目前的版本采用openjdk 17编译。Rust通用代码生成器：莲花，基于Golang通用代码生成器：仙童的架构。为一软件war包，可以部署在 Tomcat 9的webapps目录下，启动即可运行。

#### 项目图片

![输入图片说明](RedLotus.jpg)



###  软件架构 

后端代码生成物是rust 1.78环境。采用Axum 0.6.20, tokio 1.32.0 , sqlx 0.7.1数据库访问层和MariaDB, MySQL 8，PostgreSQL数据库。

前端代码生成物可运行在Nodejs 21,18或14上。是Vue ElementUI 架构。

### 新的大版本号

下一个大版本号是Rust通用代码生成器：莲花 3.0.0 紫莲 Purple Lotus。将在数月内启动开发。

![输入图片说明](pl_3.jpeg)

现在的大版本号是Rust通用代码生成器：莲花 1.0.0 红莲 Red Lotus

![输入图片说明](RedLotus.jpg)

### 近期版本

Rust通用代码生成器莲花，红莲尝鲜版三十一。此版本是一个缺陷修复版，修复了Oracle后端代码生成物的弹性登录模块的编译错误。

Rust通用代码生成器莲花，红莲尝鲜版三十。此版本是一个缺陷修复版，修复了MariaDB或MySQL后端代码生成物的弹性登录模块的编译警告。

Rust通用代码生成器莲花，红莲尝鲜版二十九。此版本是一个缺陷修复版，修复了MariaDB或MySQL后端代码生成的缺陷。

Rust通用代码生成器莲花，红莲尝鲜版二十八。此版本完善了空值处理，修复了登录数据清洗功能。修复了下拉列表的空值数据。并有更多缺陷修复。

Rust通用代码生成器莲花，红莲尝鲜版二十七。此版本完善了PostgreSQL自动反射功能。完善空值处理，并有更多缺陷修复。

Rust通用代码生成器莲花，红莲尝鲜版二十六，此版本完善了数据库自动反射功能。完善了多对多候选功能，完善了下拉列表的空值。

Rust通用代码生成器莲花，红莲尝鲜版二十五，此版本完善了PostgreSQL数据库自动反射功能。完善了编辑器，所有Domain可以通过下拉菜单选择。完善了多对多候选叠加时的语法检查。

Rust通用代码生成器莲花，红莲尝鲜版二十五，此版本完善了PostgreSQL数据库自动反射功能。完善了编辑器，所有Domain可以通过下拉菜单选择。完善了多对多候选叠加时的语法检查。

Rust通用代码生成器莲花，红莲尝鲜版二十四，此版本完善了多对多候选功能，实现了多对多候选的持久化和反射功能。增强了数据库自动反射功能和模板向导的元数据与数据编辑器。改进了编译警告和编译错。

Rust 通用代码生成器莲花，红莲尝鲜版二十三，此版本新增了多对多候选功能，增强了数据库自动反射功能和模板向导的编辑器。

Rust 通用代码生成器莲花，红莲尝鲜版二十二，此版本新增了数据库自动反射功能，可以为遗留数据库配上操作软件。此版本增强了模板向导界面的编辑器功能，可以不清空数据的情况下编辑项目的元数据和数据。可以切换域对象和枚举，可以清洗登录数据。此版本彻底修复了枚举和哑数据功能。并有前端界面更新。

通用代码生成器阵列是通过Excel模板来表征系统的。和市面上大多数使用数据库来表征系统的不一样。此功能可以将数据库反射成通用代码生成器的一个项目。并进一步反射成Excel模板和前端后端的代码生成物。此功能拥有强大的编辑器功能，可以对反射而来的元数据和数据深入编辑，生成相应的结果。同时，对编辑器的深度改进，使用户可以方便的将域对象切换为枚举，或将枚举切换为域对象，也可以清洗登录数据，使密码自动设为用户名。

有了数据库自动反射功能，Rust通用代码生成器莲花成了可以在数分钟内为遗留数据库配上完整的Rust后端软件和Vue前端软件的能力。非常实用，非常强大。欢迎大家试用。

###  重点组件和功能 

- 动词算子
- 域对象
- 棱柱
- 项目 
- 时空之门前端代码生成器引擎
- 弹性登录模块
- Grid页生成模块
- 多对多生成模块
- 数据库脚本生成模块
- 动态椰子树功能群
- 动词否定功能群
- 字段否定功能群
- 高级定制功能群
- 部分生成功能群
- 自动生成差异版本功能群
- 上传生成界面
- 模板向导生成界面
- 自动生成差异版本生成界面
- Excel数据导出功能
- PDF数据导出功能
- 数据库后端模式
- 数据库工具模式
- 哑数据模式


###  安装教程 

- 先装好open jdk 17和Apache Tomcat 9，如果您从源码编译代码生成器还需要事先装好Apache Maven
- 下载编译好的Rust通用代码生成器莲花的war包，或者下载源码，解压后使用mvn install命令编译，得到代码生成器的war包
- 把代码生成器的war包的名字改短，成为Lotus.war，并放置在Apache Tomcat 9的webapps目录下
- 启动Apache Tomcat 9
- 访问http://localhost:8080/Lotus/
- 即可使用代码生成器

###  后端代码生成物运行

- 使用代码生成器生成一个示例或自己的SGS2模板，得到两个代码生成物，一个前端，一个后端
- 将后端项目下载并解压
- 根据您的数据库选项，配置好您后端的数据库，并使用后端代码生成物的sql文件夹下的数据库脚本完成建库建表并加载初始化数据
- 您需要检查后端代码生成物内的配置文件，确认数据库和用户名密码的正确性
- 您需要安装配置好rust 1.69或其他兼容版本的rust环境
- 在后端代码生成物的根目录下运行cargo run命令启动rust后端项目
- 启动完毕后访问后端代码生成物，http://localhost:8082

###  前端代码生成物运行

- 使用代码生成器生成一个示例或自己的SGS2模板，得到两个代码生成物，一个前端，一个后端
- 将前端代码生成物解压
- 确认您已经安装好和您选择的Nodejs版本相容的前端Nodejs环境
- 使用npm install -registry=https://registry.npm.taobao.org 安装Nodejs依赖包
- 如果您使用的是Nodejs 21和Nodejs18，请运行命令：export NODE_OPTIONS=--openssl-legacy-provider
- 运行命令：node --max-http-header-size=1000000 ./node_modules/.bin/webpack-dev-server --inline --progress --config build/webpack.dev.conf.js
- 如此访问前端代码生成物，http://localhost:8000/



###  运行截图 

#### 数据库反射为项目截屏

![输入图片说明](DBReflectProject.png)

#### 软件截屏

![输入图片说明](lotus_excelg.png)

![输入图片说明](Lotus_excelWizard.png)

![输入图片说明](Lotus_compare2.png)

#### 后端代码生成物截屏

日期时间对话框

![输入图片说明](datetime.png)

单表操作

![输入图片说明](lotus_grid.png)

多对多

![输入图片说明](rust_mtm.png)

复杂版面，树表

![输入图片说明](lotus_treegrid.png)

图形报表

![输入图片说明](lotus_echarts.png)

图片功能

![输入图片说明](lotus_pic.png)

Excel数据导出

![输入图片说明](Lotus_excel_export.png)

PDF数据导出

![输入图片说明](Lotus_pdf_export.png)

#### 前端代码生成物截屏

Vue前端日期对话框

![输入图片说明](front_date.png)

Vue独立前端，登录界面

![输入图片说明](Lotus_frontend_login.png)

Vue独立前端，内页

![输入图片说明](Lotus_frontend_inner.png)

###  百度话题
＃通用代码生成器＃

###  尝鲜版三十一的二进制发布包下载

[https://gitee.com/jerryshensjf/Lotus/attach_files](https://gitee.com/jerryshensjf/Lotus/attach_files)


###  Nodejs前端代码生成物运行指南 

莲花尝鲜版二十一消除了95%的EsLint编译警告。并且，您可以忽略全部编译警告。

莲花尝鲜版二十支持Nodejs 21, 18 和 14三种Nodejs环境。

首先，使用Rust通用代码生成器莲花的红莲尝鲜版二十生成前端代码生成物，生成时需选择Nodejs相应版本。

将代码生成物的前端和后端代码生成物的压缩包拷入工作目录并完成解压缩，部署并启动后端项目。

进入前端代码生成物的根目录，需选择正确的Nodejs版本。

运行命令：npm install -registry=https://registry.npm.taobao.org

此命令使用淘宝镜像安装Nodejs依赖包。

如果您使用的是Nodejs 21和Nodejs18，请运行命令：export NODE_OPTIONS=--openssl-legacy-provider

此命令暴露了一个必须的内存变量。

运行命令：node --max-http-header-size=1000000 ./node_modules/.bin/webpack-dev-server --inline --progress --config build/webpack.dev.conf.js

此命令启动了前端项目。

访问：http://localhost:8000/


### 源码编译用户指南

通用代码生成器已经支持自己编译源码，我已把原来缺的前端代码生成器的jar包上传。支持大家自行编译源码。

需要注意的是，现在我的开发平台是Fedora 37上的openjdk 17。所以大家编译源码最好使用openjdk17。编译好的war包运行在apache tomcat 9.0上。

已有jdk8的用户报告默认下载的代码生成器war包在他的平台上无法运行。您如果遇到类似问题请报告。我的电子邮件是：jerry_shen_sjf@qq.com

附openjdk 17下载地址：

[https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)

### 近期视频

Rust通用代码生成器莲花，红莲尝鲜版三十一，视频请见：

[https://www.bilibili.com/video/BV1f6BBYCEEV/](https://www.bilibili.com/video/BV1f6BBYCEEV/)

Rust通用代码生成器莲花，红莲尝鲜版三十，视频请见：

[https://www.bilibili.com/video/BV13vSuYyE6P/](https://www.bilibili.com/video/BV13vSuYyE6P/)

Rust通用代码生成器莲花，红莲尝鲜版二十八，视频请见：

[https://www.bilibili.com/video/BV13NDYYjE4P/](https://www.bilibili.com/video/BV13NDYYjE4P/)

Rust通用代码生成器莲花，红莲尝鲜版二十七，视频请见：

[https://www.bilibili.com/video/BV1qnDVYeEnR/](https://www.bilibili.com/video/BV1qnDVYeEnR/)

[https://www.bilibili.com/video/BV1qJDVYNE6b/](https://www.bilibili.com/video/BV1qJDVYNE6b/)

Rust通用代码生成器莲花，红莲尝鲜版二十六，视频请见：

[https://www.bilibili.com/video/BV1MT421k7GT/](https://www.bilibili.com/video/BV1MT421k7GT/)

[https://www.bilibili.com/video/BV1kZ421T7oT/](https://www.bilibili.com/video/BV1kZ421T7oT/)

Rust通用代码生成器莲花，红莲尝鲜版二十五，此版本完善了PostgreSQL数据库自动反射功能。完善了编辑器，所有Domain可以通过下拉菜单选择。完善了多对多候选叠加时的语法检查。视频请见：

[https://www.bilibili.com/video/BV1bZ421M75E/](https://www.bilibili.com/video/BV1bZ421M75E/)

​
Rust通用代码生成器莲花的红莲尝鲜版二十四公布了技术视频，详细讲解了三大部分生成功能群。此三个功能群都是为在代码开发全程，迭代式的使用通用代码生成器而设计。是程序员的好帮手。视频请见：

[https://www.bilibili.com/video/BV14w4m1q74f/​](https://www.bilibili.com/video/BV14w4m1q74f/​)

最近的视频介绍了最新版尝鲜版二十四对编辑器功能的改进，视频请见：

[https://www.bilibili.com/video/BV1jU411d756/](https://www.bilibili.com/video/BV1jU411d756/)

Rust通用代码生成器莲花，红莲尝鲜版二十四，视频请见：

[https://www.bilibili.com/video/BV1rz421Y7U7/](https://www.bilibili.com/video/BV1rz421Y7U7/)

Rust 通用代码生成器莲花，红莲尝鲜版二十三，此版本新增了多对多候选功能，增强了数据库自动反射功能和模板向导的编辑器。

[https://www.bilibili.com/video/BV1KJ4m1N7gs/](https://www.bilibili.com/video/BV1KJ4m1N7gs/)

Rust通用代码生成器莲花发布红莲尝鲜版二十二发布介绍视频，支持数据库自动反射功能，视频请见：

[https://www.bilibili.com/video/BV1fK421h74U/](https://www.bilibili.com/video/BV1fK421h74U/)

[https://www.bilibili.com/video/BV1cE421371y/](https://www.bilibili.com/video/BV1cE421371y/)

Rust通用代码生成器莲花发布红莲尝鲜版二十一发布介绍视频，前端代码生成物大翻新，视频请见：

[https://www.bilibili.com/video/BV1UH4y1j7td/](https://www.bilibili.com/video/BV1UH4y1j7td/)

Rust通用代码生成器莲花发布红莲尝鲜版二十介绍视频，视频请见：

[https://www.bilibili.com/video/BV1GW4y1c7vA/](https://www.bilibili.com/video/BV1GW4y1c7vA/)

Rust通用代码生成器莲花发布深度修复版红莲尝鲜版十九介绍视频，介绍了PostgreSQL代码生成。视频请见：

[https://www.bilibili.com/video/BV1bC4y1C7bT/](https://www.bilibili.com/video/BV1bC4y1C7bT/)

Rust通用代码生成器莲花发布深度修复版红莲尝鲜版十八介绍视频，初学者指南，详细介绍代码生成器环境搭建，编译，运行和使用代码生成物，欢迎使用。视频请见：

[https://www.bilibili.com/video/BV1364y157Zg/](https://www.bilibili.com/video/BV1364y157Zg/)

尝鲜版十八的视频请见：

[https://www.bilibili.com/video/BV1sa4y1d7cz/](https://www.bilibili.com/video/BV1sa4y1d7cz/)

尝鲜版十七的视频请见：

[https://www.bilibili.com/video/BV1pG411i7Qa/](https://www.bilibili.com/video/BV1pG411i7Qa/)

[https://www.bilibili.com/video/BV1iC4y1j7rd/](https://www.bilibili.com/video/BV1iC4y1j7rd/)

[https://www.bilibili.com/video/BV1rQ4y1t7qJ/](https://www.bilibili.com/video/BV1rQ4y1t7qJ/)


###  版本历史和视频 

[版本历史和视频](https://gitee.com/jerryshensjf/Lotus/blob/master/History.md)


###  源码研读者注意事项 

无垠式代码生成器第一个完整版本源码，有兴趣可以抄写一下：

[https://gitee.com/jerryshensjf/InfinityGPGenerator_0_6_5](https://gitee.com/jerryshensjf/InfinityGPGenerator_0_6_5)

相关技术视频：

[https://www.bilibili.com/video/BV1fX4y1u7Bn/](https://www.bilibili.com/video/BV1fX4y1u7Bn/)

[https://www.bilibili.com/video/BV1rv41187qY/](https://www.bilibili.com/video/BV1rv41187qY/)

[https://www.bilibili.com/video/BV1xv41187cA/](https://www.bilibili.com/video/BV1xv41187cA/)

[https://www.bilibili.com/video/BV1t64y1v7WA/](https://www.bilibili.com/video/BV1t64y1v7WA/)


###  **交流QQ群** 


- 一群：动词算子式代码生成器群 277689737
- 二群：UI设计实验室 70646187
- 三群：动力建站 255973110


### 作者电子邮件：jerry_shen_sjf@qq.com

### 官方配乐：张也《展翅飞翔》

