### 版本历史

本软件的架构由Golang通用代码生成器仙童改写而来。用Java写成。现在使用Open JDK 17编译。

Rust通用代码生成器莲花，红莲尝鲜版二十。此版本开始支持Nodejs21,18加上原来支持的Nodejs 14。现在莲花支持三种Nodejs环境。适应性大大增强，也给您的使用带来了更多方便。

Rust通用代码生成器莲花深度修复版红莲尝鲜版十九。红莲尝鲜版十九修复了前端代码生成的一些缺陷。

Rust通用代码生成器莲花尝鲜版十八。修复了前端代码生成。此版本支持MariaDB，MySQL，PostgreSQL和Oracle四种数据库。此版本支持枚举和datadummy模式。请部署在Tomcat9的webapps目录下。

Rust通用代码生成器莲花尝鲜版十八。修复了动词否定功能群和复杂版面和图形报表联合使用时的一些缺陷。

Rust通用代码生成器莲花尝鲜版十七。此版本是深度修复版，修复规范了下拉列表的空值。前端项目和后端项目经过了更严密的测试。

Rust通用代码生成器莲花尝鲜版十六。此版本是深度修复版，修复了高级定制功能群，自动生成差异版本功能群，动词否定功能群和字段否定功能群里的一系列缺陷，并有其他缺陷修复。此版本支持MariaDB，MySQL，PostgreSQL和Oracle四种数据库。此版本支持枚举和datadummy模式。

Rust通用代码生成器莲花尝鲜版十五。此版本是测试修复版，没有新功能，修复了尝鲜版十四的DataDummy功能。

Rust通用代码生成器莲花尝鲜版十四。此版本是测试修复版，没有新功能，都是尝鲜版十二的功能，但有错误修复。修复Oracle数据库，datadummy模式和枚举。此版本支持MariaDB，MySQL，PostgreSQL和Oracle四种数据库。

Rust通用代码生成器莲花尝鲜版十二。此版本支持Oracle数据库。此版本支持MariaDB，MySQL，PostgreSQL和Oracle四种数据库。

Rust通用代码生成器莲花尝鲜版十一。此版本是一个修复增强版本，此版本完善了DataDummy模式，并完善了枚举的使用。并修复了数据库模式的脚本缺陷。暂不支持Oracle数据库。欢迎使用纯内存数据模式的原型功能，此功能为售前和产品经理开发，欢迎使用。

Rust通用代码生成器莲花尝鲜版十。此版本支持DataDummy模式，即纯内存数据模式的原型功能，为售前和产品经理开发。支持枚举，优化了模块结构，main.rs只包含入口点和最低限度的其他程序，其他模块都放在lib.rs里，前端代码生成器的默认图片位置改到static目录下，方便vue前端的构建。

Rust通用代码生成器莲花尝鲜版九。此版本更新Axum至最新版0.6.20,改进了前端登录失败的错误提示，去除了后端运行时在控制台上输出的调试信息。

Rust通用代码生成器莲花尝鲜版八已发布。此版本更新JQuery EasyUI至最新版1.10.17。此版本解决了编译警告问题。支持日期与日期时间的空值。修复了尝鲜版七过度更新引起的后端编辑不支持更新图片的缺陷。和其他一些缺陷。

Rust通用代码生成器莲花已发布红莲尝鲜版七，这是迄今为止最重要的里程碑。最重要的更新是数据库访问框架从0.3.5大幅更新至0.7.1最新版。这是一个重大的更新，尝试了几次才顺利完成了这个规划了很久的更新。第二个更新是消除了代码生成物上90%的编译警告，大幅改善了代码生成物的代码质量。而后通过更多的测试，修复了一系列缺陷。

Rust通用代码生成器莲花尝鲜版六。此版本支持日期与日期时间，支持三大部分生成功能群。支持自动登录模块，支持修改自己的资料和登陆密码。修复了尝鲜版五的克隆和批克隆出错问题，修复了尝鲜版五编辑用户会导致密码失效问题。和其他一些缺陷。暂不支持Oracle数据库，暂不支持空值。功能对标java通用代码生成器光电音之王版本。

Rust通用代码生成器莲花尝鲜版5。此版本是第一个功能完整的可用版本，支持自动登录模块，支持Axum最新版0.6.18,彻底改进了数据库访问层，解决了每次查询新建一个数据库链接，以致只能查询10余次的重大缺陷，经过更多测试，此版本支持MariaDB,MySQL和PostgreSQL数据库。支持所附全部非Oracle示例。本版本的功能和质量均达到可用水平。
尝鲜版4。通过编程和测试，终于彻底修复了莲花生成的Vue前端。现在，前端和后端可以顺畅的工作，没有任何障碍了。莲花尝鲜版4除了弹性登录模块和Oracle数据库的功能外，其他功能都已完备。大家已经可以试用了。

莲花尝鲜版3升级Axum至0.6.6,并消除2/3的编译警告。后续版本有可能将Axum重新降级至0.4。Axum0.6的粉丝请使用此版本。您可以使用所附非Oracle示例测试系统，可以得到可以运行的代码生成物。

此版本支持Go语言兼容性，即Go语言通用代码生成器仙童的模板直接生成Rust代码生成物。也支持Java兼容性，即支持java通用代码生成器光，和平之翼代码生成器，无垠式代码生成器的Excel模板直接生成Rust代码生成物。

尝鲜版4支持Excel,PDF数据导出。支持Vue，ElementUI的独立前端。支持MySQL，MariaDB和PostgreSQL数据库。支持图片功能。支持所附全部非Oracle示例。欢迎试用。

### 介绍视频

Rust通用代码生成器莲花发布红莲尝鲜版二十介绍视频，视频请见：

[https://www.bilibili.com/video/BV1GW4y1c7vA/](https://www.bilibili.com/video/BV1GW4y1c7vA/)

Rust通用代码生成器莲花发布深度修复版红莲尝鲜版十九介绍视频，介绍了PostgreSQL代码生成。视频请见：

[https://www.bilibili.com/video/BV1bC4y1C7bT/](https://www.bilibili.com/video/BV1bC4y1C7bT/)

Rust通用代码生成器莲花发布深度修复版红莲尝鲜版十八介绍视频，初学者指南，详细介绍代码生成器环境搭建，编译，运行和使用代码生成物，欢迎使用。视频请见：

[https://www.bilibili.com/video/BV1364y157Zg/](https://www.bilibili.com/video/BV1364y157Zg/)

尝鲜版十八的视频请见：

[https://www.bilibili.com/video/BV1sa4y1d7cz/](https://www.bilibili.com/video/BV1sa4y1d7cz/)

尝鲜版十七的视频请见：

[https://www.bilibili.com/video/BV1pG411i7Qa/](https://www.bilibili.com/video/BV1pG411i7Qa/)

[https://www.bilibili.com/video/BV1iC4y1j7rd/](https://www.bilibili.com/video/BV1iC4y1j7rd/)

[https://www.bilibili.com/video/BV1rQ4y1t7qJ/](https://www.bilibili.com/video/BV1rQ4y1t7qJ/)

尝鲜版十六的视频请见：

[https://www.bilibili.com/video/BV1Mw411x7FP/](https://www.bilibili.com/video/BV1Mw411x7FP/)

尝鲜版十五的视频请见：

[https://www.bilibili.com/video/BV1Pw411X7h5/](https://www.bilibili.com/video/BV1Pw411X7h5/)

[https://www.bilibili.com/video/BV1VC4y1Z7KF/](https://www.bilibili.com/video/BV1VC4y1Z7KF/)

尝鲜版十四的视频请见：

[https://www.bilibili.com/video/BV1Fz4y1F7jf/](https://www.bilibili.com/video/BV1Fz4y1F7jf/)

尝鲜版十二的介绍视频请见：

[https://www.bilibili.com/video/BV1MH4y1U7A1/](https://www.bilibili.com/video/BV1MH4y1U7A1/)

尝鲜版十一的介绍视频请见：

[https://www.bilibili.com/video/BV1Qm4y1V77Q/](https://www.bilibili.com/video/BV1Qm4y1V77Q/)

[https://www.bilibili.com/video/BV1Ez4y157k2/](https://www.bilibili.com/video/BV1Ez4y157k2/)

尝鲜版十的介绍视频请见：

[https://www.bilibili.com/video/BV1Fh4y1P7xF/](https://www.bilibili.com/video/BV1Fh4y1P7xF/)

尝鲜版九的介绍视频请见：

[https://www.bilibili.com/video/BV1Hp4y1E7T2/](https://www.bilibili.com/video/BV1Hp4y1E7T2/)

尝鲜版八介绍视频请见：

[https://www.bilibili.com/video/BV1qG411f7io/](https://www.bilibili.com/video/BV1qG411f7io/)

[https://www.bilibili.com/video/BV1yV4y1Y7pM/](https://www.bilibili.com/video/BV1yV4y1Y7pM/)

尝鲜版七介绍视频请见：

[https://www.bilibili.com/video/BV1Kz4y1W7eH/](https://www.bilibili.com/video/BV1Kz4y1W7eH/)

尝鲜版六介绍视频请见：

[https://www.bilibili.com/video/BV1Cj41197nY/](https://www.bilibili.com/video/BV1Cj41197nY/)

尝鲜版五介绍视频请见：

[https://www.bilibili.com/video/BV1Cg4y1574C/](https://www.bilibili.com/video/BV1Cg4y1574C/)

Rust通用代码生成器莲花尝鲜版4发布介绍视频，从源码开始构建Rust通用代码生成器莲花。并生成一个例程的前后端并演示运行。
视频请见：

[https://www.bilibili.com/video/BV11N411c7H6/](https://www.bilibili.com/video/BV11N411c7H6/)

尝鲜版3

[https://www.bilibili.com/video/BV1co4y1a7Ca/](https://www.bilibili.com/video/BV1co4y1a7Ca/)

尝鲜版2

[https://www.bilibili.com/video/BV1aj411N7YB/](https://www.bilibili.com/video/BV1aj411N7YB/)


### 尝鲜版二十一的二进制发布包下载

[https://gitee.com/jerryshensjf/Lotus/attach_files](https://gitee.com/jerryshensjf/Lotus/attach_files)



### Nodejs前端代码生成物运行指南

莲花尝鲜版二十支持Nodejs 21, 18 和 14三种Nodejs环境。

首先，使用Rust通用代码生成器莲花的红莲尝鲜版二十生成前端代码生成物，生成时需选择Nodejs相应版本。

将代码生成物的前端和后端代码生成物的压缩包拷入工作目录并完成解压缩，部署并启动后端项目。

进入前端代码生成物的根目录，需选择正确的Nodejs版本。

运行命令：npm install -registry=https://registry.npm.taobao.org

此命令使用淘宝镜像安装Nodejs依赖包。

如果您使用的是Nodejs 21和Nodejs18，请运行命令：export NODE_OPTIONS=--openssl-legacy-provider

此命令暴露了一个必须的内存变量。

运行命令：node --max-http-header-size=1000000 ./node_modules/.bin/webpack-dev-server --inline --progress --config build/webpack.dev.conf.js

此命令启动了前端项目。

访问：http://localhost:8000/
