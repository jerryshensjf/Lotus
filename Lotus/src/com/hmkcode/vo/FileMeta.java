package com.hmkcode.vo;

import java.io.InputStream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties({"content"})
public class FileMeta {	
	private String fileName;
	private String fileSize;
	private String fileType;
	private String twitter;
	private String ignoreWarning;
	private String genFormatted;
	private String generateFront;
	private String autoConfig;
	private String frontUseProjectName;
	private String frontUseControllerPrefix;
	private String sqlComments;
	private String javaCompatibility;
	private String goCompatibility;

	private String useAdvancedCustomize;
	private String genUi;
	private String genController;
	private String genService;
	private String genServiceImpl;
	private String genDao;
	private String genDaoImpl;
	
	private String useUserPartial;
	private String exportStr;
	
	private InputStream content;
	
	public String getUseUserPartial() {
		return useUserPartial;
	}
	public void setUseUserPartial(String useUserPartial) {
		this.useUserPartial = useUserPartial;
	}
	public String getExportStr() {
		return exportStr;
	}
	public void setExportStr(String exportStr) {
		this.exportStr = exportStr;
	}	
	
	public String getJavaCompatibility() {
		return javaCompatibility;
	}
	public String getGoCompatibility() {
		return goCompatibility;
	}
	public void setGoCompatibility(String goCompatibility) {
		this.goCompatibility = goCompatibility;
	}
	public void setJavaCompatibility(String javaCompatibility) {
		this.javaCompatibility = javaCompatibility;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public InputStream getContent(){
		return this.content;
	}
	public void setContent(InputStream content){
		this.content = content;
	}
	public String getTwitter(){
		return this.twitter;
	}
	public void setTwitter(String twitter){
		this.twitter = twitter;
	}
	
	@Override
	public String toString() {
		return "FileMeta [fileName=" + fileName + ", fileSize=" + fileSize+
				", fileType=" + fileType + ",ignoreWarning="+ignoreWarning+ 
				",generateFront="+generateFront+
				",frontUseProjectName="+frontUseProjectName+
				",genFormatted="+genFormatted+
				",sqlComments="+sqlComments+
				"]";
	}
	public String getIgnoreWarning() {
		return ignoreWarning;
	}
	public void setIgnoreWarning(String ignoreWarning) {
		this.ignoreWarning = ignoreWarning;
	}
	public String getGenerateFront() {
		return generateFront;
	}
	public void setGenerateFront(String generateFront) {
		this.generateFront = generateFront;
	}
	public String getFrontUseProjectName() {
		return frontUseProjectName;
	}
	public void setFrontUseProjectName(String frontUseProjectName) {
		this.frontUseProjectName = frontUseProjectName;
	}
	public String getGenFormatted() {
		return genFormatted;
	}
	public void setGenFormatted(String genFormatted) {
		this.genFormatted = genFormatted;
	}
	public String getGenUi() {
		return genUi;
	}
	public void setGenUi(String genUi) {
		this.genUi = genUi;
	}
	public String getGenController() {
		return genController;
	}
	public void setGenController(String genController) {
		this.genController = genController;
	}
	public String getGenService() {
		return genService;
	}
	public void setGenService(String genService) {
		this.genService = genService;
	}
	public String getGenServiceImpl() {
		return genServiceImpl;
	}
	public void setGenServiceImpl(String genServiceImpl) {
		this.genServiceImpl = genServiceImpl;
	}
	public String getGenDao() {
		return genDao;
	}
	public void setGenDao(String genDao) {
		this.genDao = genDao;
	}
	public String getGenDaoImpl() {
		return genDaoImpl;
	}
	public void setGenDaoImpl(String genDaoImpl) {
		this.genDaoImpl = genDaoImpl;
	}
	public String getUseAdvancedCustomize() {
		return useAdvancedCustomize;
	}
	public void setUseAdvancedCustomize(String useAdvancedCustomize) {
		this.useAdvancedCustomize = useAdvancedCustomize;
	}
	public String getFrontUseControllerPrefix() {
		return frontUseControllerPrefix;
	}
	public void setFrontUseControllerPrefix(String frontUseControllerPrefix) {
		this.frontUseControllerPrefix = frontUseControllerPrefix;
	}
	public String getAutoConfig() {
		return autoConfig;
	}
	public void setAutoConfig(String autoConfig) {
		this.autoConfig = autoConfig;
	}	
	public String getSqlComments() {
		return sqlComments;
	}
	public void setSqlComments(String sqlComments) {
		this.sqlComments = sqlComments;
	}
}
