package com.hmkcode;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.hmkcode.vo.FileMeta;

public class MultipartRequestHandler {

	public static List<FileMeta> uploadByApacheFileUpload(HttpServletRequest request) throws IOException, ServletException{
				
		List<FileMeta> files = new LinkedList<FileMeta>();

		String twitter = "";
		String ignoreWarning = "";
		String genFormatted = "";
		String generateFront = "";
		String autoConfig = "";
		String frontUseProjectName = "";
		String frontUseControllerPrefix = "";
		String sqlComments = "";
		String javaCompatibility = "";
		String goCompatibility = "";
		
		String useAdvancedCustomize = "";
		String genUi = "";
		String genController = "";
		String genService = "";
		String genServiceImpl = "";
		String genDao = "";
		String genDaoImpl = "";
		
		String useUserPartial = "";
		String exportStr = "";
		
		// 1. Check request has multipart content
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		FileMeta temp = null;
		
		// 2. If yes (it has multipart "files")
		if(isMultipart){

			// 2.1 instantiate Apache FileUpload classes
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			

			// 2.2 Parse the request
			try {
				
				// 2.3 Get all uploaded FileItem
				List<FileItem> items = upload.parseRequest(request);
				
				// 2.4 Go over each FileItem
				for(FileItem item:items){
					
					// 2.5 if FileItem is not of type "file"
				    if (item.isFormField()) {

				    	// 2.6 Search for "twitter" parameter
				        if(item.getFieldName().equals("twitter"))
				        	twitter = item.getString();
				        if(item.getFieldName().equals("ignoreWarning"))
				        	ignoreWarning = item.getString();
				        if(item.getFieldName().equals("genFormatted"))
				        	genFormatted = item.getString();
				        if(item.getFieldName().equals("generateFront"))
				        	generateFront = item.getString();
				        if(item.getFieldName().equals("autoConfig"))
				        	autoConfig = item.getString();
				        if(item.getFieldName().equals("frontUseProjectName"))
				        	frontUseProjectName = item.getString();
				        if(item.getFieldName().equals("frontUseControllerPrefix"))
				        	frontUseControllerPrefix = item.getString();				        
				        if(item.getFieldName().equals("sqlComments"))
				        	sqlComments = item.getString();
				        if(item.getFieldName().equals("javaCompatibility"))
				        	javaCompatibility = item.getString();	
				        if(item.getFieldName().equals("goCompatibility"))
				        	goCompatibility = item.getString();
				        
				        if(item.getFieldName().equals("useAdvancedCustomize"))
				        	useAdvancedCustomize = item.getString();
				        if(item.getFieldName().equals("genUi"))
				        	genUi = item.getString();
				        if(item.getFieldName().equals("genController"))
				        	genController = item.getString();
				        if(item.getFieldName().equals("genService"))
				        	genService = item.getString();
				        if(item.getFieldName().equals("genServiceImpl"))
				        	genServiceImpl = item.getString();
				        if(item.getFieldName().equals("genDao"))
				        	genDao = item.getString();
				        if(item.getFieldName().equals("genDaoImpl"))
				        	genDaoImpl = item.getString();
				        
				        if(item.getFieldName().equals("useUserPartial"))
				        	useUserPartial = item.getString();
				        if(item.getFieldName().equals("exportStr"))
				        	exportStr = item.getString();
				        
				    } else {
				       
				    	// 2.7 Create FileMeta object
				    	temp = new FileMeta();
						temp.setFileName(item.getName());
						temp.setContent(item.getInputStream());
						temp.setFileType(item.getContentType());
						temp.setFileSize(item.getSize()/1024+ "Kb");
						temp.setIgnoreWarning(ignoreWarning);
						temp.setGenFormatted(genFormatted);
						temp.setGenerateFront(generateFront);
						temp.setAutoConfig(autoConfig);
						temp.setFrontUseProjectName(frontUseProjectName);
						temp.setFrontUseControllerPrefix(frontUseControllerPrefix);						
						temp.setSqlComments(sqlComments);
						temp.setJavaCompatibility(javaCompatibility);
						temp.setGoCompatibility(goCompatibility);
						
						temp.setUseAdvancedCustomize(useAdvancedCustomize);
						temp.setGenUi(genUi);
						temp.setGenController(genController);
						temp.setGenService(genService);
						temp.setGenServiceImpl(genServiceImpl);
						temp.setGenDao(genDao);
						temp.setGenDaoImpl(genDaoImpl);
						
						temp.setUseUserPartial(useUserPartial);
						temp.setExportStr(exportStr);
						
				    	// 2.7 Add created FileMeta object to List<FileMeta> files
						files.add(temp);
				       
				    }
				}
				
				// 2.8 Set "twitter" parameter 
				for(FileMeta fm:files){
					fm.setTwitter(twitter);
					fm.setIgnoreWarning(ignoreWarning);
					fm.setGenFormatted(genFormatted);
					fm.setGenerateFront(generateFront);
					fm.setAutoConfig(autoConfig);
					fm.setFrontUseProjectName(frontUseProjectName);
					fm.setFrontUseControllerPrefix(frontUseControllerPrefix);
					fm.setSqlComments(sqlComments);
					fm.setGoCompatibility(goCompatibility);
					fm.setJavaCompatibility(javaCompatibility);
					
					fm.setUseAdvancedCustomize(useAdvancedCustomize);
					fm.setGenUi(genUi);
					fm.setGenController(genController);
					fm.setGenService(genService);
					fm.setGenServiceImpl(genServiceImpl);
					fm.setGenDao(genDao);
					fm.setGenDaoImpl(genDaoImpl);
					
					fm.setUseUserPartial(useUserPartial);
					fm.setExportStr(exportStr);
				}
				
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
		}
		return files;
	}

	
	// this method is used to get file name out of request headers
	// 
	private static String getFilename(Part part) {
	    for (String cd : part.getHeader("content-disposition").split(";")) {
	        if (cd.trim().startsWith("filename")) {
	            String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
	            return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
	        }
	    }
	    return null;
	}
}
