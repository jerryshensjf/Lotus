package org.light.utils;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.light.domain.Method;
import org.light.exception.ValidateException;

public class MethodUtil {
	public static Map<String,Set<Method>> divideMethodSetWithTempTag(Set<Method> methods, String defaultTag) throws ValidateException{
		Map<String,Set<Method>> result = new TreeMap<>();
		for (Method m:methods) {
			if (StringUtil.isBlank(m.getTempTag())||m.getTempTag().equals(defaultTag)) {
				Set<Method> cur = result.get(defaultTag);
				if (cur==null) {
					cur = new TreeSet<Method>();
					cur.add(m);
					result.put(defaultTag, cur);
				}else {
					cur.add(m);
					result.put(defaultTag, cur);
				}
			}else {
				Set<Method> cur = result.get(m.getTempTag());
				if (cur==null) {
					cur = new TreeSet<Method>();
					cur.add(m);
					result.put(m.getTempTag(), cur);
				}else {
					cur.add(m);
					result.put(m.getTempTag(), cur);
				}
			}
		}
		return result;
	}	
}
