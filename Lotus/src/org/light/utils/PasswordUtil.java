package org.light.utils;

import java.security.MessageDigest;
import java.util.List;

import org.apache.shiro.crypto.hash.Sha1Hash;

public class PasswordUtil {
	public static String sha1(String val) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(val.getBytes());
		byte[] digest = md.digest();

		StringBuffer hexstr = new StringBuffer();
		String shaHex = "";
		for (int i = 0; i < digest.length; i++) {
			shaHex = Integer.toHexString(digest[i] & 0xFF);
			if (shaHex.length() < 2) {
				hexstr.append(0);
			}
			hexstr.append(shaHex);
		}
		return hexstr.toString();
	}

	public static String generateSalt(List<String> salts) throws Exception {
		String salt = "";
		char[] pinletters = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

		do {
			salt = "";
			for (int i = 0; i < 32; i++) {
				salt += pinletters[(int) (Math.random() * 16)];
			}
		} while (!uniqueSalt(salt, salts));
		return salt;
	}

	public static boolean uniqueSalt(String salt, List<String> salts) throws Exception {
		if (!StringUtil.isBlank(salt)&&salts!=null) {
			if (salts.contains(salt))
				return false;
			else
				return true;
		} else if (salts == null) {
			System.out.println("The salts is empty!");
			return true;
		}
		else {
			System.out.println("The salt is empty!");
			return false;
		}
	}
	
	public static String getPassword(String plain,String salt) throws Exception{
		plain = sha1(plain);
		return multiSha1Hash(plain,salt,3);
	}
	
	public static String getShiroPassword(String plain,String salt) throws Exception{
		return new Sha1Hash(plain,salt,3).toString();
	}	

	protected static String multiSha1Hash(String password, String salt, int hashIterations) throws Exception {
	  MessageDigest digest = MessageDigest.getInstance("SHA1");
	  if (salt != null) {
	    digest.reset();
	    digest.update(salt.getBytes());
	  }
	  byte[] hashed = digest.digest(password.getBytes());
	  int iterations = hashIterations - 1;

	  for(int i = 0; i < iterations; ++i) { // 根据迭代次数进行多次散列
	    digest.reset();
	    hashed = digest.digest(hashed);
	  }
	  
	  	StringBuffer hexstr = new StringBuffer();
		String shaHex = "";
		for (int i = 0; i < hashed.length; i++) {
			shaHex = Integer.toHexString(hashed[i] & 0xFF);
			if (shaHex.length() < 2) {
				hexstr.append(0);
			}
			hexstr.append(shaHex);
		}
		return hexstr.toString();
	}
	
	public static void main(String [] args) throws Exception{
		String password = getPassword("Mala","1f7b169c");
		String shiroPassword = getShiroPassword("Mala","1f7b169c");
		System.out.println(password);
		System.out.println(shiroPassword);
		System.out.println(password.equals(shiroPassword));
		System.out.println(generateSalt(null));
		
		String password2 = getPassword("admin","8097797");
		String password3 = multiSha1Hash("admin","8097797",3);
		
		String coded ="4d72912e724ae44df602814025aceaf629175665";
		System.out.println(password2.equals(coded));
		System.out.println(password2);
		System.out.println(coded);
		System.out.println(password3);
		
		String password4 = getPassword("jerry","d282f7fa");
		String coded2 ="b7a21511ea761d77d287daebcb9bbfc528661bd4";
		System.out.println(password4);
		System.out.println(coded2);
		
		String password5 = getPassword("admin","1cf9e37002c1d2d50e96d5bd8599b4be");		
		String coded5 ="2668084130cd9368c0efac6748f317726b3e5fde";
		System.out.println(password5);
		System.out.println(coded5);		
	}
}
