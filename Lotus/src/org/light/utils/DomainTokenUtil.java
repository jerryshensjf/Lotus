package org.light.utils;

import java.util.Set;

import org.light.domain.Domain;
import org.light.domain.Field;


public class DomainTokenUtil {
	public static String changeDomainFieldtoTableColum(String value){
		StringBuilder sb = new StringBuilder(value);
		StringBuilder sb0 = new StringBuilder("");
		boolean continueCap = false;
		for(int i=0; i < sb.length(); i++){
			char ch = sb.charAt(i);
			if (ch<='Z'&& ch>='A'&&i>0&&!continueCap){
				sb0.append("_").append((""+ch).toLowerCase());
				continueCap = true;
			}else if (ch<='Z'&& ch>='A'&&i==0){
				sb0.append((""+ch).toLowerCase());
				continueCap = true;
			} else if (ch<='Z'&& ch>='A'&&continueCap){
				sb0.append((""+ch).toLowerCase());
			}else if (ch<='z'&& ch>='a') {
				sb0.append(ch);
				continueCap = false;
			}else {
				sb0.append(ch);
			}
		}
		return sb0.toString();
	}
	
	public static String generateTableCommaFields(Domain domain){
		Set<Field> set = domain.getFields();
		StringBuilder sb = new StringBuilder();
		for (Field f: set){
			sb.append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
		}
		if (set.size() > 0) sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public static String generateTableCommaFieldsWithDeniedFields(Domain domain,Set<Field> deniedFields){
		Set<Field> set = domain.getFields();
		StringBuilder sb = new StringBuilder();
		for (Field f: set){
			if (!deniedFields.contains(f)) {
				if ("Image".equalsIgnoreCase(f.getFieldType())) {
					if ("mysql".equalsIgnoreCase(domain.getDbType())||"mariadb".equalsIgnoreCase(domain.getDbType())){
						sb.append(changeImageFieldtoTableColum(f)).append(",");	
					} else if ("postgresql".equalsIgnoreCase(domain.getDbType())||"pgsql".equalsIgnoreCase(domain.getDbType())) {
						sb.append(changeImageFieldtoPgTableColum(f)).append(",");	
					} else if ("oracle".equalsIgnoreCase(domain.getDbType())) {
						sb.append(changeImageFieldtoPgTableColum(f)).append(",");	
					}
				}else {
					sb.append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
				}
			}else {
				if ("String".equalsIgnoreCase(f.getFieldType())||"str".equalsIgnoreCase(f.getFieldType())) {
					sb.append(" '' as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
				}else if ("i64".equalsIgnoreCase(f.getFieldType())||"i32".equalsIgnoreCase(f.getFieldType())||"f64".equalsIgnoreCase(f.getFieldType())||"f32".equalsIgnoreCase(f.getFieldType())) {
					sb.append(" 0 as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
				} else if ("bool".equalsIgnoreCase(f.getFieldType())) {
					sb.append(" false as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
				} else {
					sb.append(" NULL as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
				}
			}
		}
		if (set.size() > 0) sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	private static Object changeImageFieldtoTableColum(Field f) {
		//return "CASE WHEN "+StringUtil.getSnakeName(f.getFieldName())+" IS NOT NULL then to_base64("+StringUtil.getSnakeName(f.getFieldName())+")   else '' end as "+StringUtil.getSnakeName(f.getFieldName());
		return StringUtil.getSnakeName(f.getFieldName());
	}
	
	private static Object changeImageFieldtoPgTableColum(Field f) {
		//return "CASE WHEN "+StringUtil.getSnakeName(f.getFieldName())+" IS NOT NULL then encode("+StringUtil.getSnakeName(f.getFieldName())+"::bytea,'base64')   else '' end as "+StringUtil.getSnakeName(f.getFieldName());
		return StringUtil.getSnakeName(f.getFieldName());
	}

	public static String generateTableCommaFieldsWithTablePrefix(Domain domain) throws Exception{
		Set<Field> set = domain.getFields();
		StringBuilder sb = new StringBuilder();
		for (Field f: set){
			sb.append(domain.getDbPrefix()+TableStringUtil.domainNametoTableName(domain)).append(".").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
		}
		if (set.size() > 0) sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public static String generateTableCommaFieldsWithTablePrefixWithoutImage(Domain domain) throws Exception{
		Set<Field> set = domain.getFields();
		StringBuilder sb = new StringBuilder();
		for (Field f: set){
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sb.append(" null as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
			} else {
				sb.append(domain.getDbPrefix()+TableStringUtil.domainNametoTableName(domain)).append(".").append(changeDomainFieldtoTableColum(f.getFieldName())).append(",");
			}
		}
		if (set.size() > 0) sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public static String generateTableCommaFieldsWithEmptyValuesExceptDomainId(Domain domain) throws Exception{
		Set<Field> set = domain.getFieldsWithoutId();
		StringBuilder sb = new StringBuilder();
		for (Field f: set){
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sb.append(" null as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(", ");
			}else {
				sb.append(" ").append(TypeUtil.findEmptySqlTypeToken(f.getFieldType())).append(" as ").append(changeDomainFieldtoTableColum(f.getFieldName())).append(", ");
			}
		}
		if (set.size() > 0) sb.deleteCharAt(sb.length()-2);
		return sb.toString();
	}
}
