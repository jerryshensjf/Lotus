package org.light.utils;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.Var;

public class FieldUtil {
	public static String generateRequestGetParameterString(Field field, Var request){
		String type = field.getFieldType();
		switch (type) {
		case "long": return "Long.parseLong(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "int": return "Integer.parseInt(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "float": return "Float.parseFloat(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "double": return "Double.parseDouble(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "String": return request.getVarName() + ".getParameter(\""+field.getFieldName()+"\")";
		case "boolean": return "Boolean.parseBoolean("+request.getVarName()+ ".getParameter(\""+field.getFieldName()+"\"))";
		case "BigDecimal": return "new BigDecimal("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "Timestamp": return "new Timestamp("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "Date": return "new Date("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		default : return null;
		}
	}
	
	public static Set<Field> filterDeniedFields(Set<Field> allFields, Set<Field> deniedFields) {
		Set<Field> results = new TreeSet<Field>(new FieldSerialComparator());
		outer: for (Field f : allFields) {
			for (Field df : deniedFields) {
				String fieldName = f.getFieldName();
				if (fieldName.equals(df.getFieldName())) {
					continue outer;
				}
			}
			results.add(f);
		}
		return results;
	}
	
	public static String genFieldArrayTextQStr(List<Field> fields) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append("\"").append(f.getText()).append("\",");
		}
		String retStr = sb.toString();
		if (retStr.contains(",")) return retStr.substring(0,retStr.length()-1);
		else return retStr;
	}
	
	public static String genFieldArrayLowerFirstNameWithPrefixStr(List<Field> fields, String prefix) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append(prefix).append(f.getLowerFirstFieldName()).append(",");
		}
		String retStr = sb.toString();
		if (retStr.contains(",")) return retStr.substring(0,retStr.length()-1);
		else return retStr;
	}

	public static boolean isNumeric(Field f) {
		boolean retVal = false;
		String fieldType = f.getFieldType();
		if ("i32".equals(fieldType)||"i64".equals(fieldType)||
				"f32".equals(fieldType)||"f64".equals(fieldType)) {
			retVal = true;
		}
		return retVal;
	}
	
	public static boolean isSameType(Field f1,Field f2) {
		if (f1== null && f2==null) return true;
		if ((f1== null && f2!=null) ||(f1!= null && f2 ==null) ) return false;
		if (f1.getFieldType().equals(f2.getFieldType())) return true;
		if (f1.getClassType().getTypeName().equals(f2.getClassType().getTypeName())) return true;
		return false;
	}
	
	public static String findTypeDefaultValueString(String typeName) {
		if ("i32".equals(typeName)||"i64".equals(typeName)) {
			return "0";
		}else if ("f32".equals(typeName)||"f64".equals(typeName)) {
			return "0.0";
		}  else if ("bool".equals(typeName)) {
			return "false";
		} else if ("String".equalsIgnoreCase(typeName)) {
			return "";
		}else if ("date".equalsIgnoreCase(typeName)) {
			return "None";
		} else if ("datetime".equalsIgnoreCase(typeName)) {
			return "None";
		} else if ("image".equalsIgnoreCase(typeName)) {
			return "";
		}
		return "";
	}
	
	public static String findTypeSqlDefaultValueString(String typeName) {
		if ("i32".equals(typeName)||"i64".equals(typeName)) {
			return "0";
		} else if ("f32".equals(typeName)||"f64".equals(typeName)) {
			return "0";
		} else if ("bool".equals(typeName)) { 
			return "false";
		} else if ("String".equalsIgnoreCase(typeName)) {
			return "";
		}else if ("date".equalsIgnoreCase(typeName)) {
			return "null";
		} else if ("datetime".equalsIgnoreCase(typeName)) {
			return "null";
		} else if ("image".equalsIgnoreCase(typeName)) {
			return "";
		}
		return null;
	}
	
	public static String findTypeDefaultValueStringToken(String typeName) {
		if ("i32".equals(typeName)||"i64".equals(typeName)) {
			return "0";
		}else if ("f32".equals(typeName)||"f64".equals(typeName)) {
			return "0.0";
		}  else if ("bool".equals(typeName)) {
			return "false";
		} else if ("String".equalsIgnoreCase(typeName)) {
			return "\"\".to_string()";
		}else if ("date".equalsIgnoreCase(typeName)) {
			return "None";
		} else if ("datetime".equalsIgnoreCase(typeName)) {
			return "None";
		} else if ("image".equalsIgnoreCase(typeName)) {
			return "\"\".to_string()";
		}
		return "\"\".to_string()";
	}
	
	public static String findTypeValueToken(Field f) {
		if ("i32".equals(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "0";
			} else {
				return f.getFieldValue().trim() + " as i32";
			}
		} if ("i64".equals(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "0";
			} else {
				return f.getFieldValue().trim() + " as i64";
			}
		} else if (f instanceof Dropdown) {
			if (StringUtil.isBlank(f.getFieldValue())||"null".equalsIgnoreCase(f.getFieldValue().trim())){
				return "0 as i64";
			} else {
				return f.getFieldValue().trim() + " as i64";
			}
		} else if ("f32".equals(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "0.0";
			} else {
				return f.getFieldValue().trim() +" as f32";
			}
		} else if ("f64".equals(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "0.0";
			} else {
				return f.getFieldValue().trim() +" as f64";
			}
		} else if ("bool".equals(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "false";
			} else {
				return f.getFieldValue().trim();
			}
		} else if ("String".equalsIgnoreCase(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())){
				return "\"\".to_string()";
			} else {
				return "\""+f.getFieldValue().trim()+"\".to_string()";
			}
		}else if ("date".equalsIgnoreCase(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())|| "null".equalsIgnoreCase(f.getFieldValue())||"None".equalsIgnoreCase(f.getFieldValue())){
				return "None";
			} else {
				return "Some(NaiveDate::parse_from_str(\""+f.getFieldValue().trim()+"\",DATE_FORMAT).unwrap())";
			}
		} else if ("datetime".equalsIgnoreCase(f.getFieldType())) {
			if (StringUtil.isBlank(f.getFieldValue().trim())|| "null".equalsIgnoreCase(f.getFieldValue())||"None".equalsIgnoreCase(f.getFieldValue())){
					return "None";
			} else {
				return "Some(NaiveDateTime::parse_from_str(\""+f.getFieldValue().trim()+"\",DATE_TIME_FORMAT).unwrap())";
			}
		} else if ("image".equalsIgnoreCase(f.getFieldType())) {
			return "\"\".to_string()";
		}
		return "";
	}
}