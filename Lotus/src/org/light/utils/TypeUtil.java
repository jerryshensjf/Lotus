package org.light.utils;

import org.light.domain.Type;

public class TypeUtil {
	public static boolean isNumeric(Type type){
		if (type.getTypeName().equals("i32")||type.getTypeName().equalsIgnoreCase("i64")||
				type.getTypeName().equalsIgnoreCase("f32")||type.getTypeName().equalsIgnoreCase("f64")){
			return true;
		}else {
			return false;		
		}		
	}
	
	public static String findNullType(String typeName) {
		switch (typeName) {
		case "bool": 
			return "NullBool";
		case "string": 
			return "NullString";
		case "int64": 
			return "NullInt64";
		case "float64": 
			return "NullFloat64";
		case "int32": 
			return "NullInt32";
		default:
				return typeName;
		}
	}
	
	public static String findEmptySqlTypeToken(String typeName) {
		typeName = typeName.toLowerCase();
		switch (typeName) {
		case "bool": 
			return "false";
		case "string": 
			return "''";
		case "i64": 
			return "0";
		case "i32": 
			return "0";
		case "f64": 
			return "0";
		case "f32": 
			return "0";
		case "datetime": 
			return "null";
		case "date": 
			return "null";
		case "image": 
			return "null";
		default:
			return "null";
		}
	}
	
	public static String findEmptyTypeValueString(String typeName) {
		typeName = typeName.toLowerCase();
		switch (typeName) {
		case "bool": 
			return "false";
		case "String": 
			return "\"\"";
		case "i64": 
			return "0";
		case "i32": 
			return "0";
		case "f64": 
			return "0";
		case "f32": 
			return "0";
		case "datetime": 
			return "\"\"";
		case "date": 
			return "\"\"";
		case "image": 
			return "\"\"";
		default:
			return "\"\"";
		}
	}
}
