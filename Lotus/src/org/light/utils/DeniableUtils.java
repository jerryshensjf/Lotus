package org.light.utils;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;

public class DeniableUtils {
	protected final static String[] oneDomainVerbs = { "Activate","ActivateAll","Add", "Clone", "CloneAll", "Delete", "DeleteAll", "FindById",
			"FindByName", "ListAll", "ListAllByPage","ListActive", "SearchByName", "SearchByFields", "SearchByFieldsByPage",
			"SoftDelete", "SoftDeleteAll", "Toggle", "ToggleOne", "Update", "Export", "ExportPDF", "FilterExcel",
			"FilterPDF","View","ExportWord","FilterWord","ExportPPT","FilterPPT"};
	
	public static String [] getOneDomainVerbs() {
		return oneDomainVerbs;
	}

	public static TreeSet<String> parseVerbDenyString(String domainName, String verbsDenyStr) throws ValidateException {
		TreeSet<String> allverbs = new TreeSet<String>();
		allverbs.addAll(Arrays.asList(oneDomainVerbs));
		TreeSet<String> results = new TreeSet<String>();
		ValidateInfo info = new ValidateInfo();
		if (verbsDenyStr.equalsIgnoreCase("none")||"".equals(verbsDenyStr)) {
			return results;
		} else if (verbsDenyStr.equalsIgnoreCase("max")||verbsDenyStr.equalsIgnoreCase("max+")) {
			results.addAll(allverbs);
			results.remove("ListActive");
			results.remove("ListAll");
			results.remove("FindById");
			results.remove("FindByName");
			results.remove("SearchByFieldsByPage");
			results.remove("SearchByFields");
			return results;
		} else {
			String[] tokens = verbsDenyStr.split(",");
			if (tokens!=null&&tokens.length>0&&tokens[0].equalsIgnoreCase("max+")&&tokens.length>=2){
				results.addAll(allverbs);
				results.remove("ListActive");
				results.remove("ListAll");
				results.remove("FindById");
				results.remove("FindByName");
				results.remove("SearchByFieldsByPage");
				results.remove("SearchByFields");
				for (int i=1;i<tokens.length;i++) {
					if (!StringUtil.isBlank(tokens[i])) {
						String token = StringUtil.capFirst(tokens[i]);
						if (!allverbs.contains(token)) {
							info.addCompileError("域对象" + domainName + "中动词名" +token+ "不正确！");
						}
						else {
							results.remove(token);
						}
					}
				}
			}else {
				for (String token : tokens) {
					if (!StringUtil.isBlank(token)) {
						if (!allverbs.contains(token)) {
							info.addCompileError("域对象" + domainName + "中动词名" +token+ "不正确！");
						}else {
							results.add(StringUtil.capFirst(token));
						}
					}
				}
			}
					
			if (results.contains("SearchByFieldsByPage")) {
				info.addCompileError("域对象" + domainName + "中动词SearchByFieldsByPage不能否定。");
			}
			if (results.contains("ListActive")) {
				info.addCompileError("域对象" + domainName + "中动词ListActive不能否定。");
			}
			if (results.contains("FindById")) {
				info.addCompileError("域对象" + domainName + "中动词FindById不能否定。");
			}
			if (results.contains("FindByName")) {
				info.addCompileError("域对象" + domainName + "中动词FindByName不能否定。");
			}
			if (results.contains("Activate") && !results.contains("ActivateAll")) {
				info.addCompileError("域对象" + domainName + "中动词ActivateAll依赖Activate。");
			}
			if (results.contains("Add") && !results.contains("Clone")) {
				info.addCompileError("域对象" + domainName + "中动词Clone依赖Add。");
			}
			if (results.contains("Add") && !results.contains("CloneAll")) {
				info.addCompileError("域对象" + domainName + "中动词CloneAll依赖Add。");
			}
			if (results.contains("Clone") && !results.contains("CloneAll")) {
				info.addCompileError("域对象" + domainName + "中动词CloneAll依赖Clone。");
			}
			if (results.contains("Delete") && !results.contains("DeleteAll")) {
				info.addCompileError("域对象" + domainName + "中动词DeleteAll依赖Delete。");
			}
			if (results.contains("FindById") && !results.contains("Clone")) {
				info.addCompileError("域对象" + domainName + "中动词Clone依赖FindById。");
			}
			if (results.contains("FindById") && !results.contains("CloneAll")) {
				info.addCompileError("域对象" + domainName + "中动词CloneAll依赖FindById。");
			}
			if (results.contains("ListAll") && !results.contains("Export")) {
				info.addCompileError("域对象" + domainName + "中动词Export依赖ListAll。");
			}
			if (results.contains("ListAll") && !results.contains("ExportPDF")) {
				info.addCompileError("域对象" + domainName + "中动词ExportPDF依赖ListAll。");
			}
			if (results.contains("ListAll") && !results.contains("ExportWprd")) {
				info.addCompileError("域对象" + domainName + "中动词ExportWprd依赖ListAll。");
			}
			if (results.contains("ListAll") && !results.contains("ExportPPT")) {
				info.addCompileError("域对象" + domainName + "中动词ExportPPT依赖ListAll。");
			}
			if (results.contains("SearchByFields") && !results.contains("FilterExcel")) {
					info.addCompileError("域对象" + domainName + "中动词FilterExcel依赖SearchByFields。");
			}
			if (results.contains("SearchByFields") && !results.contains("FilterPDF")) {
				info.addCompileError("域对象" + domainName + "中动词FilterPDF依赖SearchByFields。");
			}
			if (results.contains("SearchByFields") && !results.contains("FilterWord")) {
				info.addCompileError("域对象" + domainName + "中动词FilterWord依赖SearchByFields。");
			}
			if (results.contains("SearchByFields") && !results.contains("FilterPPT")) {
				info.addCompileError("域对象" + domainName + "中动词FilterPPT依赖SearchByFields。");
			}
			if (results.contains("SoftDelete") && !results.contains("SoftDeleteAll")) {
				info.addCompileError("域对象" + domainName + "中动词SoftDeleteAll依赖SoftDelete。");
			}
			if (results.contains("Toggle") && !results.contains("ToggleOne")) {
				info.addCompileError("域对象" + domainName + "中动词ToggleOne依赖Toggle。");
			}
			if (info != null && info.getCompileErrors().size() > 0) {
				throw new ValidateException(info);
			} else {
				return results;
			}
		}
	}
	
	public static TreeSet<String> parseVerbFieldDenyString(String domainName,String verbsDenyStr,String fieldsDemyStr) throws ValidateException{
		TreeSet<String> allverbs = new TreeSet<String>();
		allverbs.addAll(Arrays.asList(oneDomainVerbs));
		TreeSet<String> denyedFields = new TreeSet<String>();
		denyedFields.addAll(Arrays.asList(fieldsDemyStr.split(",")));
		TreeSet<String> results = parseVerbDenyString(domainName,verbsDenyStr);
		if (denyedFields.contains("activefield")) {
			results.add("SoftDelete");
			results.add("SoftDeleteAll");
			results.add("Activate");
			results.add("ActivateAll");
			results.add("Toggle");
			results.add("ToggleOne");	
			results.add("Add");
			results.add("Update");
			results.add("Clone");
			results.add("CloneAll");
		}
		if (denyedFields.contains("domainname")) {
			results.add("FindByName");
			results.add("SearchByName");
		}
		if (denyedFields.contains("domainid")) {						
			allverbs.remove("SearchByFields");
			allverbs.remove("Export");
			allverbs.remove("ExportPDF");
			allverbs.remove("ExportWord");
			allverbs.remove("ExportPPT");
			allverbs.remove("FilterExcel");
			allverbs.remove("FilterPDF");
			allverbs.remove("FilterWord");
			allverbs.remove("FilterPPT");
			allverbs.remove("SearchByFieldsByPage");
			allverbs.remove("ListAll");
			allverbs.addAll(results);
			results=allverbs;
		}
		return results;
	}

	public static TreeSet<String> parseVerbFieldDenyFieldReadonlyString(String domainName,String verbsDenyStr,String fieldsDemyStr,String fieldsReadonlyStr) throws ValidateException{
		TreeSet<String> allverbs = new TreeSet<String>();
		allverbs.addAll(Arrays.asList(oneDomainVerbs));
		TreeSet<String> results = parseVerbFieldDenyString(domainName,verbsDenyStr,fieldsDemyStr);
		TreeSet<String> readonlyFields = new TreeSet<String>();
		readonlyFields.addAll(Arrays.asList(fieldsReadonlyStr.split(",")));
		if (readonlyFields.contains("domainid")) {
			results.remove("SearchByFieldsByPage");
			results.add("Add");
			results.add("Clone");
			results.add("CloneAll");
			results.add("Delete");
			results.add("DeleteAll");
		}else if (readonlyFields.contains("activefield")) {
			results.add("SoftDelete");
			results.add("SoftDeleteAll");
			results.add("Activate");
			results.add("ActivateAll");
			results.add("Toggle");
			results.add("ToggleOne");
		}
		return results;
	}
	
	public static boolean isSameDenyString(String denyString1,String denyString2) throws ValidateException{
		if (denyString1.equals(denyString2)) return true;
		else {
			Set<String> denies1 = parseVerbDenyString("domain1",denyString1);
			Set<String> denies2 = parseVerbDenyString("domain2",denyString2);
			for (String str:denies1) {
				System.out.print(str+",");
			}
			System.out.println("");
			for (String str:denies2) {
				System.out.print(str+",");
			}
			for (String verb:denies1) {
				if (!denies2.contains(verb)) return false;
			}
			for (String verb2:denies2) {
				if (!denies1.contains(verb2)) return false;
			}
			return true;
		}
	}
}