package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToManyCandidate;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.8
 *
 */
public class UpdateMtmCandidateByIndexFacade extends HttpServlet {
	private static final long serialVersionUID = -7184024582821774018L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateMtmCandidateByIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int index = -1;
		Map<String, Object> result = new TreeMap<String, Object>();
		if (request.getParameter("index")!=null) index = Integer.valueOf(StringUtil.nullTrim(request.getParameter("index")));		
		String masterName = StringUtil.nullTrim(request.getParameter("masterName"));
		String slaveName = StringUtil.nullTrim(request.getParameter("slaveName"));
		String slaveAlias = StringUtil.nullTrim(request.getParameter("slaveAlias"));
		String label = StringUtil.nullTrim(request.getParameter("label"));
		try {	
			ManyToManyCandidate mtmc = new ManyToManyCandidate();
			boolean isNew = false;
			if (!StringUtil.isBlank(masterName)||!StringUtil.isBlank(slaveName)||!StringUtil.isBlank(slaveAlias)||!StringUtil.isBlank(label)) isNew = true;
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>index) {
				mtmc = ExcelWizardFacade.getInstance().getMtmCandidates().get(index);
				isNew = false;
			}				
			mtmc.setMasterName(masterName);
			mtmc.setSlaveAlias(slaveAlias);
			mtmc.setLabel(label);
			List<Domain> projectDomains = ExcelWizardFacade.getInstance().getDomains();
			Domain master = DomainUtil.findDomainInList(projectDomains, masterName);
			Domain slave = DomainUtil.findDomainInList(projectDomains, slaveName);
			mtmc.setMaster(master);
			mtmc.setSlave(slave);
			if (mtmc.getMasterId() == null) {
				Field masterId = new Field();
				masterId.setFieldType("i64");
				masterId.setFieldName(master.getLowerFirstDomainName()+"Id");
				mtmc.setMasterId(masterId);
			}
			if (mtmc.getSlaveId() == null) {
				Field slaveId = new Field();
				slaveId.setFieldType("i64");
				slaveId.setFieldName(master.getLowerFirstDomainName()+"Id");
				if (!StringUtil.isBlank(slaveAlias)) slaveId.setFieldName(StringUtil.lowerFirst(slaveAlias)+"Id");
				mtmc.setSlaveId(slaveId);
			}
			if (!isNew) {
				ExcelWizardFacade.getInstance().getMtmCandidates().set(index, mtmc);
				for (ManyToManyCandidate dmtmc: ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(index)) {
					dmtmc.setMasterName(mtmc.getMasterName());
					dmtmc.setSlaveAlias(mtmc.getSlaveAlias());
					dmtmc.setLabel(mtmc.getLabel());
					dmtmc.setMaster(mtmc.getMaster());
					dmtmc.setSlave(mtmc.getSlave());
					if (dmtmc.getMasterId() == null) {
						dmtmc.setMasterId(mtmc.getMasterId());
					}
					if (dmtmc.getSlaveId() == null) {
						dmtmc.setSlaveId(mtmc.getSlaveId());
					}
				}
			}
			else ExcelWizardFacade.getInstance().getMtmCandidates().add(mtmc);			
			
			result.put("success", true);
			result.put("data", null);			
		} catch(Exception e) {
			e.printStackTrace();			
			result.put("success", false);
			result.put("data", null);
		}
		out.print(JSONObject.fromObject(result));
	}
}
