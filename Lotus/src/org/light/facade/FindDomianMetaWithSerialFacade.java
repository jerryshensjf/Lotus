package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2017/11/25
 *
 */
public class FindDomianMetaWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = 8350069920208925062L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindDomianMetaWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = 0;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		
		Domain domain0 = null;
		if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
			domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
			List<Map<String,String>> fieldNames = new ArrayList<>();
			Map<String,String> data = new TreeMap<>();
			data.put("字段","字段");
			for (Field f: domain0.getFields()) {			
				data.put(f.getFieldName(),f.getFieldName());
			}
			for (ManyToMany mtm:domain0.getManyToManies()) {
				data.put(mtm.getStandardName(),mtm.getStandardName());
			}
			fieldNames.add(data);
			data = new TreeMap<>();
			data.put("字段","字段类型");
			for (Field f: domain0.getFields()) {
				if (f instanceof Dropdown) data.put(f.getFieldName(),((Dropdown)f).getTargetName());
				else data.put(f.getFieldName(),f.getFieldType());
			}
			for (ManyToMany mtm:domain0.getManyToManies()) {
				data.put(mtm.getStandardName(),mtm.getManyToManySalveName());
			}
			fieldNames.add(data);
			data = new TreeMap<>();
			data.put("字段","长度");
			for (Field f: domain0.getFields()) {			
				data.put(f.getFieldName(),f.getLengthStr());
			}
			for (ManyToMany mtm:domain0.getManyToManies()) {
				data.put(mtm.getStandardName(),"");
			}
			fieldNames.add(data);
			data = new TreeMap<>();
			data.put("字段","字段标签");
			for (Field f: domain0.getFields()) {			
				data.put(f.getFieldName(),f.getLabel());
			}
			for (ManyToMany mtm:domain0.getManyToManies()) {
				data.put(mtm.getStandardName(),mtm.getSlaveAliasLabel());
			}
			fieldNames.add(data);
			
			if (DomainUtil.expendFixedName(domain0)) {
				data = new TreeMap<>();
				data.put("字段","固定名字");
				for (Field f: domain0.getFields()) {			
					if (!StringUtil.isBlank(f.getFixedName())&&!f.getFixedName().equals(f.getFieldName())){
						data.put(f.getFieldName(), f.getFixedName());
					}else {
						data.put(f.getFieldName(), "");
					}
				}
				fieldNames.add(data);
			}

			try {
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", fieldNames);
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}	
	}
}
