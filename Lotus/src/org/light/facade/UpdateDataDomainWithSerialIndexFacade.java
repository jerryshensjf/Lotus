package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.7
 *
 */
public class UpdateDataDomainWithSerialIndexFacade extends HttpServlet {
	private static final long serialVersionUID = -908091036578461987L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateDataDomainWithSerialIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial") != null)
			serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));
		int index = -1;
		if (request.getParameter("index") != null)
			index = Integer.valueOf(StringUtil.nullTrim(request.getParameter("index")));
		
		String domainid = StringUtil.nullTrim(request.getParameter("domainid"));
		String domainidvalue = StringUtil.nullTrim(request.getParameter("domainidvalue"));
		String domainiddeny = StringUtil.nullTrim(request.getParameter("domainiddeny"));
		String domainname = StringUtil.nullTrim(request.getParameter("domainname"));
		String domainnamevalue = StringUtil.nullTrim(request.getParameter("domainnamevalue"));
		String domainnamedeny = StringUtil.nullTrim(request.getParameter("domainnamedeny"));
		String activefield = StringUtil.nullTrim(request.getParameter("activefield"));
		String activefieldvalue = StringUtil.nullTrim(request.getParameter("activefieldvalue"));
		String activefielddeny = StringUtil.nullTrim(request.getParameter("activefielddeny"));
		
		String [] fieldname = request.getParameterValues("fieldname[]");
		String [] fieldvalue = request.getParameterValues("fieldvalue[]");
		
		
		try {
			Domain domain0 = new Domain();
			if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains()==null) {
				ExcelWizardFacade.getInstance().addDomain(domain0);
			}
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null && ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
			}
			Field fdomainid;
			if (!"true".equalsIgnoreCase(domainiddeny)) {
				fdomainid = domain0.getDomainId();
				fdomainid.setFieldName(domainid);
				fdomainid.setFieldValue(domainidvalue);
				domain0.setDomainId(fdomainid);
			}
			Field fdomainname;
			if (!"true".equalsIgnoreCase(domainnamedeny)) {
				fdomainname = domain0.getDomainName();
				fdomainname.setFieldName(domainname);
				fdomainname.setFieldValue(domainnamevalue);
				domain0.setDomainName(fdomainname);
			}
			Field factive;
			if (!"true".equalsIgnoreCase(activefielddeny)) {
				factive = domain0.getActive();
				factive.setFieldName(activefield);
				factive.setFieldValue(activefieldvalue);
				domain0.setActive(factive);
			}
			if (fieldname!=null) {
				for (int i=0;i<fieldname.length;i++) {
					for (Field f:domain0.getPlainFields()) {
						if (fieldname[i].equals(f.getFieldName())) f.setFieldValue(fieldvalue[i]);
					}
					for (ManyToMany mtm:domain0.getManyToManies()) {
						if (fieldname[i].equals(mtm.getStandardName())) mtm.setValues(fieldvalue[i]);
					}
				}
			}

			if (index >=0) ExcelWizardFacade.getInstance().setDataDomains(DomainUtil.updateDataDomainFromList(ExcelWizardFacade.getInstance().getDataDomains(),domain0,index));
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
