package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONArray;

/**
 * 
 * @author Jerry Shen 2021.2.6
 *
 */
public class FindDataDomianWithSerialIndexFacade extends HttpServlet {
	private static final long serialVersionUID = -171296491289572440L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindDataDomianWithSerialIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial") != null)
			serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));
		int index = -1;
		if (request.getParameter("index") != null)
			index = Integer.valueOf(StringUtil.nullTrim(request.getParameter("index")));

		try {
			if (ExcelWizardFacade.getInstance() != null && ExcelWizardFacade.getInstance().getDomains() != null
					&& ExcelWizardFacade.getInstance().getDomains().size() > serial) {
				Domain domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				List<Domain> dataDomains = DomainUtil.filterDataDomainList(
						ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				
				if (index >=0) domain0= dataDomains.get(index);
				List<Map<String, String>> fieldNames = new ArrayList<>();

				for (Field f : domain0.getFields()) {
					Map<String, String> data = new TreeMap<>();
					if (domain0.getDomainId() != null
							&& domain0.getDomainId().getFieldName().equals(f.getFieldName())) {
						data.put("fieldMeta", "domainid");
					} else if (domain0.getDomainName() != null
							&& domain0.getDomainName().getFieldName().equals(f.getFieldName())) {
						data.put("fieldMeta", "domainname");
					} else if (domain0.getActive() != null
							&& domain0.getActive().getFieldName().equals(f.getFieldName())) {
						data.put("fieldMeta", "activefield");
					} else if (f instanceof Dropdown) {
						data.put("fieldMeta", "dropdown");
					} else {
						data.put("fieldMeta", "field");
					}
					data.put("fieldName", f.getFieldName());
					data.put("fieldType", f.getFieldType());
					data.put("fieldLabel", f.getLabel());
					data.put("fieldLength", f.getLengthStr());
					if (!StringUtil.isBlank(f.getFixedName())&&!f.getFixedName().equals(f.getFieldName())){
						data.put("fieldFixedName", f.getFixedName());
					}else {
						data.put("fieldFixedName", "");
					}
					if (f instanceof Dropdown && ("0".equals(f.getFieldValue())||StringUtil.isNegativeInteger(f.getFieldValue()))) {
						data.put("fieldvalue", "");
					} else {
						data.put("fieldvalue", f.getFieldValue());
					}
					fieldNames.add(data);
				}
				for (ManyToMany mtm : domain0.getManyToManies()) {
					Map<String, String> data = new TreeMap<>();
					data.put("fieldMeta", "manytomany");
					data.put("fieldName", mtm.getStandardName());
					data.put("fieldType", mtm.getManyToManySalveName());
					data.put("fieldLabel", mtm.getSlaveAliasLabel());
					data.put("fieldLength", "");
					data.put("fieldFixedName", "");
					data.put("fieldvalue", mtm.getValues());
					fieldNames.add(data);
				}
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				out.print(JSONArray.fromObject(fieldNames));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
