package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.light.core.LayoutComb;
import org.light.core.ReportComb;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToManyCandidate;
import org.light.domain.Project;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.layouts.ParentChildGridLayout;
import org.light.layouts.TreeGridLayout;
import org.light.layouts.TreeParentChildLayout;
import org.light.reports.EChartsCompareGridReport;
import org.light.reports.EChartsGridReport;
import org.light.reports.EChartsReport;
import org.light.simpleauth.SimpleAuthModule;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.wizard.ExcelWizard;
import org.light.wizard.ProjectWizard;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.1
 *
 */
public class ExcelWizardFacade extends HttpServlet {
	private static final long serialVersionUID = -8679560776041236308L;
	protected static Logger logger = Logger.getLogger(ExcelWizardFacade.class);

	private static Project instance = new Project();
	
	public static void setInstance(Project project) {
		instance = project;
	}
	
	public static Project getInstance() {
		return instance;
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExcelWizardFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String command = StringUtil.nullTrim(request.getParameter("command"));
		
		if ("createNewProject".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();
			params.put("fileName", StringUtil.nullTrim(request.getParameter("fileName")));
			params.put("project", StringUtil.nullTrim(request.getParameter("project")));
			params.put("packagetoken", StringUtil.nullTrim(request.getParameter("packagetoken")));
			params.put("dbprefix", StringUtil.nullTrim(request.getParameter("dbprefix")));
			params.put("dbname", StringUtil.nullTrim(request.getParameter("dbname")));
			params.put("dbusername", StringUtil.nullTrim(request.getParameter("dbusername")));
			params.put("dbpassword", StringUtil.nullTrim(request.getParameter("dbpassword")));
			params.put("dbtype", StringUtil.nullTrim(request.getParameter("dbtype")));
			params.put("technicalstack", StringUtil.nullTrim(request.getParameter("technicalstack")));
			params.put("title", StringUtil.nullTrim(request.getParameter("title")));
			params.put("subtitle", StringUtil.nullTrim(request.getParameter("subtitle")));
			params.put("footer", StringUtil.nullTrim(request.getParameter("footer")));
			params.put("crossorigin", StringUtil.nullTrim(request.getParameter("crossorigin")));
			params.put("resolution", StringUtil.nullTrim(request.getParameter("resolution")));
			params.put("domainsuffix", StringUtil.nullTrim(request.getParameter("domainsuffix")));
			params.put("daosuffix", StringUtil.nullTrim(request.getParameter("daosuffix")));
			params.put("daoimplsuffix", StringUtil.nullTrim(request.getParameter("daoimplsuffix")));
			params.put("servicesuffix", StringUtil.nullTrim(request.getParameter("servicesuffix")));
			params.put("serviceimplsuffix", StringUtil.nullTrim(request.getParameter("serviceimplsuffix")));
			params.put("controllersuffix", StringUtil.nullTrim(request.getParameter("controllersuffix")));
			params.put("domainnamingsuffix", StringUtil.nullTrim(request.getParameter("domainnamingsuffix")));
			params.put("controllernamingsuffix", StringUtil.nullTrim(request.getParameter("controllernamingsuffix")));
			params.put("language", StringUtil.nullTrim(request.getParameter("language")));
			params.put("schema", StringUtil.nullTrim(request.getParameter("schema")));
			params.put("frontbaseapi", StringUtil.nullTrim(request.getParameter("frontbaseapi")));
			params.put("frontendUi", StringUtil.nullTrim(request.getParameter("frontendUi")));
			params.put("backendUi", StringUtil.nullTrim(request.getParameter("backendUi")));
			params.put("computerlanguage", StringUtil.nullTrim(request.getParameter("computerlanguage")));

			ExcelWizardFacade.setInstance(ProjectWizard.createNewProject(params));	
			
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("mode", "createNewProject");
			out.print(JSONObject.fromObject(result));
		}
		
		if ("updateProject".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();
			params.put("fileName", StringUtil.nullTrim(request.getParameter("fileName")));
			params.put("project", StringUtil.nullTrim(request.getParameter("project")));
			params.put("packagetoken", StringUtil.nullTrim(request.getParameter("packagetoken")));
			params.put("dbprefix", StringUtil.nullTrim(request.getParameter("dbprefix")));
			params.put("dbname", StringUtil.nullTrim(request.getParameter("dbname")));
			params.put("dbusername", StringUtil.nullTrim(request.getParameter("dbusername")));
			params.put("dbpassword", StringUtil.nullTrim(request.getParameter("dbpassword")));
			params.put("dbtype", StringUtil.nullTrim(request.getParameter("dbtype")));
			params.put("technicalstack", StringUtil.nullTrim(request.getParameter("technicalstack")));
			params.put("title", StringUtil.nullTrim(request.getParameter("title")));
			params.put("subtitle", StringUtil.nullTrim(request.getParameter("subtitle")));
			params.put("footer", StringUtil.nullTrim(request.getParameter("footer")));
			params.put("crossorigin", StringUtil.nullTrim(request.getParameter("crossorigin")));
			params.put("resolution", StringUtil.nullTrim(request.getParameter("resolution")));
			params.put("domainsuffix", StringUtil.nullTrim(request.getParameter("domainsuffix")));
			params.put("daosuffix", StringUtil.nullTrim(request.getParameter("daosuffix")));
			params.put("daoimplsuffix", StringUtil.nullTrim(request.getParameter("daoimplsuffix")));
			params.put("servicesuffix", StringUtil.nullTrim(request.getParameter("servicesuffix")));
			params.put("serviceimplsuffix", StringUtil.nullTrim(request.getParameter("serviceimplsuffix")));
			params.put("controllersuffix", StringUtil.nullTrim(request.getParameter("controllersuffix")));
			params.put("domainnamingsuffix", StringUtil.nullTrim(request.getParameter("domainnamingsuffix")));
			params.put("controllernamingsuffix", StringUtil.nullTrim(request.getParameter("controllernamingsuffix")));
			params.put("language", StringUtil.nullTrim(request.getParameter("language")));
			params.put("schema", StringUtil.nullTrim(request.getParameter("schema")));
			params.put("frontbaseapi", StringUtil.nullTrim(request.getParameter("frontbaseapi")));
			params.put("frontendUi", StringUtil.nullTrim(request.getParameter("frontendUi")));
			params.put("backendUi", StringUtil.nullTrim(request.getParameter("backendUi")));
			params.put("computerlanguage", StringUtil.nullTrim(request.getParameter("computerlanguage")));

			ExcelWizardFacade.setInstance(ProjectWizard.updateProject(ExcelWizardFacade.getInstance(),params));	
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("mode", "updateProject");
			out.print(JSONObject.fromObject(result));
		}
		
		if ("createNewDomain".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("domain", StringUtil.nullTrim(request.getParameter("domain")));
			params.put("plural", StringUtil.nullTrim(request.getParameter("plural")));
			params.put("tableprefix", StringUtil.nullTrim(request.getParameter("tableprefix")));
			params.put("domainlabel", StringUtil.nullTrim(request.getParameter("domainlabel")));
			params.put("verbdenies", StringUtil.nullTrim(request.getParameter("verbdenies")));
			
			Domain domain = ProjectWizard.createNewDomain(params);
			try {
				ExcelWizardFacade.getInstance().addDomain(domain);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createNewDomain");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("createNewEnum".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("domain", StringUtil.nullTrim(request.getParameter("domain")));
			params.put("plural", StringUtil.nullTrim(request.getParameter("plural")));
			params.put("tableprefix", StringUtil.nullTrim(request.getParameter("tableprefix")));
			params.put("domainlabel", StringUtil.nullTrim(request.getParameter("domainlabel")));
			params.put("verbdenies", StringUtil.nullTrim(request.getParameter("verbdenies")));
			
			org.light.domain.Enum myenum = ProjectWizard.createNewEnum(params);
			try {
				ExcelWizardFacade.getInstance().addDomain(myenum);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createNewEnum");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		if ("createNewMtmCandidate".equals(command)){
			ManyToManyCandidate mtmc = new ManyToManyCandidate();
			try {
				List<ManyToManyCandidate> list = new ArrayList<>();
				ExcelWizardFacade.getInstance().addMtmCandidate(mtmc);
				ExcelWizardFacade.getInstance().getMtmCandidatesValues().add(list);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createNewMtmCandidate");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateDomain".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("domain", StringUtil.nullTrim(request.getParameter("domain")));
			params.put("plural", StringUtil.nullTrim(request.getParameter("plural")));
			params.put("tableprefix", StringUtil.nullTrim(request.getParameter("tableprefix")));
			params.put("domainlabel", StringUtil.nullTrim(request.getParameter("domainlabel")));
			params.put("verbdenies", StringUtil.nullTrim(request.getParameter("verbdenies")));
			
			int serial = 0;
			Domain domain = null;
			try {
				if (!StringUtil.isBlank(request.getParameter("serial"))) serial = Integer.valueOf(request.getParameter("serial"));
				if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains().size()<=serial){
					ExcelWizardFacade.getInstance().addDomain(ProjectWizard.createNewDomain(params));
				}
				if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
					domain = ExcelWizardFacade.getInstance().getDomains().get(serial);
					String oldDomainName = domain.getStandardName();	
					domain = ProjectWizard.updateDomain(domain,params);
					ExcelWizardFacade.getInstance().updateDomain(serial, domain);
					for (List<Domain> dataDomains: ExcelWizardFacade.getInstance().getDataDomains()){
						for (Domain d: dataDomains){
							if (d.getStandardName().equals(oldDomainName)){
								d.setStandardName(params.get("domain"));
								d.setPlural(params.get("plural"));
								d.setTablePrefix(params.get("tableprefix"));
								d.setLabel(params.get("domainlabel"));
								d.setVerbDeniesStr(params.get("verbdenies"));
							}
						}
					}
				}			
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateDomain");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("downloadExcel".equals(command)){
			String root = request.getSession().getServletContext().getRealPath("/").replace('\\', '/');
			String genExcelFile = ExcelWizardFacade.getInstance().getExcelTemplateName();
			String ignoreWarning = request.getParameter("ignoreWarning");
			if (StringUtil.isBlank(genExcelFile)) genExcelFile="gen.xls";
			try {
				logger.debug("outputdatadomains:"+ExcelWizardFacade.getInstance().getDataDomains().size());
				ExcelWizard.outputExcelWorkBook(ExcelWizardFacade.getInstance(), (root+ "source/").replace("\\", "/"),genExcelFile);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "downloadExcel");
				result.put("fileName",genExcelFile);
				out.print(JSONObject.fromObject(result));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("createNewModule".equals(command)){
			try {			
				org.light.core.Module simpleAuth = new SimpleAuthModule();
				ExcelWizardFacade.getInstance().removeAllModules();
				ExcelWizardFacade.getInstance().addModule(simpleAuth);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createNewModule");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateSimpleAuthModule".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();

			params.put("module", "SimpleAuth");
			params.put("userdomain", StringUtil.nullTrim(request.getParameter("userdomain")));
			params.put("roledomain", StringUtil.nullTrim(request.getParameter("roledomain")));
			params.put("privilegedomain", StringUtil.nullTrim(request.getParameter("privilegedomain")));
			try {
				org.light.core.Module simpleAuth = ProjectWizard.createNewSimpleAuthModule(ExcelWizardFacade.getInstance(),params);
				ExcelWizardFacade.getInstance().removeAllModules();
				ExcelWizardFacade.getInstance().addModule(simpleAuth);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateSimpleAuthModule");
				out.print(JSONObject.fromObject(result));
			}  catch(ValidateException ve) {
				Map<String, Object> result = new TreeMap<String, Object>();
				ValidateInfo info = ve.getValidateInfo();
				result.put("success", false);
				result.put("errors", info.getCompileErrors());
				result.put("warnings", info.getCompileWarnings());
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("deleteSimpleAuthModule".equals(command)){
			Map<String,String> params = new TreeMap<String,String>();
			try {
				ExcelWizardFacade.getInstance().removeAllModules();
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "deleteSimpleAuthModule");
				out.print(JSONObject.fromObject(result));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("clearLoginDataSimpleAuthModule".equals(command)){
			try {
				ExcelWizardFacade.getInstance().clearLoginDataSimpleAuthModule();
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "clearLoginDataSimpleAuthModule");
				out.print(JSONObject.fromObject(result));
			} catch(ValidateException ve) {
				ve.printStackTrace();
				ValidateInfo info = ve.getValidateInfo();
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", false);
				if (info.getCompileErrors().size()>0) result.put("errors", info.getCompileErrors());
				if (info.getCompileWarnings().size()>0) result.put("warnings", info.getCompileWarnings());
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
				ValidateInfo info = new ValidateInfo();
				info.addCompileError("清洗登录数据失败！");
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", false);
				result.put("errors", info.getCompileErrors());
				out.print(JSONObject.fromObject(result));
			}
		}
		
		if ("createParentChildLayout".equals(command)){
			try {			
				LayoutComb parentChild = new ParentChildGridLayout();
				ExcelWizardFacade.getInstance().addLayoutComb(parentChild);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createParentChildLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateParentChildLayout".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();				
				Domain parentDomain = null;
				Domain childDomain = null;
				try {
					parentDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("parentdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				try {
					childDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("childdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				String parentidfieldName = StringUtil.nullTrim(request.getParameter("parentidfield"));
				if (StringUtil.isBlank(parentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未设置。");
				else if (info.success(false)) {
					Field f = childDomain.findFieldByFieldName(parentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(parentDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象不是父域对象。");
					}
				}
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				ParentChildGridLayout parentChild = new ParentChildGridLayout(parentDomain,childDomain,parentidfieldName,ExcelWizardFacade.getInstance().getDbType());
				ExcelWizardFacade.getInstance().setLayoutComb(index,parentChild);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateParentChildLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateTreeGridLayout".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();
				Domain treeDomain = null;
				Domain childDomain = null;
				try {
					treeDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("treedomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				try {
					childDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("childdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				String innertreeparentidfieldName = StringUtil.nullTrim(request.getParameter("innertreeparentidfield"));
				if (StringUtil.isBlank(innertreeparentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield字段未设置。");
				else if (info.success(false)) {
					Field f = treeDomain.findFieldByFieldName(innertreeparentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(treeDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield下拉列表对象不是树域对象。");
					}
				}
				
				String parentidfieldName = StringUtil.nullTrim(request.getParameter("parentidfield"));
				if (StringUtil.isBlank(parentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未设置。");
				else if (info.success(false)) {
					Field f = childDomain.findFieldByFieldName(parentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(treeDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象不是树域对象。");
					}
				}
				
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				TreeGridLayout layout = new TreeGridLayout(treeDomain,childDomain,innertreeparentidfieldName,parentidfieldName,ExcelWizardFacade.getInstance().getDbType());
				ExcelWizardFacade.getInstance().setLayoutComb(index,layout);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateTreeGridLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateTreeParentChildLayout".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();
				Domain treeDomain = null;
				Domain parentDomain = null;
				Domain childDomain = null;
				try {
					treeDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("treedomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				try {
					parentDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("parentdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				try {
					childDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("childdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}
				String innertreeparentidfieldName = StringUtil.nullTrim(request.getParameter("innertreeparentidfield"));
				if (StringUtil.isBlank(innertreeparentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield字段未设置。");
				else if (info.success(false)) {
					Field f = treeDomain.findFieldByFieldName(innertreeparentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(treeDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"innertreeparentidfield下拉列表对象不是树域对象。");
					}
				}
				
				String treeparentidfieldName = StringUtil.nullTrim(request.getParameter("treeparentidfield"));
				if (StringUtil.isBlank(treeparentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"treeparentidfieldName字段未设置。");
				else if (info.success(false)) {
					Field f = parentDomain.findFieldByFieldName(treeparentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"treeparentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"treeparentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(treeDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"treeparentidfield下拉列表对象不是树域对象。");
					}
				}
				
				String parentidfieldName = StringUtil.nullTrim(request.getParameter("parentidfield"));
				if (StringUtil.isBlank(parentidfieldName)) info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未设置。");
				else if (info.success(false)) {
					Field f = childDomain.findFieldByFieldName(parentidfieldName);				
					if (f==null)info.addCompileError("复杂版面"+(index+1)+"parentidfield字段未找到。");
					else if (f instanceof Dropdown){
						Dropdown dp= (Dropdown)f;
						if (dp.getTarget()==null) info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象未找到。"); 
						else if (!dp.getTarget().getStandardName().equals(parentDomain.getStandardName()))
							info.addCompileError("复杂版面"+(index+1)+"parentidfield下拉列表对象不是父域对象。");
					}
				}
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				TreeParentChildLayout layout = new TreeParentChildLayout(treeDomain,parentDomain,childDomain,innertreeparentidfieldName,treeparentidfieldName,parentidfieldName,ExcelWizardFacade.getInstance().getDbType());
				ExcelWizardFacade.getInstance().setLayoutComb(index,layout);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateTreeParentChildLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("deleteLayout".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ExcelWizardFacade.getInstance().getLayoutCombs().remove(index);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "deleteLayout");
				out.print(JSONObject.fromObject(result));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateParentChildLayout".equals(command)){
			try {		
				int index = Integer.parseInt(request.getParameter("index"));
				LayoutComb parentChild = new ParentChildGridLayout();
				ExcelWizardFacade.getInstance().setLayoutComb(index,parentChild);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateParentChildLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateTreeGridLayout".equals(command)){
			try {		
				int index = Integer.parseInt(request.getParameter("index"));
				LayoutComb treeGrid = new TreeGridLayout();
				ExcelWizardFacade.getInstance().setLayoutComb(index,treeGrid);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateTreeGridLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateTreeParentChildLayout".equals(command)){
			try {	
				int index = Integer.parseInt(request.getParameter("index"));
				LayoutComb treeParentChild = new TreeParentChildLayout();
				ExcelWizardFacade.getInstance().setLayoutComb(index,treeParentChild);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateTreeParentChildLayout");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("createEchartsReport".equals(command)){
			try {			
				ReportComb report = new EChartsReport();
				ExcelWizardFacade.getInstance().addReportComb(report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "createEchartsReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("deleteReport".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ExcelWizardFacade.getInstance().getReportCombs().remove(index);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "deleteReport");
				out.print(JSONObject.fromObject(result));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateEchartsReport".equals(command)){
			try {	
				int index = Integer.parseInt(request.getParameter("index"));
				ReportComb report = new EChartsReport();
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateEchartsReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateEchartsGridReport".equals(command)){
			try {	
				int index = Integer.parseInt(request.getParameter("index"));
				ReportComb report = new EChartsGridReport();
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateEchartsGridReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("recreateEchartsCompareGridReport".equals(command)){
			try {	
				int index = Integer.parseInt(request.getParameter("index"));
				ReportComb report = new EChartsCompareGridReport();
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "recreateEchartsCompareGridReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateEchartsReport".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();
				EChartsReport report = new EChartsReport();
				Domain reportDomain = null;
				Field yname = null;
				List<Field> xaxisfields = new ArrayList<>();
				try {
					reportDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("reportdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}

				String ynameName = StringUtil.nullTrim(request.getParameter("yname"));
				if (StringUtil.isBlank(ynameName)) {
					info.addCompileError("报表"+index+"字段yname未设置。");
				}else {
					yname = reportDomain.findFieldByFieldName(ynameName);
					if (yname==null) info.addCompileError("报表"+index+"字段"+ynameName+"未找到。");
				}

				if (info.success(false)) {
					String xaxisfieldsNames = StringUtil.nullTrim(request.getParameter("xaxisfields"));
					if (StringUtil.isBlank(xaxisfieldsNames)) {
						info.addCompileError("报表"+index+"字段xaxisfields未设置。");
					}else {					
						String [] xaxisfieldsNamesArr = xaxisfieldsNames.split(",");
						for (String str:xaxisfieldsNamesArr) {
							Field f = reportDomain.findFieldByFieldName(str);
							if (f==null) info.addCompileError("报表"+index+"字段"+str+"未找到。");
							else xaxisfields.add(f);
						}
					}
				}
				
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				
				report.setReportDomain(reportDomain);
				report.setxAxisFields(xaxisfields);
				report.setyName(yname);
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateEchartsReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateEchartsGridReport".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();
				EChartsGridReport report = new EChartsGridReport();
				Domain reportDomain = null;
				Field yname = null;
				List<Field> xaxisfields = new ArrayList<>();
				try {
					reportDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("reportdomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}

				if (info.success(false)) {
					String ynameName = StringUtil.nullTrim(request.getParameter("yname"));
					if (StringUtil.isBlank(ynameName)) {
						info.addCompileError("报表"+(index+1)+"字段yname未设置。");
					}else {					
						yname = reportDomain.findFieldByFieldName(ynameName);
						if (yname==null) info.addCompileError("报表"+(index+1)+"字段"+ynameName+"未找到。");
					}
	
					String xaxisfieldsNames = StringUtil.nullTrim(request.getParameter("xaxisfields"));
	
					if (StringUtil.isBlank(xaxisfieldsNames)) {
						info.addCompileError("报表"+(index+1)+"字段xaxisfields未设置。");
					}else {
						String [] xaxisfieldsNamesArr = xaxisfieldsNames.split(",");
						for (String str:xaxisfieldsNamesArr) {
							Field f = reportDomain.findFieldByFieldName(str);
							if (f==null) info.addCompileError("报表"+(index+1)+"字段"+str+"未找到。");
							else xaxisfields.add(f);
						}
					}
				}
				
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				
				report.setReportDomain(reportDomain);
				report.setxAxisFields(xaxisfields);
				report.setyName(yname);
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateEchartsGridReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("updateEchartsCompareGridReport".equals(command)){
			try {
				int index = Integer.parseInt(request.getParameter("index"));
				ValidateInfo info = new ValidateInfo();
				EChartsCompareGridReport report = new EChartsCompareGridReport();
				Domain planDomain = null;
				Field planyname = null;
				List<Field> planxaxisfields = new ArrayList<>();
				
				Domain actionDomain = null;
				Field actionyname = null;
				List<Field> actionxaxisfields = new ArrayList<>();
				try {
					planDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("plandomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}

				if (info.success(false)) {
					String planynameName = StringUtil.nullTrim(request.getParameter("planyname"));
					if (StringUtil.isBlank(planynameName)) {
						info.addCompileError("报表"+(index+1)+"字段planyname未设置。");
					}else {
						planyname = planDomain.findFieldByFieldName(planynameName);
						if (planyname==null) info.addCompileError("报表"+(index+1)+"字段"+planynameName+"未找到。");
					}
	
					String planxaxisfieldsNames = StringUtil.nullTrim(request.getParameter("planxaxisfields"));
	
					if (StringUtil.isBlank(planxaxisfieldsNames)) {
						info.addCompileError("报表"+(index+1)+"字段planxaxisfields未设置。");
					}else {
						if (info.success(false)) {
							String [] planxaxisfieldsNamesArr = planxaxisfieldsNames.split(",");
							for (String str:planxaxisfieldsNamesArr) {
								Field f = planDomain.findFieldByFieldName(str);
								if (f==null) info.addCompileError("报表"+(index+1)+"字段"+str+"未找到。");
								else planxaxisfields.add(f);
							}
						}
					}
				}
				
				try {
					actionDomain = DomainUtil.findDomainInList(ExcelWizardFacade.getInstance().getDomains(),StringUtil.nullTrim(request.getParameter("actiondomain")));
				}catch (ValidateException ve) {
					info = info.mergeValidateInfo(ve.getValidateInfo());
				}

				String actionynameName = StringUtil.nullTrim(request.getParameter("actionyname"));
				if (StringUtil.isBlank(actionynameName)) {
					info.addCompileError("报表"+(index+1)+"字段actionyname未设置。");
				}else {
					actionyname = actionDomain.findFieldByFieldName(actionynameName);
					if (actionyname==null) info.addCompileError("报表"+(index+1)+"字段"+actionynameName+"未找到。");
				}

				if (info.success(false)) {
					String actionxaxisfieldsNames = StringUtil.nullTrim(request.getParameter("actionxaxisfields"));
	
					if (StringUtil.isBlank(actionxaxisfieldsNames)) {
						info.addCompileError("报表"+(index+1)+"字段actionxaxisfields未设置。");
					}else {
						if (info.success(false)) {
							String [] actionxaxisfieldsNamesArr = actionxaxisfieldsNames.split(",");
							for (String str:actionxaxisfieldsNamesArr) {
								Field f = actionDomain.findFieldByFieldName(str);
								if (f==null) info.addCompileError("报表"+(index+1)+"字段"+str+"未找到。");
								else actionxaxisfields.add(f);
							}
						}
					}
				}
				
				if (!info.success(false)) {
					Map<String, Object> result = new TreeMap<String, Object>();
					result.put("success", false);
					result.put("errors", info.getCompileErrors());
					result.put("warnings", info.getCompileWarnings());
					out.print(JSONObject.fromObject(result));
					return;
				}
				
				report.setPlanDomain(planDomain);
				report.setPlanxAxisFields(planxaxisfields);
				report.setPlanyName(planyname);
				report.setActionDomain(actionDomain);
				report.setActionxAxisFields(actionxaxisfields);
				report.setActionyName(actionyname);
				ExcelWizardFacade.getInstance().setReportComb(index,report);
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("mode", "updateEchartsCompareGridReport");
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
