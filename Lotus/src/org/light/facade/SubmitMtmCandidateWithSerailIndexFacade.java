package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToManyCandidate;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.8
 *
 */
public class SubmitMtmCandidateWithSerailIndexFacade extends HttpServlet {
	private static final long serialVersionUID = 4714134168953016075L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubmitMtmCandidateWithSerailIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (!StringUtil.isBlank(request.getParameter("serial"))) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		int index = -1;
		if (!StringUtil.isBlank(request.getParameter("index"))) index = Integer.valueOf(StringUtil.nullTrim(request.getParameter("index")));		
		String masterIdValue = StringUtil.nullTrim(request.getParameter("masterId"));
		String slaveIdsValue = StringUtil.nullTrim(request.getParameter("slaveIds"));
		try {				
			if (ExcelWizardFacade.getInstance()!=null && serial >= 0 && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>serial) {
				if (index<0) {
					ManyToManyCandidate mtmc = (ManyToManyCandidate)ExcelWizardFacade.getInstance().getMtmCandidates().get(serial).deepClone();
					mtmc.setMasterIdValue(masterIdValue);
					mtmc.setSlaveIdValues(slaveIdsValue);
					ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial).add(mtmc);
				} else {
					ManyToManyCandidate mtmc = ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial).get(index);
					mtmc.setMasterIdValue(masterIdValue);
					mtmc.setSlaveIdValues(slaveIdsValue);
					ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial).set(index, mtmc);
				}
				
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
				return;
			}
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		}
	}
}
