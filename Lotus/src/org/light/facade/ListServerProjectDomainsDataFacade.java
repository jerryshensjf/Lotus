package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.core.LayoutComb;
import org.light.core.ReportComb;
import org.light.domain.Domain;
import org.light.domain.ManyToManyCandidate;
import org.light.domain.Project;
import org.light.layouts.ParentChildGridLayout;
import org.light.layouts.TreeGridLayout;
import org.light.layouts.TreeParentChildLayout;
import org.light.reports.EChartsCompareGridReport;
import org.light.reports.EChartsGridReport;
import org.light.reports.EChartsReport;
import org.light.simpleauth.SimpleAuthModule;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.2
 *
 */
public class ListServerProjectDomainsDataFacade extends HttpServlet {
	private static final long serialVersionUID = -6496939187597601137L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListServerProjectDomainsDataFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			Project p = ExcelWizardFacade.getInstance();
			//if (p == null || "".equals(p.getStandardName())) throw new ValidateException("活跃项目为空，请设置或新建项目。");
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			
			result.put("fileName", p.getExcelTemplateName());
			result.put("project", p.getStandardName());
			result.put("packagetoken", p.getPackageToken());
			result.put("dbprefix", p.getDbPrefix());
			result.put("dbname", p.getDbName());
			result.put("dbusername", p.getDbUsername());
			result.put("dbpassword", p.getDbPassword());
			result.put("dbtype", p.getDbType());
			result.put("technicalstack", p.getTechnicalstack());
			result.put("title", p.getTitle());
			result.put("subtitle", p.getSubTitle());
			result.put("footer", p.getFooter());
			result.put("crossorigin", p.getCrossOrigin());
			result.put("resolution", p.getResolution());
			result.put("domainsuffix", p.getDomainSuffix());
			result.put("daosuffix", p.getDaoSuffix());
			result.put("daoimplsuffix", p.getDaoimplSuffix());
			result.put("servicesuffix", p.getServiceSuffix());
			result.put("serviceimplsuffix", p.getServiceimplSuffix());
			result.put("controllersuffix", p.getControllerSuffix());
			result.put("domainnamingsuffix", p.getDomainNamingSuffix());
			result.put("controllernamingsuffix", p.getControllerNamingSuffix());
			result.put("language", p.getLanguage());
			result.put("schema", p.getSchema());
			result.put("frontbaseapi", p.getFrontBaseApi());
			result.put("frontendUi", p.getFrontendUi());
			result.put("backendUi", p.getBackendUi());
			result.put("computerlanguage", p.getComputerLanguage());
			
			List<Domain> domains = p.getDomains();
			List<Map<String,String>> domainsData = new ArrayList<>();
			for (Domain d:domains) {
				String isEnum = d instanceof org.light.domain.Enum ? "true":"false";
				Map<String,String> data = new TreeMap<>();
				data.put("domainName", d.getStandardName());
				data.put("plural", d.getPlural());
				data.put("tableprefix", d.getTablePrefix());
				data.put("domainlabel", d.getLabel());
				data.put("verbdenies", d.getVerbDeniesStr());
				data.put("isEnum",isEnum);
				domainsData.add(data);
			}
			result.put("domains",domainsData);
			
			List<ManyToManyCandidate> mtmCandidates = p.getMtmCandidates();
			List<Map<String,String>> mtmCandidatesData = new ArrayList<>();
			for (ManyToManyCandidate mtmc:mtmCandidates) {
				Map<String,String> data = new TreeMap<>();
				data.put("masterName", mtmc.getMasterName());
				data.put("slaveAlias", mtmc.getSlaveAlias());
				if (mtmc.getSlave()!=null) data.put("slaveName", mtmc.getSlave().getStandardName());
				else data.put("slaveName", "");
				data.put("label",mtmc.getLabel());
				String masterIdName = "";
				if (mtmc.getMasterId() != null) masterIdName = mtmc.getMasterId().getLowerFirstFieldName();
				String slaveAliasIdName = "";
				if (mtmc.getSlaveId() != null) slaveAliasIdName = mtmc.getSlaveId().getLowerFirstFieldName();
				data.put("masterId", masterIdName);
				data.put("slaveAliasId", slaveAliasIdName);
				mtmCandidatesData.add(data);
			}
			result.put("mtmCandidates",mtmCandidatesData);
			
			List<List<Map<String,String>>> mtmCandidatesValuesData = new ArrayList<>();
			List<List<ManyToManyCandidate>> mtmCandidatedatas = p.getMtmCandidatesValues();
			for (List<ManyToManyCandidate> mtmcl : mtmCandidatedatas) {
				List<Map<String,String>> mtmclData = new ArrayList<>();
				for (ManyToManyCandidate mtmc:mtmcl) {
					Map<String,String> data = new TreeMap<>();
					data.put("masterId", mtmc.getMasterIdValue());
					data.put("slaveIds", mtmc.getSlaveIdValues());
					mtmclData.add(data);
				}
				mtmCandidatesValuesData.add(mtmclData);
			}
			result.put("mtmCandidatesValues",mtmCandidatesValuesData);
			
			List<org.light.core.Module> modules = p.getModules();
			List<Map<String,String>> modulesData = new ArrayList<>();
			for (org.light.core.Module m:modules) {
				Map<String,String> data = new TreeMap<>();				
				if (m instanceof SimpleAuthModule) {
					data.put("module","SimpleAuth");
					SimpleAuthModule sam = (SimpleAuthModule)m;
					if(sam.getUserDomain() != null) data.put("userdomain", sam.getUserDomain().getStandardName());
					if(sam.getRoleDomain() != null) data.put("roledomain", sam.getRoleDomain().getStandardName());
					if(sam.getPrivilegeDomain() != null) data.put("privilegedomain", sam.getPrivilegeDomain().getStandardName());
				}
				modulesData.add(data);
			}
			result.put("modules",modulesData);
			
			List<LayoutComb> layouts = p.getLayoutCombs();
			List<Map<String,String>> layoutsData = new ArrayList<>();
			for (LayoutComb lc:layouts) {
				Map<String,String> data = new TreeMap<>();				
				if (lc instanceof ParentChildGridLayout) {
					data.put("mylayout", "ParentChild");
					ParentChildGridLayout pcl = (ParentChildGridLayout)lc;
					data.put("standardName",pcl.getStandardName());
					if (pcl.getParentDomain()!=null) data.put("parentdomain", pcl.getParentDomain().getStandardName());
					if (pcl.getChildDomain()!=null) data.put("childdomain", pcl.getChildDomain().getStandardName());
					if (!StringUtil.isBlank(pcl.getParentId())) data.put("parentidfield", pcl.getParentId());
				}
				if (lc instanceof TreeGridLayout) {
					data.put("mylayout", "TreeGrid");
					TreeGridLayout tgl = (TreeGridLayout)lc;
					data.put("standardName",tgl.getStandardName());
					if (tgl.getParentTreeDomain()!=null)data.put("treedomain", tgl.getParentTreeDomain().getStandardName());
					if (tgl.getChildDomain()!=null)data.put("childdomain", tgl.getChildDomain().getStandardName());
					if (!StringUtil.isBlank(tgl.getInnerTreeParentId())) data.put("innertreeparentidfield", tgl.getInnerTreeParentId());
					if (!StringUtil.isBlank(tgl.getParentId())) data.put("parentidfield", tgl.getParentId());
				}
				if (lc instanceof TreeParentChildLayout) {
					data.put("mylayout", "TreeParentChild");
					TreeParentChildLayout tpcl = (TreeParentChildLayout)lc;
					data.put("standardName",tpcl.getStandardName());
					if (tpcl.getTreeDomain()!=null)data.put("treedomain", tpcl.getTreeDomain().getStandardName());
					if (tpcl.getParentDomain()!=null)data.put("parentdomain", tpcl.getParentDomain().getStandardName());
					if (tpcl.getChildDomain()!=null)data.put("childdomain", tpcl.getChildDomain().getStandardName());
					if (!StringUtil.isBlank(tpcl.getInnerTreeParentId())) data.put("innertreeparentidfield", tpcl.getInnerTreeParentId());
					if (!StringUtil.isBlank(tpcl.getTreeParentId())) data.put("treeparentidfield", tpcl.getTreeParentId());
					if (!StringUtil.isBlank(tpcl.getParentId())) data.put("parentidfield", tpcl.getParentId());
				}
				layoutsData.add(data);
			}
			result.put("mylayouts",layoutsData);
			
			List<ReportComb> reports = p.getReportCombs();
			List<Map<String,String>> reportsData = new ArrayList<>();
			for (ReportComb rc:reports) {
				Map<String,String> data = new TreeMap<>();
				if (rc instanceof EChartsReport) {
					data.put("report", "EchartsReport");
					EChartsReport ecr = (EChartsReport)rc;
					data.put("standardName",ecr.getStandardName());
					if(ecr.getReportDomain()!=null) data.put("reportdomain", ecr.getReportDomain().getStandardName());
					if (!StringUtil.isBlank(ecr.getxAxisFieldsNames())) data.put("xaxisfields",ecr.getxAxisFieldsNames());
					if(ecr.getyName()!=null) data.put("yname", ecr.getyName().getFieldName());
				}
				if (rc instanceof EChartsGridReport) {
					data.put("report", "EchartsGridReport");
					EChartsGridReport ecgr = (EChartsGridReport)rc;
					data.put("standardName",ecgr.getStandardName());
					if(ecgr.getReportDomain()!=null) data.put("reportdomain", ecgr.getReportDomain().getStandardName());
					if (!StringUtil.isBlank(ecgr.getxAxisFieldsNames())) data.put("xaxisfields",ecgr.getxAxisFieldsNames());
					if(ecgr.getyName()!=null) data.put("yname", ecgr.getyName().getFieldName());
				}
				if (rc instanceof EChartsCompareGridReport) {
					data.put("report", "EchartsCompareGridReport");
					EChartsCompareGridReport eccgr = (EChartsCompareGridReport)rc;
					data.put("standardName",eccgr.getStandardName());
					if(eccgr.getPlanDomain()!=null) data.put("plandomain", eccgr.getPlanDomain().getStandardName());
					if (!StringUtil.isBlank(eccgr.getPlanxAxisFieldsNames())) data.put("planxaxisfields",eccgr.getPlanxAxisFieldsNames());
					if(eccgr.getPlanyName()!=null) data.put("planyname", eccgr.getPlanyName().getFieldName());
					if(eccgr.getActionDomain()!=null) data.put("actiondomain", eccgr.getActionDomain().getStandardName());
					if (!StringUtil.isBlank(eccgr.getActionxAxisFieldsNames())) data.put("actionxaxisfields",eccgr.getActionxAxisFieldsNames());
					if(eccgr.getActionyName()!=null) data.put("actionyname", eccgr.getActionyName().getFieldName());
				}
				reportsData.add(data);
			}
			result.put("reports",reportsData);
			
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("reason", e.getMessage());
			out.print(JSONObject.fromObject(result));
		}
	
	}
}
