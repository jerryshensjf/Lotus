package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.utils.DeniableUtils;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.4.11
 *
 */
public class ValidateVerbDenyStrFacade extends HttpServlet {
	private static final long serialVersionUID = 4481920243656582501L;
	protected static Logger logger = Logger.getLogger(ValidateVerbDenyStrFacade.class);

	public ValidateVerbDenyStrFacade() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String domainName = request.getParameter("domainName");
		String verbDenyStr = request.getParameter("verbDenyStr");
		
		Map<String, Object> result = new TreeMap<String, Object>();
		try {
			DeniableUtils.parseVerbDenyString(domainName,verbDenyStr);
			result.put("success",true);
			out.print(JSONObject.fromObject(result));
		} catch (ValidateException e) {
			ValidateException ev = (ValidateException) e;
			result.put("success", false);
			ValidateInfo info = ev.getValidateInfo();
			List<String> compileErrors = info.getCompileErrors();
			result.put("compileErrors", compileErrors);
			List<String> compileWarnings = info.getCompileWarnings();
			result.put("compileWarnings", compileWarnings);
			out.print(JSONObject.fromObject(result));
		}
	}
}
