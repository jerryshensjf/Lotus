package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.4.2
 *
 */
public class SwitchToDomainWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = 693205622613988270L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SwitchToDomainWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = 0;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		try {				
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				Domain domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				if (!(domain0 instanceof org.light.domain.Enum)) throw new ValidateException("对象并非枚举！");
				org.light.domain.Enum myEnum = (org.light.domain.Enum) domain0;
				Domain myDomain = myEnum.toDomain();
				List<List<Domain>> datadomains = DomainUtil.switchDataDomainsEnumToDomainFromLists(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				ExcelWizardFacade.getInstance().getDomains().set(serial, myDomain);
				ExcelWizardFacade.getInstance().setDataDomains(datadomains);

				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch(Exception e) {
			e.printStackTrace();
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		}
	}
}
