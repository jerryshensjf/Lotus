package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.StringUtil;
import org.light.wizard.ProjectWizard;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.1.29
 *
 */
public class CreateDomianFieldsWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -598489528766854598L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateDomianFieldsWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		String domainid = StringUtil.nullTrim(request.getParameter("domainid"));
		String domainidtype = StringUtil.nullTrim(request.getParameter("domainidtype"));		
		String domainidlength = StringUtil.nullTrim(request.getParameter("domainidlength"));
		String domainidlabel = StringUtil.nullTrim(request.getParameter("domainidlabel"));
		String domainidfixedname = StringUtil.nullTrim(request.getParameter("domainidfixedname"));
		String domainiddeny = StringUtil.nullTrim(request.getParameter("domainiddeny"));
		String domainname = StringUtil.nullTrim(request.getParameter("domainname"));
		String domainnametype = StringUtil.nullTrim(request.getParameter("domainnametype"));
		String domainnamelength = StringUtil.nullTrim(request.getParameter("domainnamelength"));
		String domainnamelabel = StringUtil.nullTrim(request.getParameter("domainnamelabel"));
		String domainnamefixedname = StringUtil.nullTrim(request.getParameter("domainnamefixedname"));
		String domainnamedeny = StringUtil.nullTrim(request.getParameter("domainnamedeny"));
		String activefield = StringUtil.nullTrim(request.getParameter("activefield"));
		String activefieldtype = StringUtil.nullTrim(request.getParameter("activefieldtype"));
		String activefieldlength = StringUtil.nullTrim(request.getParameter("activefieldlength"));
		String activefieldlabel = StringUtil.nullTrim(request.getParameter("activefieldlabel"));
		String activefieldfixedname = StringUtil.nullTrim(request.getParameter("activefieldfixedname"));
		String activefielddeny = StringUtil.nullTrim(request.getParameter("activefielddeny"));
		
		String [] fieldcatlog = request.getParameterValues("fieldcatlog[]");
		String [] field = request.getParameterValues("field[]");
		String [] fieldtype = request.getParameterValues("fieldtype[]");
		String [] fieldlength = request.getParameterValues("fieldlength[]");
		String [] fieldlabel = request.getParameterValues("fieldlabel[]");
		String [] fieldfixedname = request.getParameterValues("fieldfixedname[]");
		
		
		try {
			Domain domain0 = new Domain();
			Field fdomainid;
			if ("false".equalsIgnoreCase(domainiddeny)&&!StringUtil.isBlank(domainid)) {
				fdomainid = new Field(domainid,domainidtype);
				fdomainid.setLengthStr(domainidlength);
				fdomainid.setLabel(domainidlabel);
				if (!StringUtil.isBlank(domainidfixedname)&&!domainidfixedname.equals(domainid)) {
					fdomainid.setFixedName(domainidfixedname);
				}
				fdomainid.setSerial(-300L);
				domain0.setDomainId(fdomainid);				
			}
			Field fdomainname;
			if ("false".equalsIgnoreCase(domainnamedeny)&&!StringUtil.isBlank(domainname)) {
				fdomainname = new Field(domainname,domainnametype);
				fdomainname.setLengthStr(domainnamelength);
				fdomainname.setLabel(domainnamelabel);
				if (!StringUtil.isBlank(domainnamefixedname)&&!domainnamefixedname.equals(domainname)) {
					fdomainname.setFixedName(domainnamefixedname);
				}
				fdomainname.setSerial(-200L);
				domain0.setDomainName(fdomainname);
			}
			Field factive;
			if ("false".equalsIgnoreCase(activefielddeny)&&!StringUtil.isBlank(activefield)) {
				factive = new Field(activefield,activefieldtype);
				factive.setLengthStr(activefieldlength);
				factive.setLabel(activefieldlabel);
				if (!StringUtil.isBlank(activefieldfixedname)&&!activefieldfixedname.equals(activefield)) {
					factive.setFixedName(activefieldfixedname);
				}
				factive.setSerial(-100L);
				domain0.setActive(factive);
			}
			if (fieldcatlog!=null) {
				Set<String> fieldNames = new TreeSet<>();
				for (int i=0;i<fieldcatlog.length;i++) {
					Map<String,String> params = new TreeMap<>();
					params.put("fieldcatlog",fieldcatlog[i]);
					params.put("fieldname",field[i]);
					params.put("fieldtype",fieldtype[i]);
					params.put("fieldlength",fieldlength[i]);
					params.put("fieldlabel",fieldlabel[i]);
					params.put("fieldfixedname",fieldfixedname[i]);
					params.put("serial",""+(i*100));
					if ("manytomany".equals(fieldcatlog[i])) {
						ManyToMany mtm =  ProjectWizard.createNewManyToMany(domain0, params);
						domain0.addManyToMany(mtm);
					}else {
						if (!fieldNames.contains(field[i])) {
							Field f = ProjectWizard.createNewField(params);						
							fieldNames.add(field[i]);
							domain0.addField(f);
						}						
					}
				}
			}
			if (ExcelWizardFacade.getInstance()!=null&&(ExcelWizardFacade.getInstance().getDomains()==null||ExcelWizardFacade.getInstance().getDomains().size()<=serial)) {
				ExcelWizardFacade.getInstance().addDomain(domain0);
			}
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null && ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				Domain domain1 = ExcelWizardFacade.getInstance().getDomains().get(Integer.valueOf(serial));
				domain1.setFields(new TreeSet<Field>());
				domain1.setDomainId(null);
				domain1.setDomainName(null);
				domain1.setActive(null);
				domain1.setFields(domain0.getPlainFields());
				domain1.setDomainId(domain0.getDomainId());
				domain1.setDomainName(domain0.getDomainName());
				domain1.setActive(domain0.getActive());		
				domain1.setManyToManies(domain0.getManyToManies());
			};
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
