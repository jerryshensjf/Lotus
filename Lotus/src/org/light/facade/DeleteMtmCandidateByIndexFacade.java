package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.ManyToManyCandidate;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.8
 *
 */
public class DeleteMtmCandidateByIndexFacade extends HttpServlet {
	private static final long serialVersionUID = -6922431941266890096L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteMtmCandidateByIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int index = 0;
		if (request.getParameter("index")!=null) index = Integer.valueOf(StringUtil.nullTrim(request.getParameter("index")));		
		try {				
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>index) {
				//List<List<ManyToManyCandidate>> mtmcvs = ExcelWizardFacade.getInstance().getMtmCandidatesValues();
				ExcelWizardFacade.getInstance().getMtmCandidates().remove(index);
				//mtmcvs.remove(index);

				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
