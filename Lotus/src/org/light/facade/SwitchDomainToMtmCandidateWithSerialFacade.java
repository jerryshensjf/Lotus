package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.ManyToManyCandidate;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.5.21
 *
 */
public class SwitchDomainToMtmCandidateWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = 3699405822545298170L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SwitchDomainToMtmCandidateWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		try {				
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				Domain domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				List<Domain> domains = ExcelWizardFacade.getInstance().getDomains();
				List<Domain> datadomains = DomainUtil.filterDataDomainList(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				ManyToManyCandidate mtmc0 = DomainUtil.switchDomainToManyToManyCandidate(domain0, domains);
				List<ManyToManyCandidate> vmtmcs = new ArrayList<>();
				for (Domain dd:datadomains) {
					vmtmcs.add(DomainUtil.switchDomainToManyToManyCandidate(dd,domains));
				}
				ExcelWizardFacade.getInstance().getDomains().remove(serial);
				ExcelWizardFacade.getInstance().setDataDomains(DomainUtil.removeDataDomainsFromLists(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName()));
				ExcelWizardFacade.getInstance().addMtmCandidate(mtmc0);
				ExcelWizardFacade.getInstance().addMtmCandidateValues(vmtmcs);				

				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch(ValidateException ve) {
			ve.printStackTrace();
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("compileErrors", ve.getValidateInfo().getCompileErrors());
			result.put("compileWarnings", ve.getValidateInfo().getCompileWarnings());
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
			ValidateException ve = new ValidateException("域对象转化为多对多候选错误。");
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("compileErrors", ve.getValidateInfo().getCompileErrors());
			out.print(JSONObject.fromObject(result));
		}
	}
}
