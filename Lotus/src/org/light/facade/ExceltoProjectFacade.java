package org.light.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.javaforever.gatescore.action.ParseSpreadSheetToProjectAction;
import org.javaforever.gatescore.core.FrontProject;
import org.javaforever.poitranslator.core.ProjectExcelWorkBook;
import org.light.domain.Project;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;

import com.hmkcode.MultipartRequestHandler;
import com.hmkcode.vo.FileMeta;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2017/11/25
 *
 */
public class ExceltoProjectFacade extends HttpServlet {
	protected static Logger logger = Logger.getLogger(ExceltoProjectFacade.class);
	private static final long serialVersionUID = 147513641619203107L;

	private static List<FileMeta> files = new LinkedList<FileMeta>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExceltoProjectFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected Map<String,String> generateZip(String root, String filepath, boolean ignoreWarning,boolean genFormatted,boolean genUi,boolean genController,boolean genService,boolean genServiceImpl,boolean genDao,boolean genDaoImpl,boolean sqlComments,boolean javaCompatibility,boolean goCompatibility, String exportStr) throws Exception {
		InputStream is = null;
		POIFSFileSystem fs = null;
		Map<String, Object> result = new TreeMap<String, Object>();
		// request.setCharacterEncoding("UTF-8");
		File f2 = null;
		try {
			// deal with uploaded excel sheet
			String sourcefolder = root + "source/";
			f2 = new File(filepath);

			is = new FileInputStream(f2);
			fs = new POIFSFileSystem(is);
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			ProjectExcelWorkBook pewb = new ProjectExcelWorkBook();

			Project project = pewb.translate(wb,ignoreWarning,javaCompatibility,goCompatibility);
			project.setExcelTemplateName(f2.getName());
			project.setExcelTemplateFolder(root + "spreadsheets/");
			
			project.setSqlCommentsOn(sqlComments);

			logger.debug(sourcefolder);
			project.setFolderPath(sourcefolder);
			if (!StringUtil.isBlank(project.getComputerLanguage()) && !project.getComputerLanguage().equalsIgnoreCase("rust")) {
				throw new ValidateException("Rust语言通用代码生成器莲花只支持rust语言！");
			}
			if (project.getTechnicalstack() == null || "".equalsIgnoreCase(project.getTechnicalstack())
					|| "tower".equalsIgnoreCase(project.getTechnicalstack())) {
				project.setSourceFolderPath((root + "templates/").replace('\\', '/'));
				project.generateProjectZip(ignoreWarning,genFormatted,genUi,genController,genService,genServiceImpl,genDao,genDaoImpl,exportStr);
				
				Map<String,String> retMap = new HashMap<String,String>();
				retMap.put("projectName", project.getStandardName());
				retMap.put("schema", project.getSchema());

				return retMap;
			} else {
				throw new ValidateException("技术栈设置错误！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ValidateException) {
				throw e;
			}
			return null;
		} finally {
			if (is != null) {
				is.close();
				is = null;
			}
			if (fs != null) {
				fs.close();
				fs = null;
			}
		}
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		InputStream input = null;
		OutputStream output = null;
		InputStream is = null;
		POIFSFileSystem fs = null;
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		Map<String, Object> result = new TreeMap<String, Object>();
		// request.setCharacterEncoding("UTF-8");
		File f = null;
		try {
			// 1. Upload File Using Apache FileUpload
			files.addAll(MultipartRequestHandler.uploadByApacheFileUpload(request));

			String root = request.getSession().getServletContext().getRealPath("/").replace('\\', '/');

			String excelfolder = root + "spreadsheets/";
			excelfolder = excelfolder.replace('\\', '/');
			logger.debug("JerryDebug:excelfolder:" + excelfolder);

			String ignoreWarning = files.get(0).getIgnoreWarning();
			String genFormatted = files.get(0).getGenFormatted();
			String generateFront = files.get(0).getGenerateFront();
			String autoConfig = files.get(0).getAutoConfig();
			String frontUseProjectName = files.get(0).getFrontUseProjectName();
			String frontUseControllerPrefix = files.get(0).getFrontUseControllerPrefix();
			String sqlComments = files.get(0).getSqlComments();
			String javaCompatibility = files.get(0).getJavaCompatibility();
			String goCompatibility = files.get(0).getGoCompatibility();
			
			String useAdvancedCustomize=files.get(0).getUseAdvancedCustomize();
			String genUiStr=files.get(0).getGenUi();
			String genControllerStr=files.get(0).getGenController();
			String genServiceStr=files.get(0).getGenService();
			String genServiceImplStr=files.get(0).getGenServiceImpl();
			String genDaoStr=files.get(0).getGenDao();
			String genDaoImplStr=files.get(0).getGenDaoImpl();
			
			String useUserPartial = files.get(0).getUseUserPartial();
			String exportStr = files.get(0).getExportStr();
			
			String[] fileNames = files.get(0).getFileName().replace('\\', '/').split("/");
			String fileName = "";
			if (fileNames.length > 0)
				fileName = fileNames[fileNames.length - 1];
			else
				throw new ValidateException("文件上传出错");

			f = Project.createPathFile((excelfolder + fileName));

			input = files.get(0).getContent();
			if (f != null)
				output = new FileOutputStream(f);
			else
				throw new ValidateException("创建文件失败！");
			byte[] buffer = new byte[1024 * 10];

			for (int length = 0; (length = input.read(buffer)) > 0;) {
				output.write(buffer, 0, length);
			}

			files = new LinkedList<FileMeta>();
			
			boolean genUi = true;
			boolean genController=true;
			boolean genService = true;
			boolean genServiceImpl = true;
			boolean genDao = true;
			boolean genDaoImpl = true;
			if ("true".equalsIgnoreCase(useAdvancedCustomize)) {
				genUi = "true".equalsIgnoreCase(genUiStr);
				genController = "true".equalsIgnoreCase(genControllerStr);
				genService = "true".equalsIgnoreCase(genServiceStr);
				genServiceImpl = "true".equalsIgnoreCase(genServiceImplStr);
				genDao = "true".equalsIgnoreCase(genDaoStr);
				genDaoImpl = "true".equalsIgnoreCase(genDaoImplStr);
			}
			
			if ("false".equalsIgnoreCase(useUserPartial)) {
				exportStr = "";
			}
			
			// Generate source zip file
			Map<String,String> generatedMap = generateZip(root, f.getPath(),"true".equalsIgnoreCase(ignoreWarning),"true".equalsIgnoreCase(genFormatted),genUi,genController,genService,genServiceImpl,genDao,genDaoImpl,"true".equalsIgnoreCase(sqlComments),"true".equalsIgnoreCase(javaCompatibility),"true".equalsIgnoreCase(goCompatibility),exportStr);

			boolean sqlOnly = "DBTools".equalsIgnoreCase(generatedMap.get("schema"));

			String frontZipName = "";
			if ("true".equalsIgnoreCase(generateFront)&&!sqlOnly) {
				boolean useController=true;
				boolean useControllerPrefix=false;
				boolean showBackendProject=false;
				String baseApiUrl = "http://localhost:8082/";
				
				ParseSpreadSheetToProjectAction parseAction = new ParseSpreadSheetToProjectAction();
				FrontProject project = parseAction.parseRustProject(excelfolder,fileName);
				if ("true".equalsIgnoreCase(autoConfig)) {
					if ("tower".equalsIgnoreCase(project.getTechnicalstack())){
						useController=true;
						useControllerPrefix=false;
						showBackendProject=false;
					}
				}else {
					useController=true;
					useControllerPrefix=true;
					showBackendProject=false;					
				}
				frontZipName = ParseSpreadSheetToProjectAction.doParseRustExcel(excelfolder, fileName, root + "templates/",
						root + "source/",showBackendProject, useControllerPrefix,baseApiUrl,exportStr,"");
			}
			if (!StringUtil.isBlank(generatedMap.get("projectName"))) {
				result.put("success", true);
				result.put("projectName", generatedMap.get("projectName"));
				result.put("frontProjectName", frontZipName);
				out.print(JSONObject.fromObject(result));
			} else {
				throw new ValidateException("文件生成错误！");
			}
//		} catch (ValidateException ev) {
//			result.put("success", false);
//			ValidateInfo info = ev.getValidateInfo();
//			List<String> compileErrors = info.getCompileErrors();
//			result.put("compileErrors", compileErrors);
//			List<String> compileWarnings = info.getCompileWarnings();
//			result.put("compileWarnings", compileWarnings);
//			out.print(JSONObject.fromObject(result));
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ValidateException) {
				ValidateException ev = (ValidateException) e;
				result.put("success", false);
				ValidateInfo info = ev.getValidateInfo();
				List<String> compileErrors = info.getCompileErrors();
				result.put("compileErrors", compileErrors);
				List<String> compileWarnings = info.getCompileWarnings();
				result.put("compileWarnings", compileWarnings);
				out.print(JSONObject.fromObject(result));
				return;
			}
			if (!(e instanceof ValidateException)) {
				e.printStackTrace();
				result.put("success", false);
				if (e.getMessage() != null && !e.getMessage().equals("")) {
					result.put("compileErrors", new ArrayList<String>().add(e.getMessage()));
				} else {
					result.put("compileErrors", new ArrayList<String>().add("空指针或其他语法错误！"));
				}
				out.print(JSONObject.fromObject(result));
			}
		} finally {
			if (input != null)
				input.close();
			if (output != null)
				output.close();
			if (is != null)
				is.close();
			if (fs != null)
				fs.close();
			if (out != null)
				out.close();
		}
	}
}
