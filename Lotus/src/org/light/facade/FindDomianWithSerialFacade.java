package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2017/11/25
 *
 */
public class FindDomianWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -2783716426487467765L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindDomianWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = 0;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
				
		Domain domain0 = new Domain();
		if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
			domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
			
			List<Map<String,String>> fieldNames = new ArrayList<>();			
			Map<String,String> data0 = new TreeMap<>();	
			data0.put("fieldMeta","元字段类型");
			data0.put("fieldName","字段");
			data0.put("fieldType","字段类型");
			data0.put("fieldLabel","字段标签");
			data0.put("fieldLength","长度");
			data0.put("fieldFixedName","固定名字");
			fieldNames.add(data0);

			for (Field f: domain0.getFields()) {
				Map<String,String> data = new TreeMap<>();
				if (domain0.getDomainId()!=null&&domain0.getDomainId().getFieldName().equals(f.getFieldName())) {
					data.put("fieldMeta","domainid");
				} else if (domain0.getDomainName()!=null&&domain0.getDomainName().getFieldName().equals(f.getFieldName())) {
					data.put("fieldMeta","domainname");
				}else if (domain0.getActive()!=null&&domain0.getActive().getFieldName().equals(f.getFieldName())) {
					data.put("fieldMeta","activefield");
				}else if (f instanceof Dropdown) {
					data.put("fieldMeta","dropdown");
				}else {
					data.put("fieldMeta","field");
				}
				data.put("fieldName",f.getFieldName());
				if (f instanceof Dropdown) {
					data.put("fieldType",((Dropdown)f).getTargetName()) ;
				}else {
					data.put("fieldType",f.getFieldType());
				}
				data.put("fieldLabel",f.getLabel());
				data.put("fieldLength",f.getLengthStr());

				if (!StringUtil.isBlank(f.getFixedName())&&!f.getFixedName().equals(f.getFieldName())){
					data.put("fieldFixedName", f.getFixedName());
				}else {
					data.put("fieldFixedName", "");
				}
				fieldNames.add(data);
			}
			for (ManyToMany mtm:domain0.getManyToManies()) {
				Map<String,String> data = new TreeMap<>();
				data.put("fieldMeta","manytomany");
				data.put("fieldName",mtm.getStandardName());
				data.put("fieldType",mtm.getManyToManySalveName());
				data.put("fieldLabel",mtm.getSlaveAliasLabel());
				data.put("fieldLength","");
				data.put("fieldFixedName", "");
				fieldNames.add(data);
			}
			try {
				List<Domain> dataDomains = DomainUtil.filterDataDomainList(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				boolean hasDataDomains = false;
				if (dataDomains!=null&&dataDomains.size()>0)hasDataDomains=true;
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", fieldNames);
				result.put("hasDataDomains", hasDataDomains);
				out.print(JSONObject.fromObject(result));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}
