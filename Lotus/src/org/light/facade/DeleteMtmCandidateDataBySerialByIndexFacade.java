package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.ManyToManyCandidate;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.8
 *
 */
public class DeleteMtmCandidateDataBySerialByIndexFacade extends HttpServlet {
	private static final long serialVersionUID = -2288962452088937493L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteMtmCandidateDataBySerialByIndexFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		String indexs = StringUtil.nullTrim(request.getParameter("indexs"));
		String [] indexsStr = indexs.split(",");
		int [] indexsInts = new int[indexsStr.length];
		for (int i=0; i < indexsStr.length; i++) {
			indexsInts[i] = Integer.valueOf(indexsStr[i]);
		}
		try {				
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>serial) {
				for (int i=indexsInts.length -1; i >= 0; i--) {
					ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial).remove(indexsInts[i]);
				}
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
				return;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		Map<String, Object> result = new TreeMap<String, Object>();
		result.put("success", false);
		result.put("data", null);
		out.print(JSONObject.fromObject(result));
	}
}
