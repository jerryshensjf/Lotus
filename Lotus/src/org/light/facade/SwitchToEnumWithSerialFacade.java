package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.4.2
 *
 */
public class SwitchToEnumWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -8374320099875134182L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SwitchToEnumWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = 0;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		try {				
			Domain domain0 = new Domain();
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				org.light.domain.Enum myEnum = domain0.toEnum();
				List<List<Domain>> datadomains = DomainUtil.switchDataDomainsToEnumFromLists(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				ExcelWizardFacade.getInstance().getDomains().set(serial, myEnum);
				ExcelWizardFacade.getInstance().setDataDomains(datadomains);

				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
