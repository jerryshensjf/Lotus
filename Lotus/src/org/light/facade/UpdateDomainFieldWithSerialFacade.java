package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.4.4
 *
 */
public class UpdateDomainFieldWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -7258763737131741742L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateDomainFieldWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial") != null)
			serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));
		String fieldoriginalcatlog = StringUtil.nullTrim(request.getParameter("originalcatlog"));
		String fieldoriginalname = StringUtil.nullTrim(request.getParameter("originalname"));
		String fieldcatlog = StringUtil.nullTrim(request.getParameter("fieldcatlog"));
		String fieldname = StringUtil.nullTrim(request.getParameter("fieldname"));
		String fieldtype = StringUtil.nullTrim(request.getParameter("fieldtype"));
		String fieldlength = StringUtil.nullTrim(request.getParameter("fieldlength"));
		String fieldlabel = StringUtil.nullTrim(request.getParameter("fieldlabel"));
		String fieldfixedname = StringUtil.nullTrim(request.getParameter("fieldfixedname"));

		try {
			if (ExcelWizardFacade.getInstance() != null && ExcelWizardFacade.getInstance().getDomains() != null
					&& ExcelWizardFacade.getInstance().getDomains().size() > serial) {
				Domain domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				List<Domain> dataDomains = DomainUtil.filterDataDomainList(
						ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				domain0.updateFieldForAll(fieldoriginalcatlog, fieldoriginalname, fieldcatlog, fieldname, fieldtype, fieldlength, fieldlabel, fieldfixedname);
				for (Domain d : dataDomains) {
					d.updateFieldForAll(fieldoriginalcatlog, fieldoriginalname, fieldcatlog, fieldname, fieldtype, fieldlength, fieldlabel, fieldfixedname);
				}
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
