package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONArray;

/**
 * 
 * @author Jerry Shen 2017/11/25
 *
 */
public class FindDataDomiansWithDomainNameFacade extends HttpServlet {
	private static final long serialVersionUID = 8821436661286792292L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindDataDomiansWithDomainNameFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			int serial = 0;
			if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
			
			Domain domain0 = new Domain();
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null&&ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
				List<Domain> dataDomains = DomainUtil.filterDataDomainList(ExcelWizardFacade.getInstance().getDataDomains(), domain0.getStandardName());
				List<Map<String,String>> datas = new ArrayList<>();
				for (int i=0;i<dataDomains.size();i++) {
					Map<String,String> data = new TreeMap<>();
					if (i==0) data.put("字段","数据");
					for (Field f: domain0.getFields()) {
						System.out.println("JerryDebug:metafield:"+f.getFieldName());
						if (f instanceof Dropdown &&("0".equals(dataDomains.get(i).getField(f.getFieldName()).getFieldValue())||StringUtil.isNegativeInteger(dataDomains.get(i).getField(f.getFieldName()).getFieldValue()))) {
							data.put(f.getFieldName(),"");
						} else {
							data.put(f.getFieldName(),dataDomains.get(i).getField(f.getFieldName()).getFieldValue());
						}
					}
					for (ManyToMany mtm:domain0.getManyToManies()) {
						ManyToMany inMtm = dataDomains.get(i).findManyToManyByStandardName(mtm.getStandardName());
						String values = "";
						if (inMtm!=null) values = inMtm.getValues();
						data.put(mtm.getStandardName(),values);
					}
					datas.add(data);
				}
		
				if (ExcelWizardFacade.getInstance()!=null) {
					//ExcelWizardFacade.getInstance().getDataDomains().clear();
					//ExcelWizardFacade.project.addDataDomains(dataDomains);
				}
				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				out.print(JSONArray.fromObject(datas));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
