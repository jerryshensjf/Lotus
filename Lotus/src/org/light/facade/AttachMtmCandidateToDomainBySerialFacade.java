package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.ManyToManyCandidate;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.4.21
 *
 */
public class AttachMtmCandidateToDomainBySerialFacade extends HttpServlet {
	private static final long serialVersionUID = -2291683914429215129L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AttachMtmCandidateToDomainBySerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		Map<String, Object> result = new TreeMap<String, Object>();
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		try {				
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>serial) {
				ManyToManyCandidate mtmc = ExcelWizardFacade.getInstance().getMtmCandidates().get(serial);
				List<ManyToManyCandidate> dmtmcs = ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial);
				List<Domain> domains = ExcelWizardFacade.getInstance().getDomains();
				int domainSerial = DomainUtil.findDomainSerialInList(domains, mtmc.getMaster().getStandardName());
				if (domainSerial < 0) throw new ValidateException("找不到叠加的目标域对象！");
				Domain target0 = ExcelWizardFacade.getInstance().getDomains().get(domainSerial);
				if (target0 == null || !target0.hasDomainId()||!target0.hasDomainName()||!target0.hasActiveField()) throw new ValidateException("目标域对象序号,名字或活跃字段缺失！");
				Domain slave0 = DomainUtil.findDomainInListOrReturnNull(domains, mtmc.getSlave().getStandardName());
				if (slave0 == null || !slave0.hasDomainId()||!slave0.hasDomainName()||!slave0.hasActiveField()) throw new ValidateException("从域对象序号,名字或活跃字段缺失！");
				List<List<Domain>> fullDataDomains = ExcelWizardFacade.getInstance().getDataDomains();
				List<Domain> dataDomains = DomainUtil.filterDataDomainList(ExcelWizardFacade.getInstance().getDataDomains(), mtmc.getMaster().getStandardName());
				Domain target = DomainUtil.attachMtmCandidateToDomain(domains.get(domainSerial), mtmc);
				dataDomains = DomainUtil.attachMtmCandidates(dataDomains, dmtmcs);
				
				ExcelWizardFacade.getInstance().getMtmCandidates().remove(serial);
				ExcelWizardFacade.getInstance().getMtmCandidatesValues().remove(serial);

				ExcelWizardFacade.getInstance().getDomains().set(domainSerial, target);
				ExcelWizardFacade.getInstance().setDataDomains(DomainUtil.replaceSubDataDomainListInList(fullDataDomains,dataDomains));
				
				result.put("success", true);
				result.put("data", null);				
			} else {
				result.put("success", false);
				result.put("data", null);		
			}
		}  catch(ValidateException ve) {
			ve.printStackTrace();
			result.put("success", false);
			ValidateInfo info = ve.getValidateInfo();
			List<String> compileErrors = info.getCompileErrors();
			result.put("compileErrors", compileErrors);
			List<String> compileWarnings = info.getCompileWarnings();
			result.put("compileWarnings", compileWarnings);
			out.print(JSONObject.fromObject(result));
			return;	
		} catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("data", null);		
		}
		out.print(JSONObject.fromObject(result));
	}
}
