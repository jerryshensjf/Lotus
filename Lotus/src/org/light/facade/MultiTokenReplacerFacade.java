package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.exception.ValidateException;
import org.light.utils.PluralUtil;
import org.light.utils.StringUtil;

public class MultiTokenReplacerFacade extends HttpServlet {
	private static final long serialVersionUID = -5164753455371888071L;

	public MultiTokenReplacerFacade() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("application/json;charset=UTF-8");
		 PrintWriter out = response.getWriter();
		 try {
				String [] statements = request.getParameter("mycode").split("\n");
				String param1 = request.getParameter("param1");
				String target1 = request.getParameter("target1");
				Boolean autoPlural1 = Boolean.valueOf(request.getParameter("autoplural1"));
				String param2 = request.getParameter("param2");
				String target2 = request.getParameter("target2");
				Boolean autoPlural2 = Boolean.valueOf(request.getParameter("autoplural2"));
				String param3 = request.getParameter("param3");
				String target3 = request.getParameter("target3");
				Boolean autoPlural3 = Boolean.valueOf(request.getParameter("autoplural3"));
				String param4 = request.getParameter("param4");
				String target4 = request.getParameter("target4");
				Boolean autoPlural4 = Boolean.valueOf(request.getParameter("autoplural4"));
				String param5 = request.getParameter("param5");
				String target5 = request.getParameter("target5");
				Boolean autoPlural5 = Boolean.valueOf(request.getParameter("autoplural5"));
				
				StringBuilder sb = new StringBuilder();
				for (String s:statements) {	
					s=processStringWithParams(s,param1,target1,autoPlural1);
					s=processStringWithParams(s,param2,target2,autoPlural2);
					s=processStringWithParams(s,param3,target3,autoPlural3);
					s=processStringWithParams(s,param4,target4,autoPlural4);
					s=processStringWithParams(s,param5,target5,autoPlural5);
					sb.append(s).append("\n");
				}
				out.print(sb.toString());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
	 }
	 
	 private String processStringWithParams(String line, String param, String target,Boolean autoPlural) throws ValidateException {
		 	if (StringUtil.isBlank(param)||StringUtil.isBlank(target)) return line;
			String capToken = StringUtil.capFirst(param);
			String lowertoken = StringUtil.lowerFirst(capToken);
			String capTargetToken = StringUtil.capFirst(target);
			String lowerTargetToken = StringUtil.lowerFirst(capTargetToken);
			String capPlural = "";
			String lowerPlural = "";
			String capTargetPlural = "";
			String lowerTargetPlural = "";
			if (autoPlural) {
				capPlural = StringUtil.capFirst(PluralUtil.lookupPlural(capToken));
				lowerPlural = StringUtil.lowerFirst(capPlural);
				capTargetPlural = StringUtil.capFirst(PluralUtil.lookupPlural(target));
				lowerTargetPlural = StringUtil.lowerFirst(capTargetPlural);
			}
			line=line.replaceAll("\"", "\\\\\"");
			if (autoPlural) {
				line=line.replaceAll(capPlural, capTargetPlural);
				line=line.replaceAll(lowerPlural, lowerTargetPlural);
			}
			line=line.replaceAll(capToken, capTargetToken);
			line=line.replaceAll(lowertoken,lowerTargetToken);
			return line;
	 }

}
