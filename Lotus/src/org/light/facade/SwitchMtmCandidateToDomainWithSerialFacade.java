package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.ManyToManyCandidate;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2024.5.21
 *
 */
public class SwitchMtmCandidateToDomainWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = 8099519830575654764L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SwitchMtmCandidateToDomainWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial")!=null) serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		try {				
			ManyToManyCandidate mtmc0 = new ManyToManyCandidate();
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getMtmCandidates()!=null&&ExcelWizardFacade.getInstance().getMtmCandidates().size()>serial) {
				mtmc0 = ExcelWizardFacade.getInstance().getMtmCandidates().get(serial);
				Domain domain = mtmc0.toDomain();
				List<ManyToManyCandidate> vmtmcs = ExcelWizardFacade.getInstance().getMtmCandidatesValues().get(serial);
				List<Domain> datadomains = new ArrayList<>();
				for (ManyToManyCandidate mtmc:vmtmcs) {
					datadomains.add(mtmc.toDomain());
				}
				ExcelWizardFacade.getInstance().getMtmCandidates().remove(serial);
				ExcelWizardFacade.getInstance().getDomains().add(domain);
				ExcelWizardFacade.getInstance().getDataDomains().add(datadomains);

				Map<String, Object> result = new TreeMap<String, Object>();
				result.put("success", true);
				result.put("data", null);
				out.print(JSONObject.fromObject(result));
			}
		} catch(Exception e) {
			e.printStackTrace();
			ValidateException ve = new ValidateException("多对多候选转化为域对象错误。");
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", false);
			result.put("compileErrors", ve.getValidateInfo().getCompileErrors());
			out.print(JSONObject.fromObject(result));
		}
	}
}
