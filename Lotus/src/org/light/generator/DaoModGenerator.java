package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class DaoModGenerator extends Generator{
	protected List<Domain> domains = new ArrayList<>();
	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}

	public DaoModGenerator(){
		super();
		super.fileName = "mod.rs";
		super.standardName = "DaoMod";
	}

	@Override
	public StatementList generateStatementList() {
		Domain sampleDomain = this.domains.get(0);
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(100L,0,"#![allow(dead_code)]"));
		sList.add(new Statement(1000L,0,"use super::"+sampleDomain.getDomainSuffix()+"::"+DomainUtil.domainListToCapFirstDomainNameWithSuffixListStr(this.getDomains())+";"));
		sList.add(new Statement(2000L,0,"use super::"+sampleDomain.getDomainSuffix()+"::CountNum;"));
		long serial = 3000L;
		for (Domain d:this.domains) {
			sList.add(new Statement(serial,0,"use super::"+sampleDomain.getDomainSuffix()+"::"+d.getCapFirstDomainName()+"QueryRequest;"));
			serial += 1000L;
		}
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,0,"pub mod db_context;"));
		serial += 4000L;
		for (Domain domain : this.getDomains()) {
			if (domain instanceof org.light.domain.Enum || "datadummy".equalsIgnoreCase(domain.getSchema())) {
				sList.add(new Statement(serial,0,"mod "+domain.getSnakeDomainName()+"_dummydao;"));
			} else {
				sList.add(new Statement(serial,0,"mod "+domain.getSnakeDomainName()+"_dao;"));
			}
			serial += 1000L;
		}
		sList.add(new Statement(serial,0,""));
		sList.add(new Statement(serial+1000L,0,"pub type Database<'c> = db_context::Database<'c>;"));
		sList.add(new Statement(serial+2000L,0,"pub type Table<'c, T> = db_context::Table<'c, T>;"));
		sList.add(new Statement(serial+3000L,0,"pub type JoinTable<'c, T1, T2> = db_context::JoinTable<'c, T1, T2>;"));

		return WriteableUtil.merge(sList);
	}

}
