package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class StringUtilGenerator extends Generator{
	public StringUtilGenerator(){
		super();
		super.fileName = "string_util.rs";
	}

	@Override
	public StatementList generateStatementList() throws ValidateException {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(500L,0,"#![allow(dead_code)]"));
		sList.add(new Statement(1000L,0,"pub fn is_digit(str:String) -> bool{"));
		sList.add(new Statement(2000L,1,"for ch in str.chars(){"));
		sList.add(new Statement(3000L,2,"if ch.is_ascii_digit() == false {"));
		sList.add(new Statement(4000L,3,"return false;"));
		sList.add(new Statement(5000L,2,"}"));
		sList.add(new Statement(6000L,1,"}"));
		sList.add(new Statement(7000L,1,"return true;"));
		sList.add(new Statement(8000L,0,"}"));
		sList.add(new Statement(9000L,0,""));
		sList.add(new Statement(10000L,0,"pub fn right_find_under_line (ori_str:String) -> usize{"));
		sList.add(new Statement(11000L,1,"let chars = ori_str.chars().rev();"));
		sList.add(new Statement(12000L,1,"let count = chars.clone().count();"));
		sList.add(new Statement(13000L,0,""));
		sList.add(new Statement(14000L,1,"for (i, item) in chars.enumerate() {"));
		sList.add(new Statement(15000L,2,"if item == '_' {"));
		sList.add(new Statement(16000L,3,"return count-i-1;"));
		sList.add(new Statement(17000L,2,"}"));
		sList.add(new Statement(18000L,1,"}"));
		sList.add(new Statement(19000L,1,"return usize::MAX;"));
		sList.add(new Statement(20000L,0,"}"));

		return WriteableUtil.merge(sList);
	}
}
