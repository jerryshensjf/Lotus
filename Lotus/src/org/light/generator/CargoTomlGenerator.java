package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class CargoTomlGenerator extends Generator{
	protected Domain sampleDomain;
	protected String projectName;
	protected String edition;
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}

	public CargoTomlGenerator(){
		super();
		super.fileName = "Cargo.toml";
		super.standardName = "CargoToml";
	}

	@Override
	public StatementList generateStatementList() {
		String dbPackage = "mysql";
		if ("PostgreSQL".equalsIgnoreCase(sampleDomain.getDbType())||"pgsql".equalsIgnoreCase(sampleDomain.getDbType())) {
			dbPackage = "postgres";
		}
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"[package]"));
		sList.add(new Statement(2000L,0,"name = \""+this.projectName+"\""));
		sList.add(new Statement(3000L,0,"version = \"0.1.0\""));
		sList.add(new Statement(4000L,0,"edition = \"2021\""));
		sList.add(new Statement(5000L,0,""));
		sList.add(new Statement(6000L,0,"# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html"));
		sList.add(new Statement(7000L,0,"[dependencies]"));
		sList.add(new Statement(8000L,0,"axum =  {version = \"0.6.20\", features = [\"multipart\",\"headers\"] }"));
		sList.add(new Statement(9000L,0,"tokio = { version = \"1.32.0\", features = [\"full\"] }"));
		sList.add(new Statement(10000L,0,"tracing = \"0.1\""));
		sList.add(new Statement(11000L,0,"tracing-subscriber = { version=\"0.3\", features = [\"env-filter\"] }"));
		sList.add(new Statement(12000L,0,"tower-http = { version = \"0.3.5\", features = [\"fs\", \"trace\", \"cors\"] }"));
		sList.add(new Statement(13000L,0,""));
		if ("Oracle".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(14000L,0,"oracle = { version = \"0.5.7\", features = [\"chrono\"] }"));
		}else {
			sList.add(new Statement(14000L,0,"sqlx = { version = \"0.7.1\", features = [\""+dbPackage+"\",\"chrono\",\"runtime-tokio-native-tls\"] }"));
		}
		sList.add(new Statement(15000L,0,"serde = { version = \"1.0.117\", features = [\"derive\"] }"));
		sList.add(new Statement(16000L,0,"serde_json = \"1.0\""));
		sList.add(new Statement(17000L,0,"uuid = { version = \"0.8.1\", features = [\"serde\", \"v4\"] }"));
		sList.add(new Statement(18000L,0,"http = \"0.2.1\""));
		sList.add(new Statement(19000L,0,"blob = \"0.3.0\""));
		sList.add(new Statement(20000L,0,"base64 = \"0.12.1\""));
		sList.add(new Statement(21000L,0,"lazy_static = \"1.4.0\""));
		sList.add(new Statement(22000L,0,"simple_excel_writer = \"0.2.0\""));
		sList.add(new Statement(23000L,0,"printpdf =  {version = \"0.5.3\", features = [\"embedded_images\"] }"));
		sList.add(new Statement(24000L,0,"image = \"0.24.5\""));
		sList.add(new Statement(25000L,0,"sha1 = \"0.10.5\""));
		sList.add(new Statement(26000L,0,"hex = \"0.4.3\""));
		sList.add(new Statement(28000L,0,"rand = { version = \"0.8.5\", features = [\"min_const_gen\"] }"));
		
		sList.add(new Statement(29000L,0,""));
		sList.add(new Statement(30000L,0,"axum-sessions = \"0.5.0\""));
		sList.add(new Statement(31000L,0,"jsonwebtoken = \"7.2\""));
		sList.add(new Statement(32000L,0,"hyper = { version = \"0.14.17\" }"));
		sList.add(new Statement(33000L,0,"headers = \"0.3\""));
		sList.add(new Statement(34000L,0,"chrono = { version = \"0.4\", features = [\"serde\"] }"));
		sList.add(new Statement(35000L,0,"clap = { version = \"3.0.0-rc.4\", features = [\"derive\", \"env\"] }"));
		sList.add(new Statement(36000L,0,"rayon = \"1.5\""));
		sList.add(new Statement(37000L,0,"bcrypt = \"0.10\""));
		sList.add(new Statement(38000L,0,"validator = { version = \"0.14\", features = [\"derive\"] }"));
		sList.add(new Statement(39000L,0,"thiserror = \"1\""));
		sList.add(new Statement(39000L,0,"regex = \"1.7.3\""));
		sList.add(new Statement(40000L,0,"substring = \"1.4.3\""));
		sList.add(new Statement(41000L,0,"async_static = \"0.1.3\""));
		sList.add(new Statement(42000L,0,"once_cell = \"1\""));
		sList.add(new Statement(43000L,0,"querystring = \"1.1.0\""));
		sList.add(new Statement(44000L,0,"url = \"=2.5.2\""));
		return WriteableUtil.merge(sList);
	}

}
