package org.light.generator;

import org.light.domain.StatementList;
import org.light.exception.ValidateException;

public abstract class Generator {
	protected String standardName;
	protected String fileName;
	protected String packageToken;
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	public abstract StatementList generateStatementList() throws ValidateException;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
