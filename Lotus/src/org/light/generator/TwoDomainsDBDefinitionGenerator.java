package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.Domain;
import org.light.oracle.generator.Oracle11gSqlReflector;
import org.light.utils.SqlReflector;
import org.light.utils.StringUtil;

public class TwoDomainsDBDefinitionGenerator{
	protected Domain master;
	protected Domain slave;
	
	public TwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql(String dbtype,boolean commentOn,String exportStr) throws Exception{
		List<String> exports = new ArrayList<>();
		String [] exportArr = exportStr.split(",");
		for (String str :exportArr) {
			exports.add(str);
		}
		
		StringBuilder sb = new StringBuilder();
		if (StringUtil.isBlank(exportStr)||exports.contains(this.master.getStandardName())) {
			if ("Oracle".equalsIgnoreCase(dbtype)) {
				sb.append(Oracle11gSqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
			}else {
				sb.append(SqlReflector.generateLinkTableDefinition(this.master,this.slave,commentOn,dbtype)).append("\n");
			}
		}
		return sb.toString();
	}
	
	public String generateDropLinkTableSql() throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table if exists " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getStandardName())) +";\n";
			return result;
		}else {
			String result = "drop table if exists " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getAlias())) +";\n";
			return result;
		}
	}
	
	public String generateOracleDropLinkTableSql() throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getStandardName())) +";\n";
			return result;
		}else {
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+StringUtil.capFirst(slave.getAlias())) +";\n";
			return result;
		}
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}
}
