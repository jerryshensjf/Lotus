package org.light.generator;

import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Var;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;


public class NamedJavascriptBlockGenerator {
	public static JavascriptBlock documentReadyListDomainList(Domain domain, Var pagesize,Var pagenum) throws ValidateException {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("documentReadyList"+domain.getCapFirstDomainName()+"List");
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "$(document).ready(function(){"));
		sl.add(new Statement(2000,1, "listAll"+StringUtil.capFirst(domain.getPlural())+"ByPage("+pagesize.getVarName()+","+pagenum.getVarName()+");"));
		sl.add(new Statement(3000,0, "});"));
		block.setMethodStatementList(sl);
		return block;
	}
}
