package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.utils.StringUtil;
import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.Util;
import org.light.utils.WriteableUtil;

public class RequestUtilGenerator extends Util{
	public RequestUtilGenerator(){
		super();
		super.fileName = "RequestUtil.go";
	}
	
	public RequestUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "RequestUtil.go";
	}
	
	public String getPackageTokenWithDot() {
		if (StringUtil.isBlank(this.getPackageToken())) return "";
		else return this.getPackageToken()+".";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"package utils"));
		sList.add(new Statement(2000L,0,"import ("));
		sList.add(new Statement(3000L,1,"\"github.com/gin-gonic/gin\""));
		sList.add(new Statement(4000L,0,")"));
		sList.add(new Statement(5000L,0,"func DefaultFormQuery(c *gin.Context, name string, defaultValue string) string {"));
		sList.add(new Statement(6000L,1,"value := c.PostForm(name)"));
		sList.add(new Statement(7000L,1,"if value != \"\" {"));
		sList.add(new Statement(8000L,2,"return value"));
		sList.add(new Statement(9000L,1,"}else {"));
		sList.add(new Statement(10000L,2,"value = c.DefaultQuery(name,defaultValue)"));
		sList.add(new Statement(11000L,2,"return value"));
		sList.add(new Statement(12000L,1,"}"));
		sList.add(new Statement(13000L,0,"}"));

		return WriteableUtil.merge(sList).getContent();
	}

}
