package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;

public class UtilsModGenerator extends Generator{
	protected List<Domain> domains = new ArrayList<>();
	protected boolean containsAuth = false;
	
	public boolean isContainsAuth() {
		return containsAuth;
	}

	public void setContainsAuth(boolean containsAuth) {
		this.containsAuth = containsAuth;
	}

	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}

	public UtilsModGenerator(){
		super();
		super.fileName = "mod.rs";
		super.standardName = "UtilsMod";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(500L,0,"pub mod string_util;"));
		sList.add(new Statement(1000L,0,"pub mod excel_util;"));
		sList.add(new Statement(2000L,0,"pub mod pdf_util;"));
		if (containsAuth) {
			sList.add(new Statement(3000L,0,"pub mod jwt_util;"));
			sList.add(new Statement(4000L,0,"pub mod encrypt_util;"));
			sList.add(new Statement(5000L,0,"pub mod password_util;"));
		}
		return WriteableUtil.merge(sList);
	}

}
