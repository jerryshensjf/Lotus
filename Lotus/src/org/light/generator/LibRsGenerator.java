package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class LibRsGenerator extends Generator{
	protected Domain sampleDomain;
	protected boolean containsAuth = false;
	public boolean isContainsAuth() {
		return containsAuth;
	}

	public void setContainsAuth(boolean containsAuth) {
		this.containsAuth = containsAuth;
	}
	
	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}

	public LibRsGenerator(){
		super();
		super.fileName = "lib.rs";
		super.standardName = "LibRs";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+500L,0,"#![allow(non_snake_case,unused_imports)]"));
		sList.add(new Statement(serial+600L,0,"#[macro_use]"));
		sList.add(new Statement(serial+700L,0,"extern crate lazy_static;"));
		sList.add(new Statement(serial+800L,0,"extern crate simple_excel_writer as excel;"));
		sList.add(new Statement(serial+1000L,0,"use crate::"+this.sampleDomain.getDaoimplSuffix()+"::Database;"));
		sList.add(new Statement(serial+2000L,0,"use std::sync::{Arc, Mutex};"));
		sList.add(new Statement(serial+3000L,0,""));
		sList.add(new Statement(serial+4000L,0,"pub mod config;"));
		sList.add(new Statement(serial+5000L,0,"pub(crate) mod "+this.sampleDomain.getDaoimplSuffix()+";"));
		sList.add(new Statement(serial+6000L,0,"pub(crate) mod "+this.sampleDomain.getDomainSuffix()+";"));
		sList.add(new Statement(serial+6100L,0,"pub(crate) mod "+sampleDomain.getServiceimplSuffix()+";"));
		sList.add(new Statement(serial+6200L,0,"pub(crate) mod utils;"));
		sList.add(new Statement(serial+6300L,0,"pub mod "+sampleDomain.getControllerSuffix()+";"));
		if (this.containsAuth) {
			sList.add(new Statement(serial+6400L,0,"pub mod middleware;"));
		}
		sList.add(new Statement(serial+7000L,0,""));
		sList.add(new Statement(serial+8000L,0,"// AppState"));
		sList.add(new Statement(serial+9000L,0,"// This the primary dependency for our application's dependency injection."));
		sList.add(new Statement(serial+10000L,0,"// Each controller_test function that interacts with the database will require an `AppState` instance in"));
		sList.add(new Statement(serial+11000L,0,"// order to communicate with the database."));
		sList.add(new Statement(serial+12000L,0,"pub struct AppState<'a> {"));
		sList.add(new Statement(serial+13000L,1,"pub connections: Mutex<u32>,"));
		sList.add(new Statement(serial+14000L,1,"pub context: Arc<Database<'a>>,"));
		sList.add(new Statement(serial+15000L,0,"}"));

		return WriteableUtil.merge(sList);
	}

}
