package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class ExcelUtilGenerator extends Generator{
	public ExcelUtilGenerator(){
		super();
		super.fileName = "excel_util.rs";
	}

	@Override
	public StatementList generateStatementList() throws ValidateException {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+500L,0,"#![allow(dead_code)]"));
		sList.add(new Statement(serial+1000L,0,"use excel::*;"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,0,"pub fn export_excel_workbook_with_image(full_file_path:String,sheet_name:String, headers:Vec<String>,"));
		sList.add(new Statement(serial+4000L,1,"contents:Vec<Vec<String>>, is_images:Vec<bool>){"));
		sList.add(new Statement(serial+5000L,1,"let mut wb = Workbook::create(&full_file_path);"));
		sList.add(new Statement(serial+6000L,1,"let mut sheet = wb.create_sheet(&sheet_name);"));
		sList.add(new Statement(serial+10000L,0,""));
		sList.add(new Statement(serial+11000L,1,"sheet.add_column(Column { width: 10.0 });"));
		sList.add(new Statement(serial+12000L,1,"for _item in headers.clone() {"));
		sList.add(new Statement(serial+13000L,2,"sheet.add_column(Column { width: 20.0 });"));
		sList.add(new Statement(serial+14000L,1,"}"));
		sList.add(new Statement(serial+15000L,0,""));
		sList.add(new Statement(serial+16000L,1,"wb.write_sheet(&mut sheet, |sheet_writer| {"));
		sList.add(new Statement(serial+17000L,2,"let sw = sheet_writer;"));
		sList.add(new Statement(serial+18000L,2,"let _empty_head_row = Row::new();"));
		sList.add(new Statement(serial+19000L,2,"sw.append_blank_rows(1);"));
		sList.add(new Statement(serial+20000L,2,"let mut head_row = Row::new();"));
		sList.add(new Statement(serial+21000L,2,"head_row.add_cell(\"\".to_string());"));
		sList.add(new Statement(serial+22000L,2,"for header in headers.clone() {"));
		sList.add(new Statement(serial+23000L,3,"head_row.add_cell(header);"));
		sList.add(new Statement(serial+24000L,2,"}"));
		sList.add(new Statement(serial+25000L,2,"let _ = sw.append_row(head_row);"));
		sList.add(new Statement(serial+26000L,2,"for content in contents.clone(){"));
		sList.add(new Statement(serial+27000L,3,"let mut content_row = Row::new();"));
		sList.add(new Statement(serial+28000L,3,"content_row.add_cell(\"\".to_string());"));
		sList.add(new Statement(serial+29000L,3,"let mut col_index = 0;"));
		sList.add(new Statement(serial+30000L,3,"for item in content.clone() {"));
		sList.add(new Statement(serial+31000L,4,"let is_image = is_images.get(col_index);"));
		sList.add(new Statement(serial+32000L,4,"match is_image {"));
		sList.add(new Statement(serial+33000L,5,"Some(is_image) => {"));
		sList.add(new Statement(serial+34000L,6,"if *is_image {"));
		sList.add(new Statement(serial+35000L,7,"content_row.add_cell(\"\".to_string());"));
		sList.add(new Statement(serial+36000L,6,"}else {"));
		sList.add(new Statement(serial+37000L,7,"content_row.add_cell(item);"));
		sList.add(new Statement(serial+38000L,6,"}"));
		sList.add(new Statement(serial+39000L,5,"},"));
		sList.add(new Statement(serial+40000L,5,"None => content_row.add_cell(item),"));
		sList.add(new Statement(serial+41000L,4,"}"));
		sList.add(new Statement(serial+42000L,4,"col_index += 1;"));
		sList.add(new Statement(serial+43000L,3,"}"));
		sList.add(new Statement(serial+44000L,3,"let _ = sw.append_row(content_row);"));
		sList.add(new Statement(serial+45000L,2,"}"));
		sList.add(new Statement(serial+46000L,2,"Ok(())"));
		sList.add(new Statement(serial+47000L,1,"}).expect(\"write excel error!\");"));
		sList.add(new Statement(serial+48000L,1,"wb.close().expect(\"close excel error!\");"));
		sList.add(new Statement(serial+49000L,0,"}"));
		return WriteableUtil.merge(sList);
	}
}
