package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.Domain;

public abstract class DBDefinitionGenerator {
	protected String dbName;
	protected String dbType;
	protected String projectName;
	protected List<Domain> domains = new ArrayList<Domain>();
	private List<String> contents = new ArrayList<String>();
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName2) {
		dbName = dbName2;
	}
	public List<Domain> getDomains() {
		return domains;
	}
	public void setDomains(List<Domain> domains2) {
		domains = domains2;
	}
	public void addDomain(Domain domain){
		domains.add(domain);
	}
	public List<String> getContents() {
		return contents;
	}
	public void setContents(List<String> contents2) {
		contents = contents2;
	}
	public abstract String generateDBSql(boolean createNew,boolean commentOn,String exportStr) throws Exception;
	public abstract String generateDropTableSqls(boolean createNew,String exportStr) throws Exception;
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
