package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.utils.StringUtil;
import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.Util;
import org.light.utils.WriteableUtil;

public class TypeUtilGenerator extends Util{
	public TypeUtilGenerator(){
		super();
		super.fileName = "TypeUtil.go";
	}
	
	public TypeUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "TypeUtil.go";
	}
	
	public String getPackageTokenWithDot() {
		if (StringUtil.isBlank(this.getPackageToken())) return "";
		else return this.getPackageToken()+".";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"package utils"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import ("));
		sList.add(new Statement(4000L,1,"\"database/sql\""));
		sList.add(new Statement(4500L,1,"\"strconv\""));
		sList.add(new Statement(4600L,1,"\"unicode\""));
		sList.add(new Statement(5000L,0,")"));
		sList.add(new Statement(6000L,0,""));
		sList.add(new Statement(7000L,0,"func ParseNullString(nullStr sql.NullString ) string{"));
		sList.add(new Statement(8000L,1,"if (nullStr.Valid) {"));
		sList.add(new Statement(9000L,2,"return nullStr.String"));
		sList.add(new Statement(10000L,1,"}else {"));
		sList.add(new Statement(11000L,2,"return \"\""));
		sList.add(new Statement(12000L,1,"}"));
		sList.add(new Statement(13000L,0,"}"));
		sList.add(new Statement(14000L,0,""));
		sList.add(new Statement(15000L,0,"func ParseNullFloat64(nullFloat64 sql.NullFloat64) float64{"));
		sList.add(new Statement(16000L,1,"if (nullFloat64.Valid) {"));
		sList.add(new Statement(17000L,2,"return nullFloat64.Float64"));
		sList.add(new Statement(18000L,1,"}else {"));
		sList.add(new Statement(19000L,2,"return 0"));
		sList.add(new Statement(20000L,1,"}"));
		sList.add(new Statement(21000L,0,"}"));
		sList.add(new Statement(22000L,0,""));
		sList.add(new Statement(23000L,0,"func ParseNullInt64(nullInt64 sql.NullInt64) int64{"));
		sList.add(new Statement(24000L,1,"if (nullInt64.Valid) {"));
		sList.add(new Statement(25000L,2,"return nullInt64.Int64"));
		sList.add(new Statement(26000L,1,"}else {"));
		sList.add(new Statement(27000L,2,"return 0"));
		sList.add(new Statement(28000L,1,"}"));
		sList.add(new Statement(29000L,0,"}"));
		sList.add(new Statement(30000L,0,""));
		sList.add(new Statement(31000L,0,"func ParseNullBool(nullBool sql.NullBool) bool{"));
		sList.add(new Statement(32000L,1,"if (nullBool.Valid) {"));
		sList.add(new Statement(33000L,2,"return nullBool.Bool"));
		sList.add(new Statement(34000L,1,"}else {"));
		sList.add(new Statement(35000L,2,"return false"));
		sList.add(new Statement(36000L,1,"}"));
		sList.add(new Statement(37000L,0,"}"));
		sList.add(new Statement(37500L,0,""));
		
		sList.add(new Statement(38000L,0,"func ParseNullInt32(nullInt32 sql.NullInt32) int32{"));
		sList.add(new Statement(39000L,1,"if (nullInt32.Valid) {"));
		sList.add(new Statement(40000L,2,"return nullInt32.Int32"));
		sList.add(new Statement(41000L,1,"}else {"));
		sList.add(new Statement(42000L,2,"return 0"));
		sList.add(new Statement(43000L,1,"}"));
		sList.add(new Statement(44000L,0,"}"));
		sList.add(new Statement(45000L,0,""));
		
		sList.add(new Statement(46000L,0,"func ParseNullStringtoInt64(nullStr sql.NullString ) int64{"));
		sList.add(new Statement(47000L,1,"if (nullStr.Valid) {"));
		sList.add(new Statement(48000L,2,"temp, _ := strconv.ParseInt(nullStr.String, 10, 64)"));
		sList.add(new Statement(49000L,2,"return temp"));
		sList.add(new Statement(50000L,1,"}else {"));
		sList.add(new Statement(51000L,2,"return 0"));
		sList.add(new Statement(52000L,1,"}"));
		sList.add(new Statement(53000L,0,"}"));
		sList.add(new Statement(54000L,0,""));
		
		sList.add(new Statement(55000L,0,"func ParseBooltoInt(val bool) int{"));
		sList.add(new Statement(56000L,1,"if(val) {"));
		sList.add(new Statement(57000L,2,"return 1"));
		sList.add(new Statement(58000L,1,"} else {"));
		sList.add(new Statement(59000L,2,"return 0"));
		sList.add(new Statement(60000L,1,"}"));
		sList.add(new Statement(61000L,0,"}"));
		sList.add(new Statement(62000L,0,""));
		sList.add(new Statement(63000L,0,"func ParseInttoBool(val int) bool{"));
		sList.add(new Statement(64000L,1,"if(val==1) {"));
		sList.add(new Statement(65000L,2,"return true"));
		sList.add(new Statement(66000L,1,"} else {"));
		sList.add(new Statement(67000L,2,"return false"));
		sList.add(new Statement(68000L,1,"}"));
		sList.add(new Statement(69000L,0,"}"));
		sList.add(new Statement(70000L,0,""));

		sList.add(new Statement(71000L,0,"func ParseBooltoIntStr(val bool) string{"));
		sList.add(new Statement(72000L,1,"if(val) {"));
		sList.add(new Statement(73000L,2,"return \"1\""));
		sList.add(new Statement(74000L,1,"} else {"));
		sList.add(new Statement(75000L,2,"return \"0\""));
		sList.add(new Statement(76000L,1,"}"));
		sList.add(new Statement(77000L,0,"}"));
		
		sList.add(new Statement(78000L,0,"func IsDigit(str string) bool {"));
		sList.add(new Statement(79000L,1,"for _, x := range []rune(str) {"));
		sList.add(new Statement(80000L,2,"if !unicode.IsDigit(x) {"));
		sList.add(new Statement(81000L,3,"return false"));
		sList.add(new Statement(82000L,2,"}"));
		sList.add(new Statement(83000L,1,"}"));
		sList.add(new Statement(84000L,1,"return true"));
		sList.add(new Statement(85000L,0,"}"));

		return WriteableUtil.merge(sList).getContent();
	}

}
