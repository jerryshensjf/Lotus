package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ConfigRsGenerator extends Generator{
	protected Domain sampleDomain;

	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}

	public ConfigRsGenerator(){
		super();
		super.fileName = "config.rs";
		super.standardName = "ConfigRs";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,0,"use serde::Deserialize;"));
		sList.add(new Statement(serial+2000L,0,"use std::fs;"));
		sList.add(new Statement(serial+3000L,0,""));
		sList.add(new Statement(serial+4000L,0,"#[derive(Deserialize)]"));
		sList.add(new Statement(serial+5000L,0,"struct AppConfig {"));
		sList.add(new Statement(serial+6000L,1,"url: String,"));
		sList.add(new Statement(serial+7000L,1,"port: u16,"));
		sList.add(new Statement(serial+8000L,0,"}"));
		sList.add(new Statement(serial+9000L,0,"#[derive(Deserialize)]"));
		sList.add(new Statement(serial+10000L,0,"struct DaoConfig {"));
		sList.add(new Statement(serial+11000L,1,"user: String,"));
		sList.add(new Statement(serial+12000L,1,"password: String,"));
		sList.add(new Statement(serial+13000L,1,"address: String,"));
		sList.add(new Statement(serial+14000L,1,"database: String,"));
		sList.add(new Statement(serial+15000L,0,"}"));
		sList.add(new Statement(serial+16000L,0,"#[derive(Deserialize)]"));
		sList.add(new Statement(serial+17000L,0,"pub struct Config {"));
		sList.add(new Statement(serial+18000L,1,"app: AppConfig,"));
		sList.add(new Statement(serial+19000L,1,"dao: DaoConfig,"));
		sList.add(new Statement(serial+20000L,0,"}"));
		sList.add(new Statement(serial+21000L,0,""));
		sList.add(new Statement(serial+22000L,0,"impl Config {"));
		sList.add(new Statement(serial+23000L,1,"pub fn from_file(path: &'static str) -> Self {"));
		sList.add(new Statement(serial+24000L,2,"let config = fs::read_to_string(path).unwrap();"));
		sList.add(new Statement(serial+25000L,2,"serde_json::from_str(&config).unwrap()"));
		sList.add(new Statement(serial+26000L,1,"}"));
		sList.add(new Statement(serial+27000L,0,""));
		sList.add(new Statement(serial+28000L,1,"pub fn get_app_url(&self) -> String {"));
		sList.add(new Statement(serial+29000L,2,"format!(\"{0}:{1}\", self.app.url, self.app.port)"));
		sList.add(new Statement(serial+30000L,1,"}"));
		sList.add(new Statement(serial+31000L,0,""));
		sList.add(new Statement(serial+32000L,1,"pub fn get_database_url(&self) -> String {"));
		sList.add(new Statement(serial+33000L,2,"format!("));
		if ("MariaDB".equalsIgnoreCase(sampleDomain.getDbType())||"MySQL".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(serial+34000L,3,"\"mysql://{0}:{1}@{2}/{3}\","));
			sList.add(new Statement(serial+35000L,3,"self.dao.user, self.dao.password, self.dao.address, self.dao.database"));
		} else if ("PostgreSQL".equalsIgnoreCase(sampleDomain.getDbType())||"pgsql".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(serial+34000L,3,"\"postgres://{0}:{1}@{2}:5432/{3}\","));
			sList.add(new Statement(serial+35000L,3,"self.dao.user, self.dao.password, self.dao.address, self.dao.database"));
		} else if ("Oracle".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(serial+34000L,3,"\"//{0}/{1}\","));
			sList.add(new Statement(serial+35000L,3,"self.dao.address, self.dao.database"));
		}		
		sList.add(new Statement(serial+36000L,2,")"));
		sList.add(new Statement(serial+37000L,1,"}"));
		
		sList.add(new Statement(serial+39000L,0,""));
		sList.add(new Statement(serial+40000L,1,"pub fn get_database_user(&self) -> String {"));
		sList.add(new Statement(serial+41000L,2,"self.dao.user.to_string()"));
		sList.add(new Statement(serial+42000L,1,"}"));
		sList.add(new Statement(serial+43000L,0,""));
		sList.add(new Statement(serial+44000L,1,"pub fn get_database_password(&self) -> String {"));
		sList.add(new Statement(serial+45000L,2,"self.dao.password.to_string()"));
		sList.add(new Statement(serial+46000L,1,"}"));
		
		sList.add(new Statement(serial+47000L,0,"}"));

		return WriteableUtil.merge(sList);
	}

}
