package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class CountNumGenerator extends Generator{
	protected Domain sampleDomain;

	public CountNumGenerator(){
		super();
		super.fileName = "count_num.rs";
		super.standardName = "CountNum";
	}
	
	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}


	@Override
	public StatementList generateStatementList() {
		String dbRow = "MySqlRow";
		if ("PostgreSQL".equalsIgnoreCase(sampleDomain.getDbType())||"pgsql".equalsIgnoreCase(sampleDomain.getDbType())) {
			dbRow = "PgRow";
		} else if ("Oracle".equalsIgnoreCase(sampleDomain.getDbType())) {
			dbRow = "Row";
		}
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(500L,0,"#![allow(dead_code)]"));
		if ("MariaDB".equalsIgnoreCase(sampleDomain.getDbType())||"MySQL".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(1000L,0,"use sqlx::mysql::MySqlRow;"));
			sList.add(new Statement(2000L,0,"use sqlx::{FromRow, Row};"));
			sList.add(new Statement(2500L,0,"use sqlx::Error;"));
		}else if ("PostgreSQL".equalsIgnoreCase(sampleDomain.getDbType())||"pgsql".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(1000L,0,"use sqlx::postgres::PgRow;"));
			sList.add(new Statement(2000L,0,"use sqlx::{FromRow, Row};"));
			sList.add(new Statement(2500L,0,"use sqlx::Error;"));
		}else if ("Oracle".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(1000L,0,"use oracle::Row;"));
			sList.add(new Statement(2500L,0,"use oracle::Error;"));
			sList.add(new Statement(2500L,0,"use crate::"+this.sampleDomain.getDaoimplSuffix()+"::db_context::FromRow;"));
		}
		
		sList.add(new Statement(3000L,0,""));
		sList.add(new Statement(4000L,0,"pub struct CountNum{"));
		sList.add(new Statement(5000L,1,"pub count_num: i64,"));
		sList.add(new Statement(6000L,0,"}"));
		sList.add(new Statement(7000L,0,""));
		sList.add(new Statement(8000L,0,"impl<'c> FromRow<'c, "+dbRow+"> for CountNum {"));
		sList.add(new Statement(9000L,1,"fn from_row(row: &"+dbRow+") -> Result<CountNum, Error> {"));
		sList.add(new Statement(10000L,2,"Ok(CountNum{"));
		if ("Oracle".equalsIgnoreCase(this.sampleDomain.getDbType())) {
			sList.add(new Statement(11000L,3,"count_num:row.get(0).unwrap(),"));
		} else {
			sList.add(new Statement(11000L,3,"count_num:row.get(0),"));
		}
		sList.add(new Statement(12000L,2,"})"));
		sList.add(new Statement(13000L,1,"}"));
		sList.add(new Statement(14000L,0,"}"));
		
		sList.add(new Statement(15000L,0,""));
		sList.add(new Statement(16000L,0,"pub fn get_image_base64(data:Option<Vec<u8>>)->String{"));
		sList.add(new Statement(17000L,1,"match data {"));
		sList.add(new Statement(18000L,2,"Some(data) => {"));
		sList.add(new Statement(19000L,3,"base64::encode(data)"));
		sList.add(new Statement(20000L,2,"},"));
		sList.add(new Statement(21000L,2,"None => \"\".to_string()"));
		sList.add(new Statement(22000L,1,"}"));
		sList.add(new Statement(23000L,0,"}"));
		
		sList.add(new Statement(24000L,0,""));
		sList.add(new Statement(25000L,0,"pub fn get_i64(data:Option<i64>)->i64{"));
		sList.add(new Statement(26000L,1,"match data {"));
		sList.add(new Statement(27000L,2,"Some(data) => data,"));
		sList.add(new Statement(28000L,2,"None => 0"));
		sList.add(new Statement(29000L,1,"}"));
		sList.add(new Statement(30000L,0,"}"));
		sList.add(new Statement(31000L,0,""));
		sList.add(new Statement(32000L,0,"pub fn get_i32(data:Option<i32>)->i32{"));
		sList.add(new Statement(33000L,1,"match data {"));
		sList.add(new Statement(34000L,2,"Some(data) => data,"));
		sList.add(new Statement(35000L,2,"None => 0"));
		sList.add(new Statement(36000L,1,"}"));
		sList.add(new Statement(37000L,0,"}"));
		sList.add(new Statement(38000L,0,""));
		sList.add(new Statement(39000L,0,""));
		sList.add(new Statement(40000L,0,"pub fn get_f64(data:Option<f64>)->f64{"));
		sList.add(new Statement(41000L,1,"match data {"));
		sList.add(new Statement(42000L,2,"Some(data) => data,"));
		sList.add(new Statement(43000L,2,"None => 0.0"));
		sList.add(new Statement(44000L,1,"}"));
		sList.add(new Statement(45000L,0,"}"));
		sList.add(new Statement(46000L,0,""));
		sList.add(new Statement(47000L,0,"pub fn get_f32(data:Option<f32>)->f32{"));
		sList.add(new Statement(48000L,1,"match data {"));
		sList.add(new Statement(49000L,2,"Some(data) => data,"));
		sList.add(new Statement(50000L,2,"None => 0.0"));
		sList.add(new Statement(51000L,1,"}"));
		sList.add(new Statement(52000L,0,"}"));
		sList.add(new Statement(53000L,0,""));
		sList.add(new Statement(54000L,0,"pub fn get_string(data:Option<String>)->String{"));
		sList.add(new Statement(55000L,1,"match data {"));
		sList.add(new Statement(56000L,2,"Some(data) => data,"));
		sList.add(new Statement(57000L,2,"None => \"\".to_string()"));
		sList.add(new Statement(58000L,1,"}"));
		sList.add(new Statement(59000L,0,"}"));
		sList.add(new Statement(60000L,0,""));
		sList.add(new Statement(61000L,0,"pub fn get_bool(data:Option<bool>)->bool{"));
		sList.add(new Statement(62000L,1,"match data {"));
		sList.add(new Statement(63000L,2,"Some(data) => data,"));
		sList.add(new Statement(64000L,2,"None => false"));
		sList.add(new Statement(65000L,1,"}"));
		sList.add(new Statement(66000L,0,"}"));
		sList.add(new Statement(67000L,0,""));
		sList.add(new Statement(68000L,0,"pub fn get_dropdown(data:Option<i64>)->i64{"));
		sList.add(new Statement(69000L,1,"match data {"));
		sList.add(new Statement(70000L,2,"Some(data) => {"));
		sList.add(new Statement(71000L,3,"if data <= 0 { return -1;}"));
		sList.add(new Statement(72000L,3,"else { return data;}"));
		sList.add(new Statement(73000L,2,"},"));
		sList.add(new Statement(74000L,2,"None => -1"));
		sList.add(new Statement(75000L,1,"}"));
		sList.add(new Statement(76000L,0,"}"));
		
		return WriteableUtil.merge(sList);
	}

}
