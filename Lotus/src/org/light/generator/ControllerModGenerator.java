package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ControllerModGenerator extends Generator{
	protected boolean containsAuth = false;
	protected List<Domain> domains = new ArrayList<>();
	
	public boolean isContainsAuth() {
		return containsAuth;
	}

	public void setContainsAuth(boolean containsAuth) {
		this.containsAuth = containsAuth;
	}
	
	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}

	public ControllerModGenerator(){
		super();
		super.fileName = "mod.rs";
		super.standardName = "ControllerMod";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		for (Domain domain : this.getDomains()) {
			sList.add(new Statement(serial,0,"pub mod "+domain.getSnakeDomainName()+"_"+StringUtil.getSnakeName(domain.getControllerNamingSuffix())+";"));
			serial += 1000L;
		}
		if (containsAuth) {
			Domain domain = domains.get(0);
			sList.add(new Statement(serial,0,"pub mod login_"+StringUtil.getSnakeName(domain.getControllerNamingSuffix())+";"));
			sList.add(new Statement(serial+1000L,0,"pub mod profile_"+StringUtil.getSnakeName(domain.getControllerNamingSuffix())+";"));
		}
		return WriteableUtil.merge(sList);
	}

}
