package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ConfigJsonGenerator extends Generator{
	protected String dbname;
	protected String dbusername;
	protected String dbpassword;
	protected Domain sampleDomain;

	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getDbusername() {
		return dbusername;
	}

	public void setDbusername(String dbusername) {
		this.dbusername = dbusername;
	}

	public String getDbpassword() {
		return dbpassword;
	}

	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}

	public ConfigJsonGenerator(){
		super();
		super.fileName = "config.json";
		super.standardName = "ConfigJson";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(1000L,0,"{"));
		sList.add(new Statement(2000L,0,"\"app\": {"));
		sList.add(new Statement(3000L,1,"\"url\": \"127.0.0.1\","));
		sList.add(new Statement(4000L,1,"\"port\": 8082"));
		sList.add(new Statement(5000L,0,"},"));
		sList.add(new Statement(6000L,0,"\"dao\":"));
		sList.add(new Statement(7000L,0,"{"));
		sList.add(new Statement(8000L,1,"\"user\": \""+this.dbusername+"\","));
		sList.add(new Statement(9000L,1,"\"password\": \""+this.dbpassword+"\","));
		if ("PostgreSQL".equalsIgnoreCase(this.sampleDomain.getDbType())||"pgsql".equalsIgnoreCase(this.sampleDomain.getDbType())) {
			sList.add(new Statement(10000L,1,"\"address\": \"127.0.0.1\","));
		}else {
			sList.add(new Statement(10000L,1,"\"address\": \"localhost\","));
		}
		sList.add(new Statement(11000L,1,"\"database\": \""+this.dbname+"\""));
		sList.add(new Statement(12000L,0,"}"));
		sList.add(new Statement(13000L,0,"}"));

		return WriteableUtil.merge(sList);
	}

}
