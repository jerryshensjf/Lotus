package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;

public class ServiceModGenerator extends Generator{
	protected List<Domain> domains = new ArrayList<>();
	protected boolean containsAuth = false;
	
	public boolean isContainsAuth() {
		return containsAuth;
	}

	public void setContainsAuth(boolean containsAuth) {
		this.containsAuth = containsAuth;
	}

	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}

	public ServiceModGenerator(){
		super();
		super.fileName = "mod.rs";
		super.standardName = "ServiceMod";
	}

	@Override
	public StatementList generateStatementList() {
		Domain sampleDomain = this.domains.get(0);
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"use std::sync::{Arc, Mutex};"));
		sList.add(new Statement(3000L,0,"use crate::AppState;"));
		sList.add(new Statement(4000L,0,"use crate::config::Config;"));
		sList.add(new Statement(5000L,0,"use async_static::async_static;"));
		sList.add(new Statement(6000L,0,"use crate::"+sampleDomain.getDaoimplSuffix()+"::Database;"));
		sList.add(new Statement(7000L,0,""));
		if (containsAuth) {
			sList.add(new Statement(8000L,0,"pub mod login_service;"));
		}
		long serial = 9000L;
		for (Domain domain : this.getDomains()) {
			sList.add(new Statement(serial,0,"pub mod "+domain.getSnakeDomainName()+"_service"+";"));
			serial += 1000L;
		}
		sList.add(new Statement(serial+1000L,0,""));
		sList.add(new Statement(serial+2000L,0,"async_static! {"));
		sList.add(new Statement(serial+3000L,2,"static ref APP_STATE:AppState<'static> = init_db_inner().await;"));
		sList.add(new Statement(serial+4000L,0,"}"));
		sList.add(new Statement(serial+5000L,0,""));
		sList.add(new Statement(serial+6000L,0,"pub async fn init_db() -> &'static AppState<'static> {"));
		sList.add(new Statement(serial+7000L,1,"let app_state = APP_STATE.await;"));
		sList.add(new Statement(serial+8000L,1,"return app_state;"));
		sList.add(new Statement(serial+9000L,0,"}"));
		sList.add(new Statement(serial+10000L,0,""));
		sList.add(new Statement(serial+11000L,0,"pub async fn init_db_inner() -> AppState<'static>{"));
		sList.add(new Statement(serial+12000L,1,"let config_file: &'static str = \"config.json\";"));
		sList.add(new Statement(serial+13000L,1,"let config = Config::from_file(config_file);"));
		if ("Oracle".equalsIgnoreCase(sampleDomain.getDbType())) {
			sList.add(new Statement(serial+14000L,1,"let db_context = Database::new(&config.get_database_user(),&config.get_database_password(),&config.get_database_url()).await;"));
		}else {
			sList.add(new Statement(serial+14000L,1,"let db_context = Database::new(&config.get_database_url()).await;"));
		}
		sList.add(new Statement(serial+15000L,0,""));
		sList.add(new Statement(serial+16000L,1,"let app_state = AppState {"));
		sList.add(new Statement(serial+17000L,2,"connections: Mutex::new(0),"));
		sList.add(new Statement(serial+18000L,2,"context: Arc::new(db_context),"));
		sList.add(new Statement(serial+19000L,1,"};"));
		sList.add(new Statement(serial+20000L,1,"app_state"));
		sList.add(new Statement(serial+21000L,0,"}"));
		return WriteableUtil.merge(sList);
	}

}
