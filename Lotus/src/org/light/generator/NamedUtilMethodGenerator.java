package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.utils.WriteableUtil;

public class NamedUtilMethodGenerator {
	public static Method generateStoreImageFunc(Domain domain) {
		Method method = new Method();
		method.setStandardName("Store" + domain.getCapFirstDomainName()+"Picture");
		method.addSignature(new Signature(1,"key","String"));
		method.addSignature(new Signature(2,"photo","String"));
		method.setSync(true);
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+100L,1,"TEMP_"+domain.getSnakeDomainName().toUpperCase()+"_IMAGES.lock().unwrap().insert(key,photo);"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	
	public static Method generatePickImageFunc(Domain domain) {
		Method method = new Method();
		method.setStandardName("Pick" + domain.getCapFirstDomainName()+"Picture");
		method.addSignature(new Signature(1,"key","String"));
		method.setReturnType(new Type("String"));
		method.setSync(true);
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let ret:Option<String> = TEMP_"+domain.getSnakeDomainName().toUpperCase()+"_IMAGES.lock().unwrap().get(&key).cloned();"));
		sList.add(new Statement(serial+2000L,1,"TEMP_"+domain.getSnakeDomainName().toUpperCase()+"_IMAGES.lock().unwrap().insert(key,\"\".to_string());"));
		sList.add(new Statement(serial+3000L,1,"if ret.is_none() {"));
		sList.add(new Statement(serial+4000L,2,"return \"\".to_string();"));
		sList.add(new Statement(serial+5000L,1,"}"));
		sList.add(new Statement(serial+6000L,1,"ret.as_ref().unwrap().to_string()"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	
	public static Method generateImageLazyStaticMac(Domain domain) {
		Method method = new Method();
		method.setStandardName("ImageLazyStaticMac");
		method.setNoContainer(true);
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,0,"lazy_static! {"));
		sList.add(new Statement(serial+2000L,1,"static ref TEMP_"+domain.getSnakeDomainName().toUpperCase()+"_IMAGES:Mutex<HashMap<String,String>> = Mutex::new(HashMap::new());"));
		sList.add(new Statement(serial+3000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	
}
