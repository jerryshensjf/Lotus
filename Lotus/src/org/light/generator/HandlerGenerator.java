package org.light.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.AxumController;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class HandlerGenerator{
	protected String fileName = "handler.go";
	protected Domain sampleDomain;
	
	public HandlerGenerator(){
		this.fileName = "handler.go";
	}
	
	public String generateString() throws ValidateException{
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"package "+this.sampleDomain.getControllerSuffix()));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import ("));
		sList.add(new Statement(4000L,1,"\"net/http\""));
		sList.add(new Statement(5000L,1,"\""+this.sampleDomain.getProjectName()+"/pkg/errno\""));
		sList.add(new Statement(6000L,1,"\"github.com/gin-gonic/gin\""));
		sList.add(new Statement(7000L,0,")"));
		sList.add(new Statement(8000L,0,""));
		sList.add(new Statement(9000L,0,"type Response struct {"));
		sList.add(new Statement(10000L,1,"Code    int         `json:\"code\"`"));
		sList.add(new Statement(11000L,1,"Message string      `json:\"message\"`"));
		sList.add(new Statement(12000L,1,"Data    interface{} `json:\"rows\"`"));
		sList.add(new Statement(13000L,1,"Total   int         `json:\"total\"`"));
		sList.add(new Statement(14000L,1,"Success bool        `json:\"success\"`"));
		sList.add(new Statement(15000L,0,"}"));
		sList.add(new Statement(16000L,0,""));
		sList.add(new Statement(17000L,0,"func SendResponse(c *gin.Context, err error, data interface{}, total int, success bool) {"));
		sList.add(new Statement(18000L,1,"code, message := errno.DecodeErr(err)"));
		sList.add(new Statement(19000L,0,""));
		sList.add(new Statement(20000L,1,"// always return http.StatusOK"));
		sList.add(new Statement(21000L,1,"c.JSON(http.StatusOK, Response{"));
		sList.add(new Statement(22000L,2,"Code:    code,"));
		sList.add(new Statement(23000L,2,"Message: message,"));
		sList.add(new Statement(24000L,2,"Data:    data,"));
		sList.add(new Statement(25000L,2,"Success: success,"));
		sList.add(new Statement(26000L,2,"Total:   total,"));
		sList.add(new Statement(27000L,1,"})"));
		sList.add(new Statement(28000L,0,"}"));
	
		return WriteableUtil.merge(sList).getContent();
	}

	public Domain getSampleDomain() {
		return sampleDomain;
	}

	public void setSampleDomain(Domain sampleDomain) {
		this.sampleDomain = sampleDomain;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
