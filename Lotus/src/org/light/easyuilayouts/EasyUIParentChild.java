package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.ChildDatagrid;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUIParentChild extends EasyUILayout{
	protected Domain parentDomain;
	protected Domain childDomain;
	protected Field parentId;

	protected SearchPanel searchPanel;
	protected MainDatagrid mainDatagrid;
	protected AddDialog addDialog;
	protected UpdateDialog updateDialog;
	protected ViewDialog viewDialog;
	
	protected ChildDatagrid childDatagrid;
	protected AddDialog addChildDialog;
	protected UpdateDialog updateChildDialog;
	protected ViewDialog viewChildDialog;

	@Override
	public StatementList generateLayoutStatements() throws ValidateException {
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = searchPanel.generateWidgetStatements();
		if (sl1 != null) sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		if (sl2 != null) sl2.setSerial(5000L);
		StatementList sl20 = childDatagrid.generateWidgetStatements();
		if (sl20 != null) sl20.setSerial(5500L);
		StatementList sl3 = addDialog.generateWidgetStatements();
		if (sl3 != null) sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		if (sl4 != null) sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		if (sl5!=null)  sl5.setSerial(8000L);
		if (sl1!=null) sList.add(sl1);
		if (sl2!=null) sList.add(sl2);
		if (sl20!=null) sList.add(sl20);
		if (sl3!=null) sList.add(sl3);
		if (sl3!=null) sList.add(sl3);
		if (sl4!=null) sList.add(sl5);		
		
		StatementList sl30 = addChildDialog.generateWidgetStatements();
		if (sl30!=null) sl30.setSerial(10000L);
		StatementList sl40 = updateChildDialog.generateWidgetStatements();
		if (sl40!=null) sl40.setSerial(11000L);
		StatementList sl50 = viewChildDialog.generateWidgetStatements();
		if (sl50!=null) sl50.setSerial(12000L);

		if (sl30!=null) sList.add(sl30);
		if (sl40!=null) sList.add(sl40);
		if (sl50!=null) sList.add(sl50);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(1000L);
		sl1.setIndent(1);
		sList.add(sl1);
		
		StatementList sl2 = childDatagrid.generateWidgetScriptStatements();
		sl2.setSerial(2000L);
		sl2.setIndent(1);
		sList.add(sl2);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.parentDomain!=null&&this.childDomain!=null) {
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new MainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();
			
			this.childDatagrid = new ChildDatagrid();
			this.childDatagrid.setParentDomain(this.parentDomain);
			this.childDatagrid.setChildDomain(this.childDomain);
			this.childDatagrid.setParentId(this.parentId);
			this.addChildDialog = new AddDialog();
			this.updateChildDialog = new UpdateDialog();
			this.viewChildDialog = new ViewDialog();
			this.addChildDialog.setDetailPrefix("detail");
			this.updateChildDialog.setDetailPrefix("detail");
			this.viewChildDialog.setDetailPrefix("detail");

			this.searchPanel.setDomain(this.parentDomain);
			this.mainDatagrid.setDomain(this.parentDomain);
			this.addDialog.setDomain(this.parentDomain);
			this.updateDialog.setDomain(this.parentDomain);
			this.viewDialog.setDomain(this.parentDomain);
			
			this.childDatagrid.setDomain(this.childDomain);
			this.addChildDialog.setDomain(this.childDomain);
			this.updateChildDialog.setDomain(this.childDomain);
			this.viewChildDialog.setDomain(this.childDomain);
			return true;
		}
		return false;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public MainDatagrid getMainDatagrid() {
		return mainDatagrid;
	}

	public void setMainDatagrid(MainDatagrid mainDatagrid) {
		this.mainDatagrid = mainDatagrid;
	}

	public AddDialog getAddDialog() {
		return addDialog;
	}

	public void setAddDialog(AddDialog addDialog) {
		this.addDialog = addDialog;
	}

	public UpdateDialog getUpdateDialog() {
		return updateDialog;
	}

	public void setUpdateDialog(UpdateDialog updateDialog) {
		this.updateDialog = updateDialog;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

	public ChildDatagrid getChildDatagrid() {
		return childDatagrid;
	}

	public void setChildDatagrid(ChildDatagrid childDatagrid) {
		this.childDatagrid = childDatagrid;
	}

	public AddDialog getAddChildDialog() {
		return addChildDialog;
	}

	public void setAddChildDialog(AddDialog addChildDialog) {
		this.addChildDialog = addChildDialog;
	}

	public UpdateDialog getUpdateChildDialog() {
		return updateChildDialog;
	}

	public void setUpdateChildDialog(UpdateDialog updateChildDialog) {
		this.updateChildDialog = updateChildDialog;
	}

	public ViewDialog getViewChildDialog() {
		return viewChildDialog;
	}

	public void setViewChildDialog(ViewDialog viewChildDialog) {
		this.viewChildDialog = viewChildDialog;
	}

	public Field getParentId() {
		return parentId;
	}

	public void setParentId(Field parentId) {
		this.parentId = parentId;
	}

}
