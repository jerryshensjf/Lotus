package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.TreePanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUITreeGrid extends EasyUILayout{
	protected Domain parentTreeDomain;
	protected Domain childDomain;
	protected Field innerTreeParentId;
	protected Field treeParentId;
	protected TreePanel tree;
	protected SearchPanel searchPanel;
	protected MainDatagrid mainDatagrid;
	protected AddDialog addDialog;
	protected UpdateDialog updateDialog;
	protected ViewDialog viewDialog;

	@Override
	public StatementList generateLayoutStatements() throws ValidateException {
		List<Writeable> sList = new ArrayList<>();
		sList.add(new Statement(1000L,0,"<div style=\"width:15%;float:left\">"));
		StatementList sl = tree.generateWidgetStatements();
		if (sl!=null) sl.setSerial(2000L);
		if (sl!=null) sl.setIndent(1);
		if (sl!=null) sList.add(sl);
		sList.add(new Statement(3000L,0,"</div>"));
		sList.add(new Statement(4000L,0,"<div style=\"width:85%;float:right;\">"));
		StatementList sl1 = searchPanel.generateWidgetStatements();
		if (sl1!=null) sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		if (sl2!=null) sl2.setSerial(5000L);		
		StatementList sl3 = addDialog.generateWidgetStatements();
		if (sl3!=null) sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		if (sl4!=null) sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		if (sl5!=null) sl5.setSerial(8000L);
		if (sl1!=null) sList.add(sl1);
		if (sl2!=null) sList.add(sl2);
		if (sl3!=null) sList.add(sl3);
		if (sl4!=null) sList.add(sl4);
		if (sl5!=null) sList.add(sl5);	
		sList.add(new Statement(9000L,0,"</div>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl = tree.generateWidgetScriptStatements();
		sl.setSerial(1000L);
		sl.setIndent(1);
		sList.add(sl);
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(2000L);
		sl1.setIndent(1);
		sList.add(sl1);
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.parentTreeDomain!=null&&this.childDomain!=null) {
			this.tree = new TreePanel();
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new MainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();
			
			this.tree.setParentTreeDomain(this.parentTreeDomain);
			this.tree.setChildDomain(this.childDomain);
			this.tree.setInnerTreeParentId(this.innerTreeParentId);
			this.tree.setTreeParentId(this.treeParentId);
			this.searchPanel.setDomain(this.childDomain);
			this.mainDatagrid.setDomain(this.childDomain);
			this.addDialog.setDomain(this.childDomain);
			this.updateDialog.setDomain(this.childDomain);
			this.viewDialog.setDomain(this.childDomain);
			return true;
		}
		return false;
	}

	public TreePanel getTree() {
		return tree;
	}

	public void setTree(TreePanel tree) {
		this.tree = tree;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public AddDialog getAddDialog() {
		return addDialog;
	}

	public void setAddDialog(AddDialog addDialog) {
		this.addDialog = addDialog;
	}

	public UpdateDialog getUpdateDialog() {
		return updateDialog;
	}

	public void setUpdateDialog(UpdateDialog updateDialog) {
		this.updateDialog = updateDialog;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

	public MainDatagrid getMainDatagrid() {
		return mainDatagrid;
	}

	public void setMainDatagrid(MainDatagrid mainDatagrid) {
		this.mainDatagrid = mainDatagrid;
	}


	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	public Domain getParentTreeDomain() {
		return parentTreeDomain;
	}

	public void setParentTreeDomain(Domain parentTreeDomain) {
		this.parentTreeDomain = parentTreeDomain;
	}

	public Field getTreeParentId() {
		return treeParentId;
	}

	public void setTreeParentId(Field treeParentId) {
		this.treeParentId = treeParentId;
	}

	public Field getInnerTreeParentId() {
		return innerTreeParentId;
	}

	public void setInnerTreeParentId(Field innerTreeParentId) {
		this.innerTreeParentId = innerTreeParentId;
	}
}
