package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.utils.StringUtil;
import org.light.core.Writeable;
import org.light.core.Widget;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;
import org.light.verb.ListActive;
import org.light.verb.SearchByFieldsByPage;

public class TreePanel extends Widget{
	protected Domain parentTreeDomain;
	protected Domain childDomain;
	protected Field innerTreeParentId;
	protected Field treeParentId;
	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<ul id=\"tt\"></ul>"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public StatementList generateWidgetScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<Writeable>();
		if (this.parentTreeDomain == null) throw new ValidateException("TreePanel域对象为空！");
		SearchByFieldsByPage search = new SearchByFieldsByPage(this.childDomain);
		sList.add(new Statement(1000L,0,"function treeClick(node){"));
		if (this.treeParentId instanceof Dropdown) {
			sList.add(new Statement(2000L,1,"$(\"#ffsearch\").find(\"#"+this.treeParentId.getLowerFirstFieldName()+"\").combobox({"));
			sList.add(new Statement(3000L,2,"onLoadSuccess:function(){"));
			sList.add(new Statement(4000L,3,"$(\"#ffsearch\").find(\"#"+this.treeParentId.getLowerFirstFieldName()+"\").combobox('select',node.id);//菜单项可以text或者id"));
		}else {
			sList.add(new Statement(2000L,1,"$(\"#ffsearch\").find(\"#"+this.treeParentId.getLowerFirstFieldName()+"\").textbox(\"setValue\",node.id);"));
		}
		try {
			sList.add(new Statement(5000L,3,search.generateEasyUIJSActionMethod().getStandardName()+"();"));
		}catch (Exception e) {
			 throw new ValidateException("SearchByFieldsByPage动词算子有错！");
		}
		sList.add(new Statement(6000L,2,"}"));
		sList.add(new Statement(7000L,1,"});"));
		sList.add(new Statement(8000L,0,"}"));
		sList.add(new Statement(9000L,0,"$(function(){	  "));
		sList.add(new Statement(10000L,1,"$(\"#tt\").tree({"));
		sList.add(new Statement(11000L,2,"onClick:function(node){"));
		sList.add(new Statement(12000L,3,"treeClick(node);"));
		sList.add(new Statement(13000L,2,"}"));
		sList.add(new Statement(14000L,1,"});"));
		sList.add(new Statement(15000L,2,""));
		sList.add(new Statement(16000L,0,"    var addr_tree = $(\"#tt\").tree({  "));
		sList.add(new Statement(17000L,0,"        url:'',  "));
		sList.add(new Statement(18000L,0,"        method:\"post\",  "));
		sList.add(new Statement(19000L,0,"        onSelect:function(node){},  "));
		sList.add(new Statement(20000L,0,"        onLoadSuccess:function(node,data){  "));
		sList.add(new Statement(21000L,0,"           $(\"#tt li:eq(0)\").find(\"div\").addClass(\"tree-node-selected\");   //设置第一个节点高亮  "));
		sList.add(new Statement(22000L,0,"           var n = $(\"#tt\").tree(\"getSelected\");  "));
		sList.add(new Statement(23000L,0,"           if(n!=null){  "));
		sList.add(new Statement(24000L,0,"                $(\"#tt\").tree(\"select\",n.target);    //相当于默认点击了一下第一个节点，执行onSelect方法  "));
		sList.add(new Statement(25000L,0,"           		treeClick(n);"));
		sList.add(new Statement(26000L,3,"}  "));
		sList.add(new Statement(27000L,0,"        }  "));
		sList.add(new Statement(28000L,0,"    });  "));
		sList.add(new Statement(29000L,0,"}) "));
		sList.add(new Statement(30000L,1,""));
		sList.add(new Statement(31000L,0,""));
		sList.add(new Statement(32000L,0,""));
		ListActive listActive = new ListActive(this.parentTreeDomain);
		sList.add(new Statement(33000L,0,"$('#tt').tree({"));
		try {
			sList.add(new Statement(34000L,1,"url: '../"+this.parentTreeDomain.getControllerPackagePrefix()+this.parentTreeDomain.getLowerFirstDomainName()+this.parentTreeDomain.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(listActive.generateControllerMethod().getStandardName())+"',"));
		}catch (Exception e) {
			if ("english".equalsIgnoreCase(this.parentTreeDomain.getLanguage())) {
				throw new ValidateException("Verb operator belongs to TreeGrid has error.");
			} else {
				throw new ValidateException("TreeGrid所属动词算子有错！");
			}
		}
		sList.add(new Statement(35000L,1,"method:'post',"));
		sList.add(new Statement(36000L,1,"loadFilter: function(rows){"));
		sList.add(new Statement(37000L,2,"return convert(rows.rows);"));
		sList.add(new Statement(38000L,1,"}"));
		sList.add(new Statement(39000L,0,"});"));
		sList.add(new Statement(40000L,0,""));
		sList.add(new Statement(41000L,0,"function convert(rows){"));
		sList.add(new Statement(42000L,1,"function exists(rows, parentId){"));
		sList.add(new Statement(43000L,2,"for(var i=0; i<rows.length; i++){"));
		sList.add(new Statement(44000L,3,"if (rows[i]."+this.parentTreeDomain.getDomainId().getLowerFirstFieldName()+" == parentId ) return true;"));
		sList.add(new Statement(45000L,2,"}"));
		sList.add(new Statement(46000L,2,"return false;"));
		sList.add(new Statement(47000L,1,"}"));
		sList.add(new Statement(48000L,1,""));
		sList.add(new Statement(49000L,1,"var nodes = [];"));
		sList.add(new Statement(50000L,1,"// get the top level nodes"));
		sList.add(new Statement(51000L,1,"for(var i=0; i<rows.length; i++){"));
		sList.add(new Statement(52000L,2,"var row = rows[i];"));
		sList.add(new Statement(53000L,2,"if (!exists(rows, row."+this.innerTreeParentId.getLowerFirstFieldName()+")){"));
		sList.add(new Statement(54000L,3,"nodes.push({"));
		sList.add(new Statement(55000L,4,"id:row."+this.parentTreeDomain.getDomainId().getLowerFirstFieldName()+","));
		sList.add(new Statement(56000L,4,"text:row."+this.parentTreeDomain.getDomainName().getLowerFirstFieldName()));
		sList.add(new Statement(57000L,3,"});"));
		sList.add(new Statement(58000L,2,"}"));
		sList.add(new Statement(59000L,1,"}"));
		sList.add(new Statement(60000L,1,""));
		sList.add(new Statement(61000L,1,"var toDo = [];"));
		sList.add(new Statement(62000L,1,"for(var i=0; i<nodes.length; i++){"));
		sList.add(new Statement(63000L,2,"toDo.push(nodes[i]);"));
		sList.add(new Statement(64000L,1,"}"));
		sList.add(new Statement(65000L,1,"while(toDo.length){"));
		sList.add(new Statement(66000L,2,"var node = toDo.shift();	// the parent node"));
		sList.add(new Statement(67000L,2,"// get the children nodes"));
		sList.add(new Statement(68000L,2,"for(var i=0; i<rows.length; i++){"));
		sList.add(new Statement(69000L,3,"var row = rows[i];"));
		sList.add(new Statement(70000L,3,"if (row."+this.innerTreeParentId.getLowerFirstFieldName()+" == node.id){"));
		sList.add(new Statement(71000L,4,"var child = {id:row."+this.parentTreeDomain.getDomainId().getLowerFirstFieldName()+",text:row."+this.parentTreeDomain.getDomainName().getLowerFirstFieldName()+"};"));
		sList.add(new Statement(72000L,4,"if (node.children){"));
		sList.add(new Statement(73000L,5,"node.children.push(child);"));
		sList.add(new Statement(74000L,4,"} else {"));
		sList.add(new Statement(75000L,5,"node.children = [child];"));
		sList.add(new Statement(76000L,4,"}"));
		sList.add(new Statement(77000L,4,"toDo.push(child);"));
		sList.add(new Statement(78000L,3,"}"));
		sList.add(new Statement(79000L,2,"}"));
		sList.add(new Statement(80000L,1,"}"));
		sList.add(new Statement(81000L,1,"return nodes;"));
		sList.add(new Statement(82000L,0,"}"));
		
		sList.add(new Statement(83000L,0,"$(document).ready(function(){"));
		sList.add(new Statement(84000L,0,"checkAccess"+this.parentTreeDomain.getCapFirstDomainName()+"();"));
		sList.add(new Statement(85000L,0,"});"));
		
		return WriteableUtil.merge(sList);
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getParentTreeDomain() {
		return parentTreeDomain;
	}

	public void setParentTreeDomain(Domain parentTreeDomain) {
		this.parentTreeDomain = parentTreeDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}

	public Field getTreeParentId() {
		return treeParentId;
	}

	public void setTreeParentId(Field treeParentId) {
		this.treeParentId = treeParentId;
	}

	public Field getInnerTreeParentId() {
		return innerTreeParentId;
	}

	public void setInnerTreeParentId(Field innerTreeParentId) {
		this.innerTreeParentId = innerTreeParentId;
	}

}
