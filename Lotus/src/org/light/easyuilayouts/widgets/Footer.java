package org.light.easyuilayouts.widgets;

import org.light.core.Widget;
import org.light.domain.Statement;
import org.light.domain.StatementList;

public class Footer extends Widget{
	protected String footerStr = "火箭船软件工作室版权所有。作者电邮:jerry_shen_sjf@qq.com QQ群:277689737";

	@Override
	public StatementList generateWidgetStatements() {
		Statement s = new Statement(1000L,1,this.footerStr);
		StatementList sl = new StatementList();
		sl.add(s);
		sl.setSerial(this.serial);
		return sl;
	} 

	@Override
	public StatementList generateWidgetScriptStatements() {
		Statement s = new Statement(1000L,1,"");
		StatementList sl = new StatementList();
		sl.add(s);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getFooterStr() {
		return footerStr;
	}

	public void setFooterStr(String footerStr) {
		this.footerStr = footerStr;
	}

}
