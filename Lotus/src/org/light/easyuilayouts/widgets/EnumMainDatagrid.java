package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.complexverb.UpdateUploadDomainField;
import org.light.core.Widget;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.CheckAccess;
import org.light.verb.Export;
import org.light.verb.ExportPDF;
import org.light.verb.FilterExcel;
import org.light.verb.FilterPDF;
import org.light.verb.SearchByFieldsByPage;
import org.light.verb.View;

public class EnumMainDatagrid extends Widget{
	protected Domain domain;
	
	protected Set<Field> deniedFields = new TreeSet<>();
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}

	@Override
	public StatementList generateWidgetStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		if (this.domain.getLanguage().equalsIgnoreCase("english")){
			sList.add(new Statement(1000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
		}else {
			sList.add(new Statement(1000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
		}
		sList.add(new Statement(2000L,0,"<thead>"));
		sList.add(new Statement(3000L,0,"<tr>"));
		if (this.domain!=null&&this.domain.hasDomainId()) sList.add(new Statement(4000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
		serial = 5000L;
		List<Field> fields2 = new ArrayList<Field>();
		fields2.addAll(this.domain.getFieldsWithoutId());
		fields2.removeAll(deniedFields);
		fields2.sort(new FieldSerialComparator());
		for (Field f: fields2){
			if (f.getFieldType().equalsIgnoreCase("image")) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
			}else if ("datetime".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
			}else if ("date".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
			} else {
				if (!(f instanceof Dropdown)) {
						sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
				}
				if (f instanceof Dropdown) {
					sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>"));
				}
			}
			serial+=1000L;
		}
		sList.add(new Statement(serial,0,"</tr>"));
		sList.add(new Statement(serial+1000L,0,"</thead>"));
		sList.add(new Statement(serial+2000L,0,"</table>"));
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		try {			
			List<Domain> translateDomains = new ArrayList<Domain>();
			for (Field f: this.domain.getPlainFields()){
				if (!deniedFields.contains(f)) {
					if (f instanceof Dropdown){
						Dropdown dp = (Dropdown)f;
						Domain target = dp.getTarget();
						if (!target.isLegacy()&&!DomainUtil.inDomainList(target, translateDomains)){
							translateDomains.add(target);
						}
					}
				}
			}
			
			List<Writeable> sList = new ArrayList<>();
			
			sList.add(new Statement(1000L,0,"var params = {};"));			
			sList.add(new Statement(2000L,0,"var pagesize = 10;"));
			sList.add(new Statement(3000L,0,"var pagenum = 1;"));
			
			long serial = 4000L;
			for (Domain d:translateDomains) {
				sList.add(new Statement(serial,0,"var translate"+d.getCapFirstPlural()+" = [];"));
				serial += 1000L;
			}
			
			Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
			for (Field f:this.domain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					domainFieldVerbs.add(new AddUploadDomainField(this.domain,f));
					domainFieldVerbs.add(new UpdateUploadDomainField(this.domain,f));
				}
			}

			if (domainFieldVerbs!=null&&domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var toolbar = ["));
			JavascriptBlock slViewJb = ((EasyUIPositions)new View(this.domain)).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
						
			JavascriptBlock slExportJb = ((EasyUIPositions)new Export(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportJb != null) {
				StatementList  slExport =slExportJb.getMethodStatementList();
				slExport.setSerial(serial+13900L);
				sList.add(slExport);			
				sList.add(new Statement(serial+13950L,0,","));
			}
			
			JavascriptBlock slExportPDFJb = ((EasyUIPositions)new ExportPDF(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportPDFJb != null) {
				StatementList  slExportPDF =slExportPDFJb.getMethodStatementList();
				slExportPDF.setSerial(serial+13960L);
				sList.add(slExportPDF);
				sList.add(new Statement(serial+13970L,0,","));
			}
					
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));
			
			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
			sList.add(new Statement(serial+16000L,0,"checkAccess"+this.domain.getCapFirstDomainName()+"();"));
			serial += 17000L;
			for (Domain d:translateDomains) {
				sList.add(new Statement(serial,0,"translate"+d.getCapFirstPlural()+" = translateListActive"+d.getCapFirstPlural()+"();"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"$(\"#dg\").datagrid(\"load\");"));			
			sList.add(new Statement(serial+1000L,0,"});"));
			
			sList.add(new Statement(serial+18000L,0,"function clearForm(formId){"));
			sList.add(new Statement(serial+19000L,0,"$('#'+formId).form('clear');"));
			sList.add(new Statement(serial+20000L,0,"}"));
			
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			JavascriptMethod slFilterExcelJm =  ((EasyUIPositions)new FilterExcel(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterExcelJm != null) {
				StatementList slFilterExcelAction = slFilterExcelJm.generateMethodStatementListIncludingContainer(serial+29600L,0);
				sList.add(slFilterExcelAction);
			}
			
			JavascriptMethod slFilterPDFJm =  ((EasyUIPositions)new FilterPDF(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterPDFJm != null) {
				StatementList slFilterPDFAction = slFilterPDFJm.generateMethodStatementListIncludingContainer(serial+29700L,0);
				sList.add(slFilterPDFAction);
			}
			
			JavascriptMethod slCheckAccessJm =  ((EasyUIPositions)new CheckAccess(this.domain)).generateEasyUIJSActionMethod();
			if(	slCheckAccessJm != null) {
				StatementList slCheckAccessAction = slCheckAccessJm.generateMethodStatementListIncludingContainer(serial+29800L,0);
				sList.add(slCheckAccessAction);
			}
	
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanIntMethod().generateMethodStatementList(serial+30500L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseIntNegMethod().generateMethodStatementList(serial+30700L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckRadioBoxValueMethod().generateMethodStatementList(serial+32000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateToggleBtnShowMethod().generateMethodStatementList(serial+33000L));
			if (this.domain.containsDateTime()) {
				sList.add(NamedS2SMJavascriptMethodGenerator.generateParseDateTimeMethod().generateMethodStatementList(serial+34000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateFormatDateTimeMethod().generateMethodStatementList(serial+35000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateParseDateMethod().generateMethodStatementList(serial+36000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateFormatDateMethod().generateMethodStatementList(serial+37000L));
			}
			
			serial = serial+38000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());

			for (Domain d:translateDomains) {
				Dropdown dp = new Dropdown();
				dp.setTarget(d);
				sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
				sList.add(dp.generateTranslateListActiveMethod().generateMethodStatementList(serial+500L));
				serial+=1000L;
			}

			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.domain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					
					UpdateUploadDomainField uudf = new UpdateUploadDomainField(this.domain,f);
					sList.add(uudf.generateEasyUIJSActionMethod().generateMethodStatementList(serial+1000L));
					serial+=2000L;
				}
			}
			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean parse() {
		return true;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}
