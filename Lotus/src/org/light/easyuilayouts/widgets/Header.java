package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.core.Widget;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Header extends Widget{
	protected String title = "Rust通用代码生成器：莲花 的生成结果";
	protected String subTitle = "自2014开始为公众服务";

	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<div style=\"float:right;clear:both;margin-top:15px;margin-right:20px\">"));
		sList.add(new Statement(2000L,1,"<a href=\"javascript:logout()\">注销</a>"));
		sList.add(new Statement(3000L,1,"</div>"));
		Statement s0 = new Statement(4000L,1,"<h2>"+this.title+"</h2>");
		Statement s1 = new Statement(5000L,1,"<h3>"+this.subTitle+"</h3>");		
		sList.add(s0);
		sList.add(s1);
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	} 
	
	public StatementList generateEnglishWidgetStatements() {
		String titleen = "'Generate results of LightSBMEU code generator.";
		String subTitleen = "Serving public since 2014";
		if (!StringUtil.isBlank(this.title)) titleen = this.title;
		if (!StringUtil.isBlank(this.subTitle)) subTitleen = this.subTitle;
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<div style=\"float:right;clear:both;margin-top:15px;margin-right:20px\">"));
		sList.add(new Statement(2000L,1,"<a href=\"javascript:logout()\">Logout</a>"));
		sList.add(new Statement(3000L,1,"</div>"));
		Statement s0 = new Statement(4000L,1,"<h2>"+titleen+"</h2>");
		Statement s1 = new Statement(5000L,1,"<h3>"+subTitleen+"</h3>");		
		sList.add(s0);
		sList.add(s1);
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	} 


	@Override
	public StatementList generateWidgetScriptStatements() {
		Statement s = new Statement();
		StatementList sl = new StatementList();
		sl.add(s);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

}
