package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.complexverb.UpdateUploadDomainField;
import org.light.core.Widget;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Activate;
import org.light.verb.ActivateAll;
import org.light.verb.Add;
import org.light.verb.CheckAccess;
import org.light.verb.Clone;
import org.light.verb.CloneAll;
import org.light.verb.Delete;
import org.light.verb.DeleteAll;
import org.light.verb.Export;
import org.light.verb.ExportPDF;
import org.light.verb.FilterExcel;
import org.light.verb.FilterPDF;
import org.light.verb.SearchByFieldsByPage;
import org.light.verb.SoftDelete;
import org.light.verb.SoftDeleteAll;
import org.light.verb.Toggle;
import org.light.verb.ToggleOne;
import org.light.verb.Update;
import org.light.verb.View;

public class MainDatagrid extends Widget{
	protected Domain domain;	
	protected Set<Field> deniedFields = new TreeSet<>();
	
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}

	@Override
	public StatementList generateWidgetStatements() throws ValidateException {
		List<Writeable> sList = new ArrayList<>();
		if (this.domain.getLanguage().equalsIgnoreCase("english")){
			sList.add(new Statement(1000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
		}else {
			sList.add(new Statement(1000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1600px;height:600px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
		}
		sList.add(new Statement(2000L,0,"<thead>"));
		sList.add(new Statement(3000L,0,"<tr>"));
		if (this.domain!=null&&this.domain.hasDomainId()) sList.add(new Statement(4000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
		serial = 5000L;
		List<Field> fields2 = new ArrayList<Field>();
		fields2.addAll(this.domain.getFieldsWithoutId());
		fields2.removeAll(deniedFields);
		fields2.sort(new FieldSerialComparator());
		for (Field f: fields2){
			if (f.getFieldType().equalsIgnoreCase("image")) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
			}else if ("datetime".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
			}else if ("date".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
			} else {
				if (!(f instanceof Dropdown)) {
						sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>"));
				}
				if (f instanceof Dropdown) {
					sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>"));
				}
			}
			serial+=1000L;
		}
		sList.add(new Statement(serial,0,"</tr>"));
		sList.add(new Statement(serial+1000L,0,"</thead>"));
		sList.add(new Statement(serial+2000L,0,"</table>"));
		StatementList sl = WriteableUtil.merge(sList);
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		try {			
			List<Domain> translateDomains = new ArrayList<Domain>();
			for (Field f: this.domain.getPlainFields()){
				if (!deniedFields.contains(f)) {
					if (f instanceof Dropdown){
						Dropdown dp = (Dropdown)f;
						Domain target = dp.getTarget();
						if (!target.isLegacy()&&!DomainUtil.inDomainList(target, translateDomains)){
							translateDomains.add(target);
						}
					}
				}
			}
			
			List<Writeable> sList = new ArrayList<>();
			
			sList.add(new Statement(1000L,0,"var params = {};"));			
			sList.add(new Statement(2000L,0,"var pagesize = 10;"));
			sList.add(new Statement(3000L,0,"var pagenum = 1;"));
			
			long serial = 4000L;
			for (Domain d:translateDomains) {
				sList.add(new Statement(serial,0,"var translate"+d.getCapFirstPlural()+" = [];"));
				serial += 1000L;
			}
			
			Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
			for (Field f:this.domain.getPlainFields()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					domainFieldVerbs.add(new AddUploadDomainField(this.domain,f));
					domainFieldVerbs.add(new UpdateUploadDomainField(this.domain,f));
				}
			}

			if (domainFieldVerbs!=null&&domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var toolbar = ["));
			JavascriptBlock slViewJb = ((EasyUIPositions)new View(this.domain)).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
			JavascriptBlock slAddJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Add(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slAddJb != null) {
				StatementList  slAdd = slAddJb.getMethodStatementList();			
				slAdd.setSerial(serial+13100L);
				sList.add(slAdd);
				sList.add(new Statement(serial+13150L,0,","));
			}
			
			JavascriptBlock slUpdateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Update(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slUpdateJb != null) {
				StatementList  slUpdate =slUpdateJb.getMethodStatementList();
				slUpdate.setSerial(serial+13200L);
				sList.add(slUpdate);
				sList.add(new Statement(serial+13250L,0,","));
			}
			
			JavascriptBlock slSoftDeleteJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteJb != null) {
				StatementList  slSoftDelete =slSoftDeleteJb.getMethodStatementList();
				slSoftDelete.setSerial(serial+13300L);
				sList.add(slSoftDelete);
				sList.add(new Statement(serial+13320L,0,","));
			}
			
			JavascriptBlock slActivateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateJb != null) {
				StatementList  slActivate =slActivateJb.getMethodStatementList();
				slActivate.setSerial(serial+13340L);
				sList.add(slActivate);
				sList.add(new Statement(serial+13342L,0,","));
			}	
			
			JavascriptBlock slCloneJb = this.domain.hasDomainIdAndDomainName()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneJb != null) {
				StatementList  slClone=slCloneJb.getMethodStatementList();
				slClone.setSerial(serial+13345L);
				sList.add(slClone);			
				sList.add(new Statement(serial+13350L,0,","));
			}
			
			JavascriptBlock slDeleteJb = this.domain.hasDomainId()?((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteJb != null) {
				StatementList  slDelete =slDeleteJb.getMethodStatementList();
				slDelete.setSerial(serial+13400L);
				sList.add(slDelete);
				sList.add(new Statement(serial+13450L,0,","));
			}
			
			JavascriptBlock slToggleJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleJb != null) {
				StatementList  slToggle =slToggleJb.getMethodStatementList();
				slToggle.setSerial(serial+13500L);
				sList.add(slToggle);
				sList.add(new Statement(serial+13550L,0,","));
			}
			
			JavascriptBlock slToggleOneJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleOneJb != null) {
				StatementList  slToggleOne =slToggleOneJb.getMethodStatementList();
				slToggleOne.setSerial(serial+13600L);
				sList.add(slToggleOne);
				sList.add(new Statement(serial+13650L,0,",'-',"));
			}
			
			JavascriptBlock slSoftDeleteAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteAllJb != null) {
				StatementList  slSoftDeleteAll =slSoftDeleteAllJb.getMethodStatementList();
				slSoftDeleteAll.setSerial(serial+13700L);
				sList.add(slSoftDeleteAll);
				sList.add(new Statement(serial+13710L,0,","));
			}
			
			JavascriptBlock slActivateAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateAllJb != null) {
				StatementList  slActivateAll =slActivateAllJb.getMethodStatementList();
				slActivateAll.setSerial(serial+13720L);
				sList.add(slActivateAll);			
				sList.add(new Statement(serial+13722L,0,","));
			}
			
			JavascriptBlock slCloneAllJb = this.domain.hasDomainIdAndDomainName()?((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneAllJb != null) {
				StatementList  slCloneAll =slCloneAllJb.getMethodStatementList();
				slCloneAll.setSerial(serial+13725L);
				sList.add(slCloneAll);			
				sList.add(new Statement(serial+13750L,0,","));
			}
			
			JavascriptBlock slDeleteAllJb = this.domain.hasDomainId()?((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteAllJb != null) {
				StatementList  slDeleteAll =slDeleteAllJb.getMethodStatementList();
				slDeleteAll.setSerial(serial+13800L);
				sList.add(slDeleteAll);				
				sList.add(new Statement(serial+13850L,0,","));
			}
			
			JavascriptBlock slExportJb = ((EasyUIPositions)new Export(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportJb != null) {
				StatementList  slExport =slExportJb.getMethodStatementList();
				slExport.setSerial(serial+13900L);
				sList.add(slExport);			
				sList.add(new Statement(serial+13950L,0,","));
			}
			
			JavascriptBlock slExportPDFJb = ((EasyUIPositions)new ExportPDF(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportPDFJb != null) {
				StatementList  slExportPDF =slExportPDFJb.getMethodStatementList();
				slExportPDF.setSerial(serial+13960L);
				sList.add(slExportPDF);
				sList.add(new Statement(serial+13970L,0,","));
			}
			
//			JavascriptBlock slExportWordJb = ((EasyUIPositions)new ExportWord(this.domain)).generateEasyUIJSButtonBlock();
//			if (slExportWordJb != null) {
//				StatementList  slExportWord =slExportWordJb.getMethodStatementList();
//				slExportWord.setSerial(serial+13980L);
//				sList.add(slExportWord);
//				sList.add(new Statement(serial+13990L,0,","));
//			}
			
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));
			
			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
			sList.add(new Statement(serial+16000L,0,"checkAccess"+this.domain.getCapFirstDomainName()+"();"));
			serial += 17000L;
			for (Domain d:translateDomains) {
				sList.add(new Statement(serial,0,"translate"+d.getCapFirstPlural()+" = translateListActive"+d.getCapFirstPlural()+"();"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"$(\"#dg\").datagrid(\"load\");"));			
			sList.add(new Statement(serial+1000L,0,"});"));
			
			sList.add(new Statement(serial+18000L,0,"function clearForm(formId){"));
			sList.add(new Statement(serial+19000L,0,"$('#'+formId).form('clear');"));
			sList.add(new Statement(serial+20000L,0,"}"));
			
			JavascriptMethod slAddJm = this.domain.hasActiveField()? ((EasyUIPositions)new Add(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slAddJm != null) {
				StatementList slAddAction = slAddJm.generateMethodStatementListIncludingContainer(serial+21000L,0);
				sList.add(slAddAction);
			}
			
			JavascriptMethod slUpdateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Update(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slUpdateJm != null) {
				StatementList slUpdateAction = slUpdateJm.generateMethodStatementListIncludingContainer(serial+22000L,0);
				sList.add(slUpdateAction);
			}
			
			JavascriptMethod slSoftDeleteJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteJm != null) {
				StatementList slSoftDeleteAction = slSoftDeleteJm.generateMethodStatementListIncludingContainer(serial+23000L,0);
				sList.add(slSoftDeleteAction);
			}
			
			JavascriptMethod slActivateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateJm != null) {
				StatementList slActivateAction = slActivateJm.generateMethodStatementListIncludingContainer(serial+24000L,0);
				sList.add(slActivateAction);
			}
			
			JavascriptMethod slCloneJm =  this.domain.hasDomainId()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneJm != null) {
				StatementList slCloneAction = slCloneJm.generateMethodStatementListIncludingContainer(serial+25000L,0);
				sList.add(slCloneAction);
			}
			
			JavascriptMethod slDeleteJm = this.domain.hasDomainId()? ((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteJm != null) {
				StatementList slDeleteAction = slDeleteJm.generateMethodStatementListIncludingContainer(serial+28000L,0);
				sList.add(slDeleteAction);
			}
			
			JavascriptMethod slToggleJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleJm != null) {
				StatementList slToggleAction = slToggleJm.generateMethodStatementListIncludingContainer(serial+28200L,0);
				sList.add(slToggleAction);
			}
			
			JavascriptMethod slToggleOneJm = this.domain.hasDomainId()&&this.domain.hasActiveField()? ((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleOneJm != null) {
				StatementList slToggleOneAction = slToggleOneJm.generateMethodStatementListIncludingContainer(serial+28400L,0);
				sList.add(slToggleOneAction);
			}
			
			JavascriptMethod slSoftDeleteAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteAllJm != null) {
				StatementList slSoftDeleteAllAction = slSoftDeleteAllJm.generateMethodStatementListIncludingContainer(serial+28600L,0);
				sList.add(slSoftDeleteAllAction);
			}
			
			JavascriptMethod slActivateAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateAllJm != null) {
				StatementList slActivateAllAction = slActivateAllJm.generateMethodStatementListIncludingContainer(serial+28800L,0);
				sList.add(slActivateAllAction);
			}
			
			JavascriptMethod slCloneAllJm = this.domain.hasDomainId()? ((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneAllJm != null) {
				StatementList slCloneAllAction = slCloneAllJm.generateMethodStatementListIncludingContainer(serial+29000L,0);
				sList.add(slCloneAllAction);
			}
			
			JavascriptMethod slDeleteAllJm =  this.domain.hasDomainId()? ((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteAllJm != null) {
				StatementList slDeleteAllAction = slDeleteAllJm.generateMethodStatementListIncludingContainer(serial+29200L,0);
				sList.add(slDeleteAllAction);
			}
			
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			JavascriptMethod slFilterExcelJm =  ((EasyUIPositions)new FilterExcel(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterExcelJm != null) {
				StatementList slFilterExcelAction = slFilterExcelJm.generateMethodStatementListIncludingContainer(serial+29600L,0);
				sList.add(slFilterExcelAction);
			}
			
			JavascriptMethod slFilterPDFJm =  ((EasyUIPositions)new FilterPDF(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterPDFJm != null) {
				StatementList slFilterPDFAction = slFilterPDFJm.generateMethodStatementListIncludingContainer(serial+29700L,0);
				sList.add(slFilterPDFAction);
			}
			
			JavascriptMethod slCheckAccessJm =  ((EasyUIPositions)new CheckAccess(this.domain)).generateEasyUIJSActionMethod();
			if(	slCheckAccessJm != null) {
				StatementList slCheckAccessAction = slCheckAccessJm.generateMethodStatementListIncludingContainer(serial+29800L,0);
				sList.add(slCheckAccessAction);
			}
	
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanIntMethod().generateMethodStatementList(serial+30500L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseIntNegMethod().generateMethodStatementList(serial+30700L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckRadioBoxValueMethod().generateMethodStatementList(serial+32000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateToggleBtnShowMethod().generateMethodStatementList(serial+33000L));
			if (this.domain.containsDateTime()) {
				sList.add(NamedS2SMJavascriptMethodGenerator.generateParseDateTimeMethod().generateMethodStatementList(serial+34000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateFormatDateTimeMethod().generateMethodStatementList(serial+35000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateParseDateMethod().generateMethodStatementList(serial+36000L));
				sList.add(NamedS2SMJavascriptMethodGenerator.generateFormatDateMethod().generateMethodStatementList(serial+37000L));
			}
			
			serial = serial+38000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());

			for (Domain d:translateDomains) {
				Dropdown dp = new Dropdown();
				dp.setTarget(d);
				sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
				sList.add(dp.generateTranslateListActiveMethod().generateMethodStatementList(serial+500L));
				serial+=1000L;
			}

			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.domain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					
					UpdateUploadDomainField uudf = new UpdateUploadDomainField(this.domain,f);
					sList.add(uudf.generateEasyUIJSActionMethod().generateMethodStatementList(serial+1000L));
					serial+=2000L;
				}
			}
			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean parse() {
		return true;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}
