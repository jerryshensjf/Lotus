package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Widget;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.utils.WriteableUtil;
import org.light.verb.Update;

public class UpdateDialog extends Widget{
	protected Domain domain;
	protected String detailPrefix = "";	
	protected Set<Field> deniedFields = new TreeSet<>();
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
	@Override
	public StatementList generateWidgetStatements() {
		try {
			List<Writeable> sList = new ArrayList<>();
			Update updateDialog = new Update(this.domain);
			if (!updateDialog.isDenied()&&this.domain.hasDomainId()&&this.domain.hasActiveField()) {
				if (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(1000L,0,"<div class=\"easyui-window\" title=\"Update "+this.domain.getText()+"\" id=\"w"+this.detailPrefix+"update"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				} else {
					sList.add(new Statement(1000L,0,"<div class=\"easyui-window\" title=\"编辑"+this.domain.getText()+"\" id=\"w"+this.detailPrefix+"update"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(2000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(3000L,0,"<form id=\""+this.detailPrefix+"ffedit\" method=\"post\">"));
				sList.add(new Statement(4000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
				sList.add(new Statement(5000L,0,"<table cellpadding=\"5\">"));
				serial = 6000L;
				List<Field> fields4 = new ArrayList<Field>();
				fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields4.removeAll(deniedFields);
				fields4.sort(new FieldSerialComparator());
				for (Field f: fields4){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../images/blank.jpg'><br>"));
						sList.add(new Statement(serial+500L,0,"<input id=\""+this.domain.getLowerFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
					} else if (f.getFieldType().equalsIgnoreCase("datetime")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-datetimebox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,parser:parseDateTime,formatter:formatDateTime\"/></td></tr>"));
					} else if (f.getFieldType().equalsIgnoreCase("date")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-datebox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,parser:parseDate,formatter:formatDate\"/></td></tr>"));
					} else {
						if (!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("bool"))&&!f.isTextarea()) sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
						if (!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("bool"))&&f.isTextarea()) sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>"));
						if (!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("bool")) sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
						if (f instanceof Dropdown)
							sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>"));
					}
					serial+=1000L;
				}
				if (this.domain!=null&& this.domain.hasActiveField()) sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">Edit</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#"+this.detailPrefix+"ffedit').form('clear');\">Clear</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#w"+this.detailPrefix+"update"+this.domain.getCapFirstDomainName()+"').window('close')\">Cancel</a>"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">编辑</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#"+this.detailPrefix+"ffedit').form('clear');\">清除</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#w"+this.detailPrefix+"update"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));	
				StatementList sl = WriteableUtil.merge(sList);
				sl.setSerial(this.serial);
				return sl;
			}
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

}
