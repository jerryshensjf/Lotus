package org.light.easyuilayouts.widgets;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Widget;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.simpleauth.verb.ChangePasswordUser;
import org.light.utils.WriteableUtil;

public class ChangePasswordDialog extends Widget{
	protected Domain domain;
	protected String detailPrefix = "";
	
	public ChangePasswordDialog(Domain domain) {
		this.domain = domain;
	}
	
	@Override
	public StatementList generateWidgetStatements() {
		try {
			ChangePasswordUser changePassword = new ChangePasswordUser(this.domain);
			List<Writeable> sList = new ArrayList<>();
			if (!changePassword.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+8500L,0,"<div class=\"easyui-window\" title=\"Change Password\" id=\"wchangePassword\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:600px;height:400px\">"));
					sList.add(new Statement(serial+9000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
					sList.add(new Statement(serial+10000L,0,"<form id=\"ffchangePassword\" method=\"post\">"));
					sList.add(new Statement(serial+11000L,0,"<input type=\"hidden\" id=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\" name=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\">"));
					sList.add(new Statement(serial+12000L,0,"<table cellpadding=\"5\">"));
					sList.add(new Statement(serial+13000L,2,"<tr><td>New Password:</td><td><input class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+14000L,2,"<tr><td>Confirm New Password:</td><td><input class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+15000L,0,"</table>"));
					sList.add(new Statement(serial+16000L,0,"</form>"));
					sList.add(new Statement(serial+17000L,0,"<div style=\"text-align:center;padding:5px\">"));
					sList.add(new Statement(serial+18000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"changePassword"+this.domain.getCapFirstDomainName()+"()\">Change Password</a>"));
					sList.add(new Statement(serial+19000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wchangePassword').window('close')\">Close</a>"));
					sList.add(new Statement(serial+20000L,0,"</div>"));
					sList.add(new Statement(serial+21000L,0,"</div>"));
				}else {
					sList.add(new Statement(serial+8500L,0,"<div class=\"easyui-window\" title=\"设置密码\" id=\"wchangePassword\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:600px;height:400px\">"));
					sList.add(new Statement(serial+9000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
					sList.add(new Statement(serial+10000L,0,"<form id=\"ffchangePassword\" method=\"post\">"));
					sList.add(new Statement(serial+11000L,0,"<input type=\"hidden\" id=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\" name=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\">"));
					sList.add(new Statement(serial+12000L,0,"<table cellpadding=\"5\">"));
					sList.add(new Statement(serial+13000L,2,"<tr><td>新密码:</td><td><input class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+14000L,2,"<tr><td>确认新密码:</td><td><input class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+15000L,0,"</table>"));
					sList.add(new Statement(serial+16000L,0,"</form>"));
					sList.add(new Statement(serial+17000L,0,"<div style=\"text-align:center;padding:5px\">"));
					sList.add(new Statement(serial+18000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"changePassword"+this.domain.getCapFirstDomainName()+"()\">重设密码</a>"));
					sList.add(new Statement(serial+19000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wchangePassword').window('close')\">关闭</a>"));
					sList.add(new Statement(serial+20000L,0,"</div>"));				
				}
			}
			StatementList sl = WriteableUtil.merge(sList);
			sl.setSerial(this.serial);
			return sl;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public StatementList generateWidgetScriptStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

}
