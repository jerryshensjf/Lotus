package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.EnumMainDatagrid;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUIEnumGridPageLayout extends EasyUILayout{
	protected Domain domain;
	protected SearchPanel searchPanel;
	protected EnumMainDatagrid enumMainDatagrid;
	protected ViewDialog viewDialog;
	
	public EasyUIEnumGridPageLayout(Domain domain) {
		super();
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = searchPanel.generateWidgetStatements();
		if (sl1!=null) sl1.setSerial(4000L);
		StatementList sl2 = enumMainDatagrid.generateWidgetStatements();
		if (sl2!=null) sl2.setSerial(5000L);		
		StatementList sl5 = viewDialog.generateWidgetStatements();
		if (sl5!=null)  sl5.setSerial(8000L);
		if (sl1!=null) sList.add(sl1);
		if (sl2!=null) sList.add(sl2);
		if (sl5!=null) sList.add(sl5);		
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = enumMainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(1000L);
		sl1.setIndent(1);
		sList.add(sl1);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.domain!=null) {
			this.searchPanel = new SearchPanel();
			this.enumMainDatagrid = new EnumMainDatagrid();
			this.viewDialog = new ViewDialog();

			this.searchPanel.setDomain(this.domain);
			this.enumMainDatagrid.setDomain(this.domain);
			this.viewDialog.setDomain(this.domain);

			return true;
		}
		return false;
	}

	public EnumMainDatagrid getEnumMainDatagrid() {
		return enumMainDatagrid;
	}

	public void setEnumMainDatagrid(EnumMainDatagrid enumMainDatagrid) {
		this.enumMainDatagrid = enumMainDatagrid;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

}
