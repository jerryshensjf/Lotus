package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUIGridPageLayout extends EasyUILayout{
	protected Domain domain;
	protected SearchPanel searchPanel;
	protected MainDatagrid mainDatagrid;
	protected AddDialog addDialog;
	protected UpdateDialog updateDialog;
	protected ViewDialog viewDialog;
	
	public EasyUIGridPageLayout(Domain domain) {
		super();
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = searchPanel.generateWidgetStatements();
		if (sl1!=null) sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		if (sl2!=null) sl2.setSerial(5000L);
		StatementList sl3 = addDialog.generateWidgetStatements();
		if (sl3!=null) sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		if (sl4!=null) sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		if (sl5!=null)  sl5.setSerial(8000L);
		if (sl1!=null) sList.add(sl1);
		if (sl2!=null) sList.add(sl2);
		if (sl3!=null) sList.add(sl3);
		if (sl4!=null) sList.add(sl4);
		if (sl5!=null) sList.add(sl5);		
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(1000L);
		sl1.setIndent(1);
		sList.add(sl1);
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.domain!=null) {
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new MainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();

			this.searchPanel.setDomain(this.domain);
			this.mainDatagrid.setDomain(this.domain);
			this.addDialog.setDomain(this.domain);
			this.updateDialog.setDomain(this.domain);
			this.viewDialog.setDomain(this.domain);

			return true;
		}
		return false;
	}

	public SearchPanel getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(SearchPanel searchPanel) {
		this.searchPanel = searchPanel;
	}

	public MainDatagrid getMainDatagrid() {
		return mainDatagrid;
	}

	public void setMainDatagrid(MainDatagrid mainDatagrid) {
		this.mainDatagrid = mainDatagrid;
	}

	public AddDialog getAddDialog() {
		return addDialog;
	}

	public void setAddDialog(AddDialog addDialog) {
		this.addDialog = addDialog;
	}

	public UpdateDialog getUpdateDialog() {
		return updateDialog;
	}

	public void setUpdateDialog(UpdateDialog updateDialog) {
		this.updateDialog = updateDialog;
	}

	public ViewDialog getViewDialog() {
		return viewDialog;
	}

	public void setViewDialog(ViewDialog viewDialog) {
		this.viewDialog = viewDialog;
	}

}
