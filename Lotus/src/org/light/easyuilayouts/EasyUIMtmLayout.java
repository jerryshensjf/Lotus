package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.complexverb.Assign;
import org.light.complexverb.ListMyActive;
import org.light.complexverb.ListMyAvailableActive;
import org.light.complexverb.Revoke;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.CheckAccess;

public class EasyUIMtmLayout extends EasyUILayout{
	protected Domain master;
	protected Domain slave;
	protected String slaveAliasLabelOrText;
	
	public EasyUIMtmLayout(Domain master,Domain slave) {
		super();
		this.master = master;
		this.slave = slave;
	}

	@Override
	public StatementList generateLayoutStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		long serial = 0L;
		sList.add(new Statement(serial+18000L,0,"<table>"));
		sList.add(new Statement(serial+19000L,0,"<tr>"));
		sList.add(new Statement(serial+20000L,0,"<td width=\"80px\">"));
		sList.add(new Statement(serial+21000L,0,"</td>"));
		sList.add(new Statement(serial+22000L,0,"<td>"));
		if  (this.master.getLanguage().equalsIgnoreCase("english")){
			sList.add(new Statement(serial+23000L,0,"<div class=\"easyui-datalist\" title=\"Active "+this.master.getText()+"\" id=\"my"+this.master.getCapFirstDomainName()+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}else {
			sList.add(new Statement(serial+23000L,0,"<div class=\"easyui-datalist\" title=\"活跃"+this.master.getText()+"\" id=\"my"+this.master.getCapFirstDomainName()+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}
		sList.add(new Statement(serial+24000L,0,"url: '../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/listActive"+this.master.getCapFirstPlural()+"',"));
		sList.add(new Statement(serial+25000L,0,"method: 'post',"));
		sList.add(new Statement(serial+26000L,0,"valueField:'"+this.master.getDomainId().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+27000L,0,"textField:'"+this.master.getDomainName().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+28000L,0,"idField:'"+this.master.getDomainId().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+29000L,0,"onCheck:checkMyRow,"));
		sList.add(new Statement(serial+30000L,0,"onLoadSuccess:loadSuccess"));
		sList.add(new Statement(serial+31000L,0,"\">"));
		sList.add(new Statement(serial+32000L,0,"</div>"));
		sList.add(new Statement(serial+33000L,0,"</td>"));
		sList.add(new Statement(serial+34000L,0,"<td>"));	
		if  (this.master.getLanguage().equalsIgnoreCase("english")){
			sList.add(new Statement(serial+35000L,0,"<div class=\"easyui-datalist\" title=\"Current "+this.getSlaveAliasLabelOrText()+"\" id=\"my"+StringUtil.capFirst(this.slave.getAliasPlural())+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}else {
			sList.add(new Statement(serial+35000L,0,"<div class=\"easyui-datalist\" title=\"现有"+this.getSlaveAliasLabelOrText()+"\" id=\"my"+StringUtil.capFirst(this.slave.getAliasPlural())+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}
		ListMyActive listmac = new ListMyActive(this.master,this.slave);
		ListMyAvailableActive listavmac = new ListMyAvailableActive(this.master,this.slave);
		Assign assign = new Assign(this.master,this.slave);
		Revoke revoke = new Revoke(this.master,this.slave);
		sList.add(new Statement(serial+36000L,0,"url: '../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(listmac.getVerbName())+"',"));
		sList.add(new Statement(serial+37000L,0,"method: 'post',"));
		sList.add(new Statement(serial+38000L,0,"valueField:'"+this.slave.getDomainId().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+39000L,0,"textField:'"+this.slave.getDomainName().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+40000L,0,"singleSelect:false,"));
		sList.add(new Statement(serial+40500L,0,"onBeforeLoad:beforeLoad"));
		sList.add(new Statement(serial+41000L,0,"\">"));
		sList.add(new Statement(serial+42000L,0,"</div>"));
		sList.add(new Statement(serial+43000L,0,"</td>"));
		sList.add(new Statement(serial+44000L,0,"<td>"));
		sList.add(new Statement(serial+45000L,0,"<div style=\"float:left;display:inline-block\">"));
		sList.add(new Statement(serial+46000L,0,"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>"));
		sList.add(new Statement(serial+47000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\""+StringUtil.lowerFirst(assign.getVerbName())+"()\">&lt;&lt;</a><br/><br/><br/>"));
		sList.add(new Statement(serial+48000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\""+StringUtil.lowerFirst(revoke.getVerbName())+"()\">&gt;&gt;</a>"));
		sList.add(new Statement(serial+49000L,0,"</div>"));
		if  (this.master.getLanguage().equalsIgnoreCase("english")){
			sList.add(new Statement(serial+50000L,0,"<div class=\"easyui-datalist\" title=\"Available "+this.getSlaveAliasLabelOrText()+"\" id=\"myAvailable"+StringUtil.capFirst(this.slave.getAliasPlural())+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}else {
			sList.add(new Statement(serial+50000L,0,"<div class=\"easyui-datalist\" title=\"可添加"+this.getSlaveAliasLabelOrText()+"\" id=\"myAvailable"+StringUtil.capFirst(this.slave.getAliasPlural())+"\" style=\"width:200px;height:550px;float:left;display:inline;margin:0px\" data-options=\""));
		}
		sList.add(new Statement(serial+51000L,0,"url: '../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(listavmac.getVerbName())+"',"));
		sList.add(new Statement(serial+52000L,0,"method: 'post',"));
		sList.add(new Statement(serial+53000L,0,"valueField:'"+this.slave.getDomainId().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+54000L,0,"textField:'"+this.slave.getDomainName().getLowerFirstFieldName()+"',"));
		sList.add(new Statement(serial+55000L,0,"singleSelect:false,"));
		sList.add(new Statement(serial+55500L,0,"onBeforeLoad:beforeLoad"));
		sList.add(new Statement(serial+56000L,0,"\">"));
		sList.add(new Statement(serial+57000L,0,"</div>"));
		sList.add(new Statement(serial+58000L,0,"</td>"));
		sList.add(new Statement(serial+59000L,0,"</tr>"));
		sList.add(new Statement(serial+60000L,0,"</table>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws Exception{
		Assign assign = new Assign(this.master,this.slave);
		Revoke revoke = new Revoke(this.master,this.slave);
		List<Writeable> sList = new ArrayList<>();
		
		sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
		sList.add(new Statement(serial+16000L,0,"checkAccess"+this.master.getCapFirstDomainName()+"();"));
		sList.add(new Statement(serial+20000L,0,"});"));
		
		sList.add(new Statement(serial+63500L,0,"var currentIndex = 0;"));
		Var currentIndex = new Var("currentIndex", new Type("var"));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckMyRowMethod(this.master,this.slave,currentIndex).generateMethodStatementListIncludingContainer(serial+64000L,0));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateLoadSuccessMethod(this.master,currentIndex).generateMethodStatementListIncludingContainer(serial+65000L,0));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateBeforeLoadMethod(this.master).generateMethodStatementListIncludingContainer(serial+65500L,0));
		JavascriptMethod slCheckAccessJm =  ((EasyUIPositions)new CheckAccess(this.master)).generateEasyUIJSActionMethod();
		if(	slCheckAccessJm != null) {
			StatementList slCheckAccessAction = slCheckAccessJm.generateMethodStatementListIncludingContainer(serial+65600L,0);
			sList.add(slCheckAccessAction);
		}
		
		sList.add(assign.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+66000L,0));
		sList.add(revoke.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+67000L,0));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementListIncludingContainer(serial+68000L,0));
		
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {

		return false;
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	public String getSlaveAliasLabelOrText() {
		return slaveAliasLabelOrText;
	}

	public void setSlaveAliasLabelOrText(String slaveAliasLabelOrText) {
		this.slaveAliasLabelOrText = slaveAliasLabelOrText;
	}

}
