package org.light.easyuilayouts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.light.core.Widget;
import org.light.core.Writeable;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.Footer;
import org.light.easyuilayouts.widgets.Header;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class EasyUIFrameSet implements Comparable<EasyUIFrameSet>,Serializable{
	protected long serial = 1;
	protected String standardName;
	protected String content = "";
	protected String blockComment;
	protected Widget nav = new Nav();
	protected Widget header = new Header();
	protected Widget pageFooter = new Footer();
	protected EasyUILayout mainContent = new EasyUICustomManyToMany();
	protected Widget loginWidget;
	protected StatementList scripts;
	protected StatementList additionScriptFiles = new StatementList();
	protected String language = "Chinese";

	public String getBlockComment() {
		return blockComment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}

	public long getSerial() {
		return serial;
	}

	public void setSerial(long serial) {
		this.serial = serial;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getCapFirstMethodName() {
		return StringUtil.capFirst(standardName);
	}
	
	public String getLowerFirstMethodName() {
		return StringUtil.lowerFirst(standardName);
	}

	public String getThrowException() {
		return "Exception";
	}	
	@Override
	public int compareTo(EasyUIFrameSet o) {
		if (this.getSerial() > o.getSerial()) return 1;
		else if (this.getSerial() == o.getSerial()) return 0;
		else return -1;
	}

    public StatementList generateFrameSetStatementList() throws Exception{
    	if ("english".equalsIgnoreCase(this.language)) return generateEnglishFrameSetStatementList();
    	List<Writeable> sList =  new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
		sList.add(new Statement(2000L,0,"<html>"));
		sList.add(new Statement(3000L,0,"<head>"));
		sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
		
		sList.add(new Statement(4100L,0,"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"));
		sList.add(new Statement(4200L,0,"<meta http-equiv=\"Expires\" CONTENT=\"0\">"));
		sList.add(new Statement(4300L,0,"<meta http-equiv=\"Cache-Control\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(4400L,0,"<meta http-equiv=\"Pragma\" CONTENT=\"no-cache\">"));
		
		sList.add(new Statement(5000L,0,"<title>"+this.getStandardName()+"</title>"));
		sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
		sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
		sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
		sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
		sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
		sList.add(new Statement(10100L,0,"<script type=\"text/javascript\" src=\"../uploadjs/vendor/jquery.ui.widget.js\"></script>"));
		sList.add(new Statement(10200L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.iframe-transport.js\"></script>"));
		sList.add(new Statement(10300L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.fileupload.js\"></script>"));
		sList.add(new Statement(10400L,0,"<script type=\"text/javascript\" src=\"../js/auth.js\"></script>"));
		this.getAdditionScriptFiles().setSerial(10500);
		sList.add(this.getAdditionScriptFiles());
		sList.add(new Statement(11000L,0,"</head>"));
		sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
		sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:100px;background:#B3DFDA;padding:10px\">"));
		this.getHeader().setSerial(13500L);
		sList.add(this.getHeader().generateWidgetStatements());
		sList.add(new Statement(13900L,0,"</div>"));		
		sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:180px;padding:0px;\">"));
		this.getNav().setSerial(14500L);
		sList.add(this.getNav().generateWidgetStatements());
		sList.add(new Statement(15000L,0,"</div>"));
		sList.add(new Statement(16000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'属性'\" style=\"width:250px;overflow: hidden\">"));
		sList.add(new Statement(17000L,0,"</div>"));
		sList.add(new Statement(18000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:40px;background:#A9FACD;padding:10px;text-align: center\">"));
		this.getPageFooter().setSerial(18500L);
		sList.add(this.getPageFooter().generateWidgetStatements());
		sList.add(new Statement(18700L,0,"</div>"));
		sList.add(new Statement(19000L,0,"<div data-options=\"region:'center',title:'Rust语言通用代码生成器：莲花　生成结果'\">"));
		StatementList sl = this.getMainContent().generateLayoutStatements();
		sl.setSerial(19500L);
		sList.add(sl);
		sList.add(new Statement(20000L,0,"</div>"));		
	
		sList.add(new Statement(21000L,0,"</body>"));
		sList.add(new Statement(22000L,0,"<script type=\"text/javascript\">"));
		this.scripts = this.getMainContent().generateLayoutScriptStatements();
		this.scripts.setSerial(22500);
		sList.add(this.scripts);
		sList.add(new Statement(23000L,0,"</script>"));
		sList.add(new Statement(24000L,0,"</html>"));
		StatementList mylist = WriteableUtil.merge(sList);
		return mylist;
    }
    
    public StatementList generateEnglishFrameSetStatementList() throws Exception{
    	List<Writeable> sList =  new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
		sList.add(new Statement(2000L,0,"<html>"));
		sList.add(new Statement(3000L,0,"<head>"));
		sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
		
		sList.add(new Statement(4100L,0,"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"));
		sList.add(new Statement(4200L,0,"<meta http-equiv=\"Expires\" CONTENT=\"0\">"));
		sList.add(new Statement(4300L,0,"<meta http-equiv=\"Cache-Control\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(4400L,0,"<meta http-equiv=\"Pragma\" CONTENT=\"no-cache\">"));
		
		sList.add(new Statement(5000L,0,"<title>"+this.getStandardName()+"</title>"));
		sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
		sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
		sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
		sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
		sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
		sList.add(new Statement(10100L,0,"<script type=\"text/javascript\" src=\"../uploadjs/vendor/jquery.ui.widget.js\"></script>"));
		sList.add(new Statement(10200L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.iframe-transport.js\"></script>"));
		sList.add(new Statement(10300L,0,"<script type=\"text/javascript\" src=\"../uploadjs/jquery.fileupload.js\"></script>"));
		sList.add(new Statement(10400L,0,"<script type=\"text/javascript\" src=\"../js/auth.js\"></script>"));
		this.getAdditionScriptFiles().setSerial(10500);
		sList.add(this.getAdditionScriptFiles());
		sList.add(new Statement(11000L,0,"</head>"));
		sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
		sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:100px;background:#B3DFDA;padding:10px\">"));
		this.getHeader().setSerial(13500L);
		sList.add(((Header)this.getHeader()).generateEnglishWidgetStatements());
		sList.add(new Statement(13900L,0,"</div>"));		
		sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'Main Menu'\" style=\"width:180px;padding:0px;\">"));
		this.getNav().setSerial(14500L);
		sList.add(((Nav)this.getNav()).generateEnglishWidgetStatements());
		sList.add(new Statement(15000L,0,"</div>"));
		sList.add(new Statement(16000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'Attributes'\" style=\"width:250px;overflow: hidden\">"));
		sList.add(new Statement(17000L,0,"</div>"));
		sList.add(new Statement(18000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:40px;background:#A9FACD;padding:10px;text-align: center\">"));
		this.getPageFooter().setSerial(18500L);
		sList.add(this.getPageFooter().generateWidgetStatements());
		sList.add(new Statement(18700L,0,"</div>"));
		sList.add(new Statement(19000L,0,"<div data-options=\"region:'center',title:'Generate results of Fairchild golang code generator.'\">"));
		StatementList sl = this.getMainContent().generateLayoutStatements();
		sl.setSerial(19500L);
		sList.add(sl);
		sList.add(new Statement(20000L,0,"</div>"));		
	
		sList.add(new Statement(21000L,0,"</body>"));
		sList.add(new Statement(22000L,0,"<script type=\"text/javascript\">"));
		this.scripts = this.getMainContent().generateLayoutScriptStatements();
		this.scripts.setSerial(22500);
		sList.add(this.scripts);
		sList.add(new Statement(23000L,0,"</script>"));
		sList.add(new Statement(24000L,0,"</html>"));
		StatementList mylist = WriteableUtil.merge(sList);
		return mylist;
    }
	public Widget getNav() {
		return nav;
	}

	public void setNav(Widget nav) {
		this.nav = nav;
	}

	public Widget getHeader() {
		return header;
	}

	public void setHeader(Widget header) {
		this.header = header;
	}

	public Widget getPageFooter() {
		return pageFooter;
	}

	public void setPageFooter(Widget pageFooter) {
		this.pageFooter = pageFooter;
	}

	public Widget getLoginWidget() {
		return loginWidget;
	}

	public void setLoginWidget(Widget loginWidget) {
		this.loginWidget = loginWidget;
	}

	public EasyUILayout getMainContent() {
		return mainContent;
	}

	public void setMainContent(EasyUILayout mainContent) {
		this.mainContent = mainContent;
	}

	public StatementList getScripts() {
		return scripts;
	}

	public void setScripts(StatementList scripts) {
		this.scripts = scripts;
	}

	public void setTitles(String title, String subTitle,String footer) {
		Header header = (Header)this.getHeader();
		if (!StringUtil.isBlank(title)) header.setTitle(title);
		if (!StringUtil.isBlank(subTitle)) header.setSubTitle(subTitle);
		Footer pagefooter = (Footer) this.getPageFooter();
		if (!StringUtil.isBlank(footer)) pagefooter.setFooterStr(footer);
	}

	public StatementList getAdditionScriptFiles() {
		return additionScriptFiles;
	}

	public void setAdditionScriptFiles(StatementList additionScriptFiles) {
		this.additionScriptFiles = additionScriptFiles;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
