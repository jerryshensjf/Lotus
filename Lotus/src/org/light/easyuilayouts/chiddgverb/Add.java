package org.light.easyuilayouts.chiddgverb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Add extends org.light.verb.Add{
	protected String detailPrefix = "detail";
	protected Set<Field> deniedFields = new TreeSet<>();
	
	public Add(Domain d,String technicalStack,String dbType) throws ValidateException{
		super(d);
		this.technicalStack = technicalStack;
		this.dbType = dbType;
	}

	public Add(Domain d) throws ValidateException{
		super(d);
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied) return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("add"+domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Add',"));
			}else {
				sl.add(new Statement(2000,1, "text:'新增',"));
			}
			sl.add(new Statement(3000,1, "iconCls:'icon-add',"));
			sl.add(new Statement(4000,1, "handler:function(){"));
			sl.add(new Statement(5000,2, "$('#w"+this.detailPrefix+"add"+this.domain.getCapFirstDomainName()+"').window('open');"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,0, "}"));
			block.setMethodStatementList(sl);
			return block;			
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied) return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("add"+domain.getCapFirstDomainName());
			
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,1, "if ($(\"#"+this.detailPrefix+"ff\").form(\"validate\")) {"));
			sl.add(new Statement(2000,2, "$.ajax({"));
			sl.add(new Statement(3000,3, "type: \"post\","));
			sl.add(new Statement(4000,3, "url: \"../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/add"+domain.getCapFirstDomainName()+"\","));
			sl.add(new Statement(5000,3, "data: JSON.stringify({"));
			long serial = 6000;
			for (Field f: domain.getFieldsWithoutId()){
				if (!deniedFields.contains(f)) {
					if (f instanceof Dropdown){
						sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":parseIntNeg($(\"#"+this.detailPrefix+"ff\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"getValue\")),"));
					} else if (f.getFieldType().equalsIgnoreCase("bool")){
						sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":parseBoolean($(\"#"+this.detailPrefix+"ff\").find(\"input[name='"+f.getLowerFirstFieldName()+"']:checked\").val()),"));								
					} else {
						sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":$(\"#"+this.detailPrefix+"ff\").find(\"#"+f.getLowerFirstFieldName()+"\").val(),"));
					}
					serial+=1000;
				}
			}				
			sl.add(new Statement(serial,3, "}),"));
			sl.add(new Statement(serial+1000,3, "dataType: 'json',"));
			sl.add(new Statement(serial+2000,3, "contentType:\"application/json;charset=UTF-8\","));
			sl.add(new Statement(serial+3000,3, "success: function(data, textStatus) {"));
			sl.add(new Statement(serial+4000,4, "if (data.success) {"));
			sl.add(new Statement(serial+5000,5, "$('#"+this.detailPrefix+"ff').form('clear');"));
			sl.add(new Statement(serial+6000,5, "$(\"#"+this.detailPrefix+"ff\").find(\"input[name='"+domain.getActive().getLowerFirstFieldName()+"']\").get(0).checked = true;"));
			sl.add(new Statement(serial+7000,5, "$(\"#w"+this.detailPrefix+"add"+domain.getCapFirstDomainName()+"\").window('close');"));
			sl.add(new Statement(serial+8000,5, "$(\"#"+this.detailPrefix+"dg\").datagrid(\"load\");"));
			sl.add(new Statement(serial+9000,4, "}"));
			sl.add(new Statement(serial+10000,4, "},"));
			sl.add(new Statement(serial+11000,3, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(serial+12000,3, "},"));
			sl.add(new Statement(serial+13000,3, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(serial+14000,4, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(serial+15000,4, "alert(errorThrown.toString());"));
			sl.add(new Statement(serial+16000,3, "}"));
			sl.add(new Statement(serial+17000,2, "});"));
			sl.add(new Statement(serial+18000,1, "}"));
			
			method.setMethodStatementList(sl);
			return method;	
		}
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
}
