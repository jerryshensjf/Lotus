package org.light.easyuilayouts.chiddgverb;

import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.exception.ValidateException;

public class Toggle extends org.light.verb.Toggle{
	protected String detailPrefix = "detail";
	
	public Toggle(Domain d) throws ValidateException{
		super(d);
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}	
	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("toggle" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Toggle',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'切换',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-cut',"));
			sl.add(new Statement(4000, 1, "handler:function(){ "));
			sl.add(new Statement(5000, 2, "var rows = $(\"#"+this.detailPrefix+"dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			sl.add(new Statement(10000, 2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000, 3, "return;"));
			sl.add(new Statement(13000, 2, "}"));
			sl.add(new Statement(14000, 2, "var " + domain.getDomainId().getLowerFirstFieldName() + " = rows[0][\""
					+ domain.getDomainId().getLowerFirstFieldName() + "\"];"));
			sl.add(new Statement(15000, 2, "toggle" + this.domain.getCapFirstDomainName() + "("
					+ domain.getDomainId().getLowerFirstFieldName() + ");"));
			sl.add(new Statement(16000, 1, "}"));
			sl.add(new Statement(17000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("toggle" + domain.getCapFirstDomainName());
			Signature s1 = new Signature();
			s1.setName(domain.getDomainId().getLowerFirstFieldName());
			s1.setPosition(1);
			s1.setType(new Type("var"));
			method.addSignature(s1);

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/toggle" + domain.getCapFirstDomainName() + "\","));

			sl.add(new Statement(4000, 2, "data: {"));
			sl.add(new Statement(5000, 3, "\"" + domain.getDomainId().getLowerFirstFieldName() + "\":"
					+ domain.getDomainId().getLowerFirstFieldName() + ""));
			sl.add(new Statement(6000, 2, "},"));
			sl.add(new Statement(7000, 2, "dataType: 'json',"));
			sl.add(new Statement(8000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(9000, 3, "$(\"#"+this.detailPrefix+"dg\").datagrid(\"load\");"));
			sl.add(new Statement(10000, 2, "},"));
			sl.add(new Statement(11000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(12000, 2, "},"));
			sl.add(new Statement(13000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(14000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(15000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(16000, 2, "}"));
			sl.add(new Statement(17000, 1, "});"));

			method.setMethodStatementList(sl);
			return method;
		}
	}
}
