package org.light.easyuilayouts.chiddgverb;

import java.util.Set;
import java.util.TreeSet;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;

public class Update extends org.light.verb.Update{
	protected String detailPrefix = "detail";
	protected Set<Field> deniedFields = new TreeSet<>();
	
	public Update(Domain d,String technicalStack,String dbType) throws ValidateException{
		super(d);
		this.technicalStack = technicalStack;
		this.dbType = dbType;
	}
	
	public Update(Domain d) throws ValidateException{
		super(d);
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}
	
	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("Update" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Update',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'编辑',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-edit',"));
			sl.add(new Statement(4000, 1, "handler:function(){ "));
			sl.add(new Statement(5000, 2, "var rows = $(\"#"+this.detailPrefix+"dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			sl.add(new Statement(10000, 2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000, 3, "return;"));
			sl.add(new Statement(13000, 2, "}"));
			sl.add(new Statement(13500, 2,
					"$(\"#"+this.detailPrefix+"ffedit\").find(\"#" + this.domain.getDomainId().getLowerFirstFieldName()
							+ "\").val(rows[0][\"" + this.domain.getDomainId().getLowerFirstFieldName() + "\"]);"));
			long serial = 14000;
			for (Field f : domain.getFieldsWithoutIdAndActive()) {
				if (!deniedFields.contains(f)) {
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sl.add(new Statement(serial, 2, "if (!isBlank(rows[0][\""+f.getLowerFirstFieldName()+"\"]))	{"));
						sl.add(new Statement(serial+10, 3, "$(\"#"+this.detailPrefix+"ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
						sl.add(new Statement(serial+20, 2, "}else{"));
						sl.add(new Statement(serial+30, 3, "$(\"#"+this.detailPrefix+"ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"../images/blank.jpg\");"));
						sl.add(new Statement(serial+40, 2, "}"));
					}else if (f instanceof Dropdown) {
						sl.add(new Statement(serial, 2, "if (rows[0][\"" + f.getLowerFirstFieldName() + "\"] > 0) $(\"#"+this.detailPrefix+"ffedit\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").combobox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					} else {
						if (f.getFieldType().equalsIgnoreCase("bool")) {
							sl.add(new Statement(serial, 2,
									"var " + f.getLowerFirstFieldName() + "Checkboxs = $(\"#"+this.detailPrefix+"ffedit\").find(\"input[name='"
											+ f.getLowerFirstFieldName() + "']\");"));
							sl.add(new Statement(serial + 100, 2,
									"for (var i=0;i<" + f.getLowerFirstFieldName() + "Checkboxs.length;i++){"));
							sl.add(new Statement(serial + 200, 3,
									"if (" + f.getLowerFirstFieldName() + "Checkboxs.get(i).value == \"\"+rows[0][\""
											+ f.getLowerFirstFieldName() + "\"]) " + f.getLowerFirstFieldName()
											+ "Checkboxs.get(i).checked=true;"));
							sl.add(new Statement(serial + 500, 2, "}"));
						} else if (f.isTextarea()) {
							sl.add(new Statement(serial, 2, "$(\"#"+this.detailPrefix+"ffedit\").find(\"#" + f.getLowerFirstFieldName()
									+ "\").val(rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
						} else {
							sl.add(new Statement(serial, 2, "$(\"#"+this.detailPrefix+"ffedit\").find(\"#" + f.getLowerFirstFieldName()
									+ "\").textbox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
						}
					}
					serial += 1000;
				}
			}
			sl.add(new Statement(serial, 2, "var checkboxs = $(\"#"+this.detailPrefix+"ffedit\").find(\"input[name='"
					+ domain.getActive().getLowerFirstFieldName() + "']\");"));
			sl.add(new Statement(serial + 100, 2, "for (var i=0;i<checkboxs.length;i++){"));
			sl.add(new Statement(serial + 200, 3, "if (checkboxs.get(i).value == \"\"+rows[0][\""
					+ domain.getActive().getLowerFirstFieldName() + "\"]) checkboxs.get(i).checked=true;"));
			sl.add(new Statement(serial + 500, 2, "}"));
			sl.add(new Statement(serial + 1000, 2,
					"$('#w"+this.detailPrefix+"update" + domain.getCapFirstDomainName() + "').window('open');"));
			sl.add(new Statement(serial + 2000, 1, "}"));
			sl.add(new Statement(serial + 3000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("update" + domain.getCapFirstDomainName());

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/update" + domain.getCapFirstDomainName() + "\","));
			sl.add(new Statement(4000, 2, "data: JSON.stringify({"));
			long serial = 5000;
			for (Field f : domain.getFields()) {
				if (!deniedFields.contains(f)) {
					if (f instanceof Dropdown) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseIntNeg($(\"#"+this.detailPrefix+"ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").combobox(\"getValue\")),"));
					} else if (f.getFieldType().equalsIgnoreCase("bool")) {
						sl.add(new Statement(serial, 3,
								f.getLowerFirstFieldName() + ":parseBoolean($(\"#"+this.detailPrefix+"ffedit\").find(\"input[name='"
										+ f.getLowerFirstFieldName() + "']:checked\").val()),"));
					} else if (f.getFieldType().equalsIgnoreCase("i32")||f.getFieldType().equalsIgnoreCase("i64")) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseInt($(\"#"+this.detailPrefix+"ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val()),"));
					} else if (f.getFieldType().equalsIgnoreCase("f64")||f.getFieldType().equalsIgnoreCase("f32")) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseFloat($(\"#"+this.detailPrefix+"ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val()),"));
					} else if (f.isTextarea()) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#"+this.detailPrefix+"ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val(),"));
					} else {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#"+this.detailPrefix+"ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val(),"));
					}
					serial += 1000;
				}
			}
			sl.add(new Statement(serial, 2, "}),"));
			sl.add(new Statement(serial + 1000, 2, "dataType: 'json',"));
			sl.add(new Statement(serial + 2000, 2, "contentType:\"application/json;charset=UTF-8\","));
			sl.add(new Statement(serial + 3000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(serial + 4000, 3, "if (data.success){"));
			sl.add(new Statement(serial + 7000, 4,
					"$(\"#w"+this.detailPrefix+"update" + domain.getCapFirstDomainName() + "\").window('close');"));
			sl.add(new Statement(serial + 8000, 4, "$(\"#"+this.detailPrefix+"dg\").datagrid(\"load\");"));
			sl.add(new Statement(serial + 9000, 3, "}"));
			sl.add(new Statement(serial + 10000, 2, "},"));
			sl.add(new Statement(serial + 11000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(serial + 12000, 2, "},"));
			sl.add(new Statement(serial + 13000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(serial + 14000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(serial + 15000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(serial + 16000, 2, "}"));
			sl.add(new Statement(serial + 17000, 1, "}); "));

			method.setMethodStatementList(sl);
			return method;

		}
	}

	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
}
