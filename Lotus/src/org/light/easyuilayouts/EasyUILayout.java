package org.light.easyuilayouts;

import java.io.Serializable;

import org.light.domain.StatementList;
import org.light.utils.StringUtil;

public abstract class EasyUILayout implements Comparable<EasyUILayout> ,Serializable{
	protected long serial = 1;
	protected String standardName;
	protected String content = "";
	protected String blockComment;
	protected String label;
	protected String language = "Chinese";
	protected String technicalStack = "tower";
	protected String dbType = "MariaDB";
	
	public abstract StatementList generateLayoutStatements() throws Exception;
	public abstract StatementList generateLayoutScriptStatements() throws Exception;
	public abstract boolean parse() throws Exception;

	public String getBlockComment() {
		return blockComment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}

	public long getSerial() {
		return serial;
	}

	public void setSerial(long serial) {
		this.serial = serial;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getCapFirstMethodName() {
		return StringUtil.capFirst(standardName);
	}
	
	public String getLowerFirstMethodName() {
		return StringUtil.lowerFirst(standardName);
	}

	public String getThrowException() {
		return "Exception";
	}	
	@Override
	public int compareTo(EasyUILayout o) {
		if (this.getSerial() > o.getSerial()) return 1;
		else if (this.getSerial() == o.getSerial()) return 0;
		else return -1;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	public String getText(){
		if (!StringUtil.isBlank(this.getLabel())) {
			return this.label;
		}	
		else return this.getStandardName();
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTechnicalStack() {
		return technicalStack;
	}
	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
}
