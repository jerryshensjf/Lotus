package org.light.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Method;
import org.light.domain.Naming;
import org.light.limitedverb.DaoOnlyVerb;
import org.light.limitedverb.NoControllerVerb;

public abstract class Verb implements Comparable<Verb>{
	protected long verbId;
	protected long methodId;
	protected String verbName;
	protected Method method;
	protected long namingId;
	protected Naming naming;
	protected String verbToken;
	protected String verbComment;
	protected String verbContent;
	protected Domain domain;
	protected String verbReturnType;
	protected String verbReturnTypePackageToken;
	protected List<String> additionalImports = new ArrayList<String>();
	protected List<NoControllerVerb> noControllerVerbs = new ArrayList<NoControllerVerb>();
	protected List<DaoOnlyVerb> daoOnlyVerbs = new ArrayList<DaoOnlyVerb>();
	protected String label;
	protected boolean denied = false;
	protected String dbType = "MariaDB";
	protected String technicalStack = "tower";
	protected Set<Field> deniedFields = new TreeSet<>();
	protected String detailPrefix = "";
	
	public List<NoControllerVerb> getNoControllerVerbs() {
		return noControllerVerbs;
	}
	public void setNoControllerVerbs(List<NoControllerVerb> noControllerVerbs) {
		this.noControllerVerbs = noControllerVerbs;
	}
	public List<DaoOnlyVerb> getDaoOnlyVerbs() {
		return daoOnlyVerbs;
	}
	public void setDaoOnlyVerbs(List<DaoOnlyVerb> daoOnlyVerbs) {
		this.daoOnlyVerbs = daoOnlyVerbs;
	}
	public long getVerbId() {
		return verbId;
	}
	public void setVerbId(long verbId) {
		this.verbId = verbId;
	}
	public long getMethodId() {
		return methodId;
	}
	public void setMethodId(long methodId) {
		this.methodId = methodId;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public long getNamingId() {
		return namingId;
	}
	public void setNamingId(long namingId) {
		this.namingId = namingId;
	}
	public Naming getNaming() {
		return naming;
	}
	public void setNaming(Naming naming) {
		this.naming = naming;
	}
	public String getVerbToken() {
		return verbToken;
	}
	public void setVerbToken(String verbToken) {
		this.verbToken = verbToken;
	}
	public String getVerbComment() {
		return verbComment;
	}
	public void setVerbComment(String verbComment) {
		this.verbComment = verbComment;
	}
	public String getVerbContent() {
		return verbContent;
	}
	public void setVerbContent(String verbContent) {
		this.verbContent = verbContent;
	}
	public abstract Method generateDummyDaoImplMethod() throws Exception;
	public abstract Method generateDaoImplMethod() throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract Method generateControllerMethod() throws Exception;
		
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public String getVerbReturnType() {
		return verbReturnType;
	}
	public void setVerbReturnType(String verbReturnType) {
		this.verbReturnType = verbReturnType;
	}
	public String getVerbReturnTypePackageToken() {
		return verbReturnTypePackageToken;
	}
	public void setVerbReturnTypePackageToken(String verbReturnTypePackageToken) {
		this.verbReturnTypePackageToken = verbReturnTypePackageToken;
	}
	public List<String> getAdditionalImports() {
		return additionalImports;
	}
	public void setAdditionalImports(List<String> additionalImports) {
		this.additionalImports = additionalImports;
	}
	
	public Verb(Domain domain){
		super();
		this.domain = domain;
	}
	
	public Verb(Domain domain, String dbType){
		super();
		this.domain = domain;
		this.dbType = dbType;
	}
	
	public Verb(String dbType){
		super();
		this.dbType = dbType;
	}
	
	public Verb(){
		super();
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	
	@Override
	public int compareTo(Verb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	public boolean isDenied() {
		return denied;
	}
	public void setDenied(boolean denied) {
		this.denied = denied;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getTechnicalStack() {
		return technicalStack;
	}
	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}
	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}	
	
	public Set<String> getDeniedFieldNames(Set<Field> deniedFields){
		Set<String> deniedFieldNames = new TreeSet<>();
		for (Field f:deniedFields) {
			deniedFieldNames.add(f.getFieldName());
		}
		return deniedFieldNames;
	}
	public String getDetailPrefix() {
		return detailPrefix;
	}
	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}
}
