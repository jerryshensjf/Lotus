package org.light.core;

import org.light.domain.Domain;
import org.light.exception.ValidateException;
import org.light.verb.Add;
import org.light.verb.Delete;
import org.light.verb.DeleteAll;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.SoftDelete;
import org.light.verb.SoftDeleteAll;
import org.light.verb.Update;

public class VerbFactory {
	public static Verb getInstance(String type) throws Exception{
		switch (type){
		case "listAll": 		return new ListAll();
		case "listActive":		return new ListActive();
		case "findById": 		return new FindById();
		case "findByName":		return new FindByName();
		case "delete":			return new Delete();
		case "deleteAll":		return new DeleteAll();
		case "softDelete":		return new SoftDelete();
		case "softDeleteAll":	return new SoftDeleteAll();
		case "add":				return new Add();
		case "update":			return new Update();
		default:			return null;
		}
	}
	
	public static Verb getInstance(String type, Domain domain) throws ValidateException{ 
		switch (type){
		case "listAll": 		return new ListAll(domain);
		case "listActive":		return new ListActive(domain);
		case "findById": 		return new FindById(domain);
		case "findByName":		return new FindByName(domain);
		case "delete":			return new Delete(domain);
		case "deleteAll":		return new DeleteAll(domain);
		case "softDelete":		return new SoftDelete(domain);
		case "softDeleteAll":	return new SoftDeleteAll(domain);
		case "add":				return new Add(domain);
		case "update":			return new Update(domain); 
		default:			return null;
		}
	}
	
	public static Verb getInstance(String type, Domain domain, String fieldName){
		switch (type){
			default:			return null;
		}
	}
}
