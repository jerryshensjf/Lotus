package org.light.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.domain.Controller;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.simpleauth.verb.ChangeMyPasswordUser;
import org.light.simpleauth.verb.ChangePasswordUser;
import org.light.simpleauth.verb.FindMyProfileUser;
import org.light.simpleauth.verb.FindUserShadow;
import org.light.simpleauth.verb.UpdateMyProfileUser;
import org.light.simpleauth.verb.UpdateUser;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindById;
import org.light.verb.ListActive;

public class AxumPlainProfileController extends AxumController implements Comparable<Controller>{
	private static final long serialVersionUID = 6506582842158817100L;

	public AxumPlainProfileController(Domain domain) throws ValidateException {
		super(domain);
		this.domain = domain;
	}
	
	public StatementList getControllerRoutesStatementList() throws ValidateException {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"pub fn profile_"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"() -> Router {"));
		sList.add(new Statement(2000L,1,"Router::new()"));
		
		Set<Field> deniedFields = new TreeSet<>();
		deniedFields.add(this.domain.findFieldByFixedName("password"));
		deniedFields.add(this.domain.findFieldByFixedName("salt"));
		deniedFields.add(this.domain.findFieldByFixedName("loginFailure"));
		
		FindMyProfileUser findProfile = new FindMyProfileUser(this.domain,deniedFields);
		UpdateMyProfileUser updateProfile = new UpdateMyProfileUser(this.domain,deniedFields);
		ChangeMyPasswordUser changeMyPassword = new ChangeMyPasswordUser(this.domain);
		sList.add(new Statement(3000L,1,".route(\"/"+StringUtil.lowerFirst(findProfile.getVerbName())+"\", post("+StringUtil.getSnakeName(findProfile.getVerbName())+"))"));
		sList.add(new Statement(4000L,1,".route(\"/"+StringUtil.lowerFirst(updateProfile.getVerbName())+"\", post("+StringUtil.getSnakeName(updateProfile.getVerbName())+"))"));
		sList.add(new Statement(5000L,1,".route(\"/"+StringUtil.lowerFirst(changeMyPassword.getVerbName())+"\", post("+StringUtil.getSnakeName(changeMyPassword.getVerbName())+"))"));
		 
		long serial = 7000L;
		if (this.domain.containsImage()) {
			for (Field f:this.domain.getPlainFields()) {
				if ("Image".equalsIgnoreCase(f.getFieldType())) {
					AddUploadDomainField au = new AddUploadDomainField(this.domain,f);
					sList.add(new Statement(serial,1,".route(\"/"+StringUtil.lowerFirst(au.getVerbName())+"\", post("+StringUtil.getSnakeName(au.getVerbName())+"))"));
					serial += 1000L;
				}
			}
		}
		
		List<Domain> translateDomains = new ArrayList<Domain>();
		for (Field f: this.domain.getPlainFields()){
			if (f instanceof Dropdown){
				Dropdown dp = (Dropdown)f;
				Domain target = dp.getTarget();
				if (!target.isLegacy()&&!DomainUtil.inDomainList(target, translateDomains)){
					translateDomains.add(target);
				}
			}
		}
		
		for (Domain d:translateDomains) {
			ListActive list = new ListActive(d);
			sList.add(new Statement(serial,1,".route(\"/"+StringUtil.lowerFirst(list.getVerbName())+"\", post("+StringUtil.getSnakeName(list.getVerbName())+"))"));
			serial += 1000L;
		}
		
		sList.add(new Statement(serial,1,".layer(TraceLayer::new_for_http())"));
		sList.add(new Statement(serial + 1000L,0,"}"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public String generateControllerString() throws ValidateException {
		StringBuilder sb = new StringBuilder();
		Set<String> imports = this.generateImportStrings();
		imports.add("serde::Deserialize");
		imports.add("serde_json::{Value,Map}");
		imports.add("tower_http::{trace::TraceLayer}");
		imports.add("crate::"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::"+this.domain.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::"+this.domain.getCapFirstDomainName()+"Request");
		if (this.domain.containsImage()) {
			for (Field f:this.domain.getPlainFields()) {
				if ("Image".equalsIgnoreCase(f.getFieldType())) {
					imports.add("crate::"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::"+this.domain.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::pick_"+this.domain.getSnakeDomainName()+"_picture");
					imports.add("crate::"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::"+this.domain.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.domain.getControllerSuffix())+"::store_"+this.domain.getSnakeDomainName()+"_picture");
				}
			}
		}
		
		if (this.domain.containsDateTime()) {
			imports.add("chrono::{NaiveDateTime,NaiveDate}");
			imports.add("crate::"+this.domain.getDomainSuffix()+"::my_date_time_format::DATE_TIME_FORMAT");
			imports.add("crate::"+this.domain.getDomainSuffix()+"::my_date_format::DATE_FORMAT");
		}
		
		List<Domain> translateDomains = new ArrayList<Domain>();
		for (Field f: this.domain.getPlainFields()){
			if (f instanceof Dropdown){
				Dropdown dp = (Dropdown)f;
				Domain target = dp.getTarget();
				if (!target.isLegacy()&&!DomainUtil.inDomainList(target, translateDomains)){
					translateDomains.add(target);
				}
			}
		}
		
		for (Domain d:translateDomains) {
			ListActive list = new ListActive(d);
			imports.add("crate::"+d.getServiceimplSuffix()+"::"+d.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(list.getVerbName())+" as service_"+StringUtil.getSnakeName(list.getVerbName()));
		}
		
		imports.add("crate::utils::jwt_util::Claims");
		imports.add("jsonwebtoken::DecodingKey");
		imports.add("crate::utils::jwt_util::JWT_SECRET");
		imports.add("jsonwebtoken::Validation");
		
		imports.add("axum::{\r\n"
				+ "    extract::{Form, Json, Multipart},\r\n"
				+ "    http:: HeaderMap,\r\n"
				+ "    routing::post,\r\n"
				+ "    Router\r\n"
				+ "}");
		imports.add("axum_sessions::{\r\n"
				+ "    extractors::ReadableSession, \r\n"
				+ "}");
		
		imports.add("crate::"+this.domain.getDomainSuffix()+"::"+this.domain.getCapFirstDomainNameWithSuffix());
		FindById findUser = new FindById(this.domain);
		imports.add("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(findUser.getVerbName())+" as service_"+StringUtil.getSnakeName(findUser.getVerbName()));
		
		UpdateUser update = new UpdateUser(this.domain);
		ChangePasswordUser changePassword = new ChangePasswordUser(this.domain,this.domain.getDbType());
		FindUserShadow findShadow = new FindUserShadow(this.domain,this.domain.getDbType());
;		imports.add("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(update.getVerbName())+" as service_"+StringUtil.getSnakeName(update.getVerbName()));
		imports.add("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(changePassword.getVerbName())+" as service_"+StringUtil.getSnakeName(changePassword.getVerbName()));
		imports.add("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(findShadow.getVerbName())+" as service_"+StringUtil.getSnakeName(findShadow.getVerbName()));
		
		imports.add("crate::utils::encrypt_util::verify_password");
		
		imports.addAll(this.classImports);
		String importsStr = DomainUtil.generateImportStr(imports);
		sb.append("#![allow(unused_imports)]\n");
		sb.append("#![allow(unused_assignments)]\n");
		sb.append(importsStr);
		sb.append("\n");
		sb.append(getRequestsStatementList().getContent());
		sb.append(this.getControllerRoutesStatementList().getContent());
		sb.append("\n");

		Iterator it2 = this.getMethods().iterator();
		while (it2.hasNext()) {
			sb.append(((Method) it2.next()).generateMethodString()).append("\n");
		}
		
		
		return sb.toString();
	}

	public ValidateInfo validate() {
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		if (this.verbs == null || this.verbs.size() == 0) {
			info.setSuccess(false);
			info.addCompileError("Controller " + this.standardName
					+ " 动词是空！");
		}
		for (Verb v : this.verbs) {
			if (v != null && v.getDomain() == null) {
				info.setSuccess(false);
				info.addCompileError("Controller " + this.standardName + "动词"
						+ v.getVerbName() + "域对象是空值！");
			}
		}
		return info;
	}

	public AxumPlainProfileController(List<Verb> verbs, Domain domain,Boolean ignoreWarning)
			throws ValidateException {
		super(domain);
		this.domain = domain;
		try{
			for (Verb v : verbs) {
				v.setDomain(domain);
			}
			this.verbs = verbs;
			this.standardName = "Profile" + domain.getControllerNamingSuffix();
	
			ValidateInfo info = this.validate();
			if (info.success(ignoreWarning)) {
				for (Field f:domain.getFields()) {
					if (f instanceof Dropdown) {
						Dropdown dp = (Dropdown)f;
					}
				}
			} else {
				ValidateException e = new ValidateException(info);
				throw e;
			}
		}catch (Exception ex){
			ValidateInfo info = new ValidateInfo();
			info.addCompileError("AxumPlainLogin控制器新建失败！");
			ValidateException e = new ValidateException(info);
			throw e;
		}
	}
	
	@Override
	public StatementList getRequestsStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		
		sList.add(new Statement(serial+1000L,0,"#[derive(Deserialize)]"));
		sList.add(new Statement(serial+2000L,0,"#[serde(rename_all = \"camelCase\")]"));
		sList.add(new Statement(serial+3000L,0,"pub struct ChangeMy"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"Request {"));
		sList.add(new Statement(serial+4000L,1,"pub "+this.domain.findFieldByFixedName("password").getSnakeFieldName()+": String,"));
		sList.add(new Statement(serial+5000L,1,"pub old_"+this.domain.findFieldByFixedName("password").getSnakeFieldName()+":String,"));
		sList.add(new Statement(serial+6000L,0,"}"));

		return WriteableUtil.merge(sList);
	}
}
