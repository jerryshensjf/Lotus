package org.light.core;

import java.io.Serializable;

import org.light.domain.StatementList;
import org.light.exception.ValidateException;

public abstract class Widget implements Comparable<Widget>,Serializable {
	protected long serial = 0;
	protected String standardName;
	protected String content = "";
	protected String blockComment;
	protected String dbType = "MariaDB";
	protected String technicalStack = "tower";
	protected String language = "Chinese";
	public abstract StatementList generateWidgetStatements() throws Exception;
	public abstract StatementList generateWidgetScriptStatements() throws Exception;
	public abstract boolean parse() throws ValidateException;

	@Override
	public int compareTo(Widget o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}
	public long getSerial() {
		return serial;
	}
	public void setSerial(long serial) {
		this.serial = serial;
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBlockComment() {
		return blockComment;
	}
	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getTechnicalStack() {
		return technicalStack;
	}
	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
}
