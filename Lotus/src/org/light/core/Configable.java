package org.light.core;

public abstract class Configable implements Comparable<Configable>{
	protected String standardName;
	protected String putFolder;
	
	public abstract String generateConfigFileString() throws Exception;

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	@Override
	public int compareTo(Configable cf) {
		return this.standardName.compareTo(cf.getStandardName());
	}

	public String getPutFolder() {
		return putFolder;
	}

	public void setPutFolder(String putFolder) {
		this.putFolder = putFolder;
	}
}