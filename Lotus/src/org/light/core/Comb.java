package org.light.core;

import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.light.domain.Domain;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.widgets.Nav;

public abstract class Comb implements Comparable<Comb> {
	protected static Logger logger = Logger.getLogger(Comb.class);
	protected Long serial;
	protected String standardName;
	protected String label;
	protected Set<Domain> domains = new TreeSet<>();
	protected Set<Verb> verbs = new TreeSet<>();
	protected Set<EasyUIFrameSet> franes = new TreeSet<>();
	protected Nav nav;
	protected String technicalStack = "tower";
	protected String dbType = "MariaDB";
	protected String language = "Chinese";
	public abstract void generateCombFiles(String targetFolderPath) throws Exception;
	public abstract void generateCombFromDomians() throws Exception;
	public abstract ValidateInfo validateDomains() throws Exception;
	public abstract boolean validateVerbs() throws Exception;
	public abstract boolean validateLayouts() throws Exception;
	public abstract void setTitles(String title, String subTitle,String footer);
	public abstract String getDomainNamesStr();

	public String getTechnicalStack() {
		return technicalStack;
	}

	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	public Set<Domain> getDomains() {
		return domains;
	}
	public void setDomains(Set<Domain> domains) {
		this.domains = domains;
	}
	public Set<Verb> getVerbs() {
		return verbs;
	}
	public void setVerbs(Set<Verb> verbs) {
		this.verbs = verbs;
	}
	
	@Override
	public int compareTo(Comb o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Set<EasyUIFrameSet> getFranes() {
		return franes;
	}
	public void setFranes(Set<EasyUIFrameSet> franes) {
		this.franes = franes;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}
	public Long getSerial() {
		return serial;
	}
	public void setSerial(Long serial) {
		this.serial = serial;
	}
	public Nav getNav() {
		return nav;
	}
	public void setNav(Nav nav) {
		this.nav = nav;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
}
