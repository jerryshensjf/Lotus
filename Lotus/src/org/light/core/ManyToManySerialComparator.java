package org.light.core;

import java.io.Serializable;
import java.util.Comparator;

import org.light.domain.ManyToMany;

public class ManyToManySerialComparator implements Comparator<ManyToMany>,Serializable{
	private static final long serialVersionUID = -3633372590360798941L;

	@Override
	public int compare(ManyToMany o1, ManyToMany o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
