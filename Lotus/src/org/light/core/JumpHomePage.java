package org.light.core;

import org.light.domain.StatementList;

public class JumpHomePage{
	protected String jumpFolder = "pages";
	protected String fileName = "index.html";
	protected String packageToken = "";
	public JumpHomePage(){
		super();
		this.fileName = "index.html";
		this.packageToken = "";
	}

	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("<meta http-equiv=\"refresh\"  content=\"0;url="+this.jumpFolder+"/index.html\"/>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("</body>\n");
		return sb.toString();
	}

	public StatementList getStatementList(long serial, int indent) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getJumpFolder() {
		return jumpFolder;
	}

	public void setJumpFolder(String jumpFolder) {
		this.jumpFolder = jumpFolder;
	}

}
