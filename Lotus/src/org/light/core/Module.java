package org.light.core;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.ConfigFile;
import org.light.domain.Domain;
import org.light.domain.Prism;
import org.light.domain.Util;
import org.light.easyuilayouts.widgets.Nav;
import org.light.generator.DBDefinitionGenerator;
import org.light.generator.TwoDomainsDBDefinitionGenerator;

public abstract class Module implements Comparable<Module>{
	protected String standardName;
	protected String packageToken;
	protected List<Prism> prisms = new ArrayList<Prism>();
	protected List<Domain> domains = new ArrayList<Domain>();
	protected List<List<Domain>> dataDomains = new ArrayList<>();
	protected List<Util> utils = new ArrayList<Util>();
	protected List<DBDefinitionGenerator> dbDefinitionGenerators = new ArrayList<DBDefinitionGenerator>();
	protected List<Configable> configables = new ArrayList<Configable>();
	protected String label;
	protected String projectName;
	protected List<ConfigFile> configFiles = new ArrayList<ConfigFile>();
	protected List<TwoDomainsDBDefinitionGenerator> myTwoDBGenerators = new ArrayList<TwoDomainsDBDefinitionGenerator>();
	protected String folderPath = "/home/jerry/temp/";	
	protected String sourceFolderPath = "/home/jerry/temp/";	
	protected String moduleSourceFolderPath = "/home/jerry/temp/";
	protected String crossOrigin = "";
	protected String dbName;
	protected String dbPrefix = "";
	protected String dbUsername = "root";
	protected String dbPassword = "";
	protected String dbType = "MariaDB";
	protected boolean emptypassword = false;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String resolution = "high";
	protected String language = "chinese";
	protected String schema = "normal";
	protected String domainSuffix = "domain";
	protected String daoSuffix = "dao";
	protected String daoimplSuffix = "daoimpl";
	protected String serviceSuffix = "service";
	protected String serviceimplSuffix = "serviceimpl";
	protected String controllerSuffix = "controller";
	protected String domainNamingSuffix = "";
	protected String controllerNamingSuffix = "Controller";
	protected String technicalstack = "tower";
	protected Nav nav;
	
	public String getSourceFolderPath() {
		return sourceFolderPath;
	}
	public void setSourceFolderPath(String sourceFolderPath) {
		this.sourceFolderPath = sourceFolderPath;
	}
	public List<ConfigFile> getConfigFiles() {
		return configFiles;
	}
	public void setConfigFiles(List<ConfigFile> configFiles) {
		this.configFiles = configFiles;
	}
	public void addConfigFile(ConfigFile cf) {
		this.configFiles.add(cf);
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	public List<Util> getUtils() {
		return utils;
	}
	public void setUtils(List<Util> utils) {
		this.utils = utils;
	}
	public void addUtil(Util u) {
		this.utils.add(u);
	}
	public List<DBDefinitionGenerator> getDbDefinitionGenerators() {
		return dbDefinitionGenerators;
	}
	public void setDbDefinitionGenerators(List<DBDefinitionGenerator> dbDefinitionGenerators) {
		this.dbDefinitionGenerators = dbDefinitionGenerators;
	}
	public void addDbDefinitionGenerator(DBDefinitionGenerator dbdg) {
		this.dbDefinitionGenerators.add(dbdg);
	}
	public List<Configable> getConfigables() {
		return configables;
	}
	public void setConfigables(List<Configable> configables) {
		this.configables = configables;
	}
	public void addConfigable(Configable cf) {
		this.configables.add(cf);
	}
	public String getDbPrefix() {
		return dbPrefix;
	}
	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public abstract void generateModuleFiles(String targetFolderPath,Boolean genUi, Boolean genController,
			Boolean genService, Boolean genServiceImpl, Boolean genDao, Boolean genDaoImpl) throws Exception;
	public abstract void generatePrismsFromDomians() throws Exception;
	public abstract List<List<Domain>> decorateDataDomains(List<List<Domain>> dataDomains)throws Exception;
	public String getModuleSourceFolderPath() {
		return moduleSourceFolderPath;
	}
	public void setModuleSourceFolderPath(String moduleSourceFolderPath) {
		this.moduleSourceFolderPath = moduleSourceFolderPath;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public List<Prism> getPrisms() {
		return prisms;
	}
	public void setPrisms(List<Prism> prisms) {
		this.prisms = prisms;
	}
	public void addPrism(Prism prism) {
		this.prisms.add(prism);
	}
	public List<Domain> getDomains() {
		return domains;
	}
	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}
	public List<List<Domain>> getDataDomains() {
		return dataDomains;
	}
	public void setDataDomains(List<List<Domain>> dataDomains) {
		this.dataDomains = dataDomains;
	}
	public List<TwoDomainsDBDefinitionGenerator> getMyTwoDBGenerators() {
		return myTwoDBGenerators;
	}
	public void setMyTwoDBGenerators(List<TwoDomainsDBDefinitionGenerator> myTwoDBGenerators) {
		this.myTwoDBGenerators = myTwoDBGenerators;
	}
	public String getCrossOrigin() {
		return crossOrigin;
	}
	public void setCrossOrigin(String crossOrigin) {
		this.crossOrigin = crossOrigin;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbUsername() {
		return dbUsername;
	}
	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public boolean isEmptypassword() {
		return emptypassword;
	}
	public void setEmptypassword(boolean emptypassword) {
		this.emptypassword = emptypassword;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public String getFooter() {
		return footer;
	}
	public void setFooter(String footer) {
		this.footer = footer;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getDomainSuffix() {
		return domainSuffix;
	}
	public void setDomainSuffix(String domainSuffix) {
		this.domainSuffix = domainSuffix;
	}
	public String getDaoSuffix() {
		return daoSuffix;
	}
	public void setDaoSuffix(String daoSuffix) {
		this.daoSuffix = daoSuffix;
	}
	public String getDaoimplSuffix() {
		return daoimplSuffix;
	}
	public void setDaoimplSuffix(String daoimplSuffix) {
		this.daoimplSuffix = daoimplSuffix;
	}
	public String getServiceSuffix() {
		return serviceSuffix;
	}
	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}
	public String getServiceimplSuffix() {
		return serviceimplSuffix;
	}
	public void setServiceimplSuffix(String serviceimplSuffix) {
		this.serviceimplSuffix = serviceimplSuffix;
	}
	public String getControllerSuffix() {
		return controllerSuffix;
	}
	public void setControllerSuffix(String controllerSuffix) {
		this.controllerSuffix = controllerSuffix;
	}
	public String getDomainNamingSuffix() {
		return domainNamingSuffix;
	}
	public void setDomainNamingSuffix(String domainNamingSuffix) {
		this.domainNamingSuffix = domainNamingSuffix;
	}
	public String getControllerNamingSuffix() {
		return controllerNamingSuffix;
	}
	public void setControllerNamingSuffix(String controllerNamingSuffix) {
		this.controllerNamingSuffix = controllerNamingSuffix;
	}
	public String getTechnicalstack() {
		return technicalstack;
	}
	public void setTechnicalstack(String technicalstack) {
		this.technicalstack = technicalstack;
	}

	@Override
	public int compareTo(Module o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public Nav getNav() {
		return nav;
	}
	public void setNav(Nav nav) {
		this.nav = nav;
	}
}
