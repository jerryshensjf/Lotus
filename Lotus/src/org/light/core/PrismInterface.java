package org.light.core;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.light.domain.Domain;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.EasyUILayout;
import org.light.easyuilayouts.widgets.Nav;

public abstract class PrismInterface implements Comparable<PrismInterface> ,Serializable{
	protected static Logger logger = Logger.getLogger(PrismInterface.class);
	protected Long serial;
	protected String standardName;
	protected String label;
	protected Domain domain = new Domain();
	protected Set<Verb> verbs = new TreeSet<>();
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EasyUILayout eLayout;
	protected Nav nav;
	protected String technicalStack = "tower";
	protected String dbType = "MariaDB";
	protected String language = "Chinese";
	public abstract void generatePIFiles(String targetFolderPath) throws Exception;
	public abstract void generatePIFromDomian() throws Exception;
	public abstract boolean validateDomain() throws Exception;
	public abstract boolean validateVerbs() throws Exception;
	public abstract boolean validateLayout() throws Exception;
	public abstract void setTitles(String title, String subTitle,String footer);
	
	public String getTechnicalStack() {
		return technicalStack;
	}

	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	
	public Set<Verb> getVerbs() {
		return verbs;
	}
	
	public void setVerbs(Set<Verb> verbs) {
		this.verbs = verbs;
	}
	
	@Override
	public int compareTo(PrismInterface o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}
	public Long getSerial() {
		return serial;
	}
	public void setSerial(Long serial) {
		this.serial = serial;
	}
	public Nav getNav() {
		return nav;
	}
	public void setNav(Nav nav) {
		this.nav = nav;
	}
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public EasyUILayout geteLayout() {
		return eLayout;
	}
	public void seteLayout(EasyUILayout eLayout) {
		this.eLayout = eLayout;
	}
	public EasyUIFrameSet getFrame() {
		return frame;
	}
	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
}
