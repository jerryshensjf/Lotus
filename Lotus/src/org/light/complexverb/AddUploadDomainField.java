package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class AddUploadDomainField extends DomainFieldVerb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(getVerbName()));
		method.addSignature(new Signature(1, "mut multipart","Multipart"));
		method.setReturnType(new Type("String"));
		Set<String> imports = new TreeSet<>();
		method.setAdditionalImports(imports);
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,2,"while let Some(field) = multipart.next_field().await.unwrap() {"));
		sList.add(new Statement(serial+2000L,3,"let name = field.name().unwrap().to_string();"));
		sList.add(new Statement(serial+3000L,3,"if name == \"files[]\" {"));
		sList.add(new Statement(serial+4000L,4,"let content_type = field.content_type().unwrap().to_string();"));
		sList.add(new Statement(serial+5000L,4,"if content_type.starts_with(\"image/\") {"));
		sList.add(new Statement(serial+6000L,5,"let data = field.bytes().await.unwrap();"));
		sList.add(new Statement(serial+7000L,5,"let data_str = base64::encode(data);"));
		sList.add(new Statement(serial+8000L,5,"store_"+this.domain.getSnakeDomainName()+"_picture(\""+this.field.getSnakeFieldName()+"\".to_string(),data_str.clone());"));
		sList.add(new Statement(serial+9000L,5,""));
		sList.add(new Statement(serial+10000L,5,"let mut map = Map::new();"));
		sList.add(new Statement(serial+11000L,5,"map.insert(\"success\".to_string(), Value::from(true));"));
		sList.add(new Statement(serial+12000L,5,"map.insert("));
		sList.add(new Statement(serial+13000L,6,"\"rows\".to_string(),"));
		sList.add(new Statement(serial+14000L,6,"Value::from(data_str.clone()),"));
		sList.add(new Statement(serial+15000L,5,");"));
		sList.add(new Statement(serial+16000L,4,""));
		sList.add(new Statement(serial+17000L,5,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
		sList.add(new Statement(serial+19000L,5,"return resultjson;"));
		sList.add(new Statement(serial+20000L,4,"}"));
		sList.add(new Statement(serial+21000L,3,"}"));
		sList.add(new Statement(serial+22000L,2,"}"));
		sList.add(new Statement(serial+23000L,2,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	public AddUploadDomainField(Domain domain,Field field){
		super();
		this.domain = domain;
		this.field = field;
		this.verbName = "AddUpload"+this.domain.getStandardName()+this.field.getCapFirstFieldName();
		this.setLabel("新增上传");
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setStandardName("upload"+StringUtil.capFirst(this.domain.getStandardName())+this.field.getCapFirstFieldName());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"$('#add"+this.domain.getCapFirstDomainName()+this.field.getCapFirstFieldName()+"Fileupload').fileupload({"));
		sList.add(new Statement(2000L,3,"autoUpload: true,"));
		sList.add(new Statement(3000L,3,"dataType: 'json',"));
		sList.add(new Statement(4000L,3,"async: false,"));
		sList.add(new Statement(5000L,3,"success: function(data, textStatus) {"));
		sList.add(new Statement(6000L,3,"if (data.success == true){"));
		sList.add(new Statement(7000L,4,"$(\"#ff\").find(\"#"+this.field.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+data.rows)"));
		sList.add(new Statement(8000L,3,"}"));
		sList.add(new Statement(9000L,3,"},"));
		sList.add(new Statement(10000L,3,"progressall: function (e, data) {"));
		sList.add(new Statement(11000L,4,"var progress = parseInt(data.loaded / data.total * 100, 10);"));
		sList.add(new Statement(12000L,3,"},"));
		sList.add(new Statement(13000L,2,"});"));
		block.setMethodStatementList(WriteableUtil.merge(sList));
		return block;

	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("show"+this.domain.getCapFirstDomainName()+this.field.getCapFirstFieldName()+"Image");
		
		Signature s1 = new Signature(1,"value","var");
		Signature s2 = new Signature(2,"row","var");
		Signature s3 = new Signature(2,"index","var");
		method.addSignature(s1);
		method.addSignature(s2);
		method.addSignature(s3);
		
		StatementList sList = new StatementList();
		sList.add(new Statement(1000L,1,"if(row."+this.field.getLowerFirstFieldName()+"){"));
		sList.add(new Statement(2000L,2,"return \"<img style='height:50px;' border='1' src='data:image/png;base64,\"+row."+this.field.getLowerFirstFieldName()+"+\"'/>\";"));
		sList.add(new Statement(3000L,1,"} else {"));
		sList.add(new Statement(4000L,2,"return \"<img style='height:50px;' border='1' src='../images/blank.jpg'/>\";"));
		sList.add(new Statement(5000L,1,"}"));
		
		method.setMethodStatementList(sList);
		return method;	
	}
}
