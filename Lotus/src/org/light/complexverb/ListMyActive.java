package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindById;

public class ListMyActive extends TwoDomainVerb{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.addSignature(new Signature(2, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		method.setReturnType(new Type("Result<Vec<"+this.slave.getCapFirstDomainNameWithSuffix()+">, Error>"));
		method.addAdditionalImport("super::"+this.slave.getCapFirstDomainNameWithSuffix());	
		method.setTempTag(this.slave.getCapFirstDomainNameWithSuffix());
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		if (!(this.slave instanceof org.light.domain.Enum)) {
			sList.add(new Statement(serial+2000L,2,"let result = sqlx::query_as("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+MybatisSqlReflector.generateSelectActiveUsingMasterIdStatement(master,slave)+"\"#"));
			} else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+PgsqlReflector.generateSelectActiveUsingMasterIdStatement(master,slave)+"\"#"));
			} else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+MybatisOracleSqlReflector.generateSelectActiveUsingMasterIdStatement(master,slave)+"\"#"));
			}
		} else {
			sList.add(new Statement(serial+2000L,2,"let result = sqlx::query_as("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+MybatisSqlReflector.generateSelectActiveIdsUsingMasterIdStatement(master,slave)+"\"#"));
			} else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+PgsqlReflector.generateSelectActiveIdsUsingMasterIdStatement(master,slave)+"\"#"));
			} else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(serial+3000L,3,"r#\""+MybatisOracleSqlReflector.generateSelectActiveIdsUsingMasterIdStatement(master,slave)+"\"#"));
			}
		}
		sList.add(new Statement(serial+3500L,2,")"));
		sList.add(new Statement(serial+4000L,2,".bind("+this.master.getSnakeDomainName()+"_id)"));
		sList.add(new Statement(serial+5000L,2,".fetch_all(&(&*self.pool).clone().unwrap())"));
		sList.add(new Statement(serial+6000L,2,".await;"));

		sList.add(new Statement(serial+11000L,2,"return result;"));
		
		if (this.master.getDbType().equalsIgnoreCase("oracle")) {
			method.setMethodStatementList(getOracleDaoimplStatementList());
		}else {
			method.setMethodStatementList(WriteableUtil.merge(sList));
		}
		return method;
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		if (this.slave instanceof org.light.domain.Enum) {
			sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateSelectActiveIdsUsingMasterIdStatement(master,slave)+"\")"));
		} else {
			sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateSelectActiveUsingMasterIdStatement(master,slave)+"\")"));
		}
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let rows = stmt.query_as(&[&"+this.master.getSnakeDomainName()+"_id"+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let mut "+StringUtil.getSnakeName(this.slave.getPlural())+" = Vec::new();"));
		sList.add(new Statement(serial+8000L,1,"for row_result in rows {"));
		sList.add(new Statement(serial+9000L,2,"let row:Row = row_result?;"));
		sList.add(new Statement(serial+10000L,2,"let "+this.slave.getSnakeDomainName()+" = "+this.slave.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+11000L,2,""+StringUtil.getSnakeName(this.slave.getPlural())+".push("+this.slave.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+12000L,1,"}"));
		sList.add(new Statement(serial+13000L,1,"Ok("+StringUtil.getSnakeName(this.slave.getPlural())+")"));

		return WriteableUtil.merge(sList);
	}
	
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));
		method.setReturnType(new Type("[] "+this.slave.getDomainSuffix()+"."+this.slave.getType()));
		method.addSignature(new Signature(1, "db","*sql.DB"));
		method.addSignature(new Signature(2,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));
		method.setReturnType(new Type("[] "+this.slave.getDomainSuffix()+"."+this.slave.getType()));
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));		
		method.addSignature(new Signature(1, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		method.setReturnType(new Type("Result<Vec<"+this.slave.getCapFirstDomainNameWithSuffix()+">, Error>"));
		method.addAdditionalImport("crate::"+this.slave.getDomainSuffix()+"::"+this.slave.getCapFirstDomainNameWithSuffix());
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		if (this.slave instanceof org.light.domain.Enum) {
			FindById find = new FindById(this.slave);
			method.addAdditionalImport("crate::"+this.slave.getServiceimplSuffix()+"::"+this.slave.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(find.getVerbName()));
			sList.add(new Statement(serial+1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(serial+2000L,1,"let "+this.slave.getSnakeDomainName()+"_ids = app_state.await.context."+StringUtil.getSnakeName(this.slave.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.master.getSnakeDomainName()+"_id).await.unwrap();"));
			sList.add(new Statement(serial+3000L,1,"let mut _result:Vec<"+this.slave.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
			sList.add(new Statement(serial+4000L,1,"for "+this.slave.getSnakeDomainName()+" in &"+this.slave.getSnakeDomainName()+"_ids {"));		
			sList.add(new Statement(serial+5000L,1,"_result.push("+StringUtil.getSnakeName(find.getVerbName())+"("+this.slave.getSnakeDomainName()+"."+this.slave.getDomainId().getSnakeFieldName()+").await.unwrap().clone());"));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,1,"return Ok(_result);"));
		} else {
			sList.add(new Statement(serial+1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(serial+2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.slave.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.master.getSnakeDomainName()+"_id).await"));
		}
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));
		method.addSignature(new Signature(1,"Form("+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_mtm_request)","Form<"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAliasOrName()+"MtmRequest>")));
		method.setReturnType(new Type("String"));
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let "+this.master.getSnakeDomainName()+"_id = "+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_mtm_request."+this.master.getSnakeDomainName()+"_id.unwrap_or_default();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let "+StringUtil.getSnakeName(this.slave.getPlural())+" = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.master.getSnakeDomainName()+"_id).await;"));
		sList.add(new Statement(serial+4000L,1,"match "+StringUtil.getSnakeName(this.slave.getPlural())+" {"));
		sList.add(new Statement(serial+5000L,2,"Err(_) => {"));
		sList.add(new Statement(serial+7000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
		sList.add(new Statement(serial+8000L,2,"},"));
		sList.add(new Statement(serial+9000L,2,"Ok("+StringUtil.getSnakeName(this.slave.getPlural())+") => {"));
		sList.add(new Statement(serial+10000L,3,"let json = serde_json::to_string_pretty(&"+StringUtil.getSnakeName(this.slave.getPlural())+").unwrap();"));
		sList.add(new Statement(serial+12000L,0,""));
		sList.add(new Statement(serial+13000L,3,"let mut map = Map::new();"));
		sList.add(new Statement(serial+14000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
		sList.add(new Statement(serial+15000L,3,"map.insert("));
		sList.add(new Statement(serial+16000L,4,"\"rows\".to_string(),"));
		sList.add(new Statement(serial+17000L,4,"serde_json::from_str(&json).unwrap(),"));
		sList.add(new Statement(serial+18000L,3,");"));
		sList.add(new Statement(serial+19000L,2,""));
		sList.add(new Statement(serial+20000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
		sList.add(new Statement(serial+22000L,3,"return resultjson;"));
		sList.add(new Statement(serial+23000L,2,"}"));
		sList.add(new Statement(serial+24000L,1,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	public ListMyActive(Domain master,Domain slave) throws ValidateException{
		super();
		this.master = master;
		this.slave = slave;
		this.setVerbName("ListActive"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAliasPlural())+"Using"+this.master.getCapFirstDomainName()+"Id");
		this.setLabel("列出所属");
		this.dbType = master.getDbType();
	}
	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(this.getVerbName()));
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.addSignature(new Signature(2, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		method.setReturnType(new Type("Result<Vec<"+this.slave.getCapFirstDomainNameWithSuffix()+">, Error>"));
		method.addAdditionalImport("super::"+this.slave.getCapFirstDomainNameWithSuffix());	
		method.setTempTag(this.slave.getCapFirstDomainNameWithSuffix());
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let mut results:Vec<"+this.slave.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
		sList.add(new Statement(serial+2000L,1,"let _link_db = get_"+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAlias())+"_link_db();"));
		sList.add(new Statement(serial+3000L,1,"for (_index, link_"+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAlias())+") in _link_db.iter().enumerate() {"));
		sList.add(new Statement(serial+4000L,2,"if link_"+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAlias())+"."+this.master.getSnakeDomainName()+"_id == "+this.master.getSnakeDomainName()+"_id {"));
		FindById findSlave = new FindById(this.slave);
		sList.add(new Statement(serial+5000L,3,"let "+this.slave.getSnakeDomainName()+" = "+StringUtil.getSnakeName(findSlave.getVerbName())+"(link_"+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAlias())+"."+StringUtil.getSnakeName(this.slave.getAlias())+"_id).await.unwrap();"));
		sList.add(new Statement(serial+6000L,6,"results.push("+this.slave.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+7000L,4,"}"));
		sList.add(new Statement(serial+8000L,3,"}"));
		sList.add(new Statement(serial+9000L,1,"Ok(results)"));
		
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

}
