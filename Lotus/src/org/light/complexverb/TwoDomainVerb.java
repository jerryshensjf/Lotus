package org.light.complexverb;

import java.io.Serializable;

import org.light.domain.Domain;
import org.light.domain.Method;

public abstract class TwoDomainVerb implements Comparable<TwoDomainVerb>,Serializable{
	protected Domain master;
	protected Domain slave;
	protected String label;
	protected String verbName;
	protected boolean denied = false;
	protected String dbType = "MariaDB";
	
	public abstract Method generateDummyDaoImplMethod() throws Exception;
	public abstract Method generateDaoImplMethod() throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract Method generateControllerMethod() throws Exception;
	public Domain getMaster() {
		return master;
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(TwoDomainVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
	
	public void setSlaveAlias(String slaveAlias){
		this.slave.setAlias(slaveAlias);
	}
	public boolean isDenied() {
		return denied;
	}
	public void setDenied(boolean denied) {
		this.denied = denied;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
}
