package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Assign extends TwoDomainVerb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.setStandardName("Assign"+this.slave.getCapFirstDomainName()+"To"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("Assign"+StringUtil.capFirst(this.slave.getAlias())+"To"+this.master.getCapFirstDomainName());
		}
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.addSignature(new Signature(2, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		method.addSignature(new Signature(3,  StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id",this.slave.getDomainId().getFieldType()));
		method.setReturnType(new Type("Result<u64, Error>"));
		
		List<Writeable> sList = new ArrayList<Writeable>();
	
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,2,"let result = sqlx::query("));
		if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
			sList.add(new Statement(serial+2000L,3,"r#\""+ MybatisSqlReflector.generateInsertLinkTwoSql(master,slave)+"\"#"));
		}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
			sList.add(new Statement(serial+2000L,3,"r#\""+ PgsqlReflector.generateInsertLinkTwoSql(master,slave)+"\"#"));
		}else if (this.getDbType().equalsIgnoreCase("Oracle")) {
			sList.add(new Statement(serial+2000L,3,"r#\""+ MybatisOracleSqlReflector.generateInsertLinkTwoSql(master,slave)+"\"#"));
		}
		sList.add(new Statement(serial+2500L,2,")"));
		sList.add(new Statement(serial+3000L,2,".bind("+this.master.getSnakeDomainName()+"_id)"));
		sList.add(new Statement(serial+4000L,2,".bind("+ StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id)"));
		sList.add(new Statement(serial+5000L,2,".execute(&(&*self.pool).clone().unwrap())"));
		sList.add(new Statement(serial+6000L,2,".await;"));
		
		sList.add(new Statement(serial+10000L,2,"match result {"));
		sList.add(new Statement(serial+11000L,3,"Ok(result) => {"));
		sList.add(new Statement(serial+12000L,4,"Ok(result.rows_affected())"));
		sList.add(new Statement(serial+13000L,3,"},"));
		sList.add(new Statement(serial+14000L,3,"Err(err) => {"));
		sList.add(new Statement(serial+15000L,4,"Err(err)"));
		sList.add(new Statement(serial+16000L,3,"}"));
		sList.add(new Statement(serial+17000L,2,"}"));

		if (this.master.getDbType().equalsIgnoreCase("oracle")) {
			method.setMethodStatementList(getOracleDaoimplStatementList());
		}else {
			method.setMethodStatementList(WriteableUtil.merge(sList));
		}
		return method;
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateInsertLinkTwoSql(this.master,this.slave)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let _ = stmt.execute(&[&"+this.master.getSnakeDomainName()+"_id,&"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id]);"));
		sList.add(new Statement(serial+7000L,1,"let _ = conn.commit();"));
		sList.add(new Statement(serial+8000L,1,"Ok(0 as u64)"));
		return WriteableUtil.merge(sList);
	}
	
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.setStandardName("Assign"+this.slave.getCapFirstDomainName()+"To"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("Assign"+StringUtil.capFirst(this.slave.getAlias())+"To"+this.master.getCapFirstDomainName());
		}
		method.setReturnType(new Type("int64"));
		
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType(),this.master.getPackageToken(),"Param(value=\"" + this.master.getLowerFirstDomainName() + "Id\")"));
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Id", this.slave.getDomainId().getClassType(),this.slave.getPackageToken(),"Param(value=\"" + this.slave.getLowerFirstDomainName() + "Id\")"));
		}else{
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Id", this.slave.getDomainId().getClassType(),this.slave.getPackageToken(),"Param(value=\"" + StringUtil.lowerFirst(this.slave.getAlias()) + "Id\")"));
		}
		return method;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(getVerbName()));
		method.setReturnType(new Type("bool"));
		method.setThrowException(true);
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){		
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		}else{
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String")));
		}
		return method;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(getVerbName()));
		method.addSignature(new Signature(1, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		if (StringUtil.isBlank(this.slave.getAlias())){
			method.addSignature(new Signature(2, this.slave.getSnakeDomainName()+"_ids",new Type("Vec<"+this.slave.getDomainId().getFieldType()+">")));
		}else {		
			method.addSignature(new Signature(2, StringUtil.getSnakeName(this.slave.getAlias())+"_ids",new Type("Vec<"+this.slave.getDomainId().getFieldType()+">")));
		}
		method.setReturnType(new Type("Result<u64, Error>"));
		
		String slaveIds = StringUtil.getSnakeName(this.slave.getStandardName())+"_ids";
		if (!StringUtil.isBlank(this.slave.getAlias())){
			slaveIds = StringUtil.getSnakeName(this.slave.getAlias())+"_ids";
		}
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let app_state = init_db();"));
		sList.add(new Statement(serial+2000L,1,"let "+StringUtil.getSnakeName(this.master.getPlural())+" = &app_state.await.context."+StringUtil.getSnakeName(this.master.getPlural())+";"));
		sList.add(new Statement(serial+3000L,1,"for "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id in "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids.iter() {"));
		sList.add(new Statement(serial+4000L,2,""+StringUtil.getSnakeName(this.master.getPlural())+"."+StringUtil.getSnakeName(generateDaoImplMethod().getStandardName())+"("+this.master.getSnakeDomainName()+"_id,*"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id).await.unwrap();"));
		sList.add(new Statement(serial+5000L,1,"}"));
		sList.add(new Statement(serial+6000L,1,"Ok(0)"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.capFirst(getVerbName()));
		method.addSignature(new Signature(1,"Form("+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_mtm_request)","Form<"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAliasOrName()+"MtmRequest>")));
		method.setReturnType(new Type("String"));
		
		String slaveIds = StringUtil.lowerFirst(this.slave.getStandardName())+"Ids";
		if (!StringUtil.isBlank(this.slave.getAlias())){
			slaveIds = StringUtil.lowerFirst(this.slave.getAlias())+"Ids";
		}
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let "+this.master.getSnakeDomainName()+"_id = "+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_mtm_request."+this.master.getSnakeDomainName()+"_id.unwrap_or_default();"));
		sList.add(new Statement(serial+2000L,1,"let "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids_str = "+this.master.getSnakeDomainName()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_mtm_request."+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids.unwrap_or_default();"));
		sList.add(new Statement(serial+3000L,1,"let string_ids:Vec<&str> = "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids_str.split(',').collect();"));
		sList.add(new Statement(serial+4000L,1,"let mut "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids:Vec<i64> = Vec::new();"));
		sList.add(new Statement(serial+5000L,0,""));
		sList.add(new Statement(serial+6000L,1,"let vecter_iterator = string_ids.iter();"));
		sList.add(new Statement(serial+7000L,1,"for "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id in vecter_iterator {"));
		sList.add(new Statement(serial+8000L,2,StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids.push("+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id.parse::<i64>().unwrap());"));
		sList.add(new Statement(serial+9000L,1,"}"));
		sList.add(new Statement(serial+10000L,0,""));
		sList.add(new Statement(serial+15000L,1,"let _result = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.master.getSnakeDomainName()+"_id,"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_ids).await;"));
		sList.add(new Statement(serial+16000L,1,"match _result {"));
		sList.add(new Statement(serial+17000L,2,"Err(_) => {"));
		sList.add(new Statement(serial+19000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
		sList.add(new Statement(serial+20000L,2,"},"));
		sList.add(new Statement(serial+21000L,2,"Ok(_result) => {"));
		sList.add(new Statement(serial+22000L,3,"let mut map = Map::new();"));
		sList.add(new Statement(serial+23000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
		sList.add(new Statement(serial+24000L,3,"map.insert("));
		sList.add(new Statement(serial+25000L,4,"\"rows\".to_string(),"));
		sList.add(new Statement(serial+26000L,4,"Value::from(\"\"),"));
		sList.add(new Statement(serial+27000L,3,");"));
		sList.add(new Statement(serial+28000L,3,""));
		sList.add(new Statement(serial+29000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
		sList.add(new Statement(serial+31000L,3,"return resultjson;"));
		sList.add(new Statement(serial+32000L,2,"}"));
		sList.add(new Statement(serial+33000L,1,"}"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	public Assign(Domain master,Domain slave) throws ValidateException{
		super();
		this.master = master;
		this.slave = slave;
		if (this.slave!=null&&StringUtil.isBlank(this.slave.getAlias())){	
			this.setVerbName("Assign"+this.slave.getCapFirstPlural()+"To"+this.master.getCapFirstDomainName());
		}else {
			this.setVerbName("Assign"+StringUtil.capFirst(this.slave.getAliasPlural())+"To"+this.master.getCapFirstDomainName());
		}
		this.setLabel("分配");
		this.dbType = master.getDbType();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		
		StatementList sl = new StatementList();
		if (StringUtil.isBlank(this.slave.getAlias())){	
			sl.add(new Statement(1000,1, "var "+this.slave.getLowerFirstDomainName()+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#myAvailable"+this.slave.getCapFirstPlural()+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			if ("english".equalsIgnoreCase(this.master.getLanguage())) {
				sl.add(new Statement(4000,1, "$.messager.alert(\"Warning\", \"Please select available "+this.slave.getText()+"！\");"));
			}else {
				sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择可添加"+this.slave.getText()+"！\");"));
			}
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, this.slave.getLowerFirstDomainName()+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, this.slave.getLowerFirstDomainName()+"Ids:"+this.slave.getLowerFirstDomainName()+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));
		} else {
			sl.add(new Statement(1000,1, "var "+StringUtil.lowerFirst(this.slave.getAlias())+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#myAvailable"+StringUtil.capFirst(this.slave.getAliasPlural())+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			if ("english".equalsIgnoreCase(this.master.getLanguage())) {
				sl.add(new Statement(4000,1, "$.messager.alert(\"Warning\", \"Please select available "+this.slave.getAliasText()+"！\");"));
			}else {
				sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择可添加"+this.slave.getAliasText()+"！\");"));
			}
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../"+this.master.getControllerPackagePrefix()+this.master.getLowerFirstDomainName()+this.master.getControllerNamingSuffix()+"/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids:"+StringUtil.lowerFirst(this.slave.getAlias())+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));			
		}
		
		method.setMethodStatementList(sl);
		return method;	
	}
	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.setStandardName("Assign"+this.slave.getCapFirstDomainName()+"To"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("Assign"+StringUtil.capFirst(this.slave.getAlias())+"To"+this.master.getCapFirstDomainName());
		}
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.addSignature(new Signature(2, this.master.getSnakeDomainName()+"_id",this.master.getDomainId().getFieldType()));
		method.addSignature(new Signature(3,  StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id",this.slave.getDomainId().getFieldType()));
		method.setReturnType(new Type("Result<u64, Error>"));
		
		List<Writeable> sList = new ArrayList<Writeable>();
	
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let mut _link_db = "+this.master.getSnakeDomainName().toUpperCase()+"_"+StringUtil.getSnakeName(this.slave.getAliasOrName()).toUpperCase()+"_LINK_DB.lock().unwrap();"));
		sList.add(new Statement(serial+2000L,1,"_link_db.retain(|_link| if (*_link)."+this.master.getSnakeDomainName()+"_id"+" == "+this.master.getSnakeDomainName()+"_id"+" &&(*_link)."+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id"+" == "+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id"+" {false } else { true });"));
		sList.add(new Statement(serial+3000L,1,""));
		sList.add(new Statement(serial+4000L,1,"let _link = Link"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAlias())+this.master.getDomainNamingSuffix()+"{"));
		sList.add(new Statement(serial+5000L,2,this.master.getSnakeDomainName()+"_id"+":"+this.master.getSnakeDomainName()+"_id,"));
		sList.add(new Statement(serial+6000L,2,StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id:"+StringUtil.getSnakeName(this.slave.getAliasOrName())+"_id,"));
		sList.add(new Statement(serial+7000L,1,"};"));
		sList.add(new Statement(serial+8000L,1,"_link_db.push(_link);"));
		sList.add(new Statement(serial+9000L,1,"return Ok(0 as u64);"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
}
