package org.light.complexverb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class UpdateUploadDomainField extends DomainFieldVerb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}
	public UpdateUploadDomainField(Domain domain,Field field){
		super();
		this.domain = domain;
		this.field = field;
		this.verbName = "UpdateUpload"+this.domain.getStandardName()+this.field.getCapFirstFieldName();
		this.setLabel("更新上传");
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setStandardName("upload"+StringUtil.capFirst(this.domain.getStandardName())+this.field.getCapFirstFieldName());
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"$('#"+this.domain.getLowerFirstDomainName()+this.field.getCapFirstFieldName()+"Fileupload').fileupload({"));
		sList.add(new Statement(2000L,3,"autoUpload: true,"));
		sList.add(new Statement(3000L,3,"dataType: 'json',"));
		sList.add(new Statement(4000L,3,"async: false,"));
		sList.add(new Statement(5000L,3,"success: function(data, textStatus) {"));
		sList.add(new Statement(6000L,3,"if (data.success == true){"));
		sList.add(new Statement(7000L,4,"$(\"#ffedit\").find(\"#"+this.field.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+data.rows)"));
		sList.add(new Statement(8000L,3,"}"));
		sList.add(new Statement(9000L,3,"},"));
		sList.add(new Statement(10000L,3,"progressall: function (e, data) {"));
		sList.add(new Statement(11000L,4,"var progress = parseInt(data.loaded / data.total * 100, 10);"));
		sList.add(new Statement(12000L,3,"},"));
		sList.add(new Statement(13000L,2,"});"));
		block.setMethodStatementList(WriteableUtil.merge(sList));
		return block;

	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("show"+this.domain.getCapFirstDomainName()+this.field.getCapFirstFieldName()+"Image");
		
		Signature s1 = new Signature(1,"value","var");
		Signature s2 = new Signature(2,"row","var");
		Signature s3 = new Signature(2,"index","var");
		method.addSignature(s1);
		method.addSignature(s2);
		method.addSignature(s3);
		
		StatementList sList = new StatementList();
		sList.add(new Statement(1000L,1,"if(row."+this.field.getLowerFirstFieldName()+"){"));
		sList.add(new Statement(2000L,2,"return \"<img style='height:50px;' border='1' src='data:image/png;base64,\"+row."+this.field.getLowerFirstFieldName()+"+\"'/>\";"));
		sList.add(new Statement(3000L,1,"} else {"));
		sList.add(new Statement(4000L,2,"return \"<img style='height:50px;' border='1' src='../images/blank.jpg'/>\";"));
		sList.add(new Statement(5000L,1,"}"));
		
		method.setMethodStatementList(sList);
		return method;	
	}
}
