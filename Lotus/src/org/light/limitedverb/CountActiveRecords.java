package org.light.limitedverb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.exception.ValidateException;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class CountActiveRecords extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CountActive" + domain.getCapFirstDomainName() + "Records");
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.setReturnType(new Type("Result<CountNum, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();

			sList.add(new Statement(500L,2,"let result = sqlx::query_as("));
			if ("oracle".equalsIgnoreCase(this.domain.getDbType())) {
				sList.add(new Statement(1000L,2,"r#\"select count(*) as countNum from " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " where " + 
						this.domain.getActive().getFeildNameAsTableColumn() + " = " + 
						this.domain.getDomainActiveInteger() + "\"#"));
			}else {
				sList.add(new Statement(1000L,2,"r#\"select count(*) as countNum from " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " where " + 
					this.domain.getActive().getFeildNameAsTableColumn() + " = " + 
					this.domain.getDomainActiveStr() + "\"#"));
			}
			sList.add(new Statement(4000L,2,")"));
			sList.add(new Statement(5000L,2,".fetch_one(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(6000L,2,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));

			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
			
		sList.add(new Statement(serial+1000L,1,"let query = \"select count(*) as countNum from " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " where " + 
				this.domain.getActive().getFeildNameAsTableColumn() + " = " + this.domain.getDomainActiveInteger() + "\";"));

		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(&query)"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let result:i64 = stmt.query_row_as(&[]).unwrap();"));
		sList.add(new Statement(serial+7000L,1,"Ok(CountNum{count_num:result})"));

		return WriteableUtil.merge(sList);
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			String s = m.generateMethodString();
			return s;
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);

			return m.generateMethodString();
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CountActive" + this.domain.getCapFirstDomainName() + "Records");
			method.addSignature(new Signature(1, "db","*sql.DB"));
			method.setReturnType(new Type("int64"));
			return method;
		}
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CountActive" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("int64"));

			return method;
		}
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CountActive" + this.domain.getCapFirstDomainName() + "Records");
			method.setReturnType(new Type("Result<CountNum, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut count:i64 = 0;"));
			sList.add(new Statement(serial+2000L,1,"for "+this.domain.getSnakeDomainName()+" in get_db() {"));
			sList.add(new Statement(serial+3000L,2,"if "+this.domain.getSnakeDomainName()+"."+this.domain.getActive().getSnakeFieldName()+" == "+this.domain.getDomainActiveStr()+" {"));
			sList.add(new Statement(serial+4000L,3,"count += 1;"));
			sList.add(new Statement(serial+5000L,2,"}"));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,1,"Ok(CountNum{count_num:count})"));
			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied)
			return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied)
			return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	public CountActiveRecords(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("ToggleOne");
		this.verbName = "countActive" + this.domain.getCapFirstDomainName() + "Records";
	}

	public CountActiveRecords() {
		super();
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CountActive" + domain.getCapFirstDomainName() + "Records");
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.setReturnType(new Type("Result<CountNum, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut result:Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
			sList.add(new Statement(serial+2000L,1,"for "+this.domain.getSnakeDomainName()+" in get_db() {"));
			sList.add(new Statement(serial+3000L,2,"if "+this.domain.getSnakeDomainName()+"."+this.domain.getActive().getSnakeFieldName()+" == "+this.domain.getDomainActiveStr()+" {"));
			sList.add(new Statement(serial+4000L,3,"result.push("+this.domain.getSnakeDomainName()+");"));
			sList.add(new Statement(serial+5000L,2,"}"));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,1,"Ok(CountNum{count_num:result.len() as i64})"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

}
