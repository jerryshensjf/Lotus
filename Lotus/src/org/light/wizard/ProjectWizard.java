package org.light.wizard;

import java.util.Map;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Project;
import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;
import org.light.simpleauth.SimpleAuthModule;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

public class ProjectWizard {	
	public static Project createNewProject(Map<String,String> params) {
		Project project = new Project();
		project.setStandardName(StringUtil.nullTrim(params.get("project")));
		project.setPackageToken(StringUtil.nullTrim(params.get("packagetoken")));
		project.setDbPrefix(StringUtil.nullTrim(params.get("dbprefix")));
		project.setDbName(StringUtil.nullTrim(params.get("dbname")));
		project.setDbUsername(StringUtil.nullTrim(params.get("dbusername")));
		project.setDbPassword(StringUtil.nullTrim(params.get("dbpassword")));
		project.setDbType(StringUtil.nullTrim(params.get("dbtype")));
		project.setTechnicalstack(StringUtil.nullTrim(params.get("technicalstack")));
		project.setTitle(StringUtil.nullTrim(params.get("title")));
		project.setSubTitle(StringUtil.nullTrim(params.get("subtitle")));
		project.setFooter(StringUtil.nullTrim(params.get("footer")));
		project.setCrossOrigin(StringUtil.nullTrim(params.get("crossorigin")));
		project.setResolution(StringUtil.nullTrim(params.get("resolution")));
		project.setDomainSuffix(StringUtil.nullTrim(params.get("domainsuffix")));
		project.setDaoSuffix(StringUtil.nullTrim(params.get("daosuffix")));
		project.setDaoimplSuffix(StringUtil.nullTrim(params.get("daoimplsuffix")));
		project.setServiceSuffix(StringUtil.nullTrim(params.get("servicesuffix")));
		project.setServiceimplSuffix(StringUtil.nullTrim(params.get("serviceimplsuffix")));
		project.setControllerSuffix(StringUtil.nullTrim(params.get("controllersuffix")));
		project.setDomainNamingSuffix(StringUtil.nullTrim(params.get("domainnamingsuffix")));
		project.setControllerNamingSuffix(StringUtil.nullTrim(params.get("controllernamingsuffix")));
		project.setLanguage(StringUtil.nullTrim(params.get("language")));
		project.setSchema(StringUtil.nullTrim(params.get("schema")));
		project.setExcelTemplateName(StringUtil.nullTrim(params.get("fileName")));
		project.setFrontBaseApi(StringUtil.nullTrim(params.get("frontbaseapi")));
		project.setFrontendUi(StringUtil.nullTrim(params.get("frontendUi")));
		project.setBackendUi(StringUtil.nullTrim(params.get("backendUi")));
		project.setComputerLanguage(StringUtil.nullTrim(params.get("computerlanguage")));
		return project;
	}
	
	public static Project updateProject(Project project,Map<String,String> params) {
		project.setStandardName(StringUtil.nullTrim(params.get("project")));
		project.setPackageToken(StringUtil.nullTrim(params.get("packagetoken")));
		project.setDbPrefix(StringUtil.nullTrim(params.get("dbprefix")));
		project.setDbName(StringUtil.nullTrim(params.get("dbname")));
		project.setDbUsername(StringUtil.nullTrim(params.get("dbusername")));
		project.setDbPassword(StringUtil.nullTrim(params.get("dbpassword")));
		project.setDbType(StringUtil.nullTrim(params.get("dbtype")));
		project.setTechnicalstack(StringUtil.nullTrim(params.get("technicalstack")));
		project.setTitle(StringUtil.nullTrim(params.get("title")));
		project.setSubTitle(StringUtil.nullTrim(params.get("subtitle")));
		project.setFooter(StringUtil.nullTrim(params.get("footer")));
		project.setCrossOrigin(StringUtil.nullTrim(params.get("crossorigin")));
		project.setResolution(StringUtil.nullTrim(params.get("resolution")));
		project.setDomainSuffix(StringUtil.nullTrim(params.get("domainsuffix")));
		project.setDaoSuffix(StringUtil.nullTrim(params.get("daosuffix")));
		project.setDaoimplSuffix(StringUtil.nullTrim(params.get("daoimplsuffix")));
		project.setServiceSuffix(StringUtil.nullTrim(params.get("servicesuffix")));
		project.setServiceimplSuffix(StringUtil.nullTrim(params.get("serviceimplsuffix")));
		project.setControllerSuffix(StringUtil.nullTrim(params.get("controllersuffix")));
		project.setDomainNamingSuffix(StringUtil.nullTrim(params.get("domainnamingsuffix")));
		project.setControllerNamingSuffix(StringUtil.nullTrim(params.get("controllernamingsuffix")));
		project.setLanguage(StringUtil.nullTrim(params.get("language")));
		project.setSchema(StringUtil.nullTrim(params.get("schema")));
		project.setExcelTemplateName(StringUtil.nullTrim(params.get("fileName")));
		project.setFrontBaseApi(StringUtil.nullTrim(params.get("frontbaseapi")));
		project.setFrontendUi(StringUtil.nullTrim(params.get("frontendUi")));
		project.setBackendUi(StringUtil.nullTrim(params.get("backendUi")));
		project.setComputerLanguage(StringUtil.nullTrim(params.get("computerlanguage")));
		return project;
	}
	
	public static Domain createNewDomain(Map<String,String> params) {
		Domain domain = new Domain();
		domain.setStandardName(StringUtil.nullTrim(params.get("domain")));
		domain.setPlural(StringUtil.nullTrim(params.get("plural")));
		domain.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		domain.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		domain.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return domain;
	}
	
	public static org.light.domain.Enum createNewEnum(Map<String,String> params) {
		org.light.domain.Enum myenum = new org.light.domain.Enum();
		myenum.setStandardName(StringUtil.nullTrim(params.get("domain")));
		myenum.setPlural(StringUtil.nullTrim(params.get("plural")));
		myenum.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		myenum.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		myenum.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return myenum;
	}
	
	public static Domain updateDomain(Domain domain,Map<String,String> params) {
		domain.setStandardName(StringUtil.nullTrim(params.get("domain")));
		domain.setPlural(StringUtil.nullTrim(params.get("plural")));
		domain.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		domain.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		domain.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return domain;
	}
	
	public static Domain updateEnum(org.light.domain.Enum myenum,Map<String,String> params) {
		myenum.setStandardName(StringUtil.nullTrim(params.get("domain")));
		myenum.setPlural(StringUtil.nullTrim(params.get("plural")));
		myenum.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		myenum.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		myenum.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return myenum;
	}
	
	public static Field createNewField(Map<String,String> params) {
		if ("field".equals(params.get("fieldcatlog"))) {
			Field field = new Field();
			field.setSerial(Long.valueOf(params.get("serial")));
			field.setFieldType(params.get("fieldtype"));
			field.setFieldName(params.get("fieldname"));
			field.setLabel(params.get("fieldlabel"));
			field.setLengthStr(params.get("fieldlength"));
			field.setFixedName(params.get("fieldfixedname"));
			return field;
		}else if  ("dropdown".equals(params.get("fieldcatlog"))) {
			Dropdown dp = new Dropdown();
			dp.setSerial(Long.valueOf(params.get("serial")));
			dp.setFieldType(params.get("fieldtype"));
			dp.setFieldName(params.get("fieldname"));
			dp.setAliasName(params.get("fieldname"));
			dp.setTargetName(params.get("fieldtype"));
			dp.setLabel(params.get("fieldlabel"));
			dp.setLengthStr(params.get("fieldlength"));
			dp.setFixedName(params.get("fieldfixedname"));
			return dp;
		}else {
			return null;
		}
	}
	
	public static ManyToMany createNewManyToMany(Domain domain,Map<String,String> params) {
		if  ("manytomany".equals(params.get("fieldcatlog"))) {
			ManyToMany mtm = new ManyToMany();
			String capFieldName = StringUtil.capFirst(params.get("fieldname"));
			if (!StringUtil.isBlank(capFieldName)) {
				mtm.setStandardName(capFieldName);
			}else {
				mtm.setStandardName(params.get("fieldtype"));
			}
			mtm.setMaster(domain);
			mtm.setManyToManySalveName(params.get("fieldtype"));
			mtm.setSlaveAliasLabel(params.get("fieldlabel"));
			if (!StringUtil.isBlank(capFieldName)) mtm.setSlaveAlias(capFieldName);
			mtm.setMaster(domain);
			return mtm;
		}else {
			return null;
		}
	}
	
	public static org.light.core.Module createNewSimpleAuthModule(Project project,Map<String,String> params) throws ValidateException {
		ValidateInfo info = new ValidateInfo();
		SimpleAuthModule simpleAuth = new SimpleAuthModule();
		simpleAuth.setStandardName(StringUtil.nullTrim(params.get("moudle")));
		Domain uDomain = null;
		Domain rDomain = null;
		Domain pDomain = null;
		try {
			uDomain = DomainUtil.findDomainInList(project.getDomains(), params.get("userdomain"));
		}catch (ValidateException ve) {
			info = info.mergeValidateInfo(ve.getValidateInfo());
		}
		try {
			rDomain = DomainUtil.findDomainInList(project.getDomains(), params.get("roledomain"));
		}catch (ValidateException ve) {
			info = info.mergeValidateInfo(ve.getValidateInfo());
		}
		try {
			pDomain = DomainUtil.findDomainInList(project.getDomains(), params.get("privilegedomain"));
		}catch (ValidateException ve) {
			info = info.mergeValidateInfo(ve.getValidateInfo());
		}
		if (!info.success(false)) throw new ValidateException(info);
		simpleAuth.setUserDomain(uDomain);
		simpleAuth.setRoleDomain(rDomain);
		simpleAuth.setPrivilegeDomain(pDomain);
		simpleAuth.setStandardName("SimpleAuth");
		return simpleAuth;
	}
}
