package org.light.simpleauth;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.Generator;
import org.light.utils.WriteableUtil;

public class DtoGenerator extends Generator{
	protected Domain userDomain;
	
	public DtoGenerator(){
		super();
		super.fileName = "dto.rs";
		super.standardName = "DTO";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"use validator::Validate;"));
		sList.add(new Statement(3000L,0,"use crate::"+this.userDomain.getDomainSuffix()+"::"+this.userDomain.getCapFirstDomainNameWithSuffix()+";"));
		sList.add(new Statement(4000L,0,"use serde::{Deserialize, Serialize};"));
		sList.add(new Statement(6000L,0,"use core::fmt::Debug;"));
		sList.add(new Statement(8000L,0,""));
		sList.add(new Statement(12000L,0,"#[derive(Debug, Deserialize, Validate)]"));
		sList.add(new Statement(13000L,0,"#[serde(rename_all = \"camelCase\")]"));
		sList.add(new Statement(14000L,0,"pub struct LoginInput {"));
		sList.add(new Statement(15000L,1,"pub "+this.userDomain.findFieldByFixedName("userName").getSnakeFieldName()+": String,"));
		sList.add(new Statement(16000L,1,"pub "+this.userDomain.findFieldByFixedName("password").getSnakeFieldName()+": String,"));
		sList.add(new Statement(17000L,0,"}"));
		sList.add(new Statement(18000L,0,""));
		sList.add(new Statement(20000L,0,"#[derive(Debug, Deserialize, Validate)]"));
		sList.add(new Statement(20500L,0,"#[serde(rename_all = \"camelCase\")]"));
		sList.add(new Statement(21000L,0,"pub struct RegisterInput {"));
		sList.add(new Statement(22000L,1,"#[validate(length(min = 4, max = 10))]"));
		sList.add(new Statement(23000L,1,"pub "+this.userDomain.findFieldByFixedName("userName").getSnakeFieldName()+": String,"));
		sList.add(new Statement(24000L,1,"#[validate(email)]"));
		sList.add(new Statement(25000L,1,"pub email: String,"));
		sList.add(new Statement(26000L,1,"#[validate(length(min = 6))]"));
		sList.add(new Statement(27000L,1,"pub "+this.userDomain.findFieldByFixedName("password").getSnakeFieldName()+": String,"));
		sList.add(new Statement(28000L,0,"}"));
		sList.add(new Statement(29000L,0,""));
		sList.add(new Statement(30000L,0,"//#[derive(Debug, SimpleObject)]"));
		sList.add(new Statement(31000L,0,"#[derive(Debug)]"));
		sList.add(new Statement(32000L,0,"pub struct AuthPayload {"));
		sList.add(new Statement(33000L,1,"pub token: String,"));
		sList.add(new Statement(34000L,1,"pub "+this.userDomain.getSnakeDomainName()+": "+this.userDomain.getCapFirstDomainNameWithSuffix()+","));
		sList.add(new Statement(35000L,0,"}"));
		sList.add(new Statement(36000L,0,""));
		sList.add(new Statement(37000L,0,"#[derive(Debug, Serialize)]"));
		sList.add(new Statement(38000L,0,"pub struct TokenPayload {"));
		sList.add(new Statement(39000L,1,"pub success:bool,"));
		sList.add(new Statement(40000L,1,"pub token: String,"));
		sList.add(new Statement(41000L,0,"}"));

		return WriteableUtil.merge(sList);		
	}

	public Domain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(Domain userDomain) {
		this.userDomain = userDomain;
	}
}
