package org.light.simpleauth.easyui;

import org.light.core.PrismInterface;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EasyUIUserPagePI extends PrismInterface{
	protected EasyUIUserPageLayout eLayout;
	protected Domain roleDomain;

	public Domain getRoleDomain() {
		return roleDomain;
	}

	public void setRoleDomain(Domain roleDomain) {
		this.roleDomain = roleDomain;
	}

	public EasyUIUserPagePI(Domain domain,Domain roleDomain,String technicalStack,String dbType) throws Exception{
		super();	
		this.setStandardName(domain.getCapFirstPlural()+"UserPage");
		this.setLabel(domain.getText());
		this.technicalStack = technicalStack;
		this.dbType = dbType;
		this.domain = domain;
		this.eLayout = new EasyUIUserPageLayout(domain,roleDomain);
		this.getFrame().setMainContent(this.eLayout);
		frame.setStandardName(this.label);
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,0,"<script type=\"text/javascript\" src=\"../js/sha1.js\"></script>"));
		frame.setAdditionScriptFiles(sl);
		frame.setLanguage(domain.getLanguage());
	}	

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder0 = "template/pages/";
		WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.domain.getPlural().toLowerCase()+".html", this.getFrame().generateFrameSetStatementList().getContent());
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public EasyUIUserPageLayout geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUIUserPageLayout eLayout) {
		this.eLayout = eLayout;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.getFrame().setNav(nav);
	}
}
