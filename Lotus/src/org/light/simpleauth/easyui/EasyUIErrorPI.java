package org.light.simpleauth.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.verb.SearchByName;
import org.light.complexverb.DomainFieldVerb;
import org.light.core.PrismInterface;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Add;
import org.light.verb.Delete;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.SoftDelete;
import org.light.verb.Update;

public class EasyUIErrorPI extends PrismInterface {
	protected String title = "Java通用代码生成器光生成结果";
	protected String subTitle = "";
	protected String footer = "";
	protected String projectNamePrefix = "";
	protected String resolution = "low";
	protected Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public EasyUIErrorPI(){
		super();
		this.standardName = "Error";
	}
	public EasyUIErrorPI(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.standardName = "Error";
		this.verbs = new TreeSet<Verb>();
		this.verbs.add(new ListAll(domain));
		this.verbs.add(new ListActive(domain));
		this.verbs.add(new Delete(domain));
		this.verbs.add(new FindById(domain));
		this.verbs.add(new FindByName(domain));
		this.verbs.add(new SoftDelete(domain));
		this.verbs.add(new Update(domain));
		this.verbs.add(new Add(domain));
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
		sList.add(new Statement(2000L,0,"<html>"));
		sList.add(new Statement(3000L,1,"<head>"));
		sList.add(new Statement(4000L,0,"<meta charset=\"utf-8\">"));
		sList.add(new Statement(5000L,0,"<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"));
		sList.add(new Statement(6000L,0,"<meta http-equiv=\"Expires\" CONTENT=\"0\">"));
		sList.add(new Statement(7000L,0,"<meta http-equiv=\"Cache-Control\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(8000L,0,"<meta http-equiv=\"Pragma\" CONTENT=\"no-cache\">"));
		sList.add(new Statement(9000L,1,"<title>"+this.title+"</title>"));
		sList.add(new Statement(10000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/themes/default/easyui.css'>"));
		sList.add(new Statement(11000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/themes/icon.css'>"));
		sList.add(new Statement(12000L,1,"<link rel='stylesheet' type='text/css' href='../easyui/demo/demo.css'>"));
		sList.add(new Statement(13000L,1,"<script type='text/javascript' src='../easyui/jquery.min.js'></script>"));
		sList.add(new Statement(14000L,1,"<script type='text/javascript' src='../easyui/jquery.easyui.min.js'></script>"));
		sList.add(new Statement(15000L,1,"<script type='text/javascript' src='../js/base64.js'></script>"));
		sList.add(new Statement(16000L,1,"</head>"));
		sList.add(new Statement(17000L,1,"<body  style=\"background:#B3DFDA;\">"));
		sList.add(new Statement(18000L,2,"<div  style=\"height: 100%;width: 100%;padding-top:60px\">"));
		sList.add(new Statement(19000L,2,"<center>"));
		sList.add(new Statement(20000L,3,"<h2>"+this.title+"</h2><br/><br/>"));
		sList.add(new Statement(21000L,3,"<img id=\"errorImg\" src=\""+getProjectNamePrefix()+"/images/snake2.png\"/>"));
		sList.add(new Statement(22000L,3,"<div style=\"padding-top: 25px;color: red\">"));
		if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
			sList.add(new Statement(23000L,4,"<span style=\"font-size:24pt\">Something is wrong!</span><br/>"));
		}else {
			sList.add(new Statement(23000L,4,"<span style=\"font-size:24pt\">出错了！</span><br/>"));
		}
		sList.add(new Statement(24000L,4,"<span id=\"errMsg\" style=\"font-size:16pt\"></span><br/>"));
		if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
			sList.add(new Statement(25000L,4,"<span id=\"butBar\"></span><input type=\"button\" onclick=\"window.location.href='"+getProjectNamePrefix()+"/login/index.html'\" value=\"Return to login page.\"/>"));
		}else {
			sList.add(new Statement(25000L,4,"<span id=\"butBar\"></span><input type=\"button\" onclick=\"window.location.href='"+getProjectNamePrefix()+"/login/index.html'\" value=\"返回登录页面\"/>"));
		}
		sList.add(new Statement(26000L,3,"</div>"));
		sList.add(new Statement(27000L,2,"</center>"));
		sList.add(new Statement(28000L,2,"</div>"));
		sList.add(new Statement(29000L,1,"</body>"));
		sList.add(new Statement(30000L,0,"<script type=\"text/javascript\">"));
		sList.add(new Statement(31000L,0,"var token = \"\";"));
		sList.add(new Statement(32000L,0,"if (window.localStorage.getItem(\"mytoken\")) token = window.localStorage.getItem(\"mytoken\");"));
		sList.add(new Statement(33000L,0,""));
		sList.add(new Statement(34000L,0,"$(document).ready(function() {"));
		sList.add(new Statement(35000L,1,"$(\"#errorImg\").attr(\"src\",\""+getProjectNamePrefix()+"/images/snake2.png\");"));
		sList.add(new Statement(36000L,1,"outputMessage();"));
		sList.add(new Statement(37000L,0,"});"));
		sList.add(new Statement(38000L,0,""));
		sList.add(new Statement(39000L,0,"function outputMessage(){"));
		sList.add(new Statement(40000L,1,"var message = getQueryStringByName(\"errorMessage\");"));
		sList.add(new Statement(41000L,1,"var jumpUrl = getQueryStringByName(\"jumpUrl\");"));
		sList.add(new Statement(42000L,1,"$(\"#errMsg\").html(Base64.decode(message));"));
		sList.add(new Statement(43000L,1,"if (jumpUrl) $(\"#butBar\").html(\"<input type='button' value='返回' onclick='window.location.href=\\\"\"+Base64.decode(jumpUrl)+\"?token=\"+token+\"\\\"'/>\");	"));
		sList.add(new Statement(44000L,0,"}"));
		sList.add(new Statement(45000L,0,""));
		sList.add(new Statement(46000L,0,"//获取QueryString的数组"));
		sList.add(new Statement(47000L,0,"function getQueryString(){"));
		sList.add(new Statement(48000L,1,"location.search.match(new RegExp(\"[\\?\\&][^\\?\\&]+=[^\\?\\&]+\",\"g\"));"));
		sList.add(new Statement(49000L,1,"if(result == null){"));
		sList.add(new Statement(50000L,2,"return \"\";"));
		sList.add(new Statement(51000L,1,"}"));
		sList.add(new Statement(52000L,0,""));
		sList.add(new Statement(53000L,1,"for(var i = 0; i < result.length; i++){"));
		sList.add(new Statement(54000L,2,"result[i] = result[i].substring(1);"));
		sList.add(new Statement(55000L,1,"}"));
		sList.add(new Statement(56000L,1,"return result;"));
		sList.add(new Statement(57000L,0,"}"));
		sList.add(new Statement(58000L,0,""));
		sList.add(new Statement(59000L,0,"//根据QueryString参数名称获取值"));
		sList.add(new Statement(60000L,0,"function getQueryStringByName(name){"));
		sList.add(new Statement(61000L,1,"var result = location.search.match(new RegExp(\"[\\?\\&]\" + name+ \"=([^\\&]+)\",\"i\"));"));
		sList.add(new Statement(62000L,1,"if(result == null || result.length < 1){"));
		sList.add(new Statement(63000L,2,"return \"\";"));
		sList.add(new Statement(64000L,1,"}"));
		sList.add(new Statement(65000L,0,""));
		sList.add(new Statement(66000L,1,"return result[1];"));
		sList.add(new Statement(67000L,0,"}"));
		sList.add(new Statement(68000L,0,""));
		sList.add(new Statement(69000L,0,"//根据QueryString参数索引获取值"));
		sList.add(new Statement(70000L,0,"function getQueryStringByIndex(index){"));
		sList.add(new Statement(71000L,1,"if(index == null){"));
		sList.add(new Statement(72000L,2,"return \"\";"));
		sList.add(new Statement(73000L,1,"}"));
		sList.add(new Statement(74000L,0,""));
		sList.add(new Statement(75000L,1,"var queryStringList = getQueryString();"));
		sList.add(new Statement(76000L,1,"if (index >= queryStringList.length){"));
		sList.add(new Statement(77000L,2,"return \"\";"));
		sList.add(new Statement(78000L,1,"}"));
		sList.add(new Statement(79000L,0,""));
		sList.add(new Statement(80000L,1,"var result = queryStringList[index];"));
		sList.add(new Statement(81000L,1,"var startIndex = result.indexOf(\"=\") + 1;"));
		sList.add(new Statement(82000L,1,"result = result.substring(startIndex);"));
		sList.add(new Statement(83000L,1,"return result;"));
		sList.add(new Statement(84000L,0,"}"));
		sList.add(new Statement(85000L,0,"</script>"));
		sList.add(new Statement(86000L,0,"</html>"));
		return WriteableUtil.merge(sList);
	}
	
	public String getProjectNamePrefix() {
		if (StringUtil.isBlank(this.projectNamePrefix)) {
			return "";
		}else {
			return "/"+this.projectNamePrefix;
		}
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public Set<DomainFieldVerb> getDomainFieldVerbs() {
		return domainFieldVerbs;
	}

	public void setDomainFieldVerbs(Set<DomainFieldVerb> domainFieldVerbs) {
		this.domainFieldVerbs = domainFieldVerbs;
	}

	public void setProjectNamePrefix(String projectNamePrefix) {
		this.projectNamePrefix = projectNamePrefix;
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/login/";
		String relativeFolder0 = "template/login/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath +relativeFolder+ "/error.html", this.generateStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath +relativeFolder0+ "/error.html", this.generateStatementList().getContent());
		}
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle, String footer) {
		if (!StringUtil.isBlank(title)) this.title = title;
		if (!StringUtil.isBlank(subTitle)) this.subTitle = subTitle;
		if (!StringUtil.isBlank(footer)) this.footer = footer;
	}
}
