package org.light.simpleauth.easyui;

import org.light.core.PrismInterface;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EasyUIProfilePI extends PrismInterface {
	protected EasyUIProfileLayout eLayout;

	public EasyUIProfilePI(Domain domain) throws Exception{
		super();
		this.setStandardName("Profile");
		this.setLabel(domain.getText());
		this.domain = domain;
		this.eLayout = new EasyUIProfileLayout(domain);
		this.eLayout.parse();
		this.frame.setStandardName(domain.getText());
		StatementList scripts = new StatementList();
		scripts.setIndent(1);
		scripts.setSerial(1000L);
		scripts.addStatement(new Statement(1000L,0,"<script type=\"text/javascript\" src=\"../js/sha1.js\"></script>"));
		this.frame.setAdditionScriptFiles(scripts);
		this.getFrame().setMainContent(this.eLayout);
		this.getFrame().setLanguage(domain.getLanguage());
		this.technicalStack = domain.getTechnicalStack();
	}	
	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder0 = "template/pages/";
		WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+ "profile.html", this.getFrame().generateFrameSetStatementList().getContent());
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.getFrame().setNav(nav);
	}

	public EasyUIProfileLayout geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUIProfileLayout eLayout) {
		this.eLayout = eLayout;
	}
}
