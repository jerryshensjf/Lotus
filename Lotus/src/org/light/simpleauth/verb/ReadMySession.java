package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.complexverb.ListMyActive;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindById;

public class ReadMySession extends Verb implements EasyUIPositions {
	protected Domain roleDomain;
	protected Domain privilegeDomain;	

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;		
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}
	
	public ReadMySession(){
		super();
		this.setLabel("读出会话");
	}
	
	public ReadMySession(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("ReadMySession");
		this.setVerbName("ReadMySession");
		this.setLabel("读出会话");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ReadMySession");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("ReadMySession");
			method.addSignature(new Signature(1,"Query(params)","Query<HashMap<String, String>>"));
			method.addSignature(new Signature(1,"session","ReadableSession"));
			method.setReturnType(new Type("String"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"let _token = params.get(\"token\").unwrap();"));
			sList.add(new Statement(2000L,1,"if _token.is_empty() {"));
			sList.add(new Statement(3000L,2,"let _token:String = session.get(\"token\").unwrap();"));
			sList.add(new Statement(4000L,1,"}"));
			sList.add(new Statement(6000L,1,"let claims:Claims = jsonwebtoken::decode("));
			sList.add(new Statement(7000L,2,"&_token,"));
			sList.add(new Statement(8000L,2,"&DecodingKey::from_secret(JWT_SECRET.as_bytes()),"));
			sList.add(new Statement(9000L,2,"&Validation::default(),"));
			sList.add(new Statement(10000L,1,")"));
			sList.add(new Statement(11000L,1,".map(|data| data.claims).unwrap();"));
			FindById find = new FindById(this.domain);
			ListMyActive listRoles = new ListMyActive(this.domain,this.roleDomain);
			ListMyActive listPrivileges = new ListMyActive(this.roleDomain,this.privilegeDomain);
			sList.add(new Statement(13000L,1,"let "+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(find.getVerbName())+"(claims.sub).await.unwrap();"));
			sList.add(new Statement(14000L,1,"let "+StringUtil.getSnakeName(this.roleDomain.getPlural())+" = service_"+StringUtil.getSnakeName(listRoles.getVerbName())+"("+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+").await.unwrap();"));
			sList.add(new Statement(15000L,1,"let mut privileges = HashSet::<String>::new();"));
			sList.add(new Statement(16000L,1,"for "+this.roleDomain.getSnakeDomainName()+" in &"+StringUtil.getSnakeName(this.roleDomain.getPlural())+" {"));
			sList.add(new Statement(17000L,2,"let sub_privs = service_"+StringUtil.getSnakeName(listPrivileges.getVerbName())+"("+this.roleDomain.getSnakeDomainName()+"."+this.roleDomain.getDomainId().getSnakeFieldName()+").await.unwrap();"));
			sList.add(new Statement(18000L,2,"for sub_priv in sub_privs {"));
			sList.add(new Statement(19000L,3,"privileges.insert(sub_priv."+this.privilegeDomain.getDomainName().getSnakeFieldName()+");"));
			sList.add(new Statement(20000L,2,"}"));
			sList.add(new Statement(21000L,1,"}"));
			sList.add(new Statement(22000L,0,""));
			sList.add(new Statement(23000L,1,"let mut role_names = HashSet::<String>::new();"));
			sList.add(new Statement(24000L,1,"for "+this.roleDomain.getSnakeDomainName()+" in "+StringUtil.getSnakeName(this.roleDomain.getPlural())+" {"));
			sList.add(new Statement(25000L,2,"role_names.insert("+this.roleDomain.getSnakeDomainName()+"."+this.roleDomain.getDomainName().getSnakeFieldName()+");"));
			sList.add(new Statement(26000L,1,"}"));
			sList.add(new Statement(27000L,0,""));
			sList.add(new Statement(28000L,0,""));
			sList.add(new Statement(29000L,1,"let "+this.domain.getSnakeDomainName()+"_json = serde_json::to_string_pretty(&"+this.domain.getSnakeDomainName()+").unwrap();"));
			sList.add(new Statement(30000L,1,"let roles_json = serde_json::to_string_pretty(&role_names).unwrap();"));
			sList.add(new Statement(31000L,1,"let privileges_json = serde_json::to_string_pretty(&privileges).unwrap();"));
			sList.add(new Statement(32000L,0,""));
			sList.add(new Statement(33000L,1,"let mut map = Map::new();"));
			sList.add(new Statement(34000L,1,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(35000L,1,"map.insert(\"user\".to_string(),serde_json::from_str(&"+this.domain.getSnakeDomainName()+"_json).unwrap());"));
			sList.add(new Statement(36000L,1,"map.insert(\"roles\".to_string(),serde_json::from_str(&roles_json).unwrap());"));
			sList.add(new Statement(37000L,1,"map.insert(\"perms\".to_string(),serde_json::from_str(&privileges_json).unwrap());"));
			sList.add(new Statement(38000L,0,""));
			sList.add(new Statement(39000L,1,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(41000L,1,"return resultjson;"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;	
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;		
	}

	public Domain getRoleDomain() {
		return roleDomain;
	}

	public void setRoleDomain(Domain roleDomain) {
		this.roleDomain = roleDomain;
	}

	public Domain getPrivilegeDomain() {
		return privilegeDomain;
	}

	public void setPrivilegeDomain(Domain privilegeDomain) {
		this.privilegeDomain = privilegeDomain;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
