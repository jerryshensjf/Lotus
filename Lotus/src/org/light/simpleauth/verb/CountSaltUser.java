package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class CountSaltUser extends Verb implements EasyUIPositions {
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("CountSalt");		
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, "salt", "String"));
			method.setReturnType(new Type("Result<CountNum, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,2,"let result = sqlx::query_as("));
			
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(2000L,3,"r#\"select count("+this.domain.getDomainId().getSnakeFieldName()+") as countNum from  "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+"=?\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(2000L,3,"r#\"select count("+this.domain.getDomainId().getSnakeFieldName()+") as countNum from  "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+"=$1\"#"));
			}else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(2000L,3,"r#\"select count("+this.domain.getDomainId().getSnakeFieldName()+") as countNum from  "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+"=?\"#"));
			}			
			
			sList.add(new Statement(3000L,2,")"));
			sList.add(new Statement(4000L,2,".bind(salt)"));
			sList.add(new Statement(5000L,2,".fetch_one(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(6000L,2,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));
		
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
			
		sList.add(new Statement(serial+1000L,1,"let query = \"select count("+this.domain.getDomainId().getSnakeFieldName()+") as countNum from  "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+"= :1 \";"));

		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(&query)"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let result:i64 = stmt.query_row_as(&[&salt]).unwrap();"));
		sList.add(new Statement(serial+7000L,1,"Ok(CountNum{count_num:result})"));

		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;		
	}
	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}
	
	public CountSaltUser(Domain domain,String dbType) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.setVerbName("CountSalt");
		this.setLabel("盐计数");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Count Salt");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;	
	}
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;	
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("CountSalt");		
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, "salt", "String"));
			method.setReturnType(new Type("Result<CountNum, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut result:Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
			sList.add(new Statement(serial+2000L,1,"for "+this.domain.getSnakeDomainName()+" in get_db() {"));
			sList.add(new Statement(serial+3000L,2,"if "+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+" == salt {"));
			sList.add(new Statement(serial+4000L,3,"result.push("+this.domain.getSnakeDomainName()+");"));
			sList.add(new Statement(serial+5000L,2,"}"));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,1,"Ok(CountNum{count_num:result.len() as i64})"));		
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
