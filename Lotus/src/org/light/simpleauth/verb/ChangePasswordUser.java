package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindByName;

public class ChangePasswordUser extends Verb implements EasyUIPositions {
	
	public static String generateChangePasswordSql(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" set ";
		result += domain.getActive().getFieldTableColumName() + "=" + domain.getDomainActiveStr()+ ",";
		result += domain.findFieldByFixedName("password").getFieldTableColumName() +  " = ?,";
		result += domain.findFieldByFixedName("salt").getFieldTableColumName() +  " = ?,";
		result += domain.findFieldByFixedName("loginFailure").getFieldTableColumName() + " = 0 ";
		result += "where " + domain.findFieldByFixedName("userName").getFieldTableColumName() + " = ? ";
		return result;
	}
	
	public static String generateChangePasswordPgsqlSql(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" set ";
		result += domain.getActive().getFieldTableColumName() + "=" + domain.getDomainActiveStr()+ ",";
		result += domain.findFieldByFixedName("password").getFieldTableColumName() +  " = $1,";
		result += domain.findFieldByFixedName("salt").getFieldTableColumName() +  " = $2,";
		result += domain.findFieldByFixedName("loginFailure").getFieldTableColumName() + " = 0 ";
		result += "where " + domain.findFieldByFixedName("userName").getFieldTableColumName() + " = $3 ";
		return result;
	}
	
	public static String generateChangePasswordOracleSql(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" set ";
		result += domain.getActive().getFieldTableColumName() + "=" + domain.getDomainActiveInteger()+ ",";
		result += domain.findFieldByFixedName("password").getFieldTableColumName() +  " = :1, ";
		result += domain.findFieldByFixedName("salt").getFieldTableColumName() +  " = :2, ";
		result += domain.findFieldByFixedName("loginFailure").getFieldTableColumName() + " = 0 ";
		result += "where " + domain.findFieldByFixedName("userName").getFieldTableColumName() + " = :3 ";
		return result;
	}
	
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("ChangePassword"+StringUtil.capFirst(this.domain.getStandardName()));		
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"let result = sqlx::query("));
			
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(2000L,2,"r#\""+generateChangePasswordSql(domain)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(2000L,2,"r#\""+generateChangePasswordPgsqlSql(domain)+"\"#"));
			}
			
			sList.add(new Statement(3000L,1,")"));
			sList.add(new Statement(4000L,1,".bind("+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+")"));
			sList.add(new Statement(5000L,1,".bind("+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+")"));
			sList.add(new Statement(6000L,1,".bind("+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+")"));
			sList.add(new Statement(7000L,1,".execute(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(8000L,1,".await;"));
			
			sList.add(new Statement(10000L,2,"match result {"));
			sList.add(new Statement(11000L,3,"Ok(result) => {"));
			sList.add(new Statement(12000L,4,"Ok(result.rows_affected())"));
			sList.add(new Statement(13000L,3,"},"));
			sList.add(new Statement(14000L,3,"Err(err) => {"));
			sList.add(new Statement(15000L,4,"Err(err)"));
			sList.add(new Statement(16000L,3,"}"));
			sList.add(new Statement(17000L,2,"}"));
		
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}	
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+generateChangePasswordOracleSql(domain)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		
		serial += 6000L;
		
		StringBuilder sb = new StringBuilder("");
		sb.append("&").append(this.domain.getSnakeDomainName()).append(".").append(this.domain.findFieldByFixedName("password").getSnakeFieldName()).append(",");
		sb.append("&").append(this.domain.getSnakeDomainName()).append(".").append(this.domain.findFieldByFixedName("salt").getSnakeFieldName()).append(",");
		sb.append("&").append(this.domain.getSnakeDomainName()).append(".").append(this.domain.findFieldByFixedName("userName").getSnakeFieldName());

		sList.add(new Statement(serial+6000L,1,"let _ = stmt.execute(&["+sb.toString()+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let _ = conn.commit();"));
		sList.add(new Statement(serial+8000L,1,"Ok(0 as u64)"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("changePassword"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("void"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			
			method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception  {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("ChangePassword"+StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(
					new Signature(1, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			method.addAdditionalImport("crate::utils::encrypt_util::multi_sha1");
			method.addAdditionalImport("crate::utils::password_util::generate_salt");
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"let salt = generate_salt().await.unwrap();"));
			sList.add(new Statement(2000L,1,"let mut "+this.domain.getSnakeDomainName()+"2 = "+this.domain.getLowerFirstDomainName()+".clone();"));
			sList.add(new Statement(3000L,1,"let encrpt = multi_sha1("+this.domain.getLowerFirstDomainName()+"2."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+",salt.clone(),3);"));
			sList.add(new Statement(4000L,1,""+this.domain.getSnakeDomainName()+"2."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+" = encrpt;"));
			sList.add(new Statement(5000L,1,""+this.domain.getSnakeDomainName()+"2."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+" = salt.clone();"));
			sList.add(new Statement(6000L,1,"let app_state = init_db();"));
			sList.add(new Statement(7000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+".change_password_"+this.domain.getSnakeDomainName()+"("+this.domain.getSnakeDomainName()+"2).await"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	
	public ChangePasswordUser(){
		super();
		this.dbType = "MariaDB";
		this.setLabel("修改用户密码");
	}
	
	public ChangePasswordUser(String dbType){
		super();
		this.dbType = dbType;
		this.setLabel("修改用户密码");
	}
	
	public ChangePasswordUser(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = "MariaDB";
		this.setVerbName("ChangePassword"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("修改用户密码");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Change Password");
	}
	
	public ChangePasswordUser(Domain domain,String dbType) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.setVerbName("ChangePassword"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("修改用户密码");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ChangePasswordUser");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("ChangePassword"+this.domain.getCapFirstDomainName());
			method.addSignature(new Signature(1,"Form("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Form<"+this.domain.getCapFirstDomainName()+"Request>"));
			method.setReturnType(new Type("String"));	
			
			List<Writeable> sList = new ArrayList<Writeable>();

			sList.add(new Statement(1000L,1,"let "+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+".unwrap_or_default();"));
			sList.add(new Statement(2000L,1,"let password = "+this.domain.getSnakeDomainName()+"_request."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+".unwrap_or_default();"));
			FindByName find = new FindByName(this.domain);
			sList.add(new Statement(3000L,1,"let mut "+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(find.getVerbName())+"("+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+").await.unwrap();"));
			sList.add(new Statement(4000L,1,""+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+" = password;"));
			sList.add(new Statement(5000L,1,"let _result = service_change_password_"+this.domain.getSnakeDomainName()+"("+this.domain.getSnakeDomainName()+").await;"));
			sList.add(new Statement(6000L,1,"match _result {"));
			sList.add(new Statement(7000L,2,"Err(_) => {"));
			sList.add(new Statement(9000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(10000L,2,"},"));
			sList.add(new Statement(11000L,2,"Ok(_result) => {"));
			sList.add(new Statement(12000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(13000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(14000L,3,"map.insert("));
			sList.add(new Statement(15000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(16000L,4,"Value::from(\"\"),"));
			sList.add(new Statement(17000L,3,");"));
			sList.add(new Statement(18000L,3,""));
			sList.add(new Statement(19000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(21000L,3,"return resultjson;"));
			sList.add(new Statement(22000L,2,"}"));
			sList.add(new Statement(23000L,1,"}"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied) return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("changePassword"+domain.getCapFirstDomainName());
			StatementList sList = new StatementList();

			sList.add(new Statement(1000L,0,"{"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(2000L,1,"text:'Change Password',"));
			}else {
				sList.add(new Statement(2000L,1,"text:'设置密码',"));	
			}			
			sList.add(new Statement(3000L,1,"iconCls:'icon-reload',"));
			sList.add(new Statement(4000L,1,"handler:function(){"));
			sList.add(new Statement(5000L,2,"var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sList.add(new Statement(6000L,2,"if (rows == undefined || rows == null || rows.length == 0 ){"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(7000L,3,"$.messager.alert(\"Warning\",\"Please select one record.\",\"warning\");"));
			}else {
				sList.add(new Statement(7000L,3,"$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sList.add(new Statement(8000L,3,"return;"));
			sList.add(new Statement(9000L,2,"}"));
			sList.add(new Statement(10000L,2,"if (rows.length > 1) {"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(11000L,3,"$.messager.alert(\"Warning\",\"Please select one record.\",\"warning\");"));
			}else {
				sList.add(new Statement(11000L,3,"$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sList.add(new Statement(12000L,3,"return;"));
			sList.add(new Statement(13000L,2,"}"));
			sList.add(new Statement(14000L,2,"$(\"#ffchangePassword\").find(\"#"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\").val(rows[0][\""+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\"]);"));
			sList.add(new Statement(15000L,2,"$('#wchangePassword').window('open');"));
			sList.add(new Statement(16000L,1,"}"));
			sList.add(new Statement(17000L,0,"}"));
			block.setMethodStatementList(sList);
			return block;			
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied) return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("changePassword"+domain.getCapFirstDomainName());			

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(2000L,1,"var "+this.domain.getDomainName().getLowerFirstFieldName()+" = $(\"#ffchangePassword\").find(\"#"+this.domain.getDomainName().getLowerFirstFieldName()+"\").val();"));
			sList.add(new Statement(3000L,1,"var "+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+" = $(\"#ffchangePassword\").find(\"#"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\").val();"));
			sList.add(new Statement(4000L,1,"var confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+" = $(\"#ffchangePassword\").find(\"#confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"\").val();"));
			sList.add(new Statement(5000L,1,"if (isBlank("+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+")||isBlank(confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+")){"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(6000L,2,"$.messager.alert(\"Error\",\"New password can not be empty.\",\"error\");"));
			}else {
				sList.add(new Statement(6000L,2,"$.messager.alert(\"错误\",\"新密码不可为空！\",\"error\");"));
			}
			sList.add(new Statement(7000L,2,"return;"));
			sList.add(new Statement(8000L,1,"}"));
			sList.add(new Statement(9000L,1,"if ("+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"!=confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"){"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(10000L,2,"$.messager.alert(\"Error\",\"New password did not match.\",\"error\");"));
			}else {
				sList.add(new Statement(10000L,2,"$.messager.alert(\"错误\",\"新密码不匹配！\",\"error\");"));
			}
			sList.add(new Statement(11000L,2,"return;"));
			sList.add(new Statement(12000L,1,"}"));
			sList.add(new Statement(13000L,1,"$.ajax({"));
			sList.add(new Statement(14000L,2,"type: \"post\","));
			sList.add(new Statement(15000L,2,"url: \"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/changePassword"+this.domain.getCapFirstDomainName()+"\","));
			sList.add(new Statement(16000L,2,"data: {"));
			sList.add(new Statement(17000L,3,""+this.domain.getDomainName().getLowerFirstFieldName()+":"+this.domain.getDomainName().getLowerFirstFieldName()+","));
			sList.add(new Statement(18000L,3,""+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+":hex_sha1("+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"),"));
			sList.add(new Statement(19000L,2,"},"));
			sList.add(new Statement(20500L,2,"dataType: 'json',"));
			sList.add(new Statement(21000L,2,"success: function(data, textStatus) {"));
			sList.add(new Statement(22000L,3,"if (data.success) {"));
			sList.add(new Statement(23000L,4,"$(\"#wchangePassword\").window(\"close\");"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sList.add(new Statement(24000L,4,"$.messager.alert(\"Success\",\"Changed password successfully.\",\"info\");"));
			}else {
				sList.add(new Statement(24000L,4,"$.messager.alert(\"成功\",\"成功修改密码！\",\"info\");"));
			}
			sList.add(new Statement(25000L,3,"}"));
			sList.add(new Statement(26000L,2,"},"));
			sList.add(new Statement(27000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
			sList.add(new Statement(28000L,2,"},"));
			sList.add(new Statement(29000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sList.add(new Statement(30000L,3,"alert(\"Error:\"+textStatus);"));
			sList.add(new Statement(31000L,3,"alert(errorThrown.toString());"));
			sList.add(new Statement(32000L,2,"}"));
			sList.add(new Statement(33000L,1,"});"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;	
		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("ChangePassword"+StringUtil.capFirst(this.domain.getStandardName()));		
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut _db = DB.lock().unwrap();"));
			sList.add(new Statement(serial+2000L,1,"for (_index, "+this.domain.getSnakeDomainName()+"0) in &mut _db.iter_mut().enumerate(){"));
			sList.add(new Statement(serial+3000L,2,"if (*"+this.domain.getSnakeDomainName()+"0)."+this.domain.getDomainId().getSnakeFieldName()+" == "+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+" {"));
			sList.add(new Statement(serial+4000L,3,"(*"+this.domain.getLowerFirstDomainName()+"0)."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+" = "+this.domain.getLowerFirstDomainName()+".clone()."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+";"));
			sList.add(new Statement(serial+5000L,3,"(*"+this.domain.getLowerFirstDomainName()+"0)."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+" = "+this.domain.getLowerFirstDomainName()+".clone()."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+";"));
			sList.add(new Statement(serial+6000L,3,"(*"+this.domain.getLowerFirstDomainName()+"0)."+this.domain.findFieldByFixedName("loginFailure").getSnakeFieldName()+" = "+this.domain.getLowerFirstDomainName()+".clone()."+this.domain.findFieldByFixedName("loginFailure").getSnakeFieldName()+";"));
			sList.add(new Statement(serial+7000L,2,"}"));
			sList.add(new Statement(serial+8000L,1,"}"));
			sList.add(new Statement(serial+9000L,1,"return Ok(0 as u64);"));		
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
