package org.light.simpleauth.verb;

import org.light.core.Verb;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;

public class ViewUser extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}
	public ViewUser(Domain domain)throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("View");
		this.setVerbName("View"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("查看");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("View");
	}

	public ViewUser(){
		super();
		this.setLabel("查看");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("View" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Ｖiew',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'查看',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-search',"));
			sl.add(new Statement(4000, 1, "handler:function(){ "));
			sl.add(new Statement(5000, 2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			sl.add(new Statement(10000, 2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000, 3, "return;"));
			sl.add(new Statement(13000, 2, "}"));
			sl.add(new Statement(13500, 2,
					"$(\"#ffview\").find(\"#" + this.domain.getDomainId().getLowerFirstFieldName()
							+ "\").val(rows[0][\"" + this.domain.getDomainId().getLowerFirstFieldName() + "\"]);"));
			long serial = 14000;
			for (Field f : domain.getFieldsWithoutIdAndActive()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sl.add(new Statement(serial, 2, "if (rows[0][\""+f.getLowerFirstFieldName()+"\"]!=null)	{"));
					sl.add(new Statement(serial+10, 3, "$(\"#ffview\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
					sl.add(new Statement(serial+20, 2, "}else{"));
					sl.add(new Statement(serial+30, 3, "$(\"#ffview\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"../images/blank.jpg\");"));
					sl.add(new Statement(serial+40, 2, "}"));
				}else if (f instanceof Dropdown) {
					sl.add(new Statement(serial, 2, "$(\"#ffview\").find(\"#" + f.getLowerFirstFieldName()
							+ "\").combobox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
				} else {
					if (f.getFieldType().equalsIgnoreCase("bool")) {
						sl.add(new Statement(serial, 2,
								"var " + f.getLowerFirstFieldName() + "Checkboxs = $(\"#ffview\").find(\"input[name='"
										+ f.getLowerFirstFieldName() + "']\");"));
						sl.add(new Statement(serial + 100, 2,
								"for (var i=0;i<" + f.getLowerFirstFieldName() + "Checkboxs.length;i++){"));
						sl.add(new Statement(serial + 200, 3,
								"if (" + f.getLowerFirstFieldName() + "Checkboxs.get(i).value == \"\"+rows[0][\""
										+ f.getLowerFirstFieldName() + "\"]) " + f.getLowerFirstFieldName()
										+ "Checkboxs.get(i).checked=true;"));
						sl.add(new Statement(serial + 500, 2, "}"));
					} else if (f.isTextarea()) {
						sl.add(new Statement(serial, 2, "$(\"#ffview\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").val(rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					} else {
						sl.add(new Statement(serial, 2, "$(\"#ffview\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").textbox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					}
				}
				serial += 1000;
			}
			if (domain.getActive()!=null) {
				sl.add(new Statement(serial, 2, "var checkboxs = $(\"#ffview\").find(\"input[name='"
						+ domain.getActive().getLowerFirstFieldName() + "']\");"));
				sl.add(new Statement(serial + 100, 2, "for (var i=0;i<checkboxs.length;i++){"));
				sl.add(new Statement(serial + 200, 3, "if (checkboxs.get(i).value == \"\"+rows[0][\""
						+ domain.getActive().getLowerFirstFieldName() + "\"]) checkboxs.get(i).checked=true;"));
				sl.add(new Statement(serial + 500, 2, "}"));
			}
			sl.add(new Statement(serial + 1000, 2,
					"$('#wview" + domain.getCapFirstDomainName() + "').window('open');"));
			sl.add(new Statement(serial + 2000, 1, "}"));
			sl.add(new Statement(serial + 3000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
