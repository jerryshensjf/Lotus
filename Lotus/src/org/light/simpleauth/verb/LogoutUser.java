package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.utils.InterVarUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class LogoutUser extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;		
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}
	
	public LogoutUser(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("LogoutUser");
		this.setVerbName("Logout"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("注销");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("LogoutUser");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("logout"+this.domain.getCapFirstDomainName());
			method.addSignature(new Signature(1,"mut session","WritableSession"));
			method.setReturnType(new Type("String"));

			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"session.remove(\"token\");"));
			sList.add(new Statement(2000L,1,"let mut map = Map::new();"));
			sList.add(new Statement(3000L,1,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(4000L,1,"map.insert("));
			sList.add(new Statement(5000L,2,"\"rows\".to_string(),"));
			sList.add(new Statement(6000L,2,"Value::from(\"\"),"));
			sList.add(new Statement(7000L,1,");"));
			sList.add(new Statement(8000L,0,""));
			sList.add(new Statement(9000L,1,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(11000L,1,"return resultjson;"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}	

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;	
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;		
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
