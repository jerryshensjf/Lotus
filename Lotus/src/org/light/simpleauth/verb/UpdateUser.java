package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindById;

public class UpdateUser extends Verb implements EasyUIPositions {
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();			
			Set<String> deniedFieldNames = this.getDeniedFieldNames(this.deniedFields);
			
			sList.add(new Statement(500L,2,"let result = sqlx::query("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,2,"r#\""+MybatisSqlReflector.generateUpdateSqlWithDenies(domain,deniedFieldNames)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,2,"r#\""+PgsqlReflector.generateUpdateSqlWithDenies(domain,deniedFieldNames)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(1000L,2,"r#\""+MybatisOracleSqlReflector.generateUpdateSqlWithDenies(domain,deniedFieldNames)+"\"#"));
			}			
			sList.add(new Statement(2000L,2,")"));
			long serial = 3000L;
			for (Field f:domain.getFieldsWithoutId()) {
				if (!deniedFieldNames.contains(f.getFieldName())) {
					sList.add(new Statement(serial,2,".bind("+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+")"));
					serial += 1000L;
				}
			}
			sList.add(new Statement(serial,2,".bind("+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+")"));
			sList.add(new Statement(serial+2000L,2,".execute(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(serial+3000L,2,".await;"));

			sList.add(new Statement(serial+10000L,2,"match result {"));
			sList.add(new Statement(serial+11000L,3,"Ok(result) => {"));
			sList.add(new Statement(serial+12000L,4,"Ok(result.rows_affected())"));
			sList.add(new Statement(serial+13000L,3,"},"));
			sList.add(new Statement(serial+14000L,3,"Err(err) => {"));
			sList.add(new Statement(serial+15000L,4,"Err(err)"));
			sList.add(new Statement(serial+16000L,3,"}"));
			sList.add(new Statement(serial+17000L,2,"}"));

			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		
		Set<String> deniedFieldNames = this.getDeniedFieldNames(this.deniedFields);
		
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateUpdateSqlWithDenies(domain,deniedFieldNames)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		
		serial += 6000L;
		Set<Field> fieldsWithoutId = this.domain.getFieldsWithoutId();
		for (Field f: fieldsWithoutId){
			if ("bool".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,1,"let mut "+f.getSnakeFieldName()+"_int = 0;"));
				sList.add(new Statement(serial+1000L,1,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" == true {"));
				sList.add(new Statement(serial+2000L,2,f.getSnakeFieldName()+"_int = 1;"));
				sList.add(new Statement(serial+3000L,1,"}"));
				serial += 4000L;
			}
		}
		

		StringBuilder sb = new StringBuilder("");
		String fieldsStr = "";
		for (Field f: fieldsWithoutId){
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sb.append("&base64::decode(").append(this.domain.getSnakeDomainName()).append(".").append(f.getSnakeFieldName()).append(").unwrap(),");
			} else if ("bool".equalsIgnoreCase(f.getFieldType())) {
				sb.append("&").append(f.getSnakeFieldName()).append("_int,");
			} else {
				sb.append("&").append(this.domain.getSnakeDomainName()).append(".").append(f.getSnakeFieldName()).append(",");
			}
		}
		if (sb.toString().endsWith(",")) fieldsStr = sb.toString() + "&"+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName();
		
		sList.add(new Statement(serial+6000L,1,"let _ = stmt.execute(&["+fieldsStr+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let _ = conn.commit();"));
		sList.add(new Statement(serial+8000L,1,"Ok(0 as u64)"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("void"));
			method.addSignature(
					new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Udate" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("bool"));
			method.addSignature(
					new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), new Type(this.domain.getDomainSuffix()+"."+this.domain.getType())));
			return method;
		}
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(
					new Signature(1, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getSnakeDomainName()+").await"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	public UpdateUser(Domain domain)throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Update");
		this.setVerbName("Update"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("更新");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Update");
	}

	public UpdateUser(){
		super();
		this.setLabel("更新");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1,"Json("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Json<"+this.domain.getCapFirstDomainName()+"Request>"));
			method.setReturnType(new Type("String"));
			
			Set<Field> deniedFields = new TreeSet<>();
			deniedFields.add(this.domain.findFieldByFixedName("password"));
			deniedFields.add(this.domain.findFieldByFixedName("salt"));
			deniedFields.add(this.domain.findFieldByFixedName("loginFailure"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			for (Field f: this.domain.getFields()) {
				if (!deniedFields.contains(f)) {
					if (!"Image".equalsIgnoreCase(f.getFieldType())) {
						if ("datetime".equalsIgnoreCase(f.getFieldType())) {
							sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
							sList.add(new Statement(serial+100L,1,"let mut _fmt_"+f.getSnakeFieldName()+" = Option::None;"));
							sList.add(new Statement(serial+200L,1,"if "+f.getSnakeFieldName()+" != \"\" {"));
							sList.add(new Statement(serial+300L,2,"_fmt_"+f.getSnakeFieldName()+" = Some(NaiveDateTime::parse_from_str(&"+f.getSnakeFieldName()+",DATE_TIME_FORMAT).unwrap());"));
							sList.add(new Statement(serial+400L,1,"}"));
						}else if ("date".equalsIgnoreCase(f.getFieldType())) {
							sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
							sList.add(new Statement(serial+100L,1,"let mut _fmt_"+f.getSnakeFieldName()+" = Option::None;"));
							sList.add(new Statement(serial+200L,1,"if "+f.getSnakeFieldName()+" != \"\" {"));
							sList.add(new Statement(serial+300L,2,"_fmt_"+f.getSnakeFieldName()+" = Some(NaiveDate::parse_from_str(&"+f.getSnakeFieldName()+",DATE_FORMAT).unwrap());"));
							sList.add(new Statement(serial+400L,1,"}"));
						} else {
							sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
						}
						serial += 1000L;
					}
				}
			}
			
			FindById find = new FindById(this.domain);
			FindUserShadow findShadow = new FindUserShadow(this.domain,this.domain.getDbType());
			sList.add(new Statement(serial+500L,1,"let old_"+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(find.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+".clone()).await.unwrap();"));
			sList.add(new Statement(serial+1000L,1,"let ori_"+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(findShadow.getVerbName())+"(old_"+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+".clone()).await.unwrap();"));
			sList.add(new Statement(serial+2000L,1,"let "+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+" = ori_"+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+";"));
			sList.add(new Statement(serial+3000L,1,"let "+this.domain.findFieldByFixedName("loginFailure").getSnakeFieldName()+" = ori_"+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("loginFailure").getSnakeFieldName()+";"));
			sList.add(new Statement(serial+4000L,1,"let "+this.domain.findFieldByFixedName("password").getSnakeFieldName()+" = ori_"+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+";"));
			
			sList.add(new Statement(serial+5000L,1,"let _current_"+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(find.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+").await.unwrap();"));
			serial += 6000L;
			for (Field f: this.domain.getFields()) {
				if ("Image".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial+2000L,1,"let current_"+f.getSnakeFieldName()+" = _current_"+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+";"));
					sList.add(new Statement(serial+3000L,1,"let mut my_"+f.getSnakeFieldName()+" = pick_"+this.domain.getSnakeDomainName()+"_picture(\""+f.getSnakeFieldName()+"\".to_string());"));
					sList.add(new Statement(serial+4000L,1,"if my_"+f.getSnakeFieldName()+".len() == 0 && current_"+f.getSnakeFieldName()+".len()>0 {"));
					sList.add(new Statement(serial+5000L,2,"my_"+f.getSnakeFieldName()+" = current_"+f.getSnakeFieldName()+";"));
					sList.add(new Statement(serial+6000L,1,"}"));
					serial += 7000L;
				}
			}			
			
			sList.add(new Statement(serial,0,""));
			sList.add(new Statement(serial+1000L,1,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"{"));
			serial += 2000L;
			for (Field f: this.domain.getFields()) {
				if (!"Image".equalsIgnoreCase(f.getFieldType())) {
					if ("datetime".equalsIgnoreCase(f.getFieldType())) {
						sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : _fmt_"+f.getSnakeFieldName()+","));
					} else if ("date".equalsIgnoreCase(f.getFieldType())) {
						sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : _fmt_"+f.getSnakeFieldName()+","));
					} else {
						sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : "+f.getSnakeFieldName()+","));
					}
				}else {
					sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : my_"+f.getSnakeFieldName()+","));
				}
				serial += 1000L;
			}
			sList.add(new Statement(serial,1,"};"));
			sList.add(new Statement(serial+1000L,1,""));
			
			sList.add(new Statement(serial+19000L,1,"let _result = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getSnakeDomainName()+").await;"));
			sList.add(new Statement(serial+20000L,1,"match _result {"));
			sList.add(new Statement(serial+21000L,2,"Err(_) => {"));
			sList.add(new Statement(serial+23000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(serial+24000L,2,"},"));
			sList.add(new Statement(serial+25000L,2,"Ok(_result) => {"));
			sList.add(new Statement(serial+26000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(serial+27000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(serial+28000L,3,"map.insert("));
			sList.add(new Statement(serial+29000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(serial+30000L,4,"Value::from(\"\"),"));
			sList.add(new Statement(serial+31000L,3,");"));
			sList.add(new Statement(serial+32000L,3,""));
			sList.add(new Statement(serial+33000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(serial+35000L,3,"return resultjson;"));
			sList.add(new Statement(serial+36000L,2,"}"));
			sList.add(new Statement(serial+37000L,1,"}"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("Update" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Update',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'编辑',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-edit',"));
			sl.add(new Statement(4000, 1, "handler:function(){ "));
			sl.add(new Statement(5000, 2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			sl.add(new Statement(10000, 2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000, 3, "return;"));
			sl.add(new Statement(13000, 2, "}"));
			sl.add(new Statement(13500, 2,
					"$(\"#ffedit\").find(\"#" + this.domain.getDomainId().getLowerFirstFieldName()
							+ "\").val(rows[0][\"" + this.domain.getDomainId().getLowerFirstFieldName() + "\"]);"));
			long serial = 14000;
			for (Field f : domain.getFieldsWithoutIdAndActive()) {
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sl.add(new Statement(serial, 2, "if (!isBlank(rows[0][\""+f.getLowerFirstFieldName()+"\"]))	{"));
					sl.add(new Statement(serial+10, 3, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"data:image/png;base64,\"+rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
					sl.add(new Statement(serial+20, 2, "}else{"));
					sl.add(new Statement(serial+30, 3, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").prop(\"src\",\"../images/blank.jpg\");"));
					sl.add(new Statement(serial+40, 2, "}"));
				}else if (f instanceof Dropdown) {
					sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
							+ "\").combobox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
				} else {
					if (f.getFieldType().equalsIgnoreCase("bool")) {
						sl.add(new Statement(serial, 2,"checkRadioBoxValue(\"ffedit\",\""+f.getLowerFirstFieldName()+"\",rows[0][\""+f.getLowerFirstFieldName()+"\"]);"	));
					} else if (f.isTextarea()) {
						sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").val(rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					} else {
						sl.add(new Statement(serial, 2, "$(\"#ffedit\").find(\"#" + f.getLowerFirstFieldName()
								+ "\").textbox(\"setValue\",rows[0][\"" + f.getLowerFirstFieldName() + "\"]);"));
					}
				}
				serial += 1000;
			}
			sl.add(new Statement(serial, 2, "var checkboxs = $(\"#ffedit\").find(\"input[name='"
					+ domain.getActive().getLowerFirstFieldName() + "']\");"));
			sl.add(new Statement(serial + 100, 2, "for (var i=0;i<checkboxs.length;i++){"));
			sl.add(new Statement(serial + 200, 3, "if (checkboxs.get(i).value == \"\"+rows[0][\""
					+ domain.getActive().getLowerFirstFieldName() + "\"]) checkboxs.get(i).checked=true;"));
			sl.add(new Statement(serial + 500, 2, "}"));
			sl.add(new Statement(serial + 1000, 2,
					"$('#wupdate" + domain.getCapFirstDomainName() + "').window('open');"));
			sl.add(new Statement(serial + 2000, 1, "}"));
			sl.add(new Statement(serial + 3000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}


	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("update" + domain.getCapFirstDomainName());

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/update" + domain.getCapFirstDomainName() + "\","));
			sl.add(new Statement(4000, 2, "data: JSON.stringify({"));
			long serial = 5000;
			for (Field f : domain.getFieldsWithoutImage()) {
				if (!deniedFields.contains(f)) {
					if (f instanceof Dropdown) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseIntNeg($(\"#ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").combobox(\"getValue\")),"));
					} else if (f.getFieldType().equalsIgnoreCase("bool")) {
						sl.add(new Statement(serial, 3,
								f.getLowerFirstFieldName() + ":parseBoolean($(\"#ffedit\").find(\"input[name='"
										+ f.getLowerFirstFieldName() + "']:checked\").val()),"));
					}  else if (f.getFieldType().equalsIgnoreCase("i32")||f.getFieldType().equalsIgnoreCase("i64")) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseInt($(\"#ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val()),"));
					} else if (f.getFieldType().equalsIgnoreCase("f64")||f.getFieldType().equalsIgnoreCase("f32")) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseFloat($(\"#ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val()),"));
					} else if (f.isTextarea()) {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val(),"));
					} else {
						sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ffedit\").find(\"#"
								+ f.getLowerFirstFieldName() + "\").val(),"));
					}
					serial += 1000;
				}
			}
			sl.add(new Statement(serial, 2, "}),"));
			sl.add(new Statement(serial + 1000, 2, "dataType: 'json',"));
			sl.add(new Statement(serial + 2000, 2, "contentType:\"application/json;charset=UTF-8\","));
			sl.add(new Statement(serial + 3000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(serial + 4000, 3, "if (data.success){"));
			sl.add(new Statement(serial + 7000, 4,
					"$(\"#wupdate" + domain.getCapFirstDomainName() + "\").window('close');"));
			sl.add(new Statement(serial + 8000, 4, "$(\"#dg\").datagrid(\"load\");"));
			sl.add(new Statement(serial + 9000, 3, "}"));
			sl.add(new Statement(serial + 10000, 2, "},"));
			sl.add(new Statement(serial + 11000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(serial + 12000, 2, "},"));
			sl.add(new Statement(serial + 13000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(serial + 14000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(serial + 15000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(serial + 16000, 2, "}"));
			sl.add(new Statement(serial + 17000, 1, "}); "));

			method.setMethodStatementList(sl);
			return method;

		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Update" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();			
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut _db = DB.lock().unwrap();"));
			sList.add(new Statement(serial+2000L,1,"for (_index, "+this.domain.getSnakeDomainName()+"0) in &mut _db.iter_mut().enumerate(){"));
			sList.add(new Statement(serial+3000L,2,"if (*"+this.domain.getSnakeDomainName()+"0)."+this.domain.getDomainId().getSnakeFieldName()+" == "+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+" {"));
			sList.add(new Statement(serial+4000L,3,"(*"+this.domain.getSnakeDomainName()+"0) = "+this.domain.getSnakeDomainName()+".clone();"));
			sList.add(new Statement(serial+5000L,2,"}"));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,1,"return Ok(0 as u64);"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
