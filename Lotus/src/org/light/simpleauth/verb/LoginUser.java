package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class LoginUser extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception  {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Login"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+">"));		
			method.addSignature(new Signature(1, this.domain.findFieldByFixedName("userName").getSnakeFieldName(), "String"));
			method.addSignature(new Signature(2, this.domain.findFieldByFixedName("password").getSnakeFieldName(), "String"));
			method.addAdditionalImport("crate::utils::encrypt_util::verify_password");
			List<Writeable> sList = new ArrayList<Writeable>();

			FindUserShadow findShadow = new FindUserShadow(this.domain,this.domain.getDbType());
	
			sList.add(new Statement(85000L,1,"let "+this.domain.getSnakeDomainName()+" = "+StringUtil.getSnakeName(findShadow.getVerbName())+"("+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+").await?;"));
			sList.add(new Statement(86000L,1,"if verify_password("+this.domain.findFieldByFixedName("password").getSnakeFieldName()+", "+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+".clone(), "+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+".clone()).await? {"));
			sList.add(new Statement(87000L,2,"Ok("+this.domain.getSnakeDomainName()+".clone())"));
			sList.add(new Statement(88000L,1,"} else {"));
			sList.add(new Statement(89000L,2,"Err(SysError::WrongPassword)"));
			sList.add(new Statement(90000L,1,"}"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	
	public LoginUser(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("LoginUser");
		this.setVerbName("Login"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("登录");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("LoginUser");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Login" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1,"mut session","WritableSession"));
			method.addSignature(new Signature(2,"Json(input)","Json<LoginInput>"));
			method.setReturnType(new Type("String"));
			
			List<Writeable> sList = new ArrayList<Writeable>();

			sList.add(new Statement(1000L,1,"let "+this.domain.getSnakeDomainName()+" = service_login_"+this.domain.getSnakeDomainName()+"(input."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+",input."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+")"));
			sList.add(new Statement(2000L,2,".await;"));			
			sList.add(new Statement(3000L,1,"match "+this.domain.getSnakeDomainName()+" {"));
			sList.add(new Statement(4000L,1,"Ok("+this.domain.getSnakeDomainName()+") => {"));
			sList.add(new Statement(5000L,1,"let token = jwt_util::sign("+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+").unwrap();"));
			sList.add(new Statement(6000L,2,"let _ = session.insert(\"token\",token.clone());"));
			sList.add(new Statement(7000L,2,"let result = serde_json::to_string_pretty(&TokenPayload {"));
			sList.add(new Statement(8000L,3,"success:true,"));
			sList.add(new Statement(9000L,3,"token: token.clone()"));
			sList.add(new Statement(10000L,2,"}).unwrap();"));
			sList.add(new Statement(11000L,2,"result"));
			sList.add(new Statement(12000L,1,"},"));
			sList.add(new Statement(13000L,1,"Err(_) => {"));
			sList.add(new Statement(14000L,2,"let mut map = Map::new();"));
			sList.add(new Statement(15000L,3,"map.insert(\"success\".to_string(), Value::from(false));"));
			sList.add(new Statement(16000L,3,"map.insert("));
			sList.add(new Statement(17000L,4,"\"error\".to_string(),"));
			sList.add(new Statement(18000L,4,"Value::from(Error::WrongCredentials.to_string()),"));
			sList.add(new Statement(19000L,3,");"));
			sList.add(new Statement(20000L,0,""));
			sList.add(new Statement(21000L,2,"let result = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(22000L,2,"result"));
			sList.add(new Statement(23000L,2,"}"));
			sList.add(new Statement(24000L,1,"}"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;		
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
