package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.FieldUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class FindUserShadow extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(this.getVerbName());
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getDomainName().getSnakeFieldName(),this.domain.getDomainName().getFieldType()));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();	
			
			sList.add(new Statement(500L,1,"let result = sqlx::query_as("));
			
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,1,"r#\""+MybatisSqlReflector.generateFindByNameStatement(domain)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,1,"r#\""+ PgsqlReflector.generateFindByNameStatement(domain)+"\"#"));
			}
			
			sList.add(new Statement(6000L,2,")"));
			sList.add(new Statement(7000L,2,".bind("+this.domain.getDomainName().getSnakeFieldName()+")"));
			sList.add(new Statement(8000L,2,".fetch_one(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(9000L,2,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+ MybatisOracleSqlReflector.generateFindByNameStatement(domain)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let row = stmt.query_row(&[&"+this.domain.getDomainName().getSnakeFieldName()+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+8000L,1,"Ok("+this.domain.getSnakeDomainName()+")"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Find"+StringUtil.capFirst(this.domain.getStandardName())+"Shadow");
			method.setReturnType(new Type("String"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());			
			method.addSignature(new Signature(1, this.domain.findFieldByFixedName("userName").getLowerFirstFieldName(), this.domain.findFieldByFixedName("userName").getFieldRawType(),this.domain.getType().getPackageToken()));
			return method;
		}
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Find"+StringUtil.capFirst(this.domain.getStandardName())+"Shadow");
			method.setReturnType(new Type("String"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, this.domain.findFieldByFixedName("userName").getLowerFirstFieldName(), this.domain.findFieldByFixedName("userName").getFieldRawType(),this.domain.getType().getPackageToken()));
			return method;
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(this.getVerbName());
			method.addSignature(new Signature(1, this.domain.getDomainName().getSnakeFieldName(), new Type("String")));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
			
			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainName().getSnakeFieldName()+").await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}
	
	public FindUserShadow(Domain domain,String dbType) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.setVerbName("Find"+this.domain.getCapFirstDomainName()+"ShadowBy"+this.domain.getDomainName().getCapFirstFieldName());
		this.setLabel("查找用户秘密");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Find"+this.domain.getCapFirstDomainName()+"Shadow");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(this.getVerbName());
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getDomainName().getSnakeFieldName(),this.domain.getDomainName().getFieldType()));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();	
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"for mut "+this.domain.getSnakeDomainName()+" in get_db() {"));
			sList.add(new Statement(serial+2000L,2,"if "+this.domain.getSnakeDomainName()+"."+this.domain.getDomainName().getSnakeFieldName()+" == "+this.domain.getDomainName().getSnakeFieldName()+" {"));
			serial += 4000L;
			
			Set<Field> shadowFields = new TreeSet<>();
			shadowFields.add(this.domain.findFieldByFixedName("password"));
			shadowFields.add(this.domain.findFieldByFixedName("salt"));
			shadowFields.add(this.domain.findFieldByFixedName("loginFailure"));
			
			Set<Field> hiddenFields = new TreeSet<>();
			hiddenFields.addAll(this.domain.getPlainFields());
			hiddenFields.removeAll(shadowFields);
			for (Field f:hiddenFields) {
				sList.add(new Statement(serial,3,this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" = "+FieldUtil.findTypeDefaultValueStringToken(f.getFieldType())+";"));
				serial += 1000L;	
			}
			sList.add(new Statement(serial+3000L,3,"return Ok("+this.domain.getSnakeDomainName()+");"));
			sList.add(new Statement(serial+4000L,2,"}"));
			sList.add(new Statement(serial+5000L,1,"}"));
			if ("Oracle".equalsIgnoreCase(this.domain.getDbType())) {
				sList.add(new Statement(serial+6000L,1,"Err(Error::NoDataFound)"));
			} else {
				sList.add(new Statement(serial+6000L,1,"Err(Error::RowNotFound)"));
			}
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
