package org.light.simpleauth.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.Assign;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.FieldUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.FindByName;

public class AddUser extends Verb implements EasyUIPositions {
	protected Domain roleDomain;

	public Domain getRoleDomain() {
		return roleDomain;
	}

	public void setRoleDomain(Domain roleDomain) {
		this.roleDomain = roleDomain;
	}

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Add" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, StringUtil.getSnakeName(this.domain.getStandardName()), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(500L,2,"let result = sqlx::query("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,2,"r#\""+MybatisSqlReflector.generateInsertSql(domain)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,2,"r#\""+ PgsqlReflector.generateInsertSql(domain)+"\"#"));
			}			
			sList.add(new Statement(2000L,2,")"));
			long serial = 3000L;
			for (Field f:domain.getFieldsWithoutId()) {
				sList.add(new Statement(serial,2,".bind("+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+")"));
				serial += 1000L;
			}
			sList.add(new Statement(serial+2000L,2,".execute(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(serial+3000L,2,".await;"));
			
			sList.add(new Statement(serial+10000L,2,"match result {"));
			sList.add(new Statement(serial+11000L,3,"Ok(result) => {"));
			sList.add(new Statement(serial+12000L,4,"Ok(result.rows_affected())"));
			sList.add(new Statement(serial+13000L,3,"},"));
			sList.add(new Statement(serial+14000L,3,"Err(err) => {"));
			sList.add(new Statement(serial+15000L,4,"Err(err)"));
			sList.add(new Statement(serial+16000L,3,"}"));
			sList.add(new Statement(serial+17000L,2,"}"));
			
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateInsertSqlWithDeniedFields(domain,this.deniedFields)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		
		serial += 6000L;
		
		Set<Field> fieldsWithoutId = this.domain.getFieldsWithoutId();
		
		for (Field f: fieldsWithoutId){
			if ("bool".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,1,"let mut "+f.getSnakeFieldName()+"_int = 0;"));
				sList.add(new Statement(serial+1000L,1,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" == true {"));
				sList.add(new Statement(serial+2000L,2,f.getSnakeFieldName()+"_int = 1;"));
				sList.add(new Statement(serial+3000L,1,"}"));
				serial += 4000L;
			}
		}
		
		StringBuilder sb = new StringBuilder("");
		String fieldsStr = "";
		for (Field f: fieldsWithoutId){
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sb.append("&base64::decode(").append(this.domain.getSnakeDomainName()).append(".").append(f.getSnakeFieldName()).append(").unwrap(),");
			} else if ("bool".equalsIgnoreCase(f.getFieldType())) {
				sb.append("&").append(f.getSnakeFieldName()).append("_int,");
			} else {
				sb.append("&").append(this.domain.getSnakeDomainName()).append(".").append(f.getSnakeFieldName()).append(",");
			}
		}
		if (sb.toString().endsWith(",")) fieldsStr = sb.substring(0, sb.length()-1);
		
		sList.add(new Statement(serial+6000L,1,"let _ = stmt.execute(&["+fieldsStr+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let _ = conn.commit();"));
		sList.add(new Statement(serial+8000L,1,"Ok(0 as u64)"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("void"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			
			method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("bool"));
			method.setThrowException(true);
			method.addAdditionalImport("java.util.List");
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
			return method;
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Add" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(
					new Signature(1, this.domain.getSnakeDomainName(), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"let salt = generate_salt().await.unwrap();"));
			sList.add(new Statement(2000L,1,"let password = multi_sha1("+this.domain.getLowerFirstDomainName()+".clone()."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+",salt.clone(),3);"));
			sList.add(new Statement(3000L,1,"let mut "+this.domain.getSnakeDomainName()+" = "+this.domain.getSnakeDomainName()+".clone();"));
			sList.add(new Statement(4000L,1,""+this.domain.getSnakeDomainName()+"."+this.domain.findFieldByFixedName("salt").getSnakeFieldName()+" = salt.clone();"));
			sList.add(new Statement(5000L,1,""+this.domain.getLowerFirstDomainName()+"."+this.domain.findFieldByFixedName("password").getSnakeFieldName()+" = password;"));
			sList.add(new Statement(6000L,1,"let app_state = init_db();"));
			sList.add(new Statement(7000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getSnakeDomainName()+").await"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	
	public AddUser(Domain domain,Domain roleDomain) throws ValidateException{
		super();
		this.domain = domain;
		this.roleDomain = roleDomain;
		this.setVerbName("Add"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("新增");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Add");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("Add" + StringUtil.capFirst(this.domain.getStandardName()));
		method.addSignature(new Signature(1,"Json("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Json<"+this.domain.getCapFirstDomainName()+"Request>"));
		method.setReturnType(new Type("String"));
		
		FindByName findUser = new FindByName(this.domain);
		FindByName findRole = new FindByName(this.roleDomain);
		Assign assignRole = new Assign(this.domain,this.roleDomain);
		
		method.addAdditionalImport("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(findUser.getVerbName())+" as service_"+StringUtil.getSnakeName(findUser.getVerbName()));
		method.addAdditionalImport("crate::"+this.roleDomain.getServiceimplSuffix()+"::"+this.roleDomain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(findRole.getVerbName())+" as service_"+StringUtil.getSnakeName(findRole.getVerbName()));
		method.addAdditionalImport("crate::"+this.domain.getServiceimplSuffix()+"::"+this.domain.getSnakeDomainName()+"_service::"+StringUtil.getSnakeName(assignRole.getVerbName())+" as service_"+StringUtil.getSnakeName(assignRole.getVerbName()));
		
		
		List<Writeable> sList = new ArrayList<Writeable>();

		long serial = 1000L;
		for (Field f: this.domain.getFields()) {
			if (!"Image".equalsIgnoreCase(f.getFieldType())) {
				if ("datetime".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+100L,1,"let mut _fmt_"+f.getSnakeFieldName()+" = Option::None;"));
					sList.add(new Statement(serial+200L,1,"if "+f.getSnakeFieldName()+" != \"\" {"));
					sList.add(new Statement(serial+300L,2,"_fmt_"+f.getSnakeFieldName()+" = Some(NaiveDateTime::parse_from_str(&"+f.getSnakeFieldName()+",DATE_TIME_FORMAT).unwrap());"));
					sList.add(new Statement(serial+400L,1,"}"));
				}else if ("date".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+100L,1,"let mut _fmt_"+f.getSnakeFieldName()+" = Option::None;"));
					sList.add(new Statement(serial+200L,1,"if "+f.getSnakeFieldName()+" != \"\" {"));
					sList.add(new Statement(serial+300L,2,"_fmt_"+f.getSnakeFieldName()+" = Some(NaiveDate::parse_from_str(&"+f.getSnakeFieldName()+",DATE_FORMAT).unwrap());"));
					sList.add(new Statement(serial+400L,1,"}"));
				}else {
					sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
				}
				serial += 1000L;
			}
		}
		
		for (Field f: this.domain.getFields()) {
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,1,"let my_"+f.getSnakeFieldName()+" = pick_"+this.domain.getSnakeDomainName()+"_picture(\""+f.getSnakeFieldName()+"\".to_string());"));
				serial += 1000L;
			}
		}
		
		sList.add(new Statement(serial,0,""));
		sList.add(new Statement(serial+1000L,1,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"{"));
		serial += 2000L;
		for (Field f: this.domain.getFields()) {
			if (!"Image".equalsIgnoreCase(f.getFieldType())) {
				if ("datetime".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : _fmt_"+f.getSnakeFieldName()+","));
				} else if ("date".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : _fmt_"+f.getSnakeFieldName()+","));
				} else {
					sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : "+f.getSnakeFieldName()+","));
				}
			} else {
				sList.add(new Statement(serial,1,""+f.getSnakeFieldName()+" : my_"+f.getSnakeFieldName()+","));
			}
			serial += 1000L;
		}
		sList.add(new Statement(serial,1,"};"));
		sList.add(new Statement(serial+1000L,1,""));
		
		sList.add(new Statement(serial+2000L,1,"let "+this.domain.getSnakeDomainName()+"0 = service_"+StringUtil.getSnakeName(findUser.getVerbName())+"("+this.domain.getSnakeDomainName()+".clone()."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+").await;"));
		sList.add(new Statement(serial+3000L,0,""));
		sList.add(new Statement(serial+4000L,1,"match "+this.domain.getSnakeDomainName()+"0 {"));
		sList.add(new Statement(serial+5000L,2,"Ok("+this.domain.getSnakeDomainName()+"0) => {"));
		sList.add(new Statement(serial+6000L,3,"if "+this.domain.getSnakeDomainName()+"0."+this.domain.getDomainId().getSnakeFieldName()+" > 0 {"));
		if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
			sList.add(new Statement(serial+7000L,4,"return r#\"{  \"rows\": null,  \"success\": false, \"error\":\""+this.domain.getText()+" exists.\"}\"#.to_string();"));
		}else {
			sList.add(new Statement(serial+7000L,4,"return r#\"{  \"rows\": null,  \"success\": false, \"error\":\""+this.domain.getText()+"已存在\"}\"#.to_string();"));
		}
		sList.add(new Statement(serial+8000L,3,"}"));
		sList.add(new Statement(serial+9000L,2,"},"));
		sList.add(new Statement(serial+10000L,2,"Err(_) => {"));
		sList.add(new Statement(serial+11000L,2,"}"));
		sList.add(new Statement(serial+12000L,1,"}"));
		
		sList.add(new Statement(serial+19000L,1,"let _result = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getSnakeDomainName()+".clone()).await;"));
		sList.add(new Statement(serial+20000L,1,"match _result {"));
		sList.add(new Statement(serial+21000L,2,"Err(_) => {"));
		sList.add(new Statement(serial+23000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
		sList.add(new Statement(serial+24000L,2,"},"));
		sList.add(new Statement(serial+25000L,2,"Ok(_result) => {"));
		
		sList.add(new Statement(serial+25100L,3,"let "+this.roleDomain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(findRole.getVerbName())+"(\"user\".to_string()).await.unwrap();"));
		sList.add(new Statement(serial+25200L,3,"let "+this.domain.getSnakeDomainName()+" = service_"+StringUtil.getSnakeName(findUser.getVerbName())+"("+this.domain.getSnakeDomainName()+".clone()."+this.domain.findFieldByFixedName("userName").getSnakeFieldName()+").await.unwrap();"));
		sList.add(new Statement(serial+25400L,3,"let _ = service_"+StringUtil.getSnakeName(assignRole.getVerbName())+"("+this.domain.getSnakeDomainName()+"."+this.domain.getDomainId().getSnakeFieldName()+",vec!["+this.roleDomain.getSnakeDomainName()+"."+this.roleDomain.getDomainId().getSnakeFieldName()+"]).await;"));
		
		sList.add(new Statement(serial+26000L,3,"let mut map = Map::new();"));
		sList.add(new Statement(serial+27000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
		sList.add(new Statement(serial+28000L,3,"map.insert("));
		sList.add(new Statement(serial+29000L,4,"\"rows\".to_string(),"));
		sList.add(new Statement(serial+30000L,4,"Value::from(\"\"),"));
		sList.add(new Statement(serial+31000L,3,");"));
		sList.add(new Statement(serial+32000L,3,""));
		sList.add(new Statement(serial+33000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
		sList.add(new Statement(serial+35000L,3,"return resultjson;"));
		sList.add(new Statement(serial+36000L,2,"}"));
		sList.add(new Statement(serial+37000L,1,"}"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied) return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("add"+domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Add',"));
			}else {
				sl.add(new Statement(2000,1, "text:'新增',"));
			}
			sl.add(new Statement(3000,1, "iconCls:'icon-add',"));
			sl.add(new Statement(4000,1, "handler:function(){"));
			sl.add(new Statement(5000,2, "$('#wadd"+this.domain.getCapFirstDomainName()+"').window('open');"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,0, "}"));
			block.setMethodStatementList(sl);
			return block;			
		}
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied) return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("add"+domain.getCapFirstDomainName());
			
			StatementList sl = new StatementList();
			
			sl.add(new Statement(1000L,1,"if ($(\"#ff\").find(\"#"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\").val()!=$(\"#ff\").find(\"#confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"\").val()){"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sl.add(new Statement(2000L,2,"$.messager.alert(\"Error\",\"Password did not match.\",\"error\");"));
			}else {
				sl.add(new Statement(2000L,2,"$.messager.alert(\"错误\",\"密码不匹配！\",\"error\");"));
			}
			sl.add(new Statement(3000L,2,"return;"));
			sl.add(new Statement(4000L,1,"}"));
			
			sl.add(new Statement(5000,1, "if ($(\"#ff\").form(\"validate\")) {"));
			sl.add(new Statement(6000,2, "$.ajax({"));
			sl.add(new Statement(7000,3, "type: \"post\","));
			sl.add(new Statement(8000,3, "url: \"../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/add"+domain.getCapFirstDomainName()+"\","));
			sl.add(new Statement(9000,3, "data: JSON.stringify({"));
			
			sl.add(new Statement(10000,4, this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+":$(\"#ff\").find(\"#"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"\").textbox(\"getValue\"),"));
			sl.add(new Statement(11000,4, this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+":hex_sha1($(\"#ff\").find(\"#"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"\").textbox(\"getValue\")),"));
			
			Set<Field> normalFields = new TreeSet<>(new FieldSerialComparator());
			normalFields.addAll(domain.getFieldsWithoutId());
			Set<Field> deniedFields = new TreeSet<>(new FieldSerialComparator());
			deniedFields.add(this.domain.findFieldByFixedName("userName"));
			deniedFields.add(this.domain.findFieldByFixedName("password"));
			deniedFields.add(this.domain.findFieldByFixedName("salt"));
			deniedFields.add(this.domain.findFieldByFixedName("loginFailure"));
			normalFields = FieldUtil.filterDeniedFields(normalFields,deniedFields);
			long serial = 12000;
			for (Field f:normalFields){
				if (f instanceof Dropdown) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseIntNeg($(\"#ff\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").combobox(\"getValue\")),"));
				} else if (f.getFieldType().equalsIgnoreCase("bool")) {
					sl.add(new Statement(serial, 3,
							f.getLowerFirstFieldName() + ":parseBoolean($(\"#ff\").find(\"input[name='"
									+ f.getLowerFirstFieldName() + "']:checked\").val()),"));
				} else if (f.getFieldType().equalsIgnoreCase("i32")||f.getFieldType().equalsIgnoreCase("i64")) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseInt($(\"#ff\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val()),"));
				} else if (f.getFieldType().equalsIgnoreCase("f64")||f.getFieldType().equalsIgnoreCase("f32")) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":parseFloat($(\"#ff\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val()),"));
				} else if (f.isTextarea()) {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ff\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val(),"));
				} else {
					sl.add(new Statement(serial, 3, f.getLowerFirstFieldName() + ":$(\"#ff\").find(\"#"
							+ f.getLowerFirstFieldName() + "\").val(),"));
				}
				serial += 1000;
			}				
			sl.add(new Statement(serial,3, "}),"));
			sl.add(new Statement(serial+1000,3, "dataType: 'json',"));
			sl.add(new Statement(serial+2000,3, "contentType:\"application/json;charset=UTF-8\","));
			sl.add(new Statement(serial+3000,3, "success: function(data, textStatus) {"));
			sl.add(new Statement(serial+4000,4, "if (data.success) {"));
			sl.add(new Statement(serial+5000,5, "$('#ff').form('clear');"));
			sl.add(new Statement(serial+6000,5, "$(\"#ff\").find(\"input[name='"+domain.getActive().getLowerFirstFieldName()+"']\").get(0).checked = true;"));
			sl.add(new Statement(serial+7000,5, "$(\"#wadd"+domain.getCapFirstDomainName()+"\").window('close');"));
			sl.add(new Statement(serial+8000,5, "$(\"#dg\").datagrid(\"load\");"));
			if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
				sl.add(new Statement(serial+8500,5, "$.messager.alert(\"Info\",\"Register user success.\",\"info\");"));
				sl.add(new Statement(serial+9000,4, "} else {"));
				sl.add(new Statement(serial+9100,5, "$.messager.alert(\"Error\",data.error,\"error\");"));
			} else {
				sl.add(new Statement(serial+8500,5, "$.messager.alert(\"信息\",\"注册用户成功！\",\"info\");"));
				sl.add(new Statement(serial+9000,4, "} else {"));
				sl.add(new Statement(serial+9100,5, "$.messager.alert(\"错误\",data.error,\"error\");"));
			}
			sl.add(new Statement(serial+9200,4, "}"));
			sl.add(new Statement(serial+10000,4, "},"));
			sl.add(new Statement(serial+11000,3, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(serial+12000,3, "},"));
			sl.add(new Statement(serial+13000,3, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(serial+14000,4, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(serial+15000,4, "alert(errorThrown.toString());"));
			sl.add(new Statement(serial+16000,3, "}"));
			sl.add(new Statement(serial+17000,2, "});"));
			sl.add(new Statement(serial+18000,1, "}"));
			
			method.setMethodStatementList(sl);
			return method;	
		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Add" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(
					new Signature(2, StringUtil.getSnakeName(this.domain.getStandardName()), new Type(this.domain.getCapFirstDomainNameWithSuffix())));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut "+this.domain.getSnakeDomainName()+"0 = "+this.domain.getSnakeDomainName()+".clone();"));
			sList.add(new Statement(serial+2000L,1,""+this.domain.getSnakeDomainName()+"0."+this.domain.getDomainId().getSnakeFieldName()+" = max_id() +1 as i64;"));
			sList.add(new Statement(serial+3000L,1,"DB.lock().unwrap().push("+this.domain.getSnakeDomainName()+"0);"));
			sList.add(new Statement(serial+4000L,1,"return Ok(0 as u64);"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
