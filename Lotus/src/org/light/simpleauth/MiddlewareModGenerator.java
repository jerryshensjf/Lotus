package org.light.simpleauth;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.Generator;
import org.light.utils.WriteableUtil;

public class MiddlewareModGenerator extends Generator{
	public MiddlewareModGenerator(){
		super();
		super.fileName = "mod.rs";
		super.standardName = "MiddlewareMod";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(500L,0,"pub mod login_middleware;"));
		return WriteableUtil.merge(sList);
	}

}
