package org.light.simpleauth;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.Generator;
import org.light.utils.RandomUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class LoginMiddlewareGenerator extends Generator{
	protected Domain userDomain;

	public Domain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(Domain userDomain) {
		this.userDomain = userDomain;
	}

	public LoginMiddlewareGenerator(){
		super();
		super.fileName = "login_middleware.rs";
		super.standardName = "LoginMiddleware";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(500L,0,"#![allow(unused_assignments)]"));
		sList.add(new Statement(700L,0,"extern crate querystring;"));
		sList.add(new Statement(800L,0,""));
		sList.add(new Statement(1000L,0,"use axum::{"));
		sList.add(new Statement(4000L,1,"http::Request,"));
		sList.add(new Statement(5000L,1,"middleware::Next,"));
		sList.add(new Statement(6000L,1,"response::{IntoResponse,Response},"));
		sList.add(new Statement(10000L,0,"};"));
		sList.add(new Statement(11000L,0,""));
		sList.add(new Statement(12000L,0,"use crate::"+this.userDomain.getServiceimplSuffix()+"::login_service::{can_access};"));
		sList.add(new Statement(14000L,0,""));
		sList.add(new Statement(18000L,0,"use axum_sessions::SessionHandle;"));
		sList.add(new Statement(20000L,0,""));
		sList.add(new Statement(21000L,0,"pub async fn check_auth<B>(req:Request<B>, next:Next<B>) -> Response"));
		sList.add(new Statement(22000L,0,"where"));
		sList.add(new Statement(23000L,1,"B:std::fmt::Debug,"));
		sList.add(new Statement(24000L,0,"{"));
		sList.add(new Statement(25000L,1,"let uri = req.uri().clone().to_string();"));
		sList.add(new Statement(26000L,1,"let mut token = \"\".to_string();"));
		
		sList.add(new Statement(26100L,1,"if let Some(token0) = req.uri().query() {"));
		sList.add(new Statement(26200L,2,"let tokens = querystring::querify(token0);"));
		sList.add(new Statement(26300L,2,"for item in &tokens {"));
		sList.add(new Statement(26400L,3,"if item.0 == \"token\"{"));
		sList.add(new Statement(26500L,4,"token = item.1.to_string();"));
		sList.add(new Statement(26600L,3,"}"));
		sList.add(new Statement(26700L,2,"}"));
		sList.add(new Statement(26800L,1,"}"));
		
		sList.add(new Statement(27000L,1,"let mut can_access_bool  = can_access(token,uri.clone()).await;"));
		sList.add(new Statement(29000L,1,"if can_access_bool == false {"));
		sList.add(new Statement(30000L,2,"let session_handle = req.extensions().get::<SessionHandle>().unwrap();"));
		sList.add(new Statement(31000L,2,""));
		sList.add(new Statement(32000L,2,"let session = session_handle.read().await;"));
		sList.add(new Statement(33000L,2,"let stoken = session.get(\"token\");"));
		sList.add(new Statement(34000L,0,""));
		sList.add(new Statement(35000L,2,"match stoken {"));
		sList.add(new Statement(36000L,3,"Some(stoken) => {"));
		sList.add(new Statement(37000L,4,"token = stoken;"));
		sList.add(new Statement(38000L,3,"},"));
		sList.add(new Statement(39000L,3,"None => {"));
		sList.add(new Statement(40000L,4,"let headers = req.headers();"));
		sList.add(new Statement(41000L,4,"token = headers.get(\"X-Token\").unwrap().to_str().unwrap().to_string();"));
		sList.add(new Statement(42000L,3,"}"));
		sList.add(new Statement(43000L,2,"}"));
		sList.add(new Statement(48000L,2,"can_access_bool = can_access(token,uri).await;"));
		sList.add(new Statement(50000L,1,""));
		sList.add(new Statement(51000L,1,"}"));
		sList.add(new Statement(52000L,0,""));
		sList.add(new Statement(53000L,1,"if can_access_bool {"));
		sList.add(new Statement(54000L,2,"let response = next.run(req).await;"));
		sList.add(new Statement(55000L,2,"return response;"));
		sList.add(new Statement(56000L,1,"}"));
		sList.add(new Statement(57000L,1,"r#\"{  \"rows\": null,  \"success\": false, \"noAuth\":true}\"#.into_response()"));
		sList.add(new Statement(58000L,0,"}"));

		return WriteableUtil.merge(sList);
	}

}
