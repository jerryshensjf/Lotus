package org.light.simpleauth;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.Generator;
import org.light.utils.WriteableUtil;

public class JwtUtilGenerator extends Generator{
	protected Domain userDomain;

	public Domain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(Domain userDomain) {
		this.userDomain = userDomain;
	}

	public JwtUtilGenerator(){
		super();
		super.fileName = "jwt_util.rs";
		super.standardName = "JwtUtil";
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		
		sList.add(new Statement(1000L,0,"use chrono::{Duration, Utc};"));
		sList.add(new Statement(2000L,0,"use jsonwebtoken::{EncodingKey, Header};"));
		sList.add(new Statement(4000L,0,"use serde::{Deserialize, Serialize};"));
		sList.add(new Statement(5000L,0,"use crate::{"+this.userDomain.getDomainSuffix()+"::error::Result};"));
		sList.add(new Statement(6000L,0,""));
		sList.add(new Statement(7000L,0,"pub const  JWT_SECRET: &str = \"top_secret_key\";"));
		sList.add(new Statement(8000L,0,""));
		sList.add(new Statement(9000L,0,"#[derive(Debug, Deserialize, Serialize)]"));
		sList.add(new Statement(10000L,0,"pub struct Claims {"));
		sList.add(new Statement(11000L,1,"pub sub: i64,"));
		sList.add(new Statement(12000L,1,"pub exp: i64,"));
		sList.add(new Statement(13000L,1,"pub iat: i64,"));
		sList.add(new Statement(14000L,0,"}"));
		sList.add(new Statement(15000L,0,""));
		sList.add(new Statement(16000L,0,"impl Claims {"));
		sList.add(new Statement(17000L,1,"pub fn new(id: i64) -> Self {"));
		sList.add(new Statement(18000L,2,"let iat = Utc::now();"));
		sList.add(new Statement(19000L,2,"let exp = iat + Duration::hours(24);"));
		sList.add(new Statement(20000L,0,""));
		sList.add(new Statement(21000L,2,"Self {"));
		sList.add(new Statement(22000L,3,"sub: id,"));
		sList.add(new Statement(23000L,3,"iat: iat.timestamp(),"));
		sList.add(new Statement(24000L,3,"exp: exp.timestamp(),"));
		sList.add(new Statement(25000L,2,"}"));
		sList.add(new Statement(26000L,1,"}"));
		sList.add(new Statement(27000L,0,"}"));
		sList.add(new Statement(28000L,0,""));
		sList.add(new Statement(29000L,0,"pub fn sign(id: i64) -> Result<String> {"));
		sList.add(new Statement(30000L,1,"Ok(jsonwebtoken::encode("));
		sList.add(new Statement(31000L,2,"&Header::default(),"));
		sList.add(new Statement(32000L,2,"&Claims::new(id),"));
		sList.add(new Statement(33000L,2,"&EncodingKey::from_secret(JWT_SECRET.as_bytes()),"));
		sList.add(new Statement(34000L,1,")?)"));
		sList.add(new Statement(35000L,0,"}"));
		sList.add(new Statement(36000L,0,""));

		return WriteableUtil.merge(sList);
	}

}
