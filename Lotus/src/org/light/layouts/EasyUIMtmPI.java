package org.light.layouts;

import org.light.core.PrismInterface;
import org.light.domain.Domain;
import org.light.easyuilayouts.EasyUIMtmLayout;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class EasyUIMtmPI extends PrismInterface{
	protected Domain master;
	protected Domain slave;
	protected String slaveAliasLabel;
	protected EasyUIMtmLayout eLayout;

	public EasyUIMtmPI(Domain master,Domain slave,String slaveAliasLabel) throws Exception{
		super();
		this.slaveAliasLabel = slaveAliasLabel;
		this.master = master;
		this.slave = slave;
		if (StringUtil.isBlank(this.slave.getAlias())){
			this.standardName = StringUtil.lowerFirst("link"+master.getCapFirstDomainName()+slave.getCapFirstDomainName());
		}else{
			this.standardName = StringUtil.lowerFirst("link"+master.getCapFirstDomainName()+StringUtil.capFirst(slave.getAlias()));
		}
		this.setLabel("链接"+this.master.getText()+this.slave.getText());
		if ("english".equalsIgnoreCase(this.master.getLanguage())) {
			this.setLabel("Link "+this.master.getText()+" "+this.slave.getText());
		}
		this.frame.setStandardName(this.label);
		this.eLayout = new EasyUIMtmLayout(master,slave);
		this.eLayout.setSlaveAliasLabelOrText(slaveAliasLabel);
		this.eLayout.parse();
		this.getFrame().setMainContent(this.eLayout);
		frame.setLanguage(this.master.getLanguage());
	}	

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "template/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", this.getFrame().generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.standardName.toLowerCase()+".html", this.getFrame().generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.getFrame().setNav(nav);
	}
}
