package org.light.layouts;

import org.light.core.LayoutComb;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.EasyUIParentChild;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class ParentChildGridLayout extends LayoutComb{
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EasyUIParentChild eLayout = new EasyUIParentChild();
	protected Domain parentDomain;
	protected Domain childDomain;
	protected String parentId = "parentId";
	
	public ParentChildGridLayout() {
		super();
	}
	
	public ParentChildGridLayout(Domain parentDomain,Domain childDomain,String parentId,String dbType) {
		super();
		this.standardName = "ParentChild"+parentDomain.getCapFirstDomainName()+childDomain.getCapFirstDomainName();
		this.label = parentDomain.getText()+ childDomain.getText() +"父子表";
		if ("english".equalsIgnoreCase(parentDomain.getLanguage())) {
			this.label ="Parent child grid for "+parentDomain.getText()+ " and " + childDomain.getText();
		}
		this.parentDomain = parentDomain;
		this.childDomain = childDomain;
		this.parentId = parentId;
		
		this.domains.add(this.parentDomain);
		this.domains.add(this.childDomain);
		
		eLayout.setParentDomain(parentDomain);
		eLayout.setChildDomain(childDomain);
		eLayout.setParentId(childDomain.findFieldByFieldName(this.parentId));
		eLayout.parse();
		frame.setMainContent(eLayout);
		frame.setStandardName(this.label);
		frame.setLanguage(this.parentDomain.getLanguage());
	}

	public EasyUIParentChild geteLayout() {
		return eLayout;
	}

	public void seteLayout(EasyUIParentChild eLayout) {
		this.eLayout = eLayout;
	}

	public Domain getParentDomain() {
		return parentDomain;
	}

	public void setParentDomain(Domain parentDomain) {
		this.parentDomain = parentDomain;
	}

	public Domain getChildDomain() {
		return childDomain;
	}

	public void setChildDomain(Domain childDomain) {
		this.childDomain = childDomain;
	}


	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "template/pages/";
		WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		ValidateInfo info = new ValidateInfo();
		Field f = DomainUtil.findDomainFieldByFieldName(this.childDomain, this.parentId);
		if (f==null) info.addCompileError(this.getText()+"的parentId字段找不到！");
		if (f!=null && f instanceof Dropdown) {
			Dropdown dp = (Dropdown) f;
			if (!dp.getTargetName().equals(this.parentDomain.getStandardName())) {
				info.addCompileError(this.getText()+"的parentId原字段类型不匹配！");
			}			
		}
		return info;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public EasyUIFrameSet getFrame() {
		return frame;
	}

	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}
	

	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.frame.setTitles(title, subTitle, footer);
	}

	@Override
	public String getDomainNamesStr() {
		return this.parentDomain.getStandardName()+","+this.childDomain.getStandardName();
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


}
