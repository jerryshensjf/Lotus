package org.light.layouts;

import org.light.core.PrismInterface;
import org.light.easyuilayouts.EasyUIHomePageLayout;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EasyUIHomePagePI extends PrismInterface{

	public EasyUIHomePagePI(String language) throws Exception{
		super();
		this.setStandardName("Index");
		this.setLabel(getLabel());
		this.eLayout = new EasyUIHomePageLayout(domain);
		this.eLayout.setLanguage(language);
		this.eLayout.parse();
		this.getFrame().setMainContent(this.eLayout);
		this.frame.setStandardName("主页");
		if ("english".equalsIgnoreCase(language)) this.frame.setStandardName("Homepage");
		frame.setLanguage(language);		
	}	

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.getFrame().setTitles(title, subTitle, footer);
	}

	@Override
	public void generatePIFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "template/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+"index.html", this.getFrame().generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+"index.html", this.getFrame().generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generatePIFromDomian() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validateDomain() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayout() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}

}
