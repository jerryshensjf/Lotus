package org.light.reports;

import java.util.List;

import org.light.core.ReportComb;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EChartsGridReport extends ReportComb{
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EchartsGridReportLayout eLayout = new EchartsGridReportLayout();
	protected Domain reportDomain;
	protected List<Field> xAxisFields;
	protected Field yName;
	
	public EChartsGridReport() throws Exception{
		super();
	}
	
	public EChartsGridReport(Domain reportDomain,List<Field> xAxisFields,Field yName) throws Exception{
		super();
		this.standardName = "EchartsGridReport"+reportDomain.getCapFirstDomainName()+yName.getCapFirstFieldName();
		this.label = reportDomain.getText()+ yName.getText() +"Echarts网格图表";
		if ("english".equalsIgnoreCase(reportDomain.getLanguage())) {
			this.label ="Echarts grod report for"+reportDomain.getText();
		}
		this.reportDomain = reportDomain;
		this.xAxisFields = xAxisFields;
		this.yName = yName;
		
		this.domains.add(this.reportDomain);
		
		eLayout.setReportDomain(this.reportDomain);
		eLayout.setxAxisFields(this.xAxisFields);
		eLayout.setyName(this.yName);
		eLayout.parse();
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,0,"<script type=\"text/javascript\" src=\"../echarts/echarts.min.js\"></script>"));
		frame.setAdditionScriptFiles(sl);
		frame.setMainContent(eLayout);
		frame.setStandardName(this.label);
		frame.setLanguage(this.reportDomain.getLanguage());
	}

	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "template/pages/";
		WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(Domain reportDomain) {
		this.reportDomain = reportDomain;
	}
	
	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.frame.setTitles(title, subTitle, footer);
	}

	public EasyUIFrameSet getFrame() {
		return frame;
	}

	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}

	public EchartsGridReportLayout geteLayout() {
		return eLayout;
	}

	public void seteLayout(EchartsGridReportLayout eLayout) {
		this.eLayout = eLayout;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public String getTechnicalStack() {
		return technicalStack;
	}

	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}
	
	@Override
	public String getDomainNamesStr() {
		return this.reportDomain.getStandardName();
	}
	
	public String getxAxisFieldsNames() {
		StringBuilder sb = new StringBuilder();
		for (Field f:this.xAxisFields) {
			sb.append(f.getFieldName()).append(",");
		}
		String result = sb.toString();
		if (result.endsWith(",")) result = result.substring(0,result.lastIndexOf(","));
		return result;
	}
}
