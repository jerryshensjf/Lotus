package org.light.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Writeable;
import org.light.core.Widget;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.reports.verbs.ListActiveOption;
import org.light.reports.verbs.ListActiveSumOption;
import org.light.utils.DomainUtil;
import org.light.utils.WriteableUtil;

public class EchartsDiagram extends Widget{
	protected Domain domain;
	protected List<Field> xAxisFields = new ArrayList<>();
	protected Field yName;
	protected String detailPrefix = "";
	protected Set<Domain> tranlateDomains = new TreeSet<>();
	protected Set<Domain> parentTranlateDomains = new TreeSet<>();
	protected JavascriptMethod listOption;
	protected JavascriptMethod listSumOption;
	
	@Override
	public StatementList generateWidgetStatements() {
		List<Writeable> sList = new ArrayList<>();
		if ("english".equalsIgnoreCase(this.domain.getLanguage())) {
			sList.add(new Statement(66000L,0,"<div data-options=\"region:'center',title:'Diagram'\">"));
			sList.add(new Statement(67000L,0,"<input class=\"easyui-combobox\" id=\"chartstype\" name=\"chartstype\" value='line' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
			sList.add(new Statement(68000L,0,"{value: 'line',label: 'Line'},"));
			sList.add(new Statement(69000L,0,"{value: 'bar',label: 'Bar'},"));
			sList.add(new Statement(70000L,0,"{value: 'pie',label: 'Pie'},]\"/>"));
			sList.add(new Statement(71000L,0,"<input class=\"easyui-combobox\" id=\"reporttype\" name=\"reporttype\" value='original' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
			sList.add(new Statement(72000L,0,"{value: 'original',label: 'Original'},"));
			sList.add(new Statement(73000L,0,"{value: 'sum',label: 'Sum'}]\"/>"));
		}else {
			sList.add(new Statement(66000L,0,"<div data-options=\"region:'center',title:'图表'\">"));
			sList.add(new Statement(67000L,0,"<input class=\"easyui-combobox\" id=\"chartstype\" name=\"chartstype\" value='line' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
			sList.add(new Statement(68000L,0,"{value: 'line',label: '折线图'},"));
			sList.add(new Statement(69000L,0,"{value: 'bar',label: '柱状图'},"));
			sList.add(new Statement(70000L,0,"{value: 'pie',label: '饼图'},]\"/>"));
			sList.add(new Statement(71000L,0,"<input class=\"easyui-combobox\" id=\"reporttype\" name=\"reporttype\" value='original' onSelect=\"setup()\" data-options=\"valueField: 'value',textField: 'label', data: ["));
			sList.add(new Statement(72000L,0,"{value: 'original',label: '原始数据'},"));
			sList.add(new Statement(73000L,0,"{value: 'sum',label: '累加数据'}]\"/>"));
		}
		sList.add(new Statement(74000L,0,"<div id=\"main\" style=\"width: 1000px;height:600px;\"></div>"));
		sList.add(new Statement(75000L,0,"</div>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateWidgetScriptStatements() throws Exception {
		List<Domain> translateDomains = new ArrayList<Domain>();
		if (this.yName instanceof Dropdown){
			Dropdown dp = (Dropdown) this.yName;
			Domain target = dp.getTarget();
			if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
				translateDomains.add(target);
			}
		}
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,0,"var myChart = echarts.init($('#main').get(0));"));
		serial += 2000L;
		for (Domain d:translateDomains) {
			sList.add(new Statement(serial,0,"var translate"+d.getCapFirstPlural()+" = [];"));
			serial += 1000L;
		}
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,0,"function setup(){"));
		sList.add(new Statement(serial+4000L,1,"var chartstype = $(\"#chartstype\").combobox(\"getValue\");"));
		sList.add(new Statement(serial+5000L,1,"var reporttype = $(\"#reporttype\").combobox(\"getValue\");"));
		sList.add(new Statement(serial+6000L,1,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(serial+7000L,2,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+8000L,1,"}else{"));
		sList.add(new Statement(serial+9000L,2,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+10000L,1,"}"));
		sList.add(new Statement(serial+11000L,0,"}"));
		sList.add(new Statement(serial+12000L,0,"$(document).ready(function(){"));
		
		serial += 13000L;
		for (Domain d:translateDomains) {
			sList.add(new Statement(serial,0,"translate"+d.getCapFirstPlural()+" = translateListActive"+d.getCapFirstPlural()+"();"));
			serial += 1000L;
		}
		
		sList.add(new Statement(serial+13000L,1,"setup();"));
		sList.add(new Statement(serial+14000L,0,"});"));
		sList.add(new Statement(serial+15000L,0,""));
		sList.add(new Statement(serial+16000L,0,"$(function () {"));
		sList.add(new Statement(serial+17000L,1,"$('#chartstype').combobox({"));
		sList.add(new Statement(serial+18000L,2,"onSelect: function(record){"));
		sList.add(new Statement(serial+19000L,3,"var chartstype = record.value;"));
		sList.add(new Statement(serial+20000L,3,"var reporttype = $(\"#reporttype\").combobox(\"getValue\");"));
		sList.add(new Statement(serial+21000L,3,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(serial+22000L,4,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+23000L,3,"}else{"));
		sList.add(new Statement(serial+24000L,4,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+25000L,3,"}"));
		sList.add(new Statement(serial+26000L,2,"}"));
		sList.add(new Statement(serial+27000L,1,"});"));
		sList.add(new Statement(serial+28000L,1,"$('#reporttype').combobox({"));
		sList.add(new Statement(serial+29000L,2,"onSelect: function(record){"));
		sList.add(new Statement(serial+30000L,3,"var chartstype = $(\"#chartstype\").combobox(\"getValue\");"));
		sList.add(new Statement(serial+31000L,3,"var reporttype = record.value;"));
		sList.add(new Statement(serial+32000L,3,"if (reporttype == \"sum\"){"));
		sList.add(new Statement(serial+33000L,4,"myChart.setOption("+this.listSumOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+34000L,3,"}else{"));
		sList.add(new Statement(serial+35000L,4,"myChart.setOption("+this.listOption.getLowerFirstMethodName()+"(chartstype));"));
		sList.add(new Statement(serial+36000L,3,"}"));
		sList.add(new Statement(serial+37000L,2,"}"));
		sList.add(new Statement(serial+38000L,1,"});"));
		sList.add(new Statement(serial+39000L,0,"});"));
		sList.add(new Statement(serial+40000L,0,""));
		sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+41000L));
		
		ListActiveOption listActiveOption = new ListActiveOption(this.domain,this.xAxisFields,this.yName);
		ListActiveSumOption listActiveSumOption = new ListActiveSumOption(this.domain,this.xAxisFields,this.yName);
		
		sList.add(listActiveOption.generateEasyUIJSActionMethod().generateMethodStatementList(serial+42000));
		sList.add(listActiveSumOption.generateEasyUIJSActionMethod().generateMethodStatementList(serial+43000L));
		
		serial += 44000L;
		for (Domain d:translateDomains) {
			Dropdown dp = new Dropdown();
			dp.setTarget(d);
			sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
			sList.add(dp.generateTranslateListActiveMethod().generateMethodStatementList(serial+500L));
			serial+=1000L;
		}
		this.tranlateDomains.addAll(translateDomains);
		return WriteableUtil.merge(sList);
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDetailPrefix() {
		return detailPrefix;
	}

	public void setDetailPrefix(String detailPrefix) {
		this.detailPrefix = detailPrefix;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public Set<Domain> getTranlateDomains() {
		return tranlateDomains;
	}

	public void setTranlateDomains(Set<Domain> tranlateDomains) {
		this.tranlateDomains = tranlateDomains;
	}

	public Set<Domain> getParentTranlateDomains() {
		return parentTranlateDomains;
	}

	public void setParentTranlateDomains(Set<Domain> parentTranlateDomains) {
		this.parentTranlateDomains = parentTranlateDomains;
	}

	public JavascriptMethod getListOption() {
		return listOption;
	}

	public void setListOption(JavascriptMethod listOption) {
		this.listOption = listOption;
	}

	public JavascriptMethod getListSumOption() {
		return listSumOption;
	}

	public void setListSumOption(JavascriptMethod listSumOption) {
		this.listSumOption = listSumOption;
	}

}
