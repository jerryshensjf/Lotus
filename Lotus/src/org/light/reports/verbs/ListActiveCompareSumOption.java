package org.light.reports.verbs;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.easyui.EasyUIPositions;
import org.light.utils.FieldUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListActiveCompareSumOption extends Verb implements EasyUIPositions{
	protected Domain planDomain;
	protected Domain actionDomain;
	protected List<Field> planxAxisFields = new ArrayList<>();
	protected Field planyName;
	protected List<Field> actionxAxisFields = new ArrayList<>();
	protected Field actionyName;
	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;
	}
	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setStandardName("listActiveCompare"+this.planDomain.getCapFirstPlural()+this.actionDomain.getCapFirstPlural()+"SumOption");
		method.addSignature(new Signature(1,"chartstype","var"));
		List<Writeable> sList = new ArrayList<Writeable>();
		
		sList.add(new Statement(1000L,0,"var option = {};"));
		sList.add(new Statement(2000L,1,"option.title={ text: ''};"));
		sList.add(new Statement(3000L,1,"option.tooltip={};"));
		sList.add(new Statement(4000L,1,"option.xAxis={data: ["+FieldUtil.genFieldArrayTextQStr(this.planxAxisFields)+"]};"));
		sList.add(new Statement(5000L,1,"option.yAxis={};"));
		sList.add(new Statement(6000L,1,"var planData = [];"));
		sList.add(new Statement(7000L,1,"var legendData = [];"));
		sList.add(new Statement(8000L,1,"$.ajax({"));
		sList.add(new Statement(9000L,2,"type: \"post\","));
		sList.add(new Statement(10000L,2,"url: \"../"+this.planDomain.getControllerPackagePrefix()+this.planDomain.getLowerFirstDomainName()+this.planDomain.getControllerNamingSuffix()+"/listActive"+this.planDomain.getCapFirstPlural()+"\","));
		sList.add(new Statement(11000L,2,"dataType: 'json',"));
		sList.add(new Statement(12000L,2,"async:false,"));
		sList.add(new Statement(13000L,2,"contentType:\"application/json;charset=UTF-8\","));
		sList.add(new Statement(14000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(15000L,3,"if (data.success) {"));
		sList.add(new Statement(16000L,4,"$.each(data.rows, function (i, item) {"));
		sList.add(new Statement(17000L,5,"var itemdata = {};"));
		if (this.planyName instanceof Dropdown) {
			sList.add(new Statement(18000L,5,"itemdata.name = translate"+((Dropdown)this.planyName).getTarget().getCapFirstDomainName()+"(item."+this.planyName.getLowerFirstFieldName()+");"));
		}else {
			sList.add(new Statement(18000L,5,"itemdata.name = item."+this.planyName.getLowerFirstFieldName()+";"));
		}
		sList.add(new Statement(19000L,5,""));
		sList.add(new Statement(20000L,5,"legendData.push(itemdata.name);"));
		sList.add(new Statement(21000L,5,"itemdata.type = chartstype;"));
		sList.add(new Statement(22000L,5,"var itemdatadata = ["+FieldUtil.genFieldArrayLowerFirstNameWithPrefixStr(this.planxAxisFields, "item.")+"]"));
		sList.add(new Statement(25000L,5,"var itemdatasumdata = [];"));
		sList.add(new Statement(26000L,5,"for (var i=0;i<itemdatadata.length;i++){"));
		sList.add(new Statement(27000L,6,"var sum = 0;"));
		sList.add(new Statement(28000L,6,"for (var j=0;j<=i;j++){"));
		sList.add(new Statement(29000L,7,"sum += itemdatadata[j]"));
		sList.add(new Statement(30000L,6,"}"));
		sList.add(new Statement(31000L,6,"itemdatasumdata.push(sum);"));
		sList.add(new Statement(32000L,5,"}"));
		sList.add(new Statement(33000L,5,"itemdata.data = itemdatasumdata;"));
		sList.add(new Statement(34000L,5,"planData.push(itemdata);"));
		sList.add(new Statement(35000L,3,"});"));
		sList.add(new Statement(36000L,3,"}"));
		sList.add(new Statement(37000L,3,"},"));
		sList.add(new Statement(38000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
		sList.add(new Statement(39000L,2,"},"));
		sList.add(new Statement(40000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sList.add(new Statement(41000L,3,"alert(\"Error:\"+textStatus);"));
		sList.add(new Statement(42000L,3,"alert(errorThrown.toString());"));
		sList.add(new Statement(43000L,2,"}"));
		sList.add(new Statement(44000L,1,"});"));
		sList.add(new Statement(45000L,1,""));
		sList.add(new Statement(46000L,1,"$.ajax({"));
		sList.add(new Statement(47000L,2,"type: \"post\","));
		sList.add(new Statement(48000L,2,"url: \"../"+this.actionDomain.getControllerPackagePrefix()+this.actionDomain.getLowerFirstDomainName()+this.actionDomain.getControllerNamingSuffix()+"/listActive"+this.actionDomain.getCapFirstPlural()+"\","));
		sList.add(new Statement(49000L,2,"dataType: 'json',"));
		sList.add(new Statement(50000L,2,"async:false,"));
		sList.add(new Statement(51000L,2,"contentType:\"application/json;charset=UTF-8\","));
		sList.add(new Statement(52000L,2,"success: function(data, textStatus) {"));
		sList.add(new Statement(53000L,3,"if (data.success) {"));
		sList.add(new Statement(54000L,4,"$.each(data.rows, function (i, item) {"));
		sList.add(new Statement(55000L,5,"var itemdata = {};"));
		if (this.actionyName instanceof Dropdown) {
			sList.add(new Statement(56000L,5,"itemdata.name = translate"+((Dropdown)this.actionyName).getTarget().getCapFirstDomainName()+"(item."+this.actionyName.getLowerFirstFieldName()+");"));
		}else {
			sList.add(new Statement(56000L,5,"itemdata.name = item."+this.actionyName.getLowerFirstFieldName()+";"));
		}
		sList.add(new Statement(57000L,5,""));
		sList.add(new Statement(58000L,5,"legendData.push(itemdata.name);"));
		sList.add(new Statement(59000L,5,"itemdata.type = chartstype;"));
		sList.add(new Statement(60000L,5,"var itemdatadata = ["+FieldUtil.genFieldArrayLowerFirstNameWithPrefixStr(this.actionxAxisFields, "item.")+"]"));
		sList.add(new Statement(63000L,5,"var itemdatasumdata = [];"));
		sList.add(new Statement(64000L,5,"for (var i=0;i<itemdatadata.length;i++){"));
		sList.add(new Statement(65000L,6,"var sum = 0;"));
		sList.add(new Statement(66000L,6,"for (var j=0;j<=i;j++){"));
		sList.add(new Statement(67000L,7,"sum += itemdatadata[j]"));
		sList.add(new Statement(68000L,6,"}"));
		sList.add(new Statement(69000L,6,"itemdatasumdata.push(sum);"));
		sList.add(new Statement(70000L,5,"}"));
		sList.add(new Statement(71000L,5,"itemdata.data = itemdatasumdata;"));
		sList.add(new Statement(72000L,5,"planData.push(itemdata);"));
		sList.add(new Statement(73000L,3,"});"));
		sList.add(new Statement(74000L,3,"}"));
		sList.add(new Statement(75000L,3,"},"));
		sList.add(new Statement(76000L,2,"complete : function(XMLHttpRequest, textStatus) {"));
		sList.add(new Statement(77000L,2,"},"));
		sList.add(new Statement(78000L,2,"error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sList.add(new Statement(79000L,3,"alert(\"Error:\"+textStatus);"));
		sList.add(new Statement(80000L,3,"alert(errorThrown.toString());"));
		sList.add(new Statement(81000L,2,"}"));
		sList.add(new Statement(82000L,1,"});"));
		sList.add(new Statement(83000L,1,""));
		sList.add(new Statement(84000L,1,"option.legend = {},"));
		sList.add(new Statement(85000L,1,"option.legend.data = legendData;"));
		sList.add(new Statement(86000L,1,"option.series = planData;"));
		sList.add(new Statement(87000L,1,"debugger;"));
		sList.add(new Statement(88000L,1,"return option;"));

		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
	public ListActiveCompareSumOption(Domain planDomain, List<Field> planxAxisFields, Field planyName,Domain actionDomain, List<Field> actionxAxisFields, Field actionyName) {
		super();
		this.setVerbName("ListActiveCompareOption");
		this.setLabel("比较活跃记录图表");
		this.planDomain = planDomain;
		this.planxAxisFields = planxAxisFields;
		this.planyName = planyName;
		this.actionDomain = actionDomain;
		this.actionxAxisFields = actionxAxisFields;
		this.actionyName = actionyName;
	}

	public Domain getPlanDomain() {
		return planDomain;
	}

	public void setPlanDomain(Domain planDomain) {
		this.planDomain = planDomain;
	}

	public Domain getActionDomain() {
		return actionDomain;
	}

	public void setActionDomain(Domain actionDomain) {
		this.actionDomain = actionDomain;
	}

	public List<Field> getPlanxAxisFields() {
		return planxAxisFields;
	}

	public void setPlanxAxisFields(List<Field> planxAxisFields) {
		this.planxAxisFields = planxAxisFields;
	}

	public Field getPlanyName() {
		return planyName;
	}

	public void setPlanyName(Field planyName) {
		this.planyName = planyName;
	}

	public List<Field> getActionxAxisFields() {
		return actionxAxisFields;
	}

	public void setActionxAxisFields(List<Field> actionxAxisFields) {
		this.actionxAxisFields = actionxAxisFields;
	}

	public Field getActionyName() {
		return actionyName;
	}

	public void setActionyName(Field actionyName) {
		this.actionyName = actionyName;
	}
	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
