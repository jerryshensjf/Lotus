package org.light.reports;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.easyuilayouts.EasyUILayout;
import org.light.reports.verbs.ListActiveOption;
import org.light.reports.verbs.ListActiveSumOption;
import org.light.utils.WriteableUtil;
import org.light.verb.CheckAccess;

public class EchartsReportLayout extends EasyUILayout{
	protected Domain reportDomain;
	protected List<Field> xAxisFields;
	protected Field yName;
	protected EchartsDiagram diagram;

	@Override
	public StatementList generateLayoutStatements() throws Exception{		
		StatementList sl = diagram.generateWidgetStatements();
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws Exception{
		List<Writeable> sList = new ArrayList<>();
		
		sList.add(new Statement(1000L,0,"$(document).ready(function(){"));
		sList.add(new Statement(2000L,0,"checkAccess"+this.reportDomain.getCapFirstDomainName()+"();"));
		sList.add(new Statement(30000L,0,"});"));
		
		StatementList sl =  diagram.generateWidgetScriptStatements();
		sl.setSerial(4000L);
		sList.add(sl);
		JavascriptMethod slCheckAccessJm =  ((EasyUIPositions)new CheckAccess(this.reportDomain)).generateEasyUIJSActionMethod();
		if(	slCheckAccessJm != null) {
			StatementList slCheckAccessAction = slCheckAccessJm.generateMethodStatementListIncludingContainer(5000L,0);
			sList.add(slCheckAccessAction);
		}
		return WriteableUtil.merge(sList);
	}

	@Override
	public boolean parse() throws Exception{
		if (this.diagram == null) {
			ListActiveOption lao = new ListActiveOption(this.reportDomain,this.xAxisFields,this.yName);
			ListActiveSumOption laso = new ListActiveSumOption(this.reportDomain,this.xAxisFields,this.yName);
			
			this.diagram = new EchartsDiagram();
			this.diagram.setListOption(lao.generateEasyUIJSActionMethod());
			this.diagram.setListSumOption(laso.generateEasyUIJSActionMethod());
			
			this.diagram.setDomain(this.reportDomain);
			this.diagram.setxAxisFields(this.xAxisFields);
			this.diagram.setyName(this.yName);
		}
		return true;
	}

	public Domain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(Domain reportDomain) {
		this.reportDomain = reportDomain;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public EchartsDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(EchartsDiagram diagram) {
		this.diagram = diagram;
	}

	
}
