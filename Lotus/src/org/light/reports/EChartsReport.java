package org.light.reports;

import java.util.List;

import org.light.core.ReportComb;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EChartsReport extends ReportComb{
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EchartsReportLayout eLayout = new EchartsReportLayout();
	protected Domain reportDomain;
	protected List<Field> xAxisFields;
	protected Field yName;
	
	public EChartsReport() throws Exception{
		super();
	}
	
	public EChartsReport(Domain reportDomain,List<Field> xAxisFields,Field yName) throws Exception{
		super();
		this.standardName = "EchartsReport"+reportDomain.getCapFirstDomainName()+yName.getCapFirstFieldName();
		this.label = reportDomain.getText()+ yName.getText() +"Echarts图表";
		if ("english".equalsIgnoreCase(reportDomain.getLanguage())) {
			this.label ="Echarts report for"+reportDomain.getText();
		}
		this.reportDomain = reportDomain;
		this.xAxisFields = xAxisFields;
		this.yName = yName;
		
		this.domains.add(this.reportDomain);
		
		eLayout.setReportDomain(this.reportDomain);
		eLayout.setxAxisFields(this.xAxisFields);
		eLayout.setyName(this.yName);
		eLayout.parse();
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,0,"<script type=\"text/javascript\" src=\"../echarts/echarts.min.js\"></script>"));
		frame.setAdditionScriptFiles(sl);
		frame.setMainContent(eLayout);
		frame.setStandardName(this.label);
		frame.setLanguage(this.reportDomain.getLanguage());
	}

	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "template/pages/";
		WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public Domain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(Domain reportDomain) {
		this.reportDomain = reportDomain;
	}
	
	@Override
	public void setTitles(String title, String subTitle,String footer) {
		this.frame.setTitles(title, subTitle, footer);
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}
	
	@Override
	public String getDomainNamesStr() {
		return this.reportDomain.getStandardName();
	}

	public EasyUIFrameSet getFrame() {
		return frame;
	}

	public void setFrame(EasyUIFrameSet frame) {
		this.frame = frame;
	}

	public EchartsReportLayout geteLayout() {
		return eLayout;
	}

	public void seteLayout(EchartsReportLayout eLayout) {
		this.eLayout = eLayout;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}
	
	public String getxAxisFieldsNames() {
		if (this.xAxisFields==null||this.xAxisFields.size()==0) return "";
		StringBuilder sb = new StringBuilder();
		for (Field f:this.xAxisFields) {
			sb.append(f.getFieldName()).append(",");
		}
		String result = sb.toString();
		if (result.endsWith(",")) result = result.substring(0,result.lastIndexOf(","));
		return result;
	}
}
