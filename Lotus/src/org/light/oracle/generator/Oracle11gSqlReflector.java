package org.light.oracle.generator;
import java.util.Iterator;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Var;
import org.light.exception.ValidateException;
import org.light.utils.BooleanUtil;
import org.light.utils.DomainTokenUtil;
import org.light.utils.FieldUtil;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.TypeUtil;

public class Oracle11gSqlReflector {
	protected static Logger logger = Logger.getLogger(Oracle11gSqlReflector.class);
	public static String generateTableDefinition(Domain domain) throws Exception{
		String result = "create table " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" (";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
	        Field f = (Field)it.next();
	        String fieldName = f.getFieldName();
	        String fieldType = f.getFieldType();
	        result += changeDomainFieldtoTableColumDefinitionToken(domain, fieldName, fieldType)+ ",";
        }
		String  ptoken = generatePrimaryKeySqlToken(domain);
		if (ptoken.length() >0 ){
			result += ptoken;
		}else {
			result = result.substring(0,result.length()-1);
		}
        result += ");";
		return result;
	}
	
	public static String generateTableDefinitionWithComment(Domain domain) throws Exception{
		String result = "create table " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" (";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
	        Field f = (Field)it.next();
	        String fieldName = f.getFieldName();
	        String fieldType = f.getFieldType();
	        result += changeDomainFieldtoTableColumDefinitionToken(domain, fieldName, fieldType)+ ",";
        }
		String  ptoken = generatePrimaryKeySqlToken(domain);
		if (ptoken.length() >0 ){
			result += ptoken;
		}else {
			result = result.substring(0,result.length()-1);
		}
        result += ");\n";
        
        result += " comment on table "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)  +" is  '"+domain.getText()+"';\n";
        for (Field f:domain.getFields()) {
        	result += " comment  on  column  "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +"."+StringUtil.changeDomainFieldtoTableColum(f.getLowerFirstFieldName()) +" is '"+f.getText()+"';\n";
        }
		return result;
	}
	
	public static String generateDropTableStatement(Domain domain) throws Exception{
		String result = "DROP TABLE "+domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)+";";
		return result;
	}
	
	public static String generateLinkTableDefinition(Domain master, Domain slave) throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "create table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+slave.getStandardName()) +" (";
			result += StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+"Id") +" "+lookupOracleSqlType(master.getDomainId().getFieldType()) + " not null, ";
			result += StringUtil.changeDomainFieldtoTableColum(slave.getStandardName()+"Id") +" "+lookupOracleSqlType(slave.getDomainId().getFieldType()) + " not null ";
	        result += ");";
			return result;
		} else {
			String result = "create table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+slave.getAlias()) +" (";
			result += StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+"Id") +" "+lookupOracleSqlType(master.getDomainId().getFieldType()) + " not null, ";
			result += StringUtil.changeDomainFieldtoTableColum(slave.getAlias()+"Id") +" "+lookupOracleSqlType(slave.getDomainId().getFieldType()) + " not null ";
	        result += ");";
			return result;
		}
	}
	
	public static String generateDropLinkTableSql(Domain master, Domain slave) throws Exception{
		if (StringUtil.isBlank(slave.getAlias())){
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+slave.getStandardName()) +";\n";
			return result;
		}else {
			String result = "drop table " + master.getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(master.getStandardName()+slave.getAlias()) +";\n";
			return result;
		}
	}

	public static String generateInsertSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "insert into " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" ";
		Iterator it = domain.getFieldsWithoutId().iterator();
		
		result += "( ";
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
	        result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ ",";
        }
        result = result.substring(0,result.length()-1);
        result += ") values (";
		Iterator it2 = domain.getFieldsWithoutId().iterator();
        while (it2.hasNext()) {	
        	Field f = (Field)it2.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        result += "?,";
        }
        result = result.substring(0,result.length()-1);
        result += ")";
        return result;
	}
	
	public static String generateUpdateSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" set ";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getOracleFieldType();
	        if (!isPrimaryKey(domain, fieldName, fieldType)) {
	        	result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ " = ? ,";
	        }
        }
        result = result.substring(0,result.length()-1);
        result += generatePrimaryWhereParamSqlToken(domain);
        return result;
	}
	
	public static String generateDeleteSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "delete from " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " ";
        result += generatePrimaryWhereParamSqlToken(domain);
        return result;
	}
	
	public static String generateSoftDeleteSqlWithQuestionMark(Domain domain) throws Exception{
		String result = "update " +domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) + " set " + domain.getActive().getFieldName() + " = false where ";
		result += StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName())+ " = ? ";
        return result;
	}
	
	public static String changeDomainFieldtoTableColumDefinitionToken(Domain domain, String fieldName, String fieldType){
		String result = StringUtil.changeDomainFieldtoTableColum(fieldName) + " ";
		result += lookupOracleSqlType(fieldName,fieldType,domain) + " ";
		if (isPrimaryKey(domain,fieldName,fieldType)){
			result += "not null ";
		}else if (isDoaminIdOrActiveField(domain,fieldName,fieldType)){
			result += "not null ";
		}else {
			result += "null ";
		}
		return result;
	}
	
	public static boolean isPrimaryKey(Domain domain,String fieldName,String fieldType){
		boolean retVal = false;
		if (!domain.hasDomainId()) return false;
		else if ("int64".equalsIgnoreCase(fieldType)||"int".equalsIgnoreCase(fieldType)||"int32".equalsIgnoreCase(fieldType)||"string".equalsIgnoreCase(fieldType)||"varchar2(32)".equalsIgnoreCase(fieldType)||"varchar2(255)".equalsIgnoreCase(fieldType)){
			if ("id".equalsIgnoreCase(fieldName)||fieldName.equalsIgnoreCase(domain.getStandardName()+"id")||fieldName.equalsIgnoreCase(domain.getDomainId().getFieldName())){
				retVal =true;
			}
		}
		return retVal;
	}
	
	public static boolean isDoaminIdOrActiveField(Domain domain,String fieldName,String fieldType){
		if ((domain.hasActiveField()&&fieldType.equalsIgnoreCase("Integer")&&fieldName.equals(domain.getActive().getFieldName()))||(domain.hasDomainName()&&fieldType.equalsIgnoreCase("string")&&fieldName.equals(domain.getDomainName().getFieldName()))){
			return true;
		}
		return false;
	}
	
	public static String generatePrimaryKeySqlToken(Domain domain){
		String result = "";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        if (isPrimaryKey(domain, fieldName, fieldType)){
	        	result =  "primary key (" + StringUtil.changeDomainFieldtoTableColum(fieldName) +")";
	        }
        }
        return result;
	}
	
	public static String generatePrimaryWhereParamSqlToken(Domain domain){
		String result = "";
		Iterator it = domain.getFields().iterator();
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
	        if (isPrimaryKey(domain, fieldName, fieldType)){
	        	result = "where " +  StringUtil.changeDomainFieldtoTableColum(fieldName) +" = ?";
	        }
        }
        return result;
	}
	
	public static String lookupOracleSqlType(String fieldName, String fieldType,Domain domain){
		String result = fieldType;
		if (fieldType.equalsIgnoreCase("i64")) {
			result = "varchar2(32)";
		}
		if  (fieldType.equalsIgnoreCase("i32")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("f64")||fieldType.equalsIgnoreCase("f32")) {
			result = "number(20,4)";
		}
		if  (fieldType.equalsIgnoreCase("decimal")) {
			result = "number(20,4)";
		}
		if  (fieldType.equalsIgnoreCase("bool")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("image")) {
			result = "blob";
		}
		if  (fieldType.equalsIgnoreCase("datetime")||fieldType.equalsIgnoreCase("date")) {
			result = "date";
		}
		if  (fieldType.equalsIgnoreCase("string")) {
			if (fieldName.toLowerCase().contains("comment")||fieldName.toLowerCase().contains("description")||fieldName.toLowerCase().contains("content")){
				result = "varchar2(2000)";
			}else if (domain.hasDomainId()&&fieldName.equals(domain.getDomainId().getFieldName())) {
				result = "varchar2(32)";
			}else {
				result = "varchar2(255)";
			}
		}
		return result; 
	}
	
	public static String lookupOracleSqlType(String fieldName, String fieldType){
		String result = fieldType;
		if (fieldType.equalsIgnoreCase("i64")) {
			result = "varchar2(32)";
		}
		if  (fieldType.equalsIgnoreCase("i32")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("f32")||fieldType.equalsIgnoreCase("f64")) {
			result = "number(10,4)";
		}
		if  (fieldType.equalsIgnoreCase("BigDecimal")||fieldType.equalsIgnoreCase("decimal")) {
			result = "number(10,4)";
		}
		if  (fieldType.equalsIgnoreCase("bool")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("datetime")) {
			result = "datetime";
		}
		if  (fieldType.equalsIgnoreCase("date")) {
			result = "date";
		}
		if  (fieldType.equalsIgnoreCase("String")) {
			if (fieldName.toLowerCase().contains("comment")||fieldName.toLowerCase().contains("description")||fieldName.toLowerCase().contains("content")){
				result = "varchar2(2000)";
			}else {
				result = "varchar2(255)";
			}
		}
		return result; 
	}
	
	public static String lookupOracleSqlType(String fieldType){
		String result = fieldType;
		if (fieldType.equalsIgnoreCase("i64")) {
			result = "varchar2(32)";
		}
		if  (fieldType.equalsIgnoreCase("i32")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("f32")||fieldType.equalsIgnoreCase("f64")) {
			result = "number(10,4)";
		}
		if  (fieldType.equalsIgnoreCase("bool")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("String")) {
			result = "varchar2(255)";
		}
		return result; 
	}
	
	public static String lookupSqlType(String fieldType){
		String result = "";
		if (fieldType.equalsIgnoreCase("i64")) {
			result = "BigInt";
		}
		if  (fieldType.equalsIgnoreCase("i32")) {
			result = "Integer";
		}
		if  (fieldType.equalsIgnoreCase("f32")||fieldType.equalsIgnoreCase("f64")) {
			result = "Double";
		}
		if  (fieldType.equalsIgnoreCase("bool")) {
			result = "bool";
		}
		if  (fieldType.equalsIgnoreCase("String")) {
			result = "varchar2(255)";
		}
		return result; 
	}
	
	public static String generateSelectAllStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain);
        return result;
	}
	
	public static String generateSelectAllByPageStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" limit ?,?;";
        return result;
	}
	
	public static String generateCountRecordStatement(Domain domain, Var countNum) throws Exception{
		String result = "select count("+ StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName()) + ") as "+countNum.getVarName()+" from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain);
        return result;
	}
	
	public static String generateSelectByFieldStatement(Domain domain, Field field) throws Exception{
		String result = "select "+  DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" where "+ StringUtil.changeDomainFieldtoTableColum(field.getFieldName())+" = ?";
        return result;
	}
	
	public static String generateSelectActiveStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getActive().getFieldName())+" = 1";
        return result;
	}
	
	public static String generateFindByIdStatement(Domain domain) throws Exception{
		String result = "select "+DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName())+" = ?";
        return result;
	}
	
	public static String generateFindByNameStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainName().getFieldName())+" = ?";
        return result;
	}
	
	public static String generateSearchByNameStatement(Domain domain) throws Exception{
		String result = "select "+ DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(domain.getDomainName().getFieldName())+" like ?";
        return result;
	}
	
	public static String generateSearchByDescriptionStatement(Domain domain, Field description) throws Exception{
		String result = "select " +DomainTokenUtil.generateTableCommaFields(domain) + " from "+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain) +" where "+StringUtil.changeDomainFieldtoTableColum(description.getFieldName())+" like %?%";
        return result;
	}
	
	public static String generateInsertSqlWithValue(Domain domain) throws Exception{
		String result = "insert into " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" ";
		Iterator it = domain.getFields().iterator();
		
		result += "( ";
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
 	        if (f instanceof Dropdown) result += StringUtil.changeDomainFieldtoTableColum(((Dropdown)f).getAliasName())+ ",";
 	        else result += StringUtil.changeDomainFieldtoTableColum(fieldName)+ ",";
        }
        result = result.substring(0,result.length()-1);
        result += ") values (";
		Iterator it2 = domain.getFields().iterator();
        while (it2.hasNext()) {	
        	Field f = (Field)it2.next();
 	        String fieldValue = f.getFieldValue();
 	        if (StringUtil.isBlank(fieldValue)&&f instanceof Dropdown) {
 	        	fieldValue = "-1";
 	        }else if (StringUtil.isBlank(fieldValue)&&f.getFieldType().equalsIgnoreCase("String")) {
 	        	fieldValue = "";
 	        }else if (StringUtil.isBlank(fieldValue)&&FieldUtil.isNumeric(f)) {
 	        	fieldValue = "0";
 	        }
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
 	        try {
 	        	if (!StringUtil.isBlank(fieldValue)&&NumberUtils.isNumber(fieldValue)){
	 	        	if (Double.valueOf(fieldValue) - Math.round(Double.valueOf(fieldValue)) < 0.00005){
	 	        		fieldValue = "" + Math.round(Double.valueOf(fieldValue));
	 	        		logger.debug("JerryDebug:roundtoint:"+fieldValue);
	 	        	}
	 	        }else if(StringUtil.isBoolean(fieldValue)){
	 	        	fieldValue = ""+BooleanUtil.parseBooleanInt(fieldValue);
	 	        }
 	        } catch (Exception e){
 	        	e.printStackTrace();
 	        	throw new ValidateException("数据项"+fieldName+"类型定义错误！");
 	        }
 	        
 	       if (StringUtil.isBlank(fieldValue)&&fieldType.equalsIgnoreCase("image")) {
	        	result +=  "null,";
 	        }else if (!StringUtil.isBlank(fieldType)&&(fieldType.equalsIgnoreCase("i32")||fieldType.equalsIgnoreCase("f32")||fieldType.equalsIgnoreCase("f64")||fieldType.equalsIgnoreCase("bool"))) {
 	        	if (StringUtil.isBlank(fieldValue)) result += "0,";
 	        	else result +=  fieldValue + ",";
 	        } else if("date".equalsIgnoreCase(fieldType)){
 	        	if (StringUtil.isBlank(fieldValue)||"None".equalsIgnoreCase(fieldValue)) {
 	        		result += "null,";
 	        	}else {
 	        		result += "to_date('" + fieldValue + "','yyyy-MM-dd'),";
 	        	}
 	        } else if("datetime".equalsIgnoreCase(fieldType)){
 	        	if (StringUtil.isBlank(fieldValue)||"None".equalsIgnoreCase(fieldValue)) {
 	        		result += "null,";
 	        	}else {
 	        		result += "to_date('" + fieldValue + "','yyyy-MM-dd HH24:mi:ss'),";
 	        	}
 	        } else if (f instanceof Dropdown) {
 	        	result += ""+ fieldValue + ",";
 	        }else {
 	        	result += "'"+ fieldValue + "',";
 	        }
        }
        result = result.substring(0,result.length()-1);
        result += ");";
        return result;
	}
	
	public static String generateUpdateSqlWithValue(Domain domain) throws Exception{
		String result = "update " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" ";
		Iterator it = domain.getFieldsWithoutId().iterator();		
		result += " set  ";
		
        while (it.hasNext()) {	
        	Field f = (Field)it.next();
 	        String fieldName = f.getFieldName();
 	        String fieldType = f.getFieldType();
 			String fieldValue = f.getFieldValue();
 	        if (f instanceof Dropdown) result += StringUtil.changeDomainFieldtoTableColum(((Dropdown)f).getAliasName());
 	        else result += StringUtil.changeDomainFieldtoTableColum(fieldName);
 	        result += " = "; 
 	       if ((f instanceof Dropdown && StringUtil.isBlank(fieldValue))|| "null".equals(fieldValue)) {
	        	result = result + " -1 " ;
	        } else if (!StringUtil.isBlank(fieldType)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double"))) {
 	    	   if (StringUtil.isBlank(fieldValue)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double"))) result += "0,";
 	    	   else result = result + fieldValue ;
 	        } else if(StringUtil.isBoolean(fieldValue)){
 	        	result =  result + BooleanUtil.parseBooleanInt(fieldValue);
 	        }else {
 	        	result = result + "'"+ fieldValue + "'";
 	        }
 			result +=",";
        }

        result = result.substring(0,result.length()-1);
        
		result = result + " where " +domain.getDomainId().getTableColumnName()+ " = ";
		String fieldType = domain.getDomainId().getFieldType();
		String fieldValue = domain.getDomainId().getFieldValue();
		if (!StringUtil.isBlank(fieldType)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double")||fieldType.equalsIgnoreCase("boolean"))) {
	    	   if (StringUtil.isBlank(fieldValue)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double"))) result += "0,";
	    	   else result +=  fieldValue ;
	        } else {
	        	result += "'"+ fieldValue + "'";
	        }
				
        result += ";";
        return result;
	}
	
	public static String generateDeleteSqlWithValue(Domain domain) throws Exception{
		String result = "delete from " + domain.getDbPrefix() +TableStringUtil.domainNametoTableName(domain) +" where ";
		result += domain.getDomainId().getTableColumnName()+ " = ";
		String fieldType = domain.getDomainId().getFieldType();
		String fieldValue = domain.getDomainId().getFieldValue();
		if (!StringUtil.isBlank(fieldType)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double")||fieldType.equalsIgnoreCase("boolean"))) {
	    	   if (StringUtil.isBlank(fieldValue)&&(fieldType.equalsIgnoreCase("int")||fieldType.equalsIgnoreCase("Integer")||fieldType.equalsIgnoreCase("long")||fieldType.equalsIgnoreCase("float")||fieldType.equalsIgnoreCase("double"))) result += "0,";
	    	   else result +=  fieldValue ;
	        } else {
	        	result += "'"+ fieldValue + "'";
	        }
        result += ";";
        return result;
	}
		
	public static String generateMtmInsertSqlWithValues(ManyToMany mtm) throws Exception{
		if (StringUtil.isBlank(mtm.getValues())) return "";
		else {
		String [] slaveValues = mtm.getValues().split(",");
			String result = "";
			for (String slaveValue:slaveValues) {
			    result = result + "insert into " +  mtm.getMaster().getDbPrefix() + StringUtil.changeDomainFieldtoTableColum(mtm.getMaster().getStandardName()+mtm.getSlaveAlias()) +" ";
				result = result + "("+StringUtil.changeDomainFieldtoTableColum(mtm.getMaster().getStandardName()+"Id")+" , "+StringUtil.changeDomainFieldtoTableColum(mtm.getSlaveAlias()+"Id")+") values ( ";		
		        if (TypeUtil.isNumeric(mtm.getMaster().getDomainId().getFieldRawType())){
		        	result = result + mtm.getMaster().getDomainId().getFieldValue() + "," ;
		        }else {
		        	result = result +"'"+ mtm.getMaster().getDomainId().getFieldValue() + "',"; 
		        }
		        if (TypeUtil.isNumeric(mtm.getSlave().getDomainId().getFieldRawType())){
		        	result = result + slaveValue ;
		        }else {
		        	result = result +"'"+ slaveValue + "'"; 
		        }
		        result += ");\n";
			}
			return result;
		}
	}
}