package org.light.easyui;

import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;

public interface EasyUIPositions {
	public  JavascriptBlock generateEasyUIJSButtonBlock() throws Exception;
	public  JavascriptMethod generateEasyUIJSActionMethod() throws Exception;
}
