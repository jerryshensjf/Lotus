package org.light.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.light.core.Writeable;
import org.light.exception.ValidateException;
import org.light.utils.DeniableUtils;
import org.light.utils.PluralUtil;
import org.light.utils.StringUtil;
import org.light.utils.TypeUtil;
import org.light.utils.WriteableUtil;

import net.sf.json.JSONObject;

public class Domain extends Class implements Comparable<Domain>,Serializable{
	private static final long serialVersionUID = -9098947199465839222L;
	protected static Logger logger = Logger.getLogger(Domain.class);
	protected Long serial = 0L;
	protected Set<Method> setters = new TreeSet<Method>();
	protected Set<Method> getters = new TreeSet<Method>();
	protected String domainStr;
	protected Field domainId;
	protected Field domainName;
	protected Field active;
	protected String plural; 
	protected String dbPrefix = "";
	protected String tablePrefix = "";
	protected String label;
	protected Map<String,String> fieldLabels = new TreeMap<String,String>();
	protected Set<ManyToMany> manyToManies = new TreeSet<ManyToMany>();
	protected String alias = "";
	protected String aliasLabel = "";
	protected String aliasPlural = "";
	protected String domainSuffix="domain";
	protected String daoSuffix="dao";
	protected String daoimplSuffix="daoimpl";
	protected String serviceSuffix="service";
	protected String serviceimplSuffix="serviceimpl";
	protected String controllerSuffix="controller";
	protected String domainNamingSuffix = "";
	protected String controllerNamingSuffix = "Controller";
	protected String verbDeniesStr = "";
	protected String language = "chinese";
	protected String schema = "normal";
	protected String controllerPackage = "";
	protected String projectName = "";
	protected String dbType = "MariaDb";
	protected String technicalStack = "tower";
	protected List<Domain> dummyDb = new ArrayList<Domain>();
	protected String frontendUi = "vueElNode21";
	protected String backendUi = "easyUi";
	protected Set<Field> deniedFields = new TreeSet<>();
	
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}

	public List<Domain> getDummyDb() {
		return dummyDb;
	}

	public void setDummyDb(List<Domain> dummyDb) {
		this.dummyDb = dummyDb;
	}
	
	public String getTechnicalStack() {
		return technicalStack;
	}

	public void setTechnicalStack(String technicalStack) {
		this.technicalStack = technicalStack;
	}

	public Field findFieldByFixedName(String fixedName) {
		for (Field f: this.getFields()) {
			if (f.getFixedName().equals(fixedName)) {
				return f;
			}
		}
		return null;
	}

	public void setAliasPlural(String aliasPlural) {
		this.aliasPlural = aliasPlural;
	}
	
	public Domain decorateCompareTo() throws ValidateException{
		if (StringUtil.isBlank(getDomainId())) {
//			ValidateInfo info = new ValidateInfo();
//			info.addCompileWarning("域对象没有设置序号！");
//			throw new ValidateException(info);
			return this;
		}
		Interface inf = new Interface();
		inf.setStandardName("Comparable<"+getCapFirstDomainNameWithSuffix()+">");
		Method method = new Method();
		method.setStandardName("compareTo");
		method.setReturnType(new Type("int"));
		Signature si = new Signature(1, StringUtil.lowerFirst(this.getStandardName()), this.getType());
		method.addSignature(si);
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2,"return this.get"+domainId.getCapFirstFieldName()+"().compareTo("+getLowerFirstDomainName()+".get"+domainId.getCapFirstFieldName()+"());"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		
		inf.addMethod(method);
		this.addClassImplement(inf);
		
		return this;
	}
	
	public long maxSerial(){
		long maxserial = 0;
		for (Field f:this.fields){
			if (f.getSerial() > maxserial) maxserial = f.getSerial();
		}
		return maxserial;
	}

	public Field getDomainId() {
		return domainId;
	}
	public void setDomainId(Field domainId) {
		this.domainId = domainId;
		if (domainId!=null) this.domainId.setSerial(this.maxSerial() + 300);
	}
	
	public void addField(String name,String type,String lengthStr){
		if ("active".equalsIgnoreCase(name)||"deleted".equalsIgnoreCase(name)||"delete".equalsIgnoreCase(name)) {
			this.active = new Field(name, "bool");
			this.active.setSerial(-100);
			return;
		}
		if (name.equalsIgnoreCase(this.getStandardName()+"Name")) {
			this.domainName = new Field(name, "string");
			this.domainName.setSerial(-200);
			return;
		}
		if (name.equalsIgnoreCase(this.getStandardName()+"Id") ||name.equalsIgnoreCase("id")){
			this.domainId = new Field(name, type);
			this.domainId.setSerial(-300);
			return;
		}
		this.fields.add(new Field(this.maxSerial()+100L,name,type,lengthStr));
	}
	
	public boolean existsSetterByName(String methodName){
		for (Method mySetm:this.setters){
			if (mySetm.getStandardName().equalsIgnoreCase(methodName)) return true;
		}
		return false;
	}
	
	public boolean existsGetterByName(String methodName){
		for (Method mySetm:this.getters){
			if (mySetm.getStandardName().equalsIgnoreCase(methodName)) return true;
		}
		return false;
	}
	
	public void generateSetterGetter(Field f){
		String name = f.getFieldName();
		Type type = f.getClassType();
		Method setter = new Method();
		setter.setStandardName("set"+StringUtil.capFirst(name));
		setter.setReturnType(new Type("void"));
		setter.addSignature(new Signature(1,name,type));
		setter.setContent("\t\tthis."+StringUtil.lowerFirst(name)+" = "+StringUtil.lowerFirst(name)+";\n");
		setter.setSerial(f.getSerial());
		if(!existsSetterByName(setter.getStandardName())) this.setters.add(setter);
		
		Method getter = new Method();
		getter.setStandardName("get"+StringUtil.capFirst(name));
		getter.setReturnType(type);
		getter.setContent("\t\treturn this."+StringUtil.lowerFirst(name)+";\n");
		getter.setSerial(f.getSerial()+50);
		if(!existsGetterByName(getter.getStandardName())) this.getters.add(getter);
	}
	
	public void addField(String name,String type,String packageToken,String lengthStr){
		this.addField(new Field(name,type,packageToken,lengthStr));
	}
	
	@Override
	public void addField(Field f){
		if (f instanceof Dropdown){
			f.setSerial(this.maxSerial()+100);
			this.fields.add(f);
			return;
		}
		if ("active".equalsIgnoreCase(f.getFieldName())||"deleted".equalsIgnoreCase(f.getFieldName())||"delete".equalsIgnoreCase(f.getFieldName())) {
			this.active = new Field(f.getFieldName(),f.getFixedName(), "bool");
			this.active.setFieldValue(f.getFieldValue());
			this.active.setSerial(-100);
			return;
		}
		if (f.getFieldName().equals(StringUtil.lowerFirst(this.getStandardName())+"Name")) {
			this.domainName = new Field(f.getFieldName(),f.getFixedName(), "string");
			this.domainName.setFieldValue(f.getFieldValue());
			this.domainName.setSerial(-200);
			return;
		}
		if (f.getFieldName().equals(StringUtil.lowerFirst(this.getStandardName())+"Id")) {
			this.domainId = new Field(f.getFieldName(),f.getFixedName(), f.getRawType());
			this.domainId.setFieldValue(f.getFieldValue());
			this.domainId.setSerial(-300);
			return;
		}
		Field myf = new Field(this.maxSerial()+100,f.getFieldName(),f.getFixedName(),f.getFieldType(),f.getLengthStr());
		myf.setLabel(f.getLabel());
		myf.setFieldNull(f.getFieldNull());
		myf.setFieldExtra(f.getFieldExtra());
		myf.setFieldDefault(f.getFieldDefault());
		myf.setFieldValue(f.getFieldValue());
		this.fields.add(myf);
	}

	public StatementList generateClassStatementList(){
		String dbRow = "MySqlRow";
		if ("PostgreSQL".equalsIgnoreCase(this.getDbType())||"pgsql".equalsIgnoreCase(this.getDbType())) {
			dbRow = "PgRow";
		} else if ("Oracle".equalsIgnoreCase(this.getDbType())) {
			dbRow = "Row";
		}
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(500L,0,"#![allow(unused_imports,unused_assignments)]"));
		sList.add(new Statement(1000L,0,"use serde::{Deserialize, Serialize};"));
		if ("MariaDB".equalsIgnoreCase(this.getDbType())||"MySQL".equalsIgnoreCase(this.getDbType())) {
			sList.add(new Statement(2000L,0,"use sqlx::mysql::MySqlRow;"));
			sList.add(new Statement(3000L,0,"use sqlx::{FromRow, Row};"));
			sList.add(new Statement(3050L,0,"use sqlx::Error;"));
		}else if ("PostgreSQL".equalsIgnoreCase(this.getDbType())||"pgsql".equalsIgnoreCase(this.getDbType())) {
			sList.add(new Statement(2000L,0,"use sqlx::postgres::PgRow;"));
			sList.add(new Statement(3000L,0,"use sqlx::{FromRow, Row};"));
			sList.add(new Statement(3050L,0,"use sqlx::Error;"));
		}else if ("Oracle".equalsIgnoreCase(this.getDbType())) {
			sList.add(new Statement(1000L,0,"use crate::"+this.getDaoimplSuffix()+"::db_context::FromRow;"));
			sList.add(new Statement(2000L,0,"use oracle::Row;"));
			sList.add(new Statement(3050L,0,"use oracle::Error;"));
		}
		
		if (containsImage()) {
			sList.add(new Statement(3100L,0,"use crate::"+this.domainSuffix+"::count_num::get_image_base64;"));
		}
		if (containsDateTime()) {
			sList.add(new Statement(3200L,0,"use chrono::{NaiveDate,NaiveDateTime};"));
			sList.add(new Statement(3400L,0,"use crate::"+this.domainSuffix+"::my_date_time_format;"));
			sList.add(new Statement(3600L,0,"use crate::"+this.domainSuffix+"::my_date_format;"));
		}
		sList.add(new Statement(3700L,0,"use crate::"+this.domainSuffix+"::count_num::get_i64;"));
		sList.add(new Statement(3710L,0,"use crate::"+this.domainSuffix+"::count_num::get_i32;"));
		sList.add(new Statement(3720L,0,"use crate::"+this.domainSuffix+"::count_num::get_f64;"));
		sList.add(new Statement(3730L,0,"use crate::"+this.domainSuffix+"::count_num::get_f32;"));
		sList.add(new Statement(3740L,0,"use crate::"+this.domainSuffix+"::count_num::get_bool;"));
		sList.add(new Statement(3750L,0,"use crate::"+this.domainSuffix+"::count_num::get_string;"));
		sList.add(new Statement(3760L,0,"use crate::"+this.domainSuffix+"::count_num::get_dropdown;"));
		sList.add(new Statement(4000L,0,""));
		sList.add(new Statement(5000L,0,"#[derive(Serialize, Deserialize, Clone, Debug)]"));
		sList.add(new Statement(6000L,0,"#[serde(rename_all = \"camelCase\")]"));
		sList.add(new Statement(7000L,0,"pub struct "+this.getCapFirstDomainNameWithSuffix()+" {"));
		if (this.hasDomainId()) sList.add(new Statement(8000L,1,"pub "+this.getDomainId().getSnakeFieldName()+": i64,"));
		if (this.hasDomainName()) sList.add(new Statement(9000L,1,"pub "+this.getDomainName().getSnakeFieldName()+": String,"));
		if (this.hasActiveField()) sList.add(new Statement(10000L,1,"pub "+this.getActive().getSnakeFieldName()+": bool,"));
		long serial = 11000L;
		for (Field f:this.getPlainFields()) {
			if ("Image".equalsIgnoreCase(f.getFieldType())) {
				sList.add(new Statement(serial,1,"pub "+f.getSnakeFieldName()+": String,"));
			}else {
				if (f.getFieldType().equalsIgnoreCase("datetime")) {
					sList.add(new Statement(serial,1,"#[serde(with = \"my_date_time_format\")]"));
				}else if (f.getFieldType().equalsIgnoreCase("date")) {
					sList.add(new Statement(serial,1,"#[serde(with = \"my_date_format\")]"));
				}
				sList.add(new Statement(serial+500L,1,"pub "+f.getSnakeFieldName()+": "+f.getClassType()+","));
			}
			serial += 1000L;			
		}
		sList.add(new Statement(serial,0,"}"));
		sList.add(new Statement(serial+1000L,0,""));
		
		sList.add(new Statement(serial+5000L,0,"#[derive(Serialize, Deserialize, Clone)]"));
		sList.add(new Statement(serial+6000L,0,"#[serde(rename_all = \"camelCase\")]"));
		sList.add(new Statement(serial+7000L,0,"pub struct "+this.getCapFirstDomainName()+"QueryRequest {"));
		if (this.hasDomainId()) sList.add(new Statement(serial+8000L,1,"pub "+this.getDomainId().getSnakeFieldName()+": Option<String>,"));
		if (this.hasDomainName()) sList.add(new Statement(serial+9000L,1,"pub "+this.getDomainName().getSnakeFieldName()+": Option<String>,"));
		if (this.hasActiveField()) sList.add(new Statement(serial+10000L,1,"pub "+this.getActive().getSnakeFieldName()+": Option<String>,"));
		serial += 11000L;
		for (Field f:this.getPlainFields()) {
			if (!deniedFields.contains(f)) {
				sList.add(new Statement(serial,1,"pub "+f.getSnakeFieldName()+": Option<String>,"));
				serial += 1000L;
			}
		}
		sList.add(new Statement(serial,1,"pub page:Option<i32>,"));
		sList.add(new Statement(serial+1000L,1,"pub rows:Option<i32>,"));
		sList.add(new Statement(serial+1200L,"}"));
		sList.add(new Statement(serial+1400L,0,""));
		
		if ("Oracle".equalsIgnoreCase(this.getDbType())) {
			sList.add(new Statement(serial+2000L,0,"impl<'c> FromRow<'c, "+dbRow+"> for "+this.getCapFirstDomainNameWithSuffix()+" {"));
			sList.add(new Statement(serial+3000L,1,"fn from_row(row: &"+dbRow+") -> Result<Self, Error> {"));
			
			sList.add(new Statement(serial+4000L,2,"let "+this.active.getSnakeFieldName()+"_int:i32 = row.get(2).unwrap();"));

			serial += 5000L;
			int pos0 = 3;
			for (Field f:this.getPlainFields()) {
				if ("bool".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,2,"let "+f.getSnakeFieldName()+"_int:i32 = row.get("+pos0+").unwrap();"));
					serial += 1000L;
				}else if ("Image".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial+1000L,2,"let "+f.getSnakeFieldName()+" = row.get("+pos0+");"));
					sList.add(new Statement(serial+2000L,2,"let mut "+f.getSnakeFieldName()+"_str = \"\".to_string();"));
					sList.add(new Statement(serial+3000L,2,"match "+f.getSnakeFieldName()+" {"));
					sList.add(new Statement(serial+4000L,3,"Ok(bytes) => "+f.getSnakeFieldName()+"_str = get_image_base64(bytes),"));
					sList.add(new Statement(serial+5000L,3,"Err(_) => "+f.getSnakeFieldName()+"_str = \"\".to_string(),"));
					sList.add(new Statement(serial+6000L,2,"}"));
				}
				serial += 10000L;
				pos0++;
			}
			
			sList.add(new Statement(serial+4000L,2,"Ok("+this.getCapFirstDomainNameWithSuffix()+" {"));
			sList.add(new Statement(serial+5000L,3,""+this.getDomainId().getSnakeFieldName()+": row.get(0).unwrap(),"));
			sList.add(new Statement(serial+6000L,3,""+this.getDomainName().getSnakeFieldName()+": row.get(1).unwrap(),"));
			sList.add(new Statement(serial+7000L,3,this.getActive().getSnakeFieldName()+": "+this.getActive().getSnakeFieldName()+"_int == 1 ,"));
			serial += 8000L;
			int pos = 3;
			for (Field f:this.getPlainFields()) {
				if ("Image".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":"+f.getSnakeFieldName()+"_str,"));
				} else if ("bool".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":"+f.getSnakeFieldName()+"_int == 1,"));
				} else {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":row.get("+pos+").unwrap_or_default(),"));
				}
				serial += 1000L;
				pos++;
			}
			sList.add(new Statement(serial,2,"})"));
			sList.add(new Statement(serial+1000L,1,"}"));
			sList.add(new Statement(serial+2000L,0,"}"));
			serial += 3000L;
		}else {
			sList.add(new Statement(serial+2000L,0,"impl<'c> FromRow<'c, "+dbRow+"> for "+this.getCapFirstDomainNameWithSuffix()+" {"));
			sList.add(new Statement(serial+3000L,1,"fn from_row(row: &"+dbRow+") -> Result<Self, Error> {"));
			sList.add(new Statement(serial+4000L,2,"Ok("+this.getCapFirstDomainNameWithSuffix()+" {"));
			int pos = 0;
			if (this.hasDomainId()) sList.add(new Statement(serial+5000L,3,""+this.getDomainId().getSnakeFieldName()+": row.get("+ pos++ +"),"));
			if (this.hasDomainName()) sList.add(new Statement(serial+6000L,3,""+this.getDomainName().getSnakeFieldName()+": row.get("+ pos++ +"),"));
			if (this.hasActiveField()) sList.add(new Statement(serial+7000L,3,this.getActive().getSnakeFieldName()+": row.get("+ pos++ +"),"));
			serial += 8000L;
			
			for (Field f:this.getPlainFields()) {
				if (f instanceof Dropdown) {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_dropdown(row.get("+pos+")),"));
				} else if ("Image".equalsIgnoreCase(f.getFieldType())) {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_image_base64(row.get("+pos+")),"));
				} else if ("i32".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_i32(row.get("+pos+")),"));
				} else if ("i64".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_i64(row.get("+pos+")),"));
				} else if ("f32".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_f32(row.get("+pos+")),"));
				} else if ("f64".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_f64(row.get("+pos+")),"));
				} else if ("bool".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_bool(row.get("+pos+")),"));
				} else if ("string".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":get_string(row.get("+pos+")),"));
				} else {
					sList.add(new Statement(serial,3,f.getSnakeFieldName()+":row.get("+pos+"),"));
				}
				serial += 1000L;
				pos++;
			}
			sList.add(new Statement(serial,2,"})"));
			sList.add(new Statement(serial+1000L,1,"}"));
			sList.add(new Statement(serial+2000L,0,"}"));
			
			serial += 3000L;
		}
		sList.add(new Statement(serial+2200L,0,""));
		if (this.hasDomainId()) {
			sList.add(new Statement(serial+3000L,0,"impl PartialEq for "+this.getCapFirstDomainNameWithSuffix()+" {"));
			sList.add(new Statement(serial+4000L,1,"fn eq(&self, other: &Self) -> bool {"));
			sList.add(new Statement(serial+5000L,2,"self."+this.getDomainId().getSnakeFieldName()+" == other."+this.getDomainId().getSnakeFieldName()));
			sList.add(new Statement(serial+6000L,1,"}"));
			sList.add(new Statement(serial+7000L,0,"}"));
		}
		return WriteableUtil.merge(sList);
	}
	
	public String capFirst(String value){
		return value.substring(0, 1).toUpperCase()+value.substring(1);
	}
	
	public String lowerFirst(String value){
		return value.substring(0, 1).toLowerCase()+value.substring(1);
	}
	
	public int compareTo(Domain domain) {
		String myName = this.getStandardName();
		String otherName = domain.getStandardName();
		return myName.compareTo(otherName);
	}
	
	public Set<Method> getSetters() {
		return setters;
	}
	public void setSetters(Set<Method> setters) {
		this.setters = setters;
	}
	public Set<Method> getGetters() {
		return getters;
	}
	public void setGetters(Set<Method> getters) {
		this.getters = getters;
	}
	public String getDomainStr() {
		return domainStr;
	}
	public void setDomainStr(String domainStr) {
		this.domainStr = domainStr;
	}
	public Field getDomainName() {
		return domainName;
	}
	public void setDomainName(Field domainName) {
		this.domainName = domainName;
	}
	public Field getActive() {
		return active;
	}
	public void setActive(Field active) {
		this.active = active;
	}
	public String getPlural() throws ValidateException{
		if (this.plural == null || "".equals(this.plural)){
			return PluralUtil.lookupPlural(this.standardName);
		} else {
			return StringUtil.capFirst(plural);
		}
	}
	
	public String getAliasPlural() throws ValidateException {
		if (StringUtil.isBlank(this.alias)){
			return getPlural();
		}
		if (StringUtil.isBlank(this.aliasPlural)){
			return PluralUtil.lookupPlural(this.getAlias());
		} else {
			return StringUtil.capFirst(this.aliasPlural);
		}
	}
	
	public void setPlural(String plural) {
		this.plural = StringUtil.capFirst(plural);
	}
	
	public Type getType(){
		Type retType = new Type(this.getStandardNameWithSuffix(),this.getPackageToken());
		return retType;
	}
	
	@Override
	public Set<Field> getFields(){
		
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());

		if (this.domainId != null) {
			this.domainId.setSerial(-300);
			set.add(this.domainId);
		}
		if (this.domainName != null) {
			this.domainName.setSerial(-200);
			set.add(this.domainName);
		}
		if (this.active != null) {
			this.active.setSerial(-100);
			set.add(this.active);
		}
		set.addAll(super.getFields());
		return set;
	}
	
	public Set<Field> getFieldsWithoutId(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		set.addAll(super.getFields());		
		if (this.domainName != null) {
			this.domainName.setSerial(-200);
			set.add(this.domainName);
		}
		if (this.active != null) {
			this.active.setSerial(-100);
			set.add(this.active);
		}
		return set;
	}
	
	public Set<Field> getFieldsWithoutDomainName(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		set.addAll(super.getFields());	
		if (this.domainId != null) {
			this.domainId.setSerial(-300);
			set.add(this.domainId);
		}
		if (this.active != null) {
			this.active.setSerial(-100);
			set.add(this.active);
		}
		return set;
	}
	
	public Set<Field> getSearchFields(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		for (Field f:super.getFields()) {
			if (!f.getFieldType().equalsIgnoreCase("image")) set.add(f);
		}
		if (this.domainName != null) {
			this.domainName.setSerial(-200);
			set.add(this.domainName);
		}
		if (this.active != null) {
			this.active.setSerial(-100);
			set.add(this.active);
		}
		return set;
	}
	
	public Set<Field> getSearchPlainFields(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		for (Field f:super.getFields()) {
			if (!f.getFieldType().equalsIgnoreCase("image")) set.add(f);
		}
		return set;
	}
	
	public Set<Field> getFieldsWithoutImage(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		for (Field f:super.getFields()) {
			if (!f.getFieldType().equalsIgnoreCase("image")) set.add(f);
		}
		if (this.domainId != null) {
			this.domainId.setSerial(-300);
			set.add(this.domainId);
		}
		if (this.domainName != null) {
			this.domainName.setSerial(-200);
			set.add(this.domainName);
		}
		if (this.active != null) {
			this.active.setSerial(-100);
			set.add(this.active);
		}
		return set;
	}
	
	
	public Set<Field> getFieldsWithoutIdAndActive(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		set.addAll(super.getFields());		
		if (this.domainName != null) {
			this.domainName.setSerial(-200);
			set.add(this.domainName);
		}
		return set;
	}
	
	public Set<Field> getPlainFields(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		set.addAll(super.getFields());		
		return set;
	}
	
	public Set<Field> getRawFields(){
		Set<Field> set = new TreeSet<Field>(new FieldSerialComparator());
		set.addAll(super.getFields());		
		return set;
	}
	
	public String generateTypeVarString(){
		return StringUtil.capFirst(this.getStandardName()) + " " + StringUtil.lowerFirst(this.getStandardName());
	}
	
	public Field getField(String fieldName){
		for (Field f : this.getFields()){
//			if (f instanceof Dropdown){
//				Dropdown dp = (Dropdown) f;
//				if (!StringUtil.isBlank(dp.getAliasName())&&dp.getAliasName().equals(fieldName)){
//					return dp;
//				}
//			}else {
				if (!StringUtil.isBlank(f.getFieldName())&&f.getFieldName().equals(fieldName)){
					return f;
				}
//			}
		}
		return null;
	}

	public String getDbPrefix() {
		if (!StringUtil.isBlank(this.tablePrefix)) return this.tablePrefix;
		else  if (!StringUtil.isBlank(this.dbPrefix)) return this.dbPrefix;
		else return "";
	}
	
	public String getPureDbPrefix() {
		return this.dbPrefix;
	}

	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
	}
	
	public String getLowerFirstDomainName(){
		return lowerFirst(this.getStandardName());
	}
	
	public String getLowerFirstChar(){
		return lowerFirst(this.getStandardName()).substring(0,1);
	}
	
	public String getCapFirstDomainName(){
		return capFirst(this.getStandardName());
	}
	
	public String getSnakeDomainName(){
		return StringUtil.changeDomainFieldtoTableColum(lowerFirst(this.getStandardName()));
	}
	
	public String getFullName(){
		return this.getPackageToken()+ "."+this.getDomainSuffix()+"."+this.getCapFirstDomainNameWithSuffix();
	}
	public String getDomainActiveStr() throws ValidateException{
		if (this.getActive() == null ){
			ValidateException err = new ValidateException("域对象活跃字段设置错误");
			throw err;
		} else {
			if (this.getActive().getFieldName().toLowerCase().contains("delete")) return "false";
			else return "true";
		}
	}
	
	public Integer getDomainActiveInteger() throws ValidateException{
		if (this.getActive() == null ){
			ValidateException err = new ValidateException("域对象活跃字段设置错误");
			throw err;
		} else {
			if (this.getActive().getFieldName().toLowerCase().contains("delete")) return 0;
			else return 1;
		}
	}
	
	public String getDomainDeletedStr() throws ValidateException{
		if (this.getActive() == null){
			ValidateException err = new ValidateException("域对象活跃字段设置错误");
			throw err;
		} else {
			if (this.getActive().getFieldName().toLowerCase().contains("delete")) return "true";
			else return "false";
		}
	}
	
	public Integer getDomainDeletedInteger() throws ValidateException{
		if (this.getActive() == null){
			ValidateException err = new ValidateException("域对象活跃字段设置错误");
			throw err;
		} else {
			if (this.getActive().getFieldName().toLowerCase().contains("delete")) return 1;
			else return 0;
		}
	}
	
	public String getCapFirstPlural() throws ValidateException{
		return StringUtil.capFirst(this.getPlural());
	}
	
	public String getLowerFirstPlural() throws ValidateException{
		return StringUtil.lowerFirst(this.getPlural());
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (!StringUtil.isBlank(this.getAliasLabel())){
			return this.getAliasLabel();
		}
		else if (!StringUtil.isBlank(this.getLabel())) {
			return this.label;
		}
		else if (!StringUtil.isBlank(this.getAlias())) {
			return this.getAlias();
		}		
		else return this.getStandardName();
	}
	
	public String getAliasText(){
		if (!StringUtil.isBlank(this.aliasLabel)) return this.aliasLabel;
		else return this.getAlias();
	}

	public Map<String, String> getFieldLabels() {
		return fieldLabels;
	}

	public void setFieldLabels(Map<String, String> fieldLabels) {
		this.fieldLabels = fieldLabels;
	}
	
	public void putFieldLabel(String key,String value){
		this.fieldLabels.put(key, value);
	}
	
	public void decorateDomainWithLabels(){
		for (Field f: this.getFields()){
			String label = this.fieldLabels.get(f.getLowerFirstFieldName());
			if (label!=null && !label.equals("")) f.setLabel(label);
		}
	}

	public boolean equals(Object o){
		return this.standardName.equals(((Domain)o).getStandardName());
	}	
	
	public Object deepClone(){
		try {
			// 将对象写到流里
			OutputStream bo = new ByteArrayOutputStream();
			//OutputStream op = new ObjectOutputStream();
			ObjectOutputStream oo = new ObjectOutputStream(bo);
			oo.writeObject(this);
			
			// 从流里读出来
			InputStream bi = new ByteArrayInputStream(((ByteArrayOutputStream) bo).toByteArray());
			ObjectInputStream oi = new ObjectInputStream(bi);
			return (oi.readObject());
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public String toString(){
		return JSONObject.fromObject(this).toString();
	}
	
	public void setFieldValue(String fieldName, String fieldValue){
		logger.debug("JerryDebug:setFieldValue:"+fieldName+":"+fieldValue+":"+this.getStandardName());
		Field f = this.getField(fieldName);
		f.setFieldValue(fieldValue);
	}

	public ValidateInfo validate(String dbType){
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		if (this!=null && !this.isLegacy()&&(dbType.equalsIgnoreCase("mysql")||dbType.equalsIgnoreCase("mariadb")||dbType.equalsIgnoreCase("PostgreSQL")||dbType.equalsIgnoreCase("pgsql"))&&!this.getDomainId().getFieldType().equalsIgnoreCase("i64")&&!this.getDomainId().getFieldType().equals("i32")){
			info.addCompileError("域对象"+this.getStandardName()+ "类型设置错误！");
			info.setSuccess(false);
		}
		for (Field f: this.getFields()){
			String fieldname = f.getFieldName();
			if (!StringUtil.isLowerCaseLetter(fieldname)) {
				info.addCompileError("域对象"+this.getStandardName()+"字段" + fieldname + "未使用小写英文字母开头！");
			}
			if (fieldname.length() >= 2 && !StringUtil.isLowerCaseLetterPosition(fieldname,1)){
				info.addCompileError("域对象"+this.getStandardName()+"字段" + fieldname + "第二个字母未使用小写英文字母！");
			}
//			if (f instanceof Dropdown){
//				Dropdown dp = (Dropdown)f;
//				if (!dp.aliasName.equals(dp.getTarget().getLowerFirstDomainName()+"Id")){
//					info.addCompileError("域对象"+this.getStandardName()+"下列列表"+dp.getAliasName()+"没有以域对象名Id命名");
//				}
//					
//			}
		}
		
		return info;
	}
	
	public void addManyToMany(ManyToMany manyToMany) {
		long serial = 0L;
		for (ManyToMany mtm:this.manyToManies) {
			if (mtm.getSerial() != null && mtm.getSerial()>serial) serial = mtm.getSerial();
		}
		serial += 1000L;
		manyToMany.setSerial(serial);
		this.manyToManies.add(manyToMany);
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAliasLabel() {
		return aliasLabel;
	}

	public void setAliasLabel(String aliasLabel) {
		this.aliasLabel = aliasLabel;
	}

	public Set<ManyToMany> getManyToManies() {
		return manyToManies;
	}

	public void setManyToManies(Set<ManyToMany> manyToManies) {
		this.manyToManies = manyToManies;
	}
	
	public String generateFieldLabelsArrayStr(Collection<Field> fields) {
		StringBuilder sb = new StringBuilder("{");
		for (Field f:fields) {
			sb.append("\"").append(f.getText()).append("\",");
		}
		if (fields!=null && fields.size()>0) {
			sb.deleteCharAt(sb.length()-1);
		}
		sb.append("}");
		return sb.toString();
	}
	
	public String generateFieldLabelsArrayStringsWithoutBrackets(Collection<Field> fields) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append("\"").append(f.getText()).append("\".to_string(),");
		}
		if (fields!=null && fields.size()>0) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	public String generateIsImageFieldsArrayStr(Collection<Field> fields) {
		StringBuilder sb = new StringBuilder("{");
		for (Field f:fields) {
			sb.append(f.getFieldType().equalsIgnoreCase("image")).append(",");
		}
		if (fields!=null && fields.size()>0) {
			sb.deleteCharAt(sb.length()-1);
		}
		sb.append("}");
		return sb.toString();
	}
	
	public String generateIsImageFieldsArrayStringsWithoutBrackets(Collection<Field> fields) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append(f.getFieldType().equalsIgnoreCase("image")).append(",");
		}
		if (fields!=null && fields.size()>0) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	public String getAliasOrName() {
		if (!StringUtil.isBlank(this.getAlias())) {
			return this.getAlias();
		}else {
			return this.getStandardName();
		}
	}

	public String getDaoSuffix() {
		return daoSuffix;
	}

	public void setDaoSuffix(String daoSuffix) {
		this.daoSuffix = daoSuffix;
	}

	public String getDaoimplSuffix() {
		return daoimplSuffix;
	}

	public void setDaoimplSuffix(String daoimplSuffix) {
		this.daoimplSuffix = daoimplSuffix;
	}

	public String getServiceSuffix() {
		return serviceSuffix;
	}

	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}

	public String getServiceimplSuffix() {
		return serviceimplSuffix;
	}

	public void setServiceimplSuffix(String serviceimplSuffix) {
		this.serviceimplSuffix = serviceimplSuffix;
	}

	public String getControllerSuffix() {
		return controllerSuffix;
	}

	public void setControllerSuffix(String controllerSuffix) {
		this.controllerSuffix = controllerSuffix;
	}

	public String getDomainNamingSuffix() {
		return domainNamingSuffix;
	}

	public void setDomainNamingSuffix(String domainNamingSuffix) {
		this.domainNamingSuffix = domainNamingSuffix;
	}

	public String getControllerNamingSuffix() {
		return controllerNamingSuffix;
	}

	public void setControllerNamingSuffix(String controllerNamingSuffix) {
		this.controllerNamingSuffix = controllerNamingSuffix;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDomainSuffix() {
		return domainSuffix;
	}

	public void setDomainSuffix(String domainSuffix) {
		this.domainSuffix = domainSuffix;
	}
	
	public String getStandardNameWithSuffix() {
		return StringUtil.capFirst(standardName)+this.getDomainNamingSuffix();
	}
	
	public String getCapFirstDomainNameWithSuffix() {
		return StringUtil.capFirst(getStandardNameWithSuffix());
	}
	
	public String getLowerFirstDomainNameWithSuffix() {
		return StringUtil.lowerFirst(getStandardNameWithSuffix());
	}
	
	public String getSnakeDomainNameWithSuffix() {
		return StringUtil.changeDomainFieldtoTableColum(StringUtil.lowerFirst(getStandardNameWithSuffix()));
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getVerbDeniesStr() {
		return verbDeniesStr;
	}

	public void setVerbDeniesStr(String verbDeniesStr) {
		this.verbDeniesStr = verbDeniesStr;
	}
	
	public Set<String> getDeniedVerbs() throws ValidateException{
		TreeSet<String> allverbs = new TreeSet<String>();
		allverbs.addAll(Arrays.asList(DeniableUtils.getOneDomainVerbs()));
		Set<String> verDeniesOrgSet = DeniableUtils.parseVerbDenyString(this.getStandardName(),this.verbDeniesStr);
		allverbs.removeAll(verDeniesOrgSet);
		String verbDeniesOrg = "";
		for (String s:allverbs) {
			verbDeniesOrg+=",";
		}
		if (verDeniesOrgSet.contains(",")) verbDeniesOrg=verbDeniesOrg.substring(0,verbDeniesOrg.length()-1);
		String fieldsStr = "";
		if (this.domainId==null) fieldsStr += "domainid,";
		if (this.domainName==null) fieldsStr += "domainname,";
		if (this.active==null) fieldsStr += "activefield,";
		if (fieldsStr.contains(",")) fieldsStr=fieldsStr.substring(0,fieldsStr.length()-1);
		if (fieldsStr.equals("")) return verDeniesOrgSet;
		return DeniableUtils.parseVerbFieldDenyString(this.getStandardName(),this.verbDeniesStr,fieldsStr);
	}
	
	public boolean isVerbDenied(String verbName)throws ValidateException {
		return this.getDeniedVerbs().contains(verbName);
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public boolean isLegacy() {
		boolean retVal = true;
		if (this.getDomainId()!=null&&this.getDomainName()!=null&&this.getActive()!=null) retVal = false;
		return retVal;
	}
	
	public boolean hasDomainId() {
		return this.getDomainId() != null;
	}
	
	public boolean hasDomainIdAndDomainName() {
		return this.getDomainId() != null &&this.getDomainName() != null ;
	}
	
	public boolean hasDomainName() {
		return this.getDomainName() != null;
	}
	
	public boolean hasActiveField() {
		return this.getActive() != null;
	}
	
	public ManyToMany findManyToManyByStandardName(String mtmName) {
		Iterator<ManyToMany> mtmIter = this.getManyToManies().iterator();
		while (mtmIter.hasNext()) {
			ManyToMany mtm = (ManyToMany) mtmIter.next();
			if (mtm.getStandardName().equals(mtmName)) return mtm;
		}
		return null;
	}
	
	public Field findFieldByFieldName(String fieldName) {
		if (this.domainId!=null && this.domainId.getFieldName().equals(fieldName)) return this.domainId;
		if (this.domainName!=null && this.domainName.getFieldName().equals(fieldName)) return this.domainName;
		if (this.active!=null && this.active.getFieldName().equals(fieldName)) return this.active;
		Iterator<Field> fIter = this.fields.iterator();
		while(fIter.hasNext()){
			Field f = (Field) fIter.next();
			if (fieldName.equals(f.getFieldName())) return f;
		}
		return null;
	}
	
	public boolean containsImage() {
		for (Field f:this.getFields()) {
			if (f.getFieldType().equalsIgnoreCase("image")) {
				return true;
			}
		}
		return false;
	}

	public String getControllerPackage() {
		return controllerPackage;
	}

	public void setControllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
	}
	
	public String getControllerPackagePrefix() {
		if (StringUtil.isBlank(this.controllerPackage))return "";
		else return this.controllerPackage+"/";
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	
	public boolean containsFixedNames() {
		for (Field f:this.getFields()) {
			if (!f.getFixedName().equals(f.getFieldName())) return true;
		}
		return false;
	}

	public boolean containsNullableFields() {
		return this.getPlainFields()!=null&&this.getPlainFields().size()>0;
	}
	
	public boolean containsDateTime() {
		for (Field f:this.getFields()) {
			if (f.getFieldType().equalsIgnoreCase("datetime")||f.getFieldType().equalsIgnoreCase("date")) {
				return true;
			}
		}
		return false;
	}

	public String getFrontendUi() {
		return frontendUi;
	}

	public void setFrontendUi(String frontendUi) {
		this.frontendUi = frontendUi;
	}

	public String getBackendUi() {
		return backendUi;
	}

	public void setBackendUi(String backendUi) {
		this.backendUi = backendUi;
	}
	
	public org.light.domain.Enum toEnum() throws Exception{
		org.light.domain.Enum myEnum = new org.light.domain.Enum();
		myEnum.setStandardName(this.getStandardName());
		myEnum.setPlural(this.getPlural());
		myEnum.setTablePrefix(this.getTablePrefix());
		myEnum.setLabel(this.getLabel());
		myEnum.setVerbDeniesStr(this.getVerbDeniesStr());
		
		myEnum.setSerial(this.getSerial());
		myEnum.setDbPrefix(this.getPureDbPrefix());
		myEnum.setPackageToken(this.getPackageToken());
		myEnum.setLanguage(this.getLanguage());
		myEnum.setSchema(this.getSchema());
		myEnum.setDomainSuffix(this.getDomainSuffix());
		myEnum.setDomainNamingSuffix(this.getDomainNamingSuffix());
		myEnum.setDaoSuffix(this.getDaoSuffix());
		myEnum.setDaoimplSuffix(this.getDaoimplSuffix());
		myEnum.setServiceSuffix(this.getServiceSuffix());
		myEnum.setServiceimplSuffix(this.getServiceimplSuffix());
		myEnum.setControllerSuffix(this.getControllerSuffix());
		myEnum.setControllerNamingSuffix(this.getControllerNamingSuffix());
		myEnum.setProjectName(this.getStandardName());
		myEnum.setDbType(this.getDbType());
		myEnum.setControllerPackage(this.getControllerSuffix());
		myEnum.setFrontendUi(this.getFrontendUi());
		myEnum.setBackendUi(this.getBackendUi());
		
		if (this.getDomainId() != null) myEnum.setDomainId(this.getDomainId());
		if (this.getDomainName() != null) myEnum.setDomainName(this.getDomainName());
		if (this.getActive() != null) myEnum.setActive(this.getActive());
		myEnum.setFields(this.getPlainFields());
		myEnum.setManyToManies(this.getManyToManies());
		
		List<Domain> dummyEnums = new ArrayList<>();
		for (Domain d:this.getDummyDb()) {
			org.light.domain.Enum myEnum0 = (org.light.domain.Enum) myEnum.deepClone();
			if (d.getDomainId() != null) myEnum0.setDomainId(d.getDomainId());
			if (d.getDomainName() != null) myEnum0.setDomainName(d.getDomainName());
			if (d.getActive() != null) myEnum0.setActive(d.getActive());
			myEnum0.setFields(d.getPlainFields());
			myEnum0.setManyToManies(d.getManyToManies());
			dummyEnums.add(myEnum0);
		}
		myEnum.setDummyDb(dummyEnums);
		
		return myEnum;
	}
	
	public void removeField(String fieldName,String fieldcatlog) {
		if (!fieldcatlog.equals("manytomany")) {
			if (this.getDomainId()!=null && "domainid".equals(fieldcatlog)) {
				this.setDomainId(null);
				return;
			}
			if (this.getDomainName()!=null && "domainname".equals(fieldcatlog)) {
				this.setDomainName(null);
			}
			if (this.getActive()!=null && "activefield".equals(fieldcatlog)) {
				this.setActive(null);
				return;
			}
	        Iterator<Field> itf = this.fields.iterator();
	        while(itf.hasNext()){
	        	Field f = itf.next();
	        	if (f.getFieldName().equals(fieldName)) {
	        		itf.remove();
	        		return;
	        	}
	        }
		} else {
			Iterator<ManyToMany> itm = this.manyToManies.iterator();
	        while(itm.hasNext()){
	        	ManyToMany mtm = itm.next();
	        	if (mtm.getStandardName().equals(fieldName)) {
	        		itm.remove();
	        		return;
	        	}
	        }
		}
	}
	
	public void removeFieldFromAll(String fieldName,String fieldcatlog) {
		if (this.getDummyDb()!=null && this.getDummyDb().size()>0) {
			for (Domain d: this.dummyDb) {
				d.removeField(fieldName, fieldcatlog);
			}
		}
		this.removeField(fieldName, fieldcatlog);
	}
	
	public void setField(Field f) {
		String fieldName = f.getFieldName();
		if (this.domainId!=null && this.domainId.getFieldName().equals(fieldName)) this.domainId = f;
		if (this.domainName!=null && this.domainName.getFieldName().equals(fieldName)) this.domainName =f;
		if (this.active!=null && this.active.getFieldName().equals(fieldName)) this.active = f;
		Iterator<Field> fit = this.fields.iterator();
		while(fit.hasNext()) {
			Field ff = (Field) fit.next();
			if (fieldName.equals(ff.getFieldName())) {
				f.setSerial(ff.getSerial());
				this.fields.remove(ff);
				this.fields.add(f);
			}
		}
	}
	
	public void updateField(String originalFieldCatlog, String originalFieldName, String fieldCatlog, String fieldName, String fieldType, String fieldLength, String fieldLabel, String fieldFixedName) {
		if (!fieldCatlog.equals("manytomany")) {
			if (!originalFieldCatlog.equals("manytomany")) {
				Field f = this.findFieldByFieldName(originalFieldName);
				if (f != null) {
					if (fieldCatlog.equals("dropdown")) {
						Dropdown dp = new Dropdown();
						dp.setSerial(f.getSerial());
						dp.setFieldType(fieldType);
						dp.setTargetName(fieldType);
						dp.setFieldName(fieldName);
						dp.setLabel(fieldLabel);
						dp.setLengthStr(fieldLength);
						dp.setFixedName(fieldFixedName);
						dp.setFieldValue(f.getFieldValue());
						this.removeField(originalFieldName, originalFieldCatlog);
						this.fields.add(dp);
						return;
					} else if (fieldCatlog.equals("field")) {
						Field ff = new Field();
						ff.setSerial(f.getSerial());
						ff.setFieldType(fieldType);
						ff.setFieldName(fieldName);
						ff.setLabel(fieldLabel);
						ff.setLengthStr(fieldLength);
						ff.setFixedName(fieldFixedName);
						ff.setFieldValue(f.getFieldValue());
						this.removeField(originalFieldName, originalFieldCatlog);
						this.fields.add(ff);
						return;
					}
				} else {
					if (fieldCatlog.equals("dropdown")) {
						Dropdown dp = new Dropdown();
						dp.setFieldType(fieldType);
						dp.setFieldName(fieldName);
						dp.setLabel(fieldLabel);
						dp.setLengthStr(fieldLength);
						dp.setFixedName(fieldFixedName);
						dp.setFieldValue("");
						this.addField(dp);
						return;
					} else if (fieldCatlog.equals("field")) {
						f = new Field();
						f.setFieldType(fieldType);
						f.setFieldName(fieldName);
						f.setLabel(fieldLabel);
						f.setLengthStr(fieldLength);
						f.setFixedName(fieldFixedName);
						f.setFieldValue("");
						this.addField(f);
						return;
					}					
				}
			}
		} else {
			ManyToMany mtm0 = this.findManyToManyByStandardName(originalFieldName);
			ManyToMany mtm = new ManyToMany();
			String capFieldName = StringUtil.capFirst(fieldName);
			if (!StringUtil.isBlank(capFieldName)) {
				mtm.setStandardName(capFieldName);
			}else {
				mtm.setStandardName(fieldType);
			}
			mtm.setMaster(this);
			mtm.setManyToManySalveName(fieldType);
			mtm.setSlaveAliasLabel(fieldLabel);
			if (!StringUtil.isBlank(capFieldName)) mtm.setSlaveAlias(capFieldName);
			mtm.setMaster(this);
			if (mtm0 != null) mtm.setValues(mtm0.getValues());
			else mtm.setValues("");
			this.manyToManies.add(mtm);
		}
	}
	
	public void updateFieldForAll(String originalFieldCatlog, String originalFieldName,String fieldCatlog, String fieldName, String fieldType, String fieldLength, String fieldLabel, String fieldFixedName) {
		if (this.getDummyDb()!=null && this.getDummyDb().size()>0) {
			for (Domain d: this.dummyDb) {
				d.updateField(originalFieldCatlog, originalFieldName, fieldCatlog, fieldName, fieldType, fieldLength, fieldLabel, fieldFixedName);
			}
		}
		this.updateField( originalFieldCatlog, originalFieldName, fieldCatlog, fieldName, fieldType, fieldLength, fieldLabel, fieldFixedName);
	}
	
	public void pureAddPlainField(Field f) {
		this.fields.add(f);
	}
	
	public void setupDomainIdFieldForAll(String originalFieldCatlog, String originalFieldName,String fieldName, String fieldLabel, String fieldFixedName) throws ValidateException{
		if (this.getDummyDb()!=null && this.getDummyDb().size()>0) {
			for (Domain d: this.dummyDb) {
				d.setupDomainIdField(originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName);
			}
		}
		this.setupDomainIdField( originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName);
	}
	
	public void setupDomainIdField(String originalFieldCatlog, String originalFieldName, String fieldName, String fieldLabel, String fieldFixedName) throws ValidateException{
		Field f = this.findFieldByFieldName(originalFieldName);
		if (originalFieldCatlog.equals("dropdown")) {
			Field ff = new Field();
			ff.setSerial(f.getSerial());
			ff.setFieldType("i64");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr("");
			ff.setFixedName(fieldFixedName);
			ff.setFieldValue(f.getFieldValue());
			this.removeField(originalFieldName, originalFieldCatlog);
			this.domainId = ff;	
			return;
		} else if (originalFieldCatlog.equals("field")) {
			Field ff = new Field();
			ff.setSerial(f.getSerial());
			ff.setFieldType("i64");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr("");
			ff.setFixedName(fieldFixedName);
			ff.setFieldValue(f.getFieldValue());
			this.removeField(originalFieldName, originalFieldCatlog);
			this.domainId = ff;
			return;
		} else if (StringUtil.isBlank(originalFieldCatlog)) {
			Field ff = new Field();
			//ff.setSerial(f.getSerial());
			ff.setFieldType("i64");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr("");
			ff.setFixedName(fieldFixedName);
			if (this.hasDomainId()) {
				ff.setFieldValue(this.getDomainId().getFieldValue());
			} else {
				ff.setFieldValue("");
			}
			this.domainName = ff;
			return;
		} else {
			throw new ValidateException("转换域对象序号错误！");
		}
	}
	
	public void setupDomainNameField(String originalFieldCatlog, String originalFieldName, String fieldName, String fieldLabel, String fieldFixedName, String fieldlength) throws ValidateException{
		Field f = this.findFieldByFieldName(originalFieldName);
		if (originalFieldCatlog.equals("dropdown")) {
			Field ff = new Field();
			ff.setSerial(f.getSerial());
			ff.setFieldType("String");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr(fieldlength);
			ff.setFixedName(fieldFixedName);
			ff.setFieldValue(f.getFieldValue());
			this.removeField(originalFieldName, originalFieldCatlog);
			this.domainName = ff;	
			return;
		} else if (originalFieldCatlog.equals("field")) {
			Field ff = new Field();
			ff.setSerial(f.getSerial());
			ff.setFieldType("String");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr(fieldlength);
			ff.setFixedName(fieldFixedName);
			ff.setFieldValue(f.getFieldValue());
			this.removeField(originalFieldName, originalFieldCatlog);
			this.domainName = ff;
			return;
		} else if (StringUtil.isBlank(originalFieldCatlog)) {
			Field ff = new Field();
			//ff.setSerial(f.getSerial());
			ff.setFieldType("String");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr(fieldlength);
			ff.setFixedName(fieldFixedName);
			if (this.hasDomainId()) {
				ff.setFieldValue(this.getStandardName()+"_"+this.getDomainId().getFieldValue());
			} else {
				ff.setFieldValue(this.getStandardName());
			}
			this.domainName = ff;
			return;
		} else {
			throw new ValidateException("转换域对象名字错误！");
		}
	}
	
	public void setupDomainNameFieldForAll(String originalFieldCatlog, String originalFieldName,String fieldName, String fieldLabel, String fieldFixedName, String fieldlength) throws ValidateException{
		if (this.getDummyDb()!=null && this.getDummyDb().size()>0) {
			for (Domain d: this.dummyDb) {
				d.setupDomainNameField(originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName, fieldlength);
			}
		}
		this.setupDomainNameField( originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName, fieldlength);
	}
	
	public void setupDomainActiveField(String originalFieldCatlog, String originalFieldName, String fieldName, String fieldLabel, String fieldFixedName) throws ValidateException{
		Field f = this.findFieldByFieldName(originalFieldName);
		if (originalFieldCatlog.equals("field")) {
			Field ff = new Field();
			ff.setSerial(f.getSerial());
			ff.setFieldType("bool");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr("");
			ff.setFixedName(fieldFixedName);
			ff.setFieldValue(f.getFieldValue());
			this.removeField(originalFieldName, originalFieldCatlog);
			this.active = ff;
			return;
		} else if (StringUtil.isBlank(originalFieldCatlog)) {
			Field ff = new Field();
			//ff.setSerial(f.getSerial());
			ff.setFieldType("bool");
			ff.setFieldName(fieldName);
			ff.setLabel(fieldLabel);
			ff.setLengthStr("");
			ff.setFixedName(fieldFixedName);			
			this.active = ff;
			ff.setFieldValue(this.getDomainActiveStr());
		} else {
			throw new ValidateException("转换域对象名字错误！");
		}
	}
	
	public void setupDomainActiveFieldForAll(String originalFieldCatlog, String originalFieldName,String fieldName, String fieldLabel, String fieldFixedName) throws ValidateException{
		if (this.getDummyDb()!=null && this.getDummyDb().size()>0) {
			for (Domain d: this.dummyDb) {
				d.setupDomainActiveField(originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName);
			}
		}
		this.setupDomainActiveField( originalFieldCatlog, originalFieldName, fieldName, fieldLabel, fieldFixedName);
	}
}
