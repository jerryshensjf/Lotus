package org.light.domain;

import java.io.Serializable;
import java.util.Comparator;

public class DomainSerialComparator implements Comparator<Domain>,Serializable{
	private static final long serialVersionUID = -3073819745256627215L;

	@Override
	public int compare(Domain o1, Domain o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
