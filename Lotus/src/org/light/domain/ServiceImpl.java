package org.light.domain;

import java.util.Iterator;
import java.util.Set;

import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;

public class ServiceImpl extends Class {
	protected Domain domain;
	public ServiceImpl(){
		super();
	}	
	
	public ServiceImpl(Domain domain){
		super();
		this.domain = domain;
	}

	public String generateServiceImplString() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		Set<String> imports = this.generateImportStrings();
		imports.addAll(this.classImports);
		imports.add("crate::"+this.domain.getDomainSuffix()+"::"+this.domain.getCapFirstDomainNameWithSuffix());
		imports.add("crate::"+this.domain.getDomainSuffix()+"::"+this.domain.getCapFirstDomainName()+"QueryRequest");
		imports.add("crate::"+this.domain.getDomainSuffix()+"::CountNum");
		imports.add("crate::"+this.domain.getServiceimplSuffix()+"::init_db");
		
		if ("Oracle".equalsIgnoreCase(this.domain.getDbType())) {
			imports.add("oracle::Error");
		}else {
			imports.add("sqlx::Error");
		}
		
		String importsStr = DomainUtil.generateImportStr(imports);
		sb.append("#![allow(dead_code)]\n");
		sb.append(importsStr);
		Iterator it2 = this.methods.iterator();
		while(it2.hasNext()){
			Method m = (Method)it2.next();
			sb.append(m.generateMethodString()).append("\n");
		}
		
		return sb.toString();
	}


	public Domain getDomain() {
		return domain;
	}


	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
	public ValidateInfo validate(){
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		return info;
	}

}
