package org.light.domain;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.utils.StringUtil;
import org.light.utils.DomainUtil;

public class ManyToManyCandidate extends Domain{
	private static final long serialVersionUID = 1L;
	protected Field masterId;
	protected Field slaveId;
	protected Domain master;
	protected Domain slave;
	protected List<Domain> slaveList = new ArrayList<>();
	protected String masterName;
	protected String slaveAlias;
	protected String masterIdValue;
	protected String slaveIdValues;
	
	public Domain getSlave() {
		return slave;
	}

	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	
	public String getMasterIdValue() {
		return masterIdValue;
	}

	public void setMasterIdValue(String masterIdValue) {
		this.masterIdValue = masterIdValue;
	}

	public String getSlaveIdValues() {
		return slaveIdValues;
	}

	public void setSlaveIdValues(String slaveIdValues) {
		this.slaveIdValues = slaveIdValues;
	}

	public String getMasterName() {
		return masterName;
	}

	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	public String getSlaveAlias() {
		return slaveAlias;
	}

	public void setSlaveAlias(String slaveAlias) {
		this.slaveAlias = slaveAlias;
	}

	public Field getMasterId() {
		return masterId;
	}

	public void setMasterId(Field masterId) {
		this.masterId = masterId;
	}

	public Field getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(Field slaveId) {
		this.slaveId = slaveId;
	}

	public Domain getMaster() {
		return master;
	}

	public void setMaster(Domain master) {
		this.master = master;
	}

	public List<Domain> getSlaveList() {
		return slaveList;
	}

	public void setSlaveList(List<Domain> slaveList) {
		this.slaveList = slaveList;
	}
	
	public ManyToManyCandidate() {
		super();
	}
	
	public ManyToManyCandidate(Field masterId, Field slaveId, Domain master,List<Domain> slaveList) {
		super();
		this.masterId = masterId;
		this.slaveId = slaveId;
		this.master = master;
		this.slaveList = slaveList;
	}

	public ManyToMany toManyToMany() {
		ManyToMany mtm = new ManyToMany();
		mtm.setMaster(this.master);
		mtm.setSlave(this.slave);
		mtm.setSlaveAlias(this.slaveAlias);
		mtm.setMasterValue(this.masterIdValue);
		mtm.setValues(this.slaveIdValues);
		mtm.setManyToManySalveName(this.slave.getStandardName());
		mtm.setSlaveAliasLabel(this.label);
		mtm.setLabel(this.label);
		mtm.setStandardName(StringUtil.capFirst(slaveAlias));
		return mtm;
	}

	public Domain toDomain() {
		Domain domain = new Domain();
		domain.setStandardName(StringUtil.capFirst(this.masterName) + StringUtil.capFirst(this.slaveAlias));
		domain.setTablePrefix(this.getTablePrefix());
		domain.setLabel(this.getLabel());
		Field masterId = this.getMasterId();
		masterId.setFieldValue(this.getMasterIdValue());
		domain.addField(masterId);
		Field slaveId = this.getSlaveId();
		slaveId.setFieldValue(this.getSlaveIdValues());
		domain.addField(slaveId);
		return domain;
	}
}
