package org.light.domain;

import org.light.core.Writeable;

public class DragonHideStatement extends Statement{
	protected boolean dragonShow = false;

	public DragonHideStatement(long serial, String statement,boolean dragonShow){
		super(serial,0,statement);
		this.dragonShow = dragonShow;
	}
	
	public DragonHideStatement(long serial, int indent, String statement,boolean dragonShow){
		super(serial,indent,statement);
		this.dragonShow = dragonShow;
	}

	public DragonHideStatement(){
		this(0,"",false);
	}	

	@Override
	public int compareTo(Writeable o) {
		if (this.getSerial() < o.getSerial()) return -1;
		else if (this.serial == o.getSerial()) return 0;
		else return 1;
	}
	
	@Override
	public String getContentWithSerial() {
		if (dragonShow){
			StringBuilder sb = new StringBuilder();		
			sb.append(this.serial).append("\t\t：");
			for (int i=0 ; i < this.indent; i++) sb.append("\t");
			sb.append(this.statement);
			return sb.toString();
		} else {
			return null;
		}
	}
	
	@Override
	public String getContent(){
		if (dragonShow){
			return super.getContent();
		}else{
			return null;
		}
	}
	
	public boolean isDragonShow() {
		return dragonShow;
	}

	public void setDragonShow(boolean dragonShow) {
		this.dragonShow = dragonShow;
	}

	public Statement getNormalStatement(){
		if (dragonShow){
			return new Statement(this.serial,this.indent,this.statement);
		}else{
			return null;
		}
	}

}
