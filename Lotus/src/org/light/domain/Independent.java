package org.light.domain;

import java.io.Serializable;

public abstract class Independent implements Comparable<Independent>,Cloneable,Serializable{
	private static final long serialVersionUID = 1187477128045218994L;
	protected String standardName;
	protected String fileName;
	protected String content;
	protected String packageToken = "";
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	public int compareTo(Independent o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	
	public abstract String generateImplString();
}
