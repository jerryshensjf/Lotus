package org.light.domain;

import java.io.Serializable;

import org.light.utils.StringUtil;

public class Signature implements Serializable,Comparable<Signature>{
	private static final long serialVersionUID = -214951475007225220L;
	protected long signatureId;
	protected int position;
	protected Type type;
	protected String name;
	protected String packageToken;
	protected String annotation;
	
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Type getType() {
		return type;
	}
	public long getSignatureId() {
		return signatureId;
	}
	public void setSignatureId(long signatureId) {
		this.signatureId = signatureId;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Signature(int position,String name, Type type){
		this.position = position;
		this.type = type;
		this.name = name;
	}
	
	public Signature(long signatureId,int position,  String name, Type type){
		this.signatureId = signatureId;
		this.position = position;
		this.type = type;
		this.name = name;
	}
	
	public Signature(){
	}
	
	public boolean equals(Signature signature){
		if (this.type.equals(signature.getType()) && this.name.equals(signature.getName())){
			return true;
		}
		return false;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	public Signature(int position, String name, Type type, String packageToken){
		super();
		this.position = position;
		this.name = name;
		this.type = type;
		this.packageToken = packageToken;
		
	}
	
	public Signature(int position, String name, Type type, String packageToken,String annotation){
		super();
		this.position = position;
		this.name = name;
		this.type = type;
		this.packageToken = packageToken;
		this.annotation = annotation;
		
	}
	
	public Signature(int position, String name, String typeName){
		super();
		this.position = position;
		this.name = name;
		this.type = new Type(typeName,"");	
	}

	public Var getVar(){
		Var var = new Var(this.name,this.type);
		return var;
	}
	
	public String generateCallString(){
		StringBuilder sb = new StringBuilder();
		if (this.name != null) sb.append(this.name);
		if (!StringUtil.isBlank(this.type.getTypeName())) sb.append(":").append(this.type.getTypeName());
		return sb.toString();
	}
	@Override
	public int compareTo(Signature o) {
		int result = ((Integer)this.position).compareTo((Integer)o.getPosition());
		if (result == 0){
			return  this.type.compareTo(o.getType());
		}else {
			return result;
		}
	}
}
