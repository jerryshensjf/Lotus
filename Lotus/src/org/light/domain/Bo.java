package org.light.domain;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Bo extends Class{
	protected List<Method> setters;
	protected List<Method> getters;
	protected String boStr;
	protected List<Domain> domains;	
	
	public String capFirst(String value){
		return value.substring(0, 1).toUpperCase()+value.substring(1);
	}
	
	@Override
	public boolean equals(Object o){
		return (equals((Bo)o));
	}
	
	public boolean  equals(Bo o){
		String myName = this.getStandardName();
		String otherName = o.getStandardName();
		return (myName.equalsIgnoreCase(otherName));
	}

	public List<Method> getSetters() {
		return setters;
	}

	public void setSetters(List<Method> setters) {
		this.setters = setters;
	}

	public List<Method> getGetters() {
		return getters;
	}

	public void setGetters(List<Method> getters) {
		this.getters = getters;
	}

	public String getBoStr() {
		return boStr;
	}

	public void setBoStr(String boStr) {
		this.boStr = boStr;
	}

	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}
	
	public void addField(String name,String type){
		this.fields.add(new Field(name,type));
	}
	
	public void addField(String name,String type,String packageToken,String lengthStr){
		this.fields.add(new Field(name,type,packageToken,lengthStr));
	}
	
}
