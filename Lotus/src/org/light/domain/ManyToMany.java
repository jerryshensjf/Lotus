package org.light.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.light.complexverb.Assign;
import org.light.complexverb.ListMyActive;
import org.light.complexverb.ListMyAvailableActive;
import org.light.complexverb.Revoke;
import org.light.generator.TwoDomainsDBDefinitionGenerator;
import org.light.layouts.EasyUIMtmPI;
import org.light.utils.StringUtil;

public class ManyToMany implements Comparable<ManyToMany>, Serializable{
	private static final long serialVersionUID = -8844037024490738355L;
	protected Long serial;
	protected Domain master;
	protected Domain slave;
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
	protected EasyUIMtmPI euPI;
	protected String standardName;
	protected String label;
	protected String values;
	protected String manyToManyMasterName;
	protected String manyToManySalveName;
	protected String masterValue;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String crossOrigin = "";
	protected String slaveAlias = "";
	protected String resolution = "low";
	protected String slaveAliasLabel;

	public ManyToMany(){
		super();
	}
	
	public ManyToMany(String manyToManyMasterName,String manyToManySalveName){
		super();
		this.manyToManyMasterName = manyToManyMasterName;
		this.manyToManySalveName = manyToManySalveName;
	}
	
	public ManyToMany(Domain master,Domain slave,String masterValue, String values) throws Exception{
		this.master = master;
		this.slave = slave;
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euPI = new EasyUIMtmPI(master,slave,this.getSlaveAliasLabelOrText());
	}
	
	public ManyToMany(Domain master,Domain slave,String masterValue, String values, String slaveAlias) throws Exception{
		this.master = master;
		this.setManyToManyMasterName("Link"+master.getCapFirstDomainName()+StringUtil.capFirst(slaveAlias));
		this.slave = slave;
		this.slave.setAlias(slaveAlias);
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.masterValue = masterValue;
		this.values = values;
		this.slaveAlias = slaveAlias;
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euPI = new EasyUIMtmPI(master,slave,this.getSlaveAliasLabelOrText());
	}
	
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	public Assign getAssign() {
		return assign;
	}
	public void setAssign(Assign assign) {
		this.assign = assign;
	}
	public Revoke getRevoke() {
		return revoke;
	}
	public void setRevoke(Revoke revoke) {
		this.revoke = revoke;
	}
	public ListMyActive getListMyActive() {
		return listMyActive;
	}
	public void setListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}
	public ListMyAvailableActive getListMyAvailableActive() {
		return listMyAvailableActive;
	}
	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}
	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		if (!StringUtil.isBlank(this.standardName)) return this.standardName;
		else {
			if (this.master !=null && this.slave !=null){
				if (StringUtil.isBlank(this.slave.getAlias())){
					return "Link"+this.master.getStandardName()+this.slave.getStandardName();
				}else {
					return "Link"+this.master.getStandardName()+this.slave.getAlias();
				}
			}else {
				return "";
			}
		}
	}
	@Override
	public int compareTo(ManyToMany o) {
		if (!StringUtil.isBlank(this.getStandardName())&&!StringUtil.isBlank(o.getStandardName())) {
			return this.getStandardName().compareTo(o.getStandardName());
		} else {
			if (this.master!=null && this.slave != null && o.getMaster()!=null && o.getSlave()!=null) {
				int result = this.master.compareTo(o.getMaster());
				if (result == 0) {
					result = this.slave.compareTo(o.getSlave());
				}
				return result;
			}else {
				int result = this.manyToManyMasterName.compareTo(o.getManyToManyMasterName());
				if (result == 0) {
					String slaveAliasOrName = StringUtil.isBlank(this.slaveAlias)?this.manyToManySalveName:this.slaveAlias;
					String oAliasOrName = StringUtil.isBlank(o.getSlaveAlias())?o.getManyToManySalveName():o.getSlaveAlias();
					result = slaveAliasOrName.compareTo(oAliasOrName);
				}
				return result;
			}
		}
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (!StringUtil.isBlank(this.getSlaveAliasLabelOrText())) {
			if (StringUtil.isEnglishAndDigitalAndEmpty(this.getSlaveAliasLabelOrText())) {
				return "Link "+this.master.getStandardName()+" "+this.getSlaveAliasLabelOrText();
			}else {
				return "链接"+this.master.getText()+this.getSlaveAliasLabelOrText();
			}
		}
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			if (StringUtil.isBlank(this.slave.getAlias())) {
				return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
			} else {
				return "Link "+this.master.getStandardName()+" "+this.slave.getAlias();
			}
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}
	
	public TwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		TwoDomainsDBDefinitionGenerator mtg = new TwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}

	public String getValues() {
		if (StringUtil.isBlank(values)) return "";
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getManyToManySalveName() {
		return manyToManySalveName;
	}

	public void setManyToManySalveName(String manyToManySalveName) {
		this.manyToManySalveName = manyToManySalveName;
	}

	public String getMasterValue() {
		return masterValue;
	}

	public void setMasterValue(String masterValue) {
		this.masterValue = masterValue;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getCrossOrigin() {
		return crossOrigin;
	}

	public void setCrossOrigin(String crossOrigin) {
		this.crossOrigin = crossOrigin;
	}

	public String getSlaveAlias() {
		if (!StringUtil.isBlank(this.slaveAlias)) {
			return this.slaveAlias;
		} else {
			return this.manyToManySalveName;
		}
	}

	public void setSlaveAlias(String slaveAlias) {
		this.slaveAlias = slaveAlias;
	}

	public String getManyToManyMasterName() {
		return manyToManyMasterName;
	}

	public void setManyToManyMasterName(String manyToManyMasterName) {
		this.manyToManyMasterName = manyToManyMasterName;
	}

	public String getSlaveAliasLabel() {
		return slaveAliasLabel;
	}

	public void setSlaveAliasLabel(String slaveAliasLabel) {
		this.slaveAliasLabel = slaveAliasLabel;
	}

	public String getSlaveAliasLabelOrText() {
		if (!StringUtil.isBlank(this.slaveAliasLabel)) return this.getSlaveAliasLabel();
		else return this.slave.getText();
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public EasyUIMtmPI getEuPI() throws Exception {
		if (this.euPI == null) {
			this.euPI = new EasyUIMtmPI(this.master,this.slave,this.slaveAliasLabel);
		}
		return this.euPI;
	}

	public void setEuPI(EasyUIMtmPI euPI) {
		this.euPI = euPI;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public List<String> getSlaveValuesList(){
		List<String> valuesList = new ArrayList<>();
		if (!StringUtil.isBlank(values)&&values.contains(",")) {
			String [] myValues = values.split(",");
			for (String str:myValues) {
				valuesList.add(str);
			}
		}else if (!StringUtil.isBlank(values)) {
			valuesList.add(values);
		}
		return valuesList;
	}
	
	public Object deepClone(){
		try {
			// 将对象写到流里
			OutputStream bo = new ByteArrayOutputStream();
			//OutputStream op = new ObjectOutputStream();
			ObjectOutputStream oo = new ObjectOutputStream(bo);
			oo.writeObject(this);
			
			// 从流里读出来
			InputStream bi = new ByteArrayInputStream(((ByteArrayOutputStream) bo).toByteArray());
			ObjectInputStream oi = new ObjectInputStream(bi);
			return (oi.readObject());
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
