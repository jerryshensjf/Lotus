package org.light.domain;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.MethodUtil;

public class DaoImpl extends Class {
	protected Domain domain;
	protected Dao dao;
	protected Type extendedType;
	
	public Type getExtendedType() {
		return extendedType;
	}


	public void setExtendedType(Type extendedType) {
		this.extendedType = extendedType;
	}


	public String generateDaoImplString() throws ValidateException{
		StringBuilder sb = new StringBuilder("#![allow(unused_variables)]\n");
		Set<String> imports = this.generateImportStrings();
		imports.add("super::Table");
		imports.add("super::"+this.domain.getCapFirstDomainNameWithSuffix());
		imports.add("super::"+this.domain.getCapFirstDomainName()+"QueryRequest");
		imports.add("super::CountNum");	
		
		if ("Oracle".equalsIgnoreCase(this.domain.getDbType())) {
			imports.add("oracle::Error");
			imports.add("oracle::Row");
			imports.add("crate::"+this.domain.getDaoimplSuffix()+"::db_context::FromRow");
		}else {
			imports.add("sqlx::Error");
		}
		
		imports.addAll(this.classImports);
		String importsStr = DomainUtil.generateImportStr(imports);
		sb.append(importsStr);
		
		Map<String,Set<Method>> methodsMap = MethodUtil.divideMethodSetWithTempTag(this.methods, this.domain.getCapFirstDomainNameWithSuffix());
		for (String key: methodsMap.keySet()) {
			Set<Method> mset = methodsMap.get(key);
			sb.append("impl<'c> Table<'c, "+key+"> {\n");
			
			Iterator it2 = mset.iterator();
			while(it2.hasNext()){
				Method m = (Method)it2.next();
				sb.append(m.generateMethodString()).append("\n");
			}
			sb.append("}\n");
		}
		return sb.toString();
	}


	public Domain getDomain() {
		return domain;
	}


	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
	public ValidateInfo validate(){
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		return info;
	}


	public Dao getDao() {
		return dao;
	}


	public void setDao(Dao dao) {
		this.dao = dao;
	}
}
