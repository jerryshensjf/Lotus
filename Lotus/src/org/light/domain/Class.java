package org.light.domain;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.CloneableBase;
import org.light.utils.StringUtil;

public class Class implements Serializable{
	private static final long serialVersionUID = -3679019628651866562L;
	protected String standardName;
	protected Set<Method> methods = new TreeSet<Method>();
	protected List<String> tokens;
	protected String classComment;
	protected Set<Field> fields = new TreeSet<Field>(new FieldSerialComparator());
	protected String packageToken;
	protected Set<String> classAnnotations = new  TreeSet<String>();
	protected Set<String> classImports = new  TreeSet<String>();
	protected Set<Interface> classImplements = new TreeSet<Interface>();
	
	public long maxMethodSerial(){
		long maxserial = 0;
		for (Method f:this.methods){
			if (f.getSerial() > maxserial) maxserial = f.getSerial();
		}
		return maxserial;
	}
	
	public long maxFieldSerial(){
		long maxserial = 0L;
		for (Field f:this.fields){
			if (f.getSerial() > maxserial) maxserial = f.getSerial();
		}
		return maxserial;
	}

	public Set<String> getClassImports() {
		return classImports;
	}
	public void setClassImports(Set<String> classImports) {
		this.classImports = classImports;
	}
	
	public void addClassImports(String str){
		this.classImports.add(str);
	}
	
	public Set<String> getClassAnnotations() {
		return classAnnotations;
	}
	
	public void addClassAnnotation(String annotation){
		this.classAnnotations.add(annotation);
	}

	public void setClassAnnotations(Set<String> classAnnotations) {
		this.classAnnotations = classAnnotations;
	}
	public Set<Method> getMethods() {
		return methods;
	}
	public void setMethods(Set<Method> methods) {
		this.methods = methods;
	}
	public List<String> getTokens() {
		return tokens;
	}
	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}
	public String getClassComment() {
		return classComment;
	}
	public void setClassComment(String classComment) {
		this.classComment = classComment;
	}
	public Set<Field> getFields() {
		return fields;
	}
	public void setFields(Set<Field> fields) {
		this.fields = fields;
	}
	public void addField(Field field){
		long serial = this.maxFieldSerial() + 100L;
		field.setSerial(serial);
		this.fields.add(field);
	}
	
	public void addMethod(Method method){
		if (method != null) {
			long serial = this.maxMethodSerial() + 100L;
			method.setSerial(serial);
			this.methods.add(method);
		}
	}
	
	public void removeMethod(Method method){
		if (method != null) {
			this.methods.remove(method);
		}
	}
	
	public void addMethod(Method method,long serial){
		if (method != null){
			method.setSerial(serial);
			this.methods.add(method);
		}
	}
	
	public String getStandardName() {
		return StringUtil.capFirst(standardName);
	}

	public void setStandardName(String standardName) {
		this.standardName = StringUtil.capFirst(standardName);
	}

	public ValidateInfo validate(){
		ValidateInfo info = new ValidateInfo();
		for (Field f: this.fields){
			String type = f.getFieldType();
			if (!"i32".equalsIgnoreCase(type)&&!"i64".equalsIgnoreCase(type)&&!"String".equals(type)&&!"string".equals(type)&&
				!"f32".equalsIgnoreCase(type)&&!"f64".equalsIgnoreCase(type) &&!"bool".equalsIgnoreCase(type)&&
				!"image".equalsIgnoreCase(type)&&!"datetime".equalsIgnoreCase(type)&&!"date".equalsIgnoreCase(type)){
				if (! (f instanceof Dropdown) && f.getPackageToken()==null || "".equals(f.getPackageToken())){
					info.addCompileError("字段"+f.getFieldName()+ "为非基本类型，是否应该定义为dropdown?");
					info.setSuccess(false);
				}
			}
		}	
		return info;
	}
	
	public Set<String> generateImportStrings(){
		Set<String> imports = new TreeSet<String>();
		for (Method m:this.methods){
			imports.addAll(m.getAdditionalImports());
		}
		return imports;
	}
	public String getPackageToken() {
		if (StringUtil.isBlank(packageToken)) return "";
		else return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}

	public Set<Interface> getClassImplements() {
		return classImplements;
	}

	public void setClassImplements(Set<Interface> classImplements) {
		this.classImplements = classImplements;
	}
	
	public void addClassImplement(Interface classImplement){
		this.classImplements.add(classImplement);
	}
}
