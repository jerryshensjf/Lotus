package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.easyui.EasyUIPositions;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class CountByParentId extends Verb implements EasyUIPositions {

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"<select id=\"count"+this.domain.getCapFirstPlural()+"ByParentId\" resultType=\"Integer\" parameterType=\"Long\">"));
		sList.add(new Statement(2000L,2,"select count(*) countSum from cpe_product_details where parent_id = #{parentId}"));
		sList.add(new Statement(3000L,1,"</select>"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,2,"return dao.count"+this.domain.getCapFirstPlural()+"ByParentId(parentId);"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
