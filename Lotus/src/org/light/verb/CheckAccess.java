package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class CheckAccess extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;		
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}


	@Override
	public Method generateServiceImplMethod() throws Exception {
		return null;		
	}

	public CheckAccess() {
		super();
		this.setLabel("检查访问");
	}

	public CheckAccess(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("CheckAccess");
		this.setVerbName("CheckAccess" + StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("检查访问");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Check Access");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("CheckAccess" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("String"));

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("checkAccess" + domain.getCapFirstDomainName());

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"get\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/checkAccess" + domain.getCapFirstDomainName() + "\","));
			sl.add(new Statement(4000, 2, "dataType: 'json',"));
			sl.add(new Statement(5000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(6000, 3, "if (!data.success && data.noAuth){"));
			sl.add(new Statement(6500, 4, "window.location.href=\"../login/noauth.html\""));
			sl.add(new Statement(7000, 3, "}"));
			sl.add(new Statement(8000, 2, "},"));
			sl.add(new Statement(9000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(10000, 2, "},"));
			sl.add(new Statement(11000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(12000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(13000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(14000, 2, "}"));
			sl.add(new Statement(15000, 1, "}); "));

			method.setMethodStatementList(sl);
			return method;
		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		return null;
	}
}
