package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Delete extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getDomainId().getSnakeFieldName(),this.domain.getDomainId().getFieldType()));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();		
			
			sList.add(new Statement(500L,2,"let result = sqlx::query("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,1,"r#\""+ MybatisSqlReflector.generateDeleteSqlWithValue(domain)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,1,"r#\""+ PgsqlReflector.generateDeleteSqlWithValue(domain)+"\"#"));
			}	
			sList.add(new Statement(6000L,2,")"));
			sList.add(new Statement(7000L,2,".bind("+this.domain.getDomainId().getSnakeFieldName()+")"));
			sList.add(new Statement(8000L,2,".execute(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(9000L,2,".await;"));
			
			sList.add(new Statement(10000L,2,"match result {"));
			sList.add(new Statement(11000L,3,"Ok(result) => {"));
			sList.add(new Statement(12000L,4,"Ok(result.rows_affected())"));
			sList.add(new Statement(13000L,3,"},"));
			sList.add(new Statement(14000L,3,"Err(err) => {"));
			sList.add(new Statement(15000L,4,"Err(err)"));
			sList.add(new Statement(16000L,3,"}"));
			sList.add(new Statement(17000L,2,"}"));

			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateDeleteSqlWithValue(domain)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let _ = stmt.execute(&[&"+this.domain.getDomainId().getSnakeFieldName()+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let _ = conn.commit();"));
		sList.add(new Statement(serial+8000L,1,"Ok(0 as u64)"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1, this.domain.getDomainId().getFieldName(),
					this.domain.getDomainId().getClassType()));
			return method;
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("bool"));
			method.addSignature(new Signature(1, this.domain.getDomainId().getFieldName(),
					this.domain.getDomainId().getClassType()));
			return method;
		}
	}


	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1, this.domain.getDomainId().getSnakeFieldName(),
					this.domain.getDomainId().getClassType()));
			method.setReturnType(new Type("Result<u64, Error>"));

			// Service method
			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+").await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	public Delete() {
		super();
		this.setLabel("删除");
	}

	public Delete(Domain domain) throws ValidateException {
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Delete");
		this.setVerbName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("删除");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Delete");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1,"Form("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Form<"+this.domain.getCapFirstDomainName()+"Request>"));
			method.setReturnType(new Type("String"));

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let "+this.domain.getDomainId().getSnakeFieldName()+" = "+StringUtil.getSnakeName(this.domain.getStandardName())+"_request."+this.domain.getDomainId().getSnakeFieldName()+".unwrap_or_default();"));
			sList.add(new Statement(2000L,1,"let _result = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+").await;"));
			sList.add(new Statement(3000L,1,"match _result {"));
			sList.add(new Statement(4000L,2,"Err(_) => {"));
			sList.add(new Statement(6000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(7000L,2,"},"));
			sList.add(new Statement(8000L,2,"Ok(_result) => {"));
			sList.add(new Statement(9000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(10000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(11000L,3,"map.insert("));
			sList.add(new Statement(12000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(13000L,4,"Value::from(\"\"),"));
			sList.add(new Statement(14000L,3,");"));
			sList.add(new Statement(15000L,3,""));
			sList.add(new Statement(16000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(18000L,3,"return resultjson;"));
			sList.add(new Statement(19000L,2,"}"));
			sList.add(new Statement(20000L,1,"}"));
			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("delete" + domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Delete',"));
			}else {
				sl.add(new Statement(2000, 1, "text:'删除',"));
			}
			sl.add(new Statement(3000, 1, "iconCls:'icon-remove',"));
			sl.add(new Statement(4000, 1, "handler:function(){"));
			sl.add(new Statement(5000, 2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000, 2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000, 3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000, 3, "return;"));
			sl.add(new Statement(9000, 2, "}"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(10000, 2, "if ($.messager.confirm(\"Alert\",\"Do you confirm to delete record?\", function(data){"));
			}else {
				sl.add(new Statement(10000, 2, "if ($.messager.confirm(\"警告\",\"确认要删除选定记录吗？\", function(data){"));
			}
			sl.add(new Statement(11000, 3, "if (data){"));
			sl.add(new Statement(12000, 4, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(13000,5, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(13000, 5, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(14000, 5, "return;"));
			sl.add(new Statement(15000, 4, "}"));
			sl.add(new Statement(16000, 4, "var " + domain.getDomainId().getLowerFirstFieldName() + " = rows[0][\""
					+ domain.getDomainId().getLowerFirstFieldName() + "\"];"));
			sl.add(new Statement(17000, 4, "delete" + this.domain.getCapFirstDomainName() + "("
					+ domain.getDomainId().getLowerFirstFieldName() + ");"));
			sl.add(new Statement(18000, 3, "}"));
			sl.add(new Statement(19000, 2, "}));"));
			sl.add(new Statement(20000, 0, "}"));
			sl.add(new Statement(20000, 0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Domain domain = this.domain;
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("delete" + domain.getCapFirstDomainName());
			Signature s1 = new Signature();
			s1.setName(domain.getDomainId().getLowerFirstFieldName());
			s1.setPosition(1);
			s1.setType(new Type("var"));
			method.addSignature(s1);

			StatementList sl = new StatementList();
			sl.add(new Statement(1000, 1, "$.ajax({"));
			sl.add(new Statement(2000, 2, "type: \"post\","));
			sl.add(new Statement(3000, 2, "url: \"../"+this.domain.getControllerPackagePrefix() + domain.getLowerFirstDomainName()
					+ domain.getControllerNamingSuffix() + "/delete" + domain.getCapFirstDomainName() + "\","));
			sl.add(new Statement(4000, 2, "dataType: 'json',"));
			sl.add(new Statement(5000, 2, "data:{\"" + domain.getDomainId().getLowerFirstFieldName() + "\":"
					+ domain.getDomainId().getLowerFirstFieldName() + "},"));
			sl.add(new Statement(6000, 2, "success: function(data, textStatus) {"));
			sl.add(new Statement(7000, 3, "$(\"#dg\").datagrid(\"load\");"));
			sl.add(new Statement(8000, 2, "},"));
			sl.add(new Statement(9000, 2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(10000, 2, "},"));
			sl.add(new Statement(11000, 2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(12000, 3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(13000, 3, "alert(errorThrown.toString());"));
			sl.add(new Statement(14000, 2, "}"));
			sl.add(new Statement(15000, 1, "}); "));

			method.setMethodStatementList(sl);
			return method;
		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Delete" + StringUtil.capFirst(this.domain.getStandardName()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getDomainId().getSnakeFieldName(),this.domain.getDomainId().getFieldType()));
			method.setReturnType(new Type("Result<u64, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();		
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let mut _db = DB.lock().unwrap();"));
			sList.add(new Statement(serial+2000L,1,"_db.retain(|"+this.domain.getSnakeDomainName()+"| if (*"+this.domain.getSnakeDomainName()+")."+this.domain.getDomainId().getSnakeFieldName()+" == "+this.domain.getDomainId().getSnakeFieldName()+" {false } else { true });"));
			sList.add(new Statement(serial+3000L,1,"return Ok(0 as u64);"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
