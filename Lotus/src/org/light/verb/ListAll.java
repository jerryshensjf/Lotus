package org.light.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.FieldUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListAll extends Verb implements EasyUIPositions {
	protected Set<Field> deniedFields = new TreeSet<>();
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
							
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(500L,2,"let result = sqlx::query_as("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,1,"r#\""+MybatisSqlReflector.generateSelectAllStatementWithDeniedFields(domain,deniedFields)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,1,"r#\""+PgsqlReflector.generateSelectAllStatementWithDeniedFields(domain,deniedFields)+"\"#"));
			}
			sList.add(new Statement(7000L,2,")"));
			sList.add(new Statement(8000L,2,".fetch_all(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(9000L,2,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateSelectAllStatementWithDeniedFields(domain,this.deniedFields)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let rows = stmt.query_as(&[])?;"));
		
		sList.add(new Statement(serial+7000L,1,"let mut "+StringUtil.getSnakeName(this.domain.getPlural())+" = Vec::new();"));
		sList.add(new Statement(serial+8000L,1,"for row_result in rows {"));
		sList.add(new Statement(serial+9000L,2,"let row:Row = row_result?;"));
		sList.add(new Statement(serial+10000L,2,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+11000L,2,""+StringUtil.getSnakeName(this.domain.getPlural())+".push("+this.domain.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+12000L,1,"}"));
		sList.add(new Statement(serial+13000L,1,"Ok("+StringUtil.getSnakeName(this.domain.getPlural())+")"));
		return WriteableUtil.merge(sList);
	}
	
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.addSignature(new Signature(1, "db","*sql.DB"));
			method.setReturnType(new Type("[] "+this.domain.getDomainSuffix()+"."+this.domain.getType()));
			return method;
		}
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("[] "+this.domain.getDomainSuffix()+"."+this.domain.getType()));
			return method;
		}
	}
	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			Method daomethod = this.generateDaoMethodDefinition();
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"().await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}
	public ListAll() throws Exception {
		super();
		this.dbType = "MariaDB";
		if (this.domain != null)
			this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		else
			this.setVerbName("ListAll");
		this.setLabel("列出全部");
	}
	
	public ListAll(String dbType) throws Exception {
		super();
		this.dbType = dbType;
		if (this.domain != null)
			this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		else
			this.setVerbName("ListAll");
		this.setLabel("列出全部");
	}

	public ListAll(Domain domain)  throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = "MariaDB";
		this.denied = domain.isVerbDenied("ListAll");
		this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("列出全部");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ListAll");
	}
	
	public ListAll(Domain domain,String dbType)  throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.denied = domain.isVerbDenied("ListAll");
		this.setVerbName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		this.setLabel("列出全部");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ListAll");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(this.getVerbName());
			method.setReturnType(new Type("String"));

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let "+StringUtil.getSnakeName(this.domain.getPlural())+" = service_"+StringUtil.getSnakeName(this.getVerbName())+"().await;"));
			sList.add(new Statement(2000L,1,"match "+StringUtil.getSnakeName(this.domain.getPlural())+" {"));
			sList.add(new Statement(3000L,2,"Err(_) => {"));
			sList.add(new Statement(5000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(6000L,2,"},"));
			sList.add(new Statement(7000L,2,"Ok("+StringUtil.getSnakeName(this.domain.getPlural())+") => {"));
			sList.add(new Statement(8000L,3,"let json = serde_json::to_string_pretty(&"+StringUtil.getSnakeName(this.domain.getPlural())+").unwrap();"));
			sList.add(new Statement(10000L,0,""));
			sList.add(new Statement(11000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(12000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(13000L,3,"map.insert("));
			sList.add(new Statement(14000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(15000L,4,"serde_json::from_str(&json).unwrap(),"));
			sList.add(new Statement(16000L,3,");"));
			sList.add(new Statement(17000L,2,""));
			sList.add(new Statement(18000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(20000L,3,"return resultjson;"));
			sList.add(new Statement(21000L,2,"}"));
			sList.add(new Statement(22000L,1,"}"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied)
			return null;
		else {
			return null;
		}
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			return null;
		}
	}

	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("ListAll" + StringUtil.capFirst(this.domain.getPlural()));
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let mut result:Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
		sList.add(new Statement(serial+2000L,1,"for mut "+this.domain.getSnakeDomainName()+" in get_db() {"));
		serial += 4000L;
		for (Field f:this.deniedFields) {
			sList.add(new Statement(serial,2,this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" = "+FieldUtil.findTypeDefaultValueStringToken(f.getFieldType())+";"));
			serial += 1000L;	
		}
		sList.add(new Statement(serial+4000L,2,"result.push("+this.domain.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+6000L,1,"}"));
		sList.add(new Statement(serial+7000L,1,"Ok(result)"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
}
