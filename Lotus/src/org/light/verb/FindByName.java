package org.light.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.FieldUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class FindByName extends Verb implements EasyUIPositions {
	protected Set<Field> deniedFields = new TreeSet<>();
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getDomainName().getSnakeFieldName(),this.domain.getDomainName().getFieldType()));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();	
			
			sList.add(new Statement(500L,1,"let result = sqlx::query_as("));
			
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(1000L,1,"r#\""+MybatisSqlReflector.generateFindByNameStatementWithDeniedFields(domain,this.deniedFields)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(1000L,1,"r#\""+ PgsqlReflector.generateFindByNameStatementWithDeniedFields(domain,this.deniedFields)+"\"#"));
			}else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(1000L,1,"r#\""+ MybatisOracleSqlReflector.generateFindByNameStatementWithDeniedFields(domain,this.deniedFields)+"\"#"));
			}
			
			sList.add(new Statement(6000L,2,")"));
			sList.add(new Statement(7000L,2,".bind("+this.domain.getDomainName().getSnakeFieldName()+")"));
			sList.add(new Statement(8000L,2,".fetch_one(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(9000L,2,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\""+MybatisOracleSqlReflector.generateFindByNameStatementWithDeniedFields(domain,this.deniedFields)+"\")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let row = stmt.query_row(&[&"+this.domain.getDomainName().getSnakeFieldName()+"])?;"));
		sList.add(new Statement(serial+7000L,1,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+8000L,1,"Ok("+this.domain.getSnakeDomainName()+")"));
		return WriteableUtil.merge(sList);
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setReturnType(new Type(this.domain.getDomainSuffix()+"."+this.domain.getType()));
			method.addSignature(new Signature(1, "db","*sql.DB"));
			method.addSignature(new Signature(2, this.domain.getDomainName().getFieldName(), new Type("String")));
			method.setThrowException(true);

			return method;
		}
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
			method.setReturnType(new Type(this.domain.getDomainSuffix()+"."+this.domain.getType()));
			method.addSignature(new Signature(1, this.domain.getDomainName().getFieldName(), new Type("String")));
			method.setThrowException(true);

			return method;
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(
					"Find" + this.domain.getStandardName() + "By" + this.domain.getDomainName().getCapFirstFieldName());
			method.addSignature(new Signature(1, this.domain.getDomainName().getSnakeFieldName(), new Type("String")));
			method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
			
			Method daomethod = this.generateDaoMethodDefinition();

			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainName().getSnakeFieldName()+").await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}
	public FindByName() {
		super();
		if (this.domain != null)
			this.setVerbName("Find" + this.domain.getStandardName() + "By"
					+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
		else
			this.setVerbName("FindByName");
		this.setLabel("根据名称查找");
	}

	public FindByName(Domain domain)  throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("FindByName");
		this.setVerbName("Find" + this.domain.getStandardName() + "By"
				+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
		this.setLabel("根据名称查找");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("FindByName");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName(
					"Find" + this.domain.getStandardName() + "By" + this.domain.getDomainName().getCapFirstFieldName());
			method.addSignature(new Signature(1,"Form("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Form<"+this.domain.getCapFirstDomainName()+"Request>"));
			method.setReturnType(new Type("String"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(500L,1,"let "+this.domain.getDomainName().getSnakeFieldName()+" = "+StringUtil.getSnakeName(this.domain.getStandardName())+"_request."+this.domain.getDomainName().getSnakeFieldName()+".unwrap_or_default();"));

			sList.add(new Statement(1000L,1,"let "+StringUtil.getSnakeName(this.domain.getStandardName())+" = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainName().getSnakeFieldName()+").await;"));
			sList.add(new Statement(2000L,1,"match "+StringUtil.getSnakeName(this.domain.getStandardName())+" {"));
			sList.add(new Statement(3000L,2,"Err(_) => {"));
			sList.add(new Statement(5000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(6000L,2,"},"));
			sList.add(new Statement(7000L,2,"Ok("+StringUtil.getSnakeName(this.domain.getStandardName())+") => {"));
			sList.add(new Statement(8000L,3,"let json = serde_json::to_string_pretty(&"+StringUtil.getSnakeName(this.domain.getStandardName())+").unwrap();"));
			sList.add(new Statement(10000L,0,""));
			sList.add(new Statement(11000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(12000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(13000L,3,"map.insert("));
			sList.add(new Statement(14000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(15000L,4,"serde_json::from_str(&json).unwrap(),"));
			sList.add(new Statement(16000L,3,");"));
			sList.add(new Statement(17000L,2,""));
			sList.add(new Statement(18000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(20000L,3,"return resultjson;"));
			sList.add(new Statement(21000L,2,"}"));
			sList.add(new Statement(22000L,1,"}"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}
	
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}

	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("Find" + this.domain.getStandardName() + "By"
				+ StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
		method.setNoContainer(false);
		method.addSignature(new Signature(1, "&self",""));
		method.addSignature(new Signature(2, this.domain.getDomainName().getSnakeFieldName(),this.domain.getDomainName().getFieldType()));
		method.setReturnType(new Type("Result<"+this.domain.getCapFirstDomainNameWithSuffix()+", Error>"));
		
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"for mut "+this.domain.getSnakeDomainName()+" in get_db() {"));
		sList.add(new Statement(serial+2000L,2,"if "+this.domain.getSnakeDomainName()+"."+this.domain.getDomainName().getSnakeFieldName()+" == "+this.domain.getDomainName().getSnakeFieldName()+" {"));
		serial += 4000L;
		for (Field f:this.deniedFields) {
			sList.add(new Statement(serial,3,this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" = "+FieldUtil.findTypeDefaultValueStringToken(f.getFieldType())+";"));
			serial += 1000L;	
		}
		sList.add(new Statement(serial+3000L,3,"return Ok("+this.domain.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+4000L,2,"}"));
		sList.add(new Statement(serial+5000L,1,"}"));
		if ("Oracle".equalsIgnoreCase(this.domain.getDbType())) {
			sList.add(new Statement(serial+6000L,1,"Err(Error::NoDataFound)"));
		} else {
			sList.add(new Statement(serial+6000L,1,"Err(Error::RowNotFound)"));
		}
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}
}
