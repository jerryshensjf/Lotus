package org.light.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.oracle.generator.MybatisOracleSqlReflector;
import org.light.utils.DomainTokenUtil;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class FindIndexedName extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("find"+this.domain.getCapFirstDomainName()+"IndexedName");
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, "short_name", "String"));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			
			sList.add(new Statement(1000L,1,"let result = sqlx::query_as("));
			if (StringUtil.isBlank(this.getDbType())||this.getDbType().equalsIgnoreCase("MariaDB")||this.getDbType().equalsIgnoreCase("MySQL")) {
				sList.add(new Statement(2000L,1,"r#\"select "+DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,deniedFields)+" from "+DomainUtil.getTableName(this.domain)+" where "+this.domain.getDomainName().getSnakeFieldName()+" like concat(? ,'%') \"#"));
			}else if (this.getDbType().equalsIgnoreCase("PostgreSQL")||this.getDbType().equalsIgnoreCase("pgsql")) {
				sList.add(new Statement(2000L,1,"r#\"select "+DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,deniedFields) + " from "+DomainUtil.getTableName(this.domain)+" where "+this.domain.getDomainName().getSnakeFieldName()+" like $1 || '%' \"#"));
			}else if (this.getDbType().equalsIgnoreCase("Oracle")) {
				sList.add(new Statement(2000L,1,"r#\"select "+DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,deniedFields) +" from "+DomainUtil.getTableName(this.domain)+" where "+this.domain.getDomainName().getSnakeFieldName()+" like concat( :"+this.domain.getDomainName().getLowerFirstFieldName()+",'%') \"#"));
			}
			sList.add(new Statement(3000L,1,")"));		
			sList.add(new Statement(4000L,1,".bind(short_name)"));
			sList.add(new Statement(5000L,1,".fetch_all(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(6000L,1,".await;"));

			sList.add(new Statement(11000L,2,"return result;"));
					
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}	

	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial+1000L,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(\"select "+DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,deniedFields) +" from "+DomainUtil.getTableName(this.domain)+" where "+this.domain.getDomainName().getSnakeFieldName()+" like concat( :1 ,'%') \")"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let rows = stmt.query_as(&[&short_name])?;"));
		
		sList.add(new Statement(serial+7000L,1,"let mut "+StringUtil.getSnakeName(this.domain.getPlural())+" = Vec::new();"));
		sList.add(new Statement(serial+8000L,1,"for row_result in rows {"));
		sList.add(new Statement(serial+9000L,2,"let row:Row = row_result?;"));
		sList.add(new Statement(serial+10000L,2,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+11000L,2,""+StringUtil.getSnakeName(this.domain.getPlural())+".push("+this.domain.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+12000L,1,"}"));
		sList.add(new Statement(serial+13000L,1,"Ok("+StringUtil.getSnakeName(this.domain.getPlural())+")"));
		return WriteableUtil.merge(sList);
	}
	
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("find"+this.domain.getCapFirstDomainName()+"IndexedName");
			method.addSignature(new Signature(1, "name", "String"));
			method.setReturnType(new Type("Result<String, Error>"));
			method.addAdditionalImport("crate::utils::string_util::right_find_under_line");
			
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let mut short_name = name.clone();"));
			sList.add(new Statement(2000L,1,"if name.contains(\"_\") && is_digit(name.substring(right_find_under_line(name.clone())+1,name.chars().count()).to_string()) {"));
			sList.add(new Statement(3000L,2,"short_name = name.substring(0,right_find_under_line(name.clone())).to_string();"));
			sList.add(new Statement(4000L,1,"}"));
			sList.add(new Statement(5000L,0,""));
			sList.add(new Statement(6000L,1,"let app_state = init_db();"));
			sList.add(new Statement(7000L,1,"let "+StringUtil.getSnakeName(this.domain.getPlural())+" = app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"(short_name.clone()).await.unwrap();"));
			sList.add(new Statement(8000L,0,""));
			sList.add(new Statement(9000L,1,"let mut index_num = 1;"));
			sList.add(new Statement(10000L,1,"for "+this.domain.getSnakeDomainName()+" in "+StringUtil.getSnakeName(this.domain.getPlural())+" {"));
			sList.add(new Statement(11000L,2,"let name2 = "+this.domain.getSnakeDomainName()+"."+this.domain.getDomainName().getSnakeFieldName()+";"));
			sList.add(new Statement(12000L,2,"if name2.contains(\"_\") && is_digit(name2.substring(right_find_under_line(name2.clone())+1,name2.chars().count()).to_string()) {"));
			sList.add(new Statement(13000L,3,"let index = name2.substring(right_find_under_line(name2.clone())+1,name2.chars().count()).parse::<i32>().unwrap();"));
			sList.add(new Statement(14000L,3,"if index >= index_num {"));
			sList.add(new Statement(15000L,4,"index_num = index + 1;"));
			sList.add(new Statement(16000L,3,"}"));
			sList.add(new Statement(17000L,2,"}"));
			sList.add(new Statement(18000L,1,"}"));
			sList.add(new Statement(19000L,0,""));
			sList.add(new Statement(20000L,1,"let index_name = short_name.clone() + \"_\" + &index_num.to_string();"));
			sList.add(new Statement(21000L,1,"Ok(index_name)"));

			method.setMethodStatementList(WriteableUtil.merge(sList));

			return method;
		}
	}
	
	public FindIndexedName(){
		super();
		this.setVerbName("FindIndexedName");
		this.setLabel("找出索引名字");
	}
	
	public FindIndexedName(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Clone");
		this.setVerbName("Find"+this.domain.getCapFirstDomainName()+"IndexedName");
		this.setLabel("找出索引名字");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Find"+this.domain.getCapFirstDomainName()+"IndexedName");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}
	
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;		
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;		
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("find"+this.domain.getCapFirstDomainName()+"IndexedName");
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, "short_name", "String"));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();			
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,1,"let _db = get_db();"));
			sList.add(new Statement(serial+2000L,1,"let mut result:Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
			sList.add(new Statement(serial+3000L,1,"for (_index, "+this.domain.getSnakeDomainName()+") in _db.iter().enumerate() {"));
			sList.add(new Statement(serial+4000L,2,"if (*"+this.domain.getSnakeDomainName()+")."+this.domain.getDomainName().getSnakeFieldName()+".starts_with(&short_name) {"));
			sList.add(new Statement(serial+5000L,3,"result.push((*"+this.domain.getSnakeDomainName()+").clone())"));
			sList.add(new Statement(serial+6000L,2,"}"));
			sList.add(new Statement(serial+7000L,1,"}"));
			sList.add(new Statement(serial+8000L,1,"Ok(result)"));
					
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
