package org.light.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.limitedverb.CountSearchByFieldsRecords;
import org.light.utils.DomainTokenUtil;
import org.light.utils.FieldUtil;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class SearchByFields extends Verb implements EasyUIPositions {
	protected CountSearchByFieldsRecords countSearch = new CountSearchByFieldsRecords();
	protected Set<Field> deniedFields = new TreeSet<>();

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getSnakeDomainName()+"_query_request",this.domain.getCapFirstDomainName()+"QueryRequest"));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			sList.add(new Statement(serial+1000L,2,"let mut query = r#\"select " + DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,this.deniedFields) + " from " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)+" where 1=1 \"#.to_string();"));
			for (Field f: this.domain.getSearchFields()) {
				if (!deniedFields.contains(f)) {
					if ("String".equalsIgnoreCase(f.getFieldType())){
						sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
						sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" like '%\","+f.getSnakeFieldName()+",\"%' \");"));
						sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
						sList.add(new Statement(serial+8000L,2,"}"));
					}else if ("i32".equals(f.getFieldType())||"i64".equals(f.getFieldType())||"f32".equals(f.getFieldType())||"f64".equals(f.getFieldType())){
						sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
						sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" = \","+f.getSnakeFieldName()+",\" \");"));
						sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
						sList.add(new Statement(serial+8000L,2,"}"));
					}else if ("bool".equals(f.getFieldType())){
						sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
						sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() && ("+f.getSnakeFieldName()+"==\"true\" || "+f.getSnakeFieldName()+" ==\"false\"){"));
						sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" = \","+f.getSnakeFieldName()+",\" \");"));
						sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
						sList.add(new Statement(serial+8000L,2,"}"));
					}else if ("datetime".equalsIgnoreCase(f.getFieldType())||"date".equalsIgnoreCase(f.getFieldType())){
						sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
						sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and date(" + f.getSnakeFieldName() + ") = date('\","
								+ f.getSnakeFieldName() + ",\"') \"); "));
						sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
						sList.add(new Statement(serial+8000L,2,"}"));
					}
					serial += 10000L;
				}
			}
			if ("PostgreSQL".equalsIgnoreCase(this.domain.getDbType())||"pgsql".equalsIgnoreCase(this.domain.getDbType())) {
				sList.add(new Statement(serial,2,"query = format!(\"{} {}\", query, \" order by "+this.domain.getDomainId().getSnakeFieldName()+" asc \" );"));
			}
			sList.add(new Statement(serial+39000L,2,"let result = sqlx::query_as(&query)"));
			sList.add(new Statement(serial+40000L,2,".fetch_all(&(&*self.pool).clone().unwrap())"));
			sList.add(new Statement(serial+41000L,2,".await;"));

			sList.add(new Statement(serial+43000L,2,"return result;"));
			if (this.domain.getDbType().equalsIgnoreCase("oracle")) {
				method.setMethodStatementList(getOracleDaoimplStatementList());
			}else {
				method.setMethodStatementList(WriteableUtil.merge(sList));
			}
			return method;
		}
	}
	
	public StatementList getOracleDaoimplStatementList() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		long serial = 1000L;
		sList.add(new Statement(serial,1,"let conn = (&*self.pool).clone().unwrap().get().unwrap();"));
			
		sList.add(new Statement(serial+1000L,2,"let mut query = r#\"select " + DomainTokenUtil.generateTableCommaFieldsWithDeniedFields(domain,this.deniedFields) + " from " + domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)+" where 1=1 \"#.to_string();"));
		for (Field f: this.domain.getSearchFields()) {
			if (!deniedFields.contains(f)) {
				if ("String".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
					sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" like '%\","+f.getSnakeFieldName()+",\"%' \");"));
					sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
					sList.add(new Statement(serial+8000L,2,"}"));
				}else if ("i32".equals(f.getFieldType())||"i64".equals(f.getFieldType())||"f32".equals(f.getFieldType())||"f64".equals(f.getFieldType())){
					sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
					sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" = \","+f.getSnakeFieldName()+",\" \");"));
					sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
					sList.add(new Statement(serial+8000L,2,"}"));
				} else if ("bool".equals(f.getFieldType())){
					sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() && ("+f.getSnakeFieldName()+"==\"true\" || "+f.getSnakeFieldName()+" ==\"false\"){"));
					sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and "+f.getSnakeFieldName()+" = \","+f.getSnakeFieldName()+",\" \");"));
					sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
					sList.add(new Statement(serial+8000L,2,"}"));
				} else if ("date".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
					sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and to_char("+f.getSnakeFieldName()+",'yyyy-MM-dd') = '\","+f.getSnakeFieldName()+",\"' \");"));
					sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
					sList.add(new Statement(serial+8000L,2,"}"));
				} else if ("datetime".equalsIgnoreCase(f.getFieldType())){
					sList.add(new Statement(serial+4000L,2,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					sList.add(new Statement(serial+5000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
					sList.add(new Statement(serial+6000L,3,"let query_"+f.getSnakeFieldName()+"  = format!(\"{}{}{}\",\" and to_char("+f.getSnakeFieldName()+",'yyyy-MM-dd') = to_char(to_date('\","+f.getSnakeFieldName()+",\"','yyyy-MM-dd HH24:mi:ss'),'yyyy-MM-dd') \");"));
					sList.add(new Statement(serial+7000L,3,"query = format!(\"{} {}\", query, query_"+f.getSnakeFieldName()+");"));
					sList.add(new Statement(serial+8000L,2,"}"));
				}
				serial += 10000L;
			}
		}

		sList.add(new Statement(serial+3000L,1,"let mut stmt = conn"));
		sList.add(new Statement(serial+4000L,1,".statement(&query)"));
		sList.add(new Statement(serial+5000L,1,".build()?;"));
		sList.add(new Statement(serial+6000L,1,"let rows = stmt.query_as(&[])?;"));
		
		sList.add(new Statement(serial+7000L,1,"let mut "+StringUtil.getSnakeName(this.domain.getPlural())+" = Vec::new();"));
		sList.add(new Statement(serial+8000L,1,"for row_result in rows {"));
		sList.add(new Statement(serial+9000L,2,"let row:Row = row_result?;"));
		sList.add(new Statement(serial+10000L,2,"let "+this.domain.getSnakeDomainName()+" = "+this.domain.getCapFirstDomainNameWithSuffix()+"::from_row(&row).unwrap();"));
		sList.add(new Statement(serial+11000L,2,""+StringUtil.getSnakeName(this.domain.getPlural())+".push("+this.domain.getSnakeDomainName()+");"));
		sList.add(new Statement(serial+12000L,1,"}"));
		sList.add(new Statement(serial+13000L,1,"Ok("+StringUtil.getSnakeName(this.domain.getPlural())+")"));

		return WriteableUtil.merge(sList);
	}
	
	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType()));

			return method;
		}
	}
	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setReturnType(new Type("List", this.domain, this.domain.getPackageToken()));
			method.addSignature(new Signature(1, this.domain.getLowerFirstDomainName(), this.domain.getType()));
			return method;
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.addSignature(new Signature(1, this.domain.getSnakeDomainName()+"_query_request",this.domain.getCapFirstDomainName()+"QueryRequest"));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let app_state = init_db();"));
			sList.add(new Statement(2000L,1,"app_state.await.context."+StringUtil.getSnakeName(this.domain.getPlural())+"."+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getSnakeDomainName()+"_query_request).await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	public SearchByFields() {
		super();
		this.dbType = "MariaDB";
		this.setLabel("按字段搜索");
	}
	
	public SearchByFields(String dbType) {
		super();
		this.dbType = dbType;
		this.setLabel("按字段搜索");
	}

	public SearchByFields(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = "MariaDB";
		this.denied = domain.isVerbDenied("SearchByFields");
		this.countSearch = new CountSearchByFieldsRecords(domain);
		this.setVerbName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
		this.setLabel("按字段搜索");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("SearchByFields");
	}
	
	public SearchByFields(Domain domain,String dbType) throws ValidateException{
		super();
		this.domain = domain;
		this.dbType = dbType;
		this.denied = domain.isVerbDenied("SearchByFields");
		this.countSearch = new CountSearchByFieldsRecords(domain);
		this.setVerbName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
		this.setLabel("按字段搜索");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("SearchByFields");
	}


	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}
	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}
	public Set<Field> getDeniedFields() {
		return deniedFields;
	}
	public void setDeniedFields(Set<Field> deniedFields) {
		this.deniedFields = deniedFields;
	}
	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("Search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFields");
			method.setNoContainer(false);
			method.addSignature(new Signature(1, "&self",""));
			method.addSignature(new Signature(2, this.domain.getSnakeDomainName()+"_query_request",this.domain.getCapFirstDomainName()+"QueryRequest"));
			method.setReturnType(new Type("Result<Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+">, Error>"));
			
			List<Writeable> sList = new ArrayList<Writeable>();
			long serial = 1000L;
			for (Field f: this.domain.getSearchFields()) {
				if (!deniedFields.contains(f)) {
					sList.add(new Statement(serial,1,"let "+f.getSnakeFieldName()+" = "+this.domain.getSnakeDomainName()+"_query_request."+f.getSnakeFieldName()+".unwrap_or_default();"));
					serial += 1000L;
				}
			}
			sList.add(new Statement(serial+2000L,1,"let mut result:Vec<"+this.domain.getCapFirstDomainNameWithSuffix()+"> = vec![];"));
			sList.add(new Statement(serial+3000L,1,"let _db = get_db();"));
			sList.add(new Statement(serial+4000L,1,"for mut "+this.domain.getSnakeDomainName()+" in _db {"));
			sList.add(new Statement(serial+5000L,2,"let mut matches = true;"));
			
			serial += 6000L;
			for (Field f: this.domain.getSearchFields()) {
				if (!deniedFields.contains(f)) {
					if ("String".equalsIgnoreCase(f.getFieldType())){					
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+2000L,3,"if !"+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+".contains(&"+f.getSnakeFieldName()+") {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					}else if ("i32".equals(f.getFieldType())||"i64".equals(f.getFieldType())){
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+2000L,3,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+".to_string() != "+f.getSnakeFieldName()+" {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					}else if ("f32".equals(f.getFieldType())||"f64".equals(f.getFieldType())){
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+2000L,3,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" as f64 - "+f.getSnakeFieldName()+".parse::<f64>().unwrap() < 0.0001 as f64 {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					}else if ("bool".equals(f.getFieldType())){
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() && ("+f.getSnakeFieldName()+"==\"true\" || "+f.getSnakeFieldName()+" ==\"false\") {"));
						sList.add(new Statement(serial+2000L,3,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+".to_string() != "+f.getSnakeFieldName()+" {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					} else if ("datetime".equalsIgnoreCase(f.getFieldType())){
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+2000L,3,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+".unwrap_or_default().date().to_string() != NaiveDateTime::parse_from_str(&"+f.getSnakeFieldName()+",DATE_TIME_FORMAT).unwrap().date().to_string() {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					} else if ("date".equalsIgnoreCase(f.getFieldType())){
						sList.add(new Statement(serial+1000L,2,"if !"+f.getSnakeFieldName()+".is_empty() {"));
						sList.add(new Statement(serial+2000L,3,"if "+this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+".unwrap_or_default().to_string() != "+f.getSnakeFieldName()+" {"));
						sList.add(new Statement(serial+3000L,4,"matches = false;"));
						sList.add(new Statement(serial+4000L,3,"}"));
						sList.add(new Statement(serial+5000L,2,"}"));
					}				
					serial += 6000L;
				}
			}
			sList.add(new Statement(serial,2,"if matches {"));
			serial += 4000L;
			for (Field f:this.deniedFields) {
				sList.add(new Statement(serial,3,this.domain.getSnakeDomainName()+"."+f.getSnakeFieldName()+" = "+FieldUtil.findTypeDefaultValueStringToken(f.getFieldType())+";"));
				serial += 1000L;	
			}
			sList.add(new Statement(serial+1000L,3,"result.push("+this.domain.getSnakeDomainName()+");"));
			sList.add(new Statement(serial+2000L,2,"}"));
			sList.add(new Statement(serial+3000L,1,"}"));
			sList.add(new Statement(serial+4000L,1,"Ok(result)"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
}
