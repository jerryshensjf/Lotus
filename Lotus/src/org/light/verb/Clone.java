package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class Clone extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception{
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Clone"+StringUtil.capFirst(this.domain.getStandardName()));
			method.setReturnType(new Type("bool"));
			method.addSignature(new Signature(1,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getClassType()));
			return method;
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Clone"+StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1, this.domain.getDomainId().getSnakeFieldName(),
					this.domain.getDomainId().getClassType()));
			method.setReturnType(new Type("Result<u64, Error>"));
			method.addAdditionalImport("crate::utils::string_util::is_digit");
			method.addAdditionalImport("substring::Substring");
			
			FindById find = new FindById(this.domain);
			Add add = new Add(this.domain);
			FindIndexedName findIndex = new FindIndexedName(this.domain);
			
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let mut "+this.domain.getSnakeDomainName()+" = "+StringUtil.getSnakeName(find.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+").await.unwrap();"));
			sList.add(new Statement(2000L,1,"let "+this.domain.getDomainName().getSnakeFieldName()+" = "+StringUtil.getSnakeName(findIndex.getVerbName())+"("+this.domain.getSnakeDomainName()+"."+this.domain.getDomainName().getSnakeFieldName()+").await.unwrap();"));
			sList.add(new Statement(3000L,1,""+this.domain.getSnakeDomainName()+"."+this.domain.getDomainName().getSnakeFieldName()+" = "+this.domain.getDomainName().getSnakeFieldName()+";"));
			sList.add(new Statement(4000L,1,StringUtil.getSnakeName(add.getVerbName())+"("+this.domain.getSnakeDomainName()+").await"));

			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}
	
	public Clone(){
		super();
		this.setLabel("克隆");
	}
	
	public Clone(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("Clone");
		this.setVerbName("Clone"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("克隆");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("Clone");
	}

	@Override
	public Method generateControllerMethod() throws Exception{
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("Clone"+StringUtil.capFirst(this.domain.getStandardName()));
			method.addSignature(new Signature(1,"Form("+StringUtil.getSnakeName(this.domain.getStandardName())+"_request)","Form<"+this.domain.getCapFirstDomainName()+"Request>"));
			method.setReturnType(new Type("String"));
						
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"let "+this.domain.getDomainId().getSnakeFieldName()+" = "+StringUtil.getSnakeName(this.domain.getStandardName())+"_request."+this.domain.getDomainId().getSnakeFieldName()+".unwrap_or_default();"));
			sList.add(new Statement(2000L,1,"let _result = service_"+StringUtil.getSnakeName(this.getVerbName())+"("+this.domain.getDomainId().getSnakeFieldName()+").await;"));
			sList.add(new Statement(3000L,1,"match _result {"));
			sList.add(new Statement(4000L,2,"Err(_) => {"));
			sList.add(new Statement(6000L,3,"r#\"{  \"rows\": null,  \"success\": true}\"#.to_string()"));
			sList.add(new Statement(7000L,2,"},"));
			sList.add(new Statement(8000L,2,"Ok(_result) => {"));
			sList.add(new Statement(9000L,3,"let mut map = Map::new();"));
			sList.add(new Statement(10000L,3,"map.insert(\"success\".to_string(), Value::from(true));"));
			sList.add(new Statement(11000L,3,"map.insert("));
			sList.add(new Statement(12000L,4,"\"rows\".to_string(),"));
			sList.add(new Statement(13000L,4,"Value::from(\"\"),"));
			sList.add(new Statement(14000L,3,");"));
			sList.add(new Statement(15000L,3,""));
			sList.add(new Statement(16000L,3,"let resultjson = serde_json::to_string_pretty(&map).unwrap();"));
			sList.add(new Statement(18000L,3,"return resultjson;"));
			sList.add(new Statement(19000L,2,"}"));
			sList.add(new Statement(20000L,1,"}"));
			
			method.setMethodStatementList(WriteableUtil.merge(sList));
			
			return method;
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		if (this.denied) return null;
		else {
			JavascriptBlock block = new JavascriptBlock();
			block.setSerial(100);
			block.setStandardName("clone"+domain.getCapFirstDomainName());
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,0, "{"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(2000, 1, "text:'Clone',"));
			}else {
				sl.add(new Statement(2000,1, "text:'克隆',"));
			}
			sl.add(new Statement(3000,1, "iconCls:'icon-add',"));
			sl.add(new Statement(4000,1, "handler:function(){ "));
			sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
			sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(7000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(7000,3,"$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(8000,3, "return;"));
			sl.add(new Statement(9000,2, "}"));
			sl.add(new Statement(10000,2, "if (rows.length > 1) {"));
			if (this.domain.getLanguage().equalsIgnoreCase("english")) {
				sl.add(new Statement(11000,3, "$.messager.alert(\"Alert\",\"Please choose one record!\",\"warning\");"));
			}else {
				sl.add(new Statement(11000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
			}
			sl.add(new Statement(12000,3, "return;"));
			sl.add(new Statement(13000,2, "}"));
			sl.add(new Statement(14000,2, "var "+domain.getDomainId().getLowerFirstFieldName()+" = rows[0][\""+domain.getDomainId().getLowerFirstFieldName()+"\"];"));
			sl.add(new Statement(15000,2, "clone"+this.domain.getCapFirstDomainName()+"("+domain.getDomainId().getLowerFirstFieldName()+");"));
			sl.add(new Statement(16000,1, "}"));
			sl.add(new Statement(17000,0, "}"));
			block.setMethodStatementList(sl);
			return block;
		}
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		if (this.denied) return null;
		else {
			Domain domain = this.domain;
			Var id = new Var(domain.getDomainId().getLowerFirstFieldName(),new Type("var"));
			JavascriptMethod method = new JavascriptMethod();
			method.setSerial(200);
			method.setStandardName("clone"+domain.getCapFirstDomainName());
			Signature s1 = new Signature();
			s1.setName(id.getVarName());
			s1.setPosition(1);
			s1.setType(new Type("var"));	
			method.addSignature(s1);
			
			StatementList sl = new StatementList();
			sl.add(new Statement(1000,1, "$.ajax({"));
			sl.add(new Statement(2000,2, "type: \"post\","));
			sl.add(new Statement(3000,2, "url: \"../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/clone"+domain.getCapFirstDomainName()+"\","));
			sl.add(new Statement(4000,2, "data: {"));
			sl.add(new Statement(5000,3, "\""+domain.getDomainId().getLowerFirstFieldName()+"\":"+domain.getDomainId().getLowerFirstFieldName()+"	"));	
			sl.add(new Statement(6000,2, "},"));
			sl.add(new Statement(7000,2, "dataType: 'json',"));
			sl.add(new Statement(8000,2, "success: function(data, textStatus) {"));
			sl.add(new Statement(9000,3, "$(\"#dg\").datagrid(\"load\");"));
			sl.add(new Statement(10000,2, "},"));
			sl.add(new Statement(11000,2, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(12000,2, "},"));
			sl.add(new Statement(13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(14000,3, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(15000,3, "alert(errorThrown.toString());"));
			sl.add(new Statement(16000,2, "}"));
			sl.add(new Statement(17000,1, "});"));
			
			method.setMethodStatementList(sl);
			return method;	
		}
	}

	@Override
	public Method generateDummyDaoImplMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
